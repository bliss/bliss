# ESRF beamline installation

## Installation

### Git based installation

On ESRF beamlines, Bliss is installed from sources in a git repository, making
local changes possible without reinstalling every time.

* First, move to the version you want in git.

    ```shell
    cd local/bliss.git/
    git checkout {SOME_BRANCH} # could be master, 1.11.x, ...
    git pull
    ```

* Create/Update the environment thanks to the Makefile helper.

    This command **create/update** an environment named `bliss_dev` with Bliss
    and all its dependencies.

    ```shell
    # just use 'make' to know more about what the helper can do
    make bl_env NAME=bliss_dev
    ```

* Exit and re-enter into the conda environment to ensure using up-to-date modules.

### Package based installation

For static installations (sources can't be edited), a all-in-one conda package
is provided.

```shell
conda update --channel esrf-bcu bliss
```

or

```shell
conda install bliss=X.Y.Z
```

## About BLISS version

Bliss version is printed at startup, its format depends on the installation mode.
When installing Bliss as a package, the version number looks like this:

```python
...
Welcome to BLISS version 1.11.0 running on lid00 (in bliss Conda environment)
Copyright (c) Beamline Control Unit, ESRF
...
```

If a Git based installation is used, the version ends with `dev0`, meaning it is
anywhere above `1.11` in the git history.

```python
...
Welcome to BLISS v1.11.dev0 running on lid00 (in bliss_dev Conda environment)
Copyright (c) Beamline Control Unit, ESRF
...
```

The version can also be printed with:
```python
DEMO [1]: from bliss.release import version
DEMO [2]: version
 Out [2]: '1.11.dev0'
```

## Post-installation configuration

### Instrument name

In order to properly fill information about the *instrument* on which data has
been collected in future data files, do not forget to set the `instrument`
field.

Format is free, but it is a good idea to put "ESRF-" followed by the
beamline or endstation name. In capital letters.

Example, in file:`__init__.yml` located at beamline configuration root, add:

```yaml
    ...
    instrument: ESRF-ID42A
    ...
```

### ESRF data policy

The ESRF data policy needs to be [configured](data/config_data_policy.md#esrf-data-policy).

### Nexus writer

A TANGO server referred to as the *Nexus writer* needs to be [configured and
started](data/config_nexus_writer.md#summary).

!!! warning
    There must be **one Nexus writer device** per BLISS session. Do not
    forget to add a device when a new BLISS session is created.

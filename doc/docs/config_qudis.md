

quDIS is a confocal displacement sensor, based on a Fabry-Pérot or Michelson
interferometer, with highest signal stability of < 0,05 nm.

```yaml
- class: qudisController
  plugin: bliss
  module: interferometers.qudisController
  name: q10aapu1
  tcp:
    url: q10aapu1.esrf.fr:9002
```

# MercuryIPS configuration


The MercuryiPS is a intelligent superconducting magnet power supply with on-board temperature sensor for diagnostic monitoring of the magnet temperature.


The `MercuryiPS` controller class (`bliss.controllers.powersupply.mercutyips`) provides:

**1 SoftAxis (pseudo-axes)**:

- Output field setpoint (T) using a ramp rate


**6 Counters**:

- Set_point_field (tag: set_point_field)

- Power_supply_field (tag: power_supply_field)

- Sweep_rate_field (tag: ramp_rate)

- Nitrogen_level (tag: nitrogen_level)

- Helium_level (tag: helium_level)

- Magnet_field (tag: magnet_field)



## Configuration example (yml)

```yaml
-  class: MercuryiPSController
   module: powersupply.mercuryips
   plugin: generic
   name: ipsox8tid12 # ulmag7tps
   tcp:
     url: ipsox8tid12.esrf.fr:7020 # ipsoxulmag7tid12.esrf.fr:7020

   counters:
     - name : set_point_field
       tag: set_point_field
     - name : power_supply_field
       tag: power_supply_field
     - name : sweep_rate_field
       tag: ramp_rate
     - name: nitrogen_level
       tag: nitrogen_level
     - name : helium_level
       tag: helium_level
     - name: magnet_field
       tag: magnet_field

   field_axis:
     - name: field
       tolerance: 0.001
       steps_per_unit: 1
       max_field: 8
       ramp_rate: 3

   heater:
     - switching_time: 16
```

## Presentation of the MercuryiPS controller

Display information about the controller by typing its name in the shell:

```python
MERCURY [1]: ipsox8tid12

Gathering information from {'url': 'ipsox8tid12.esrf.fr:7020'}:

    Out [1]: Manufacturer = OXFORD INSTRUMENTS
             Instrument type = MERCURY IPS
             Serial number = 160650100
             Firmware version = 2.5.05.000

             Status                   : <reserved>
             Activity                 : clamp
             Set point field          : 0.0
             Power supply field       : 0.0
             Ramp rate                : 3.0
             Nitrogen level           : 4.4501
             Helium level             : 37.0
             Magnet field             : 0.0001
             Heater                   : OFF
             Persistent mode          : ON
```

Display the soft axis position:

```python
MERCURY [2]: wm(field)

            field[T]
--------  ----------
User
 High          8.000
 Current       0.000
 Low          -8.000
Offset         0.000

Dial
 High          8.000
 Current       0.000
 Low          -8.000
```


Set the output field to a target value (setpoint) with:

```python
MERCURY [3]: mv(field,0.1)
Moving field from 0.000 to 0.100
```


The device will ramp the field point up to the setpoint value at a given ramp rate (T/mn).
The `.ramp_rate` property sets or reads the ramping speed value.

```python
MERCURY [4]: ipsox8tid12.ramp_rate = 2
MERCURY [5]: ipsox8tid12.ramp_rate
    Out [5]: 2.0
```

The `.persistent_mode` property sets and reads the persistent mode corresponding to the supra conducting mode.
```python
MERCURY [6]: ipsox8tid12.persistent_mode='on'
MERCURY [7]: ipsox8tid12.persistent_mode
    Out [7]: 'ON'
```



The `.activity` property reads the state output and also allows to `clamp` it when it is unused.
```python
MERCURY [8]: ipsox8tid12.activity
    Out [8]: 'hold'
MERCURY [11]: ipsox8tid12.activity='clamp'
MERCURY [12]: ipsox8tid12.activity
    Out [12]: 'clamp'
```

## Scanning with the MercuryiPS controller

The object is exported in the session using the `counter_name` and `axis_name` defined in the YML configuration file.

```python
MERCURY [13]: ipsox8tid12.counters
    Out [13]: Namespace containing:
              .set_point_field
              .power_supply_field
              .sweep_rate_field
              .nitrogen_level
              .helium_level
              .magnet_field
```

Between two points of the scan, the output voltage will be ramped using the actual `ramprate`.

During a scan, the MercuryiPS controller will count on all counters.

```python
MERCURY [14]: ascan(field,0,0.4,4,1,ipsox8tid12)
WARNING: scan data are currently saved under /tmp, where files are volatile.
   Wed Nov 30 10:52:11 2022: Scan(number=2, name=ascan, path=/tmp/scans/mercury/data.h5)
               |   timer    |    axis    |  ipsox8tid12  |   ipsox8tid12    |  ipsox8tid12   | ipsox8tid12  |ipsox8tid12 |ipsox8tid12
        #      |   dt[s]    |  field[T]  |set_point_field|power_supply_field|sweep_rate_field|nitrogen_level|helium_level|magnet_field
   ------------|------------|------------|---------------|------------------|----------------|--------------|------------|------------
        0      |  0.00000   |  0.00000   |    0.00000    |     0.00000      |    3.00000     |   4.22600    |  37.0000   |  0.00000
        1      |  23.6121   |  0.100000  |   0.100000    |     0.100000     |    3.00000     |   4.24200    |  37.0000   |  0.100000
        2      |  46.9967   |  0.200200  |   0.200000    |     0.200200     |    3.00000     |   4.43410    |  37.0000   |  0.200200
        3      |  71.0541   |  0.299900  |   0.300000    |     0.299900     |    3.00000     |   4.45010    |  37.0000   |  0.299900
        4      |  94.9727   |  0.399900  |   0.400000    |     0.399900     |    3.00000     |   4.33810    |  37.0000   |  0.399900

   Took 0:01:37.768289[s]

    Out [15]: Scan(number=2, name=ascan, path=/tmp/scans/mercury/data.h5)
```



## Run a standard hysteresis single way loop (simple scan):
```python
hyster(start_pos, end_pos, nb_pts, tint, power_suplly_obj='ipsox8tid12', motor_name='field')
```
This `hyster` scan uses counters which do not belong to the power supply, so they need defining in the bliss session script:
```python
from bliss import setup_globals

def hyster(start_pos, end_pos, nb_pts, tint, power_suplly_obj= 'ipsox8tid12', motor_name='field'):
    """ Do a hysteresis measurement """
    power_supply = getattr(setup_globals, power_suplly_obj)
    ascan = getattr(setup_globals, "ascan")
    motor = getattr(setup_globals, motor_name)

    power_supply.persistent_mode = 'off'
    ascan(motor, start_pos, end_pos, nb_pts, tint)
    power_supply.persistent_mode = 'on'
```

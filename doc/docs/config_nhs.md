# NHS configuration

The Iseg NHS modules of this series are multi-channel high voltage power
supplies with 6 channels in several version in a NIM chassis.

The `NhsController` controller class (`bliss.controllers.powersupply.nhs`)
allows **6 SoftAxis (pseudo-axes)** corresponding to the channel ***0 to 5***.


## Configuration example (yml)

```yaml
- class: NhsController
  module: powersupply.nhs
  plugin: generic
  name: nhs60n
#  timeout: 10
  tcp:
    url: lid00limax:28400

  axes:
    - name: nhs_out0
      class: NhsAxis
      channel: 0
      low_limit: -1000
      high_limit: 1
      tolerance: 1
      velocity: 20 # ramp rate is the same for all channels

    - name: nhs_out1
      class: NhsAxis
      channel: 1
      low_limit: -1000
      high_limit: 0
      tolerance: 0.1
      velocity: 20 # ramp rate is the same for all channels

    - name: nhs_out3
      class: NhsAxis
      channel: 3
      unit: V
      low_limit: -1100
      high_limit: 1
      tolerance: .1
      velocity: 20 # ramp rate is the same for all channels

```

## Presentation of the NHS controller

Display information about the controller by typing its name in the shell.

!!!remark
    The velocity and ramp rate voltage are identic for all channels.

!!!warning
    Current values are in Ampere (A).

```python
Out [1]: Manufacturer = iseg Spezialelektronik GmbH
         Instrument type = NHS 60 105
         Serial number = 8000367
         Firmware version = 2.20

         Channel | State  | Current limit | Voltage limit |velocity (ramp rate)
         --------|--------|---------------|-------------- |--------------------
            0    | READY  |      NO       |      NO       |        20
            1    | READY  |      NO       |      NO       |        20
            3    | READY  |      NO       |      NO       |        20

```

### Set the kill enable for all the axis module.

```python
NHS [8]: nhs60n.kill_enable()
Out [8]: '0'
NHS [9]: nhs60n.kill_enable(1)
NHS [10]: nhs60n.kill_enable()
Out [10]: '1'
```

### Reset the device to save values:

* turn high voltage off with ramp for all channel ;
* set nominal voltage (V_set) to zero for all channels ;
* set current (I_set) to the current nominal for all channels (unused).


```python
NHS [2]: nhs60n.reset()
NHS [3]:
```

### To read the actual position (voltage) values of all axes:

```python

NHS [3]: wa()
Current Positions: user
                   dial
'nhs_out0` velocity changed from None to 11.0
'nhs_out1` velocity changed from None to 22.0

  nhs_out0    nhs_out1    nhs_out3[V]
----------  ----------  -------------
        -1        -1.8           -1.0
        -1        -1.8           -1.0
```

## Use a specific channel

Display more information about a specific channel. The channel number 3 in this
example is the fourth.

```python

NHS [2]: nhs_out3
Out [2]: AXIS:
    name (R): nhs_out3
    unit (R): V
    tolerance (R) (to check pos. before a move): 0.1
    motion_hooks (R): []
    limits (RW):   Low: -1100.00000  High: 1.00000
    dial (RW): -1.01000
    position (RW): -1.01000
    state (R): READY (Axis is READY)
    velocity (RW):       30.00000  (config:   30.00000)
    is not in current limit
```

Read the channel *state* of the output :

```python
NHS [11]: nhs_out3.state
Out [11]: AxisState: READY (Axis is READY)

```

Enable (state (R): READY) the axis according to the axis. It accetps : 0, 1,
"on", "ON", "off", "OFF".

```python
NHS [3]: nhs_out3.on
Out [3]: False

NHS [4]: nhs_out3.on="on"
NHS [5]: nhs_out3.on
Out [5]: True
```

The channel corresponding to the output will ramp to the value at a given
ramping rate (*velocity*)(V/s).

```python
NHS [5]: nhs_out3.ramprate_voltage # idem nhs_out3.velocity

Out [5]: 22.0

NHS [6]: nhs_out3.ramprate_voltage=10
NHS [7]: nhs_out3.ramprate_voltage
Out [7]: 10.0
```
Set the output voltage to a target value with:

```python
NHS [4]: umv(nhs_out3,-100)
Moving nhs_out3 from -1.0 to -100.0

     nhs_out3[V]
user     -23.780
dial     -23.770
```

Read the channel *status* of the output :

```python
NHS [12]: nhs_out3.status
Channel 3 status is:
	is positive = False
	is arc = False
	is input error = False
	is on = True
	is voltage ramp = False
	is emergency off = False
	is constant current = False
	is constant voltage = True
	is low current range = False
	is arc number exceeded = False
	is current bounds = False
	is voltage bounds = False
	is external inhibit = False
	is current trip = False
	is current limit = False
	is voltage limit = False
	is current ramp = False
	is current ramp up = False
	is current ramp down = False
	is voltage ramp up = False
	is voltage ramp down = False
	is voltage bound upper = False
	is voltage bound lower = False
	is flashover = False
	is flashover nb exceeded = False

```

Read the channel *event* of the output :

```python
NHS [19]: nhs_out3.events
Channel 3 event is:
	event arc = False
	event input_error = False
	event on to off = False
	event end of voltage_ramp = True
	event emergency off = False
	event constant current = False
	event constant voltage = True
	event arc number exceeded = False
	event current bounds = False
	event voltage bounds = False
	event external inhibit = False
	event current trip = False
	event current limit = False
	event voltage limit = False
	event end of current ramp = False
	event current ramp up = False
	event current ramp down = False
	event voltage ramp up = False
	event voltage ramp down = False
	event voltage bound upper = False
	event voltage bound lower = False
	event flashover = False
	event flashover nb exceeded = False
```

Clear the channel *event* of the output :

```python
NHS [20]: nhs_out3.events_clear
```

A *current limit* can be adjusted for each channel (1mA by default) :

```python
NHS [16]: nhs_out3.current_limit
Out [16]: 0.001

NHS [17]: nhs_out3.current_limit=0.001/2
NHS [18]: nhs_out3.current_limit
Out [18]: 0.0005
```

## Counting
2 counters are automatically created for each channels declared in the session.
They correspond to the measured values of the voltage and current output.


```python
NHS [23]: nhs60n.counters
Out [23]: Namespace containing:
          .nhs_out0_V
          .nhs_out0_C
          .nhs_out1_V
          .nhs_out1_C
          .nhs_out3_V
          .nhs_out3_C
```

Measurement group:
```python
NHS [3]: ACTIVE_MG
Out [3]: MeasurementGroup: measurement_nhs (state='default')
        - Existing states : 'default'

        Enabled            Disabled
        -----------------  -----------------
        nhs60n:nhs_out0_C
        nhs60n:nhs_out0_V
        nhs60n:nhs_out1_C
        nhs60n:nhs_out1_V
        nhs60n:nhs_out3_C
        nhs60n:nhs_out3_V
```

Counting example:
```python
NHS [24]: ct()
WARNING: Scan data are currently saved under /tmp, where files are volatile.
       nhs_out0_C  =        0.00000    A (       0.00000      A/s)  nhs60n
       nhs_out0_V  =       -0.510000   V (      -0.510000     V/s)  nhs60n
       nhs_out1_C  =        1.00000e-08A (       1.00000e-08  A/s)  nhs60n
       nhs_out1_V  =       -1.80000    V (      -1.80000      V/s)  nhs60n
       nhs_out3_C  =       -3.31500e-05A (      -3.31500e-05  A/s)  nhs60n
       nhs_out3_V  =     -100.000      V (    -100.000        V/s)  nhs60n
```

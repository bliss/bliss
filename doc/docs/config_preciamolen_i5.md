# Preciamolen I5

## Description

This module allows to configure Preciamolen I5 weights readout as a sampling counter.

3 counters can be configured corresponding to 3 measurement possibilities:

* raw weight
* net weight
* tare weight


## Configuration

### YAML configuration file example


```YAML
- class: I5
  module: preciamolen_i5
  name: i5
  serial:
    url: rfc2217://lid322:28038
  counters:
    - counter_name: net
      weight: net
    - counter_name: raw
      weight: raw
    - counter_name: tare
      weight: tare

```

## References

* Manufacturer documentation:  http://www.preciamolen.com/product/weighing-indicator-i5

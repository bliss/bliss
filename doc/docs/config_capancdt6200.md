# CapaNCDT 6200 configuration

The [MICRO-EPSILON](https://www.micro-epsilon.com/distance-sensors/capacitive-sensors/capancdt-6200/?sLang=en) modular multi-channels system up to 4 channels.

The `CapaNcdtController` controller class (`bliss.controllers.CapaNcdtController`) is a modular multi-channel measuring system for highest precision.
It provides:

**1 to 4 Counters**:

- measuring distance through the channel '1'

- measuring distance through the channel '2'

- measuring distance through the channel '3'

- measuring distance through the channel '4'

The measuring ranges from the below configuration (yml) is set for each sensor during the device's initialization. 
## Configuration example (yml)

```yaml
- class: CapNcdtController
  module: capa_ncdt
  plugin: generic
  name: capancdt
  hostname: microe-dt6230-1.esrf.fr
  
  counters:
    - name: capa1
      channel: 1
      measuring_range: 1000
    - name: capa2
      channel: 2
      measuring_range: 1000  
    - name: capa3
      channel: 3
      measuring_range: 1000
#    - name: capa4
#      channel: 4
```

## Presentation of the capaNCDT 6200 controller

Display information about the controller by typing its name in the shell.

```python
SESSION_CAPNCDT [1]: capancdt
            Out [1]:
                     Gathering information from microe-dt6230-1.esrf.fr:

                     Device          : DT6230
                     S/N             : 2118
                     Channel Status  : channel_available, channel_available, channel_available, no_channel_available
                     Averaging type  : moving_average
                     Averaging number: 4
```

The channel status specifies in increasing order in which channels there is a module: no channel available, channel available, math function.



```python
SESSION_CAPNCDT [2]: capancdt.channel_status
            Out [2]: 'channel_available, channel_available, channel_available, no_channel_available'
```

List the available mode of measurement avaraging:

```python
SESSION_CAPNCDT [3]: capancdt.averaging_type_available
            Out [3]: ['no_averaging:0', 'moving_average:1', 'arythmetic_average:2', 'median:3', 'dynamic_noise_rejection:4']
```


Set and read the available mode of measurement avaraging.

```python
SESSION_CAPNCDT [4]: capancdt.averaging_type=1
SESSION_CAPNCDT [5]: capancdt.averaging_type
            Out [5]: 'moving_average'
```


Set and read the number of measuring values used to calculate the average.

```python
SESSION_CAPNCDT [7]: capancdt.averaging_number=5
SESSION_CAPNCDT [8]: capancdt.averaging_number
            Out [8]: 5
```          
            
## Counting with the CapaNCDT 6200

Each channel provides a counter.

```python
SESSION_CAPNCDT [9]: capancdt.counters
            Out [9]: Namespace containing:
                     .capa1
                     .capa2
                     .capa3
```

A minimum a 0.4s counting time has to be considered.
```python
SESSION_CAPNCDT [10]: ct(.4)
Warning: scan data are currently saved under /tmp, where files are volatile.
ct: elapsed 0.409 s  (abort with Ctrl-c)
                 capa1  =        0.602252    (       1.50563      /s)  capancdt
                 capa2  =        0.407977    (       1.01994      /s)  capancdt
                 capa3  =        0.421340    (       1.05335      /s)  capancdt
            Out [10]: Scan(name=ct, path='not saved')
            
```

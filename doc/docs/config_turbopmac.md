## Configuring a TurboPMAC motor controller

This chapter explains how to configure a DeltaTau TurboPMAC motor controller.


### Specific TurboPMAC axis parameters

* **read_only**: optional, set to True to keep users from directly moving this axis.
    This can be useful when the axis should only be moved through a PLC for instance.

### YAML configuration file example

```YAML
- class: turbopmac
  tcp:
    url: id00pmac
  axes:
    - name: bragg
      steps_per_unit: 67356.444444444
      velocity: 10.0
      acceleration: 25.0
      tolerance: 1e-4
```

### Helpers

In case you need to check the full status of an axis, you can use the pmac_motor_status_to_str method:
```python
PMAC_DEV [1]: id00pmac.pmac_motor_status_to_str(bragg)
0x850000070401
0-23: Motor activated
0-18: Open Loop Mode
0-16: Integration Mode
1-18: Coordinate definition bit2
1-17: Coordinate definition bit1
1-16: Coordinate definition bit0
1-10: Home Complete
1-0: In Position
```

### Coordinate System

PMAC's virtual axes (AKA Coordinate Systems) is currently not supported by this module, but it is a WIP.

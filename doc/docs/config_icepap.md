# IcePAP

## Configuring an IcePAP
This chapter explains how to configure an IcePAP motor controller.

### Supported features

Encoder | Shutter | Trajectories | Linked axes
------- | ------- | ------------ | -----------
YES     | YES     |     YES      |     YES

### Specific IcePAP controller parameters

* **host**: controller hostname or IP address

### Specific IcePAP axis parameters

* **address**: int, motor input channel
* **autopower**: bool, automatically switch on/off power on axis

### YAML configuration file example

```YAML
controller:
  class: icepap
  host: iceid2322
  axes:
      - name: mbv4mot
        address: 1
        steps_per_unit: 817
        velocity: 0.3
        acceleration: 3
      - name: camx
        address: 34
        ...
```

!!! note
    Each `controller: ` line can be prepended with `- ` to configure multiple
    controllers in the same YAML file.

!!! note
    Beacon needs the `emotion` configuration plugin for motor controller
    YAML files interpretation.

    It is common to add an **__init__.yml** file with: `plugin: emotion`
    in the top directory of motors configuration YAML files. Another
    possibility is to add `plugin: emotion` directly in each motor controller
    YAML configuration file.

### Encoder configuration

Encoders directly plugged in the IcePAP controller can be configured directly
in the YAML configuration file.

!!! note
    An encoder can be defined on its own (`Encoder` object), or can be associated
    to an axis to add some extra checks when motion is done: for example, an exception
    can be raised if final position does not correspond to encoder position.

#### Specific IcePAP encoder parameters

* **type**: to set which encoder to read.
    * `ENCIN`: rear incremental encoder
    * `ABSENC`: rear SSI interface
    * `INPOS`: front panel encoder
    * `MOTOR`: electrical phase of the motor
    * `AXIS`: nominal axis position
    * `SYNC`: backplance SYNC input register
* **address**: encoder input channel

!!! note
    See icepap documentation for more info.
    http://wikiserv.esrf.fr/bliss/index.php/ICEPAP#Documentation

#### Encoder YAML configuration example

```YAML
controller:
  class: icepap
  host: iceid42
  axes:
      ...
  encoders:
      - name: mbv1enc
        address: 25
        type: ENCIN
        steps_per_unit: 1e5
```

More information about **Encoder** objects [here](motion_encoder.md)

### IcePAP Shutter configuration

The IcePAP controller can be put in shutter control mode (using IcePAP
LIST MODE), to operate opening and closing of a shutter. This is done
by moving back and forth a stepper motor between two pre-defined
positions. The change is trigger by an external signal.

For details, see: [Shutter configuration](config_shutter.md)


### Linked axis configuration

IcePAP controller can link several axes together, creating a new virtual axis
managed directly by the controller itself. A *linked axis* applies motions to
underlying linked motors. The result is a *fake* motor seen as a *real* Axis for
BLISS (as opposed to the virtual axes).

!!! note
    **icepapcms** must be used to create a linked axis in the controller.

    * in `Linked Axis name` field, declare a common name (ex:zelda) for all real
      motors to link. (and send and validate)

#### Specific IcePAP linked axis configuration

* `class`  must be `LinkedAxis`
* `address` is the **name** of the linked axis, as defined in **icepapcms** (ex: zelda)

Linked axis YAML configuration example:
```YAML
controller:
    class: icepap
    host: icebcu21
    axes:
      ...
      - name: motlink1       # name in Bliss
        address: zelda       # name in icepapcms (case-sensitive)
        class: LinkedAxis
        steps_per_unit: 200
        velocity: 1.0
        acceleration: 1.2
```

Linked axis info strings gives status on real axis as follow:

```
ICEPAP LINKED AXIS:
    address / icepapcms linked name: zelda
    real axis (motor1): ADDR:1  POWER:ON  CLOOP:ON  WARNING:NONE  ALARM:NO  POS:31000
    real axis (motor2): ADDR:2  POWER:ON  CLOOP:ON  WARNING:NONE  ALARM:NO  POS:32594

```

To get more details, like real motor positions/encoders, use
`real_axes_status()` method:
```
BLISS [3]:  motlink1.real_axes_status()

VIRTUAL LINKED AXIS:
Bliss axis name       : motlink1
Icepapcms name/address: zelda
POWER                 : ON
Indexer steps         : 10
Indexer in user unit  : 0.05

REAL AXIS CONTROLLED BY LINKED AXIS:
Real axis address     : 1
Real axis name        : motor1
Home switch           : YES
Homing                : NOTFOUND
Control encoder       : YES
Indexer steps         : 10
Encoder steps         : 10
Homepos steps         : unavailable
Indexer user unit     : 0.05
Encoder user unit     : 0.05
Closed loop           : ON

REAL AXIS CONTROLLED BY LINKED AXIS:
Real axis address     : 2
Real axis name        : motor2
Home switch           : YES
Homing                : NOTFOUND
Control encoder       : YES
Indexer steps         : 10
Encoder steps         : 10
Homepos steps         : unavailable
Indexer user unit     : 0.05
Encoder user unit     : 0.05
Closed loop           : ON
```

Methods specific to linked axis:

* `sync(user_position)`: Synchronizes all real linked axes members to the given
  position. No motion will take place. The position is given in user units of
  the virtual axis.
* `disprotected_move(real_axis, user_target_pos)`: Move one of the real axis to
  the given position. Real axis are accessible through `real_axes`. Example:
  ```motlink1.disprotected_move(motlink1.real_axes.motor1, 0.1)```
* `open_closed_loop()`: open closed loop on all real axes
* `reset_closed_loop()`: reset closed loop on all real axes (ie. sync axis
  position with encoder one)
* `reset()`: reset closed loop on all real axis. Then move each real to the mean
  position and finally set linked axis position to that position. This command
  implies that all axes can be moved independently. It is typically used to sync
  positions after brake release when real axis are in closed loop.



### Axis switch (signals multiplexer)

Icepap system can route signals from one of the axes trough the internal bus to
a ***SYNCHRO*** front-panel DB9 connector.

It can be used for example to read the encoder signal of any of the axes of an
icepap system without occupying too many channels of a MUSST card.

!!!note

    An icepap system can be "split" to offer more than one such multiplexer.

Configuration keywords:

* `include-rack`, `exclude-rack` to define racks that are allowed to be routed:
    - ex: `include-rack: [0, 1, 2, 3, 4, 5, 6, 7]`
    - ex: `exclude-rack: [5]`
    - by default, all the racks are used.

* `external-connector-id` to define the rack where the signal is routed.
    - ex: `external-connector-id: 3`
    - default is 0 (master DB9 connector)

* `syncpos-type` to define the signal to route.
    - the signal must be one of:
        * `AXIS`: Points to the axis nominal position
        * `MEASURE`: Points to the register used for position measurement
        * `SHFTENC`: Points to the register configured as SHFTENC
        * `TGTENC`: Points to the register configured as TGTENC
        * `CTRLENC`: Points to the register configured as CTRLENC
        * `ENCIN`: ENCIN register
        * `INPOS`: INPOS register
        * `ABSENC`: ABSENC register
        * `MOTOR`: MOTOR register
    - example: `syncpos-type: MOTOR`
    - If not specified, the default one is: `MEASURE` (which should be configured with icepapcms).


!!!note

    input/output trigger signals are also routed. They can be used to send
    or receive a pulse to or from an axis.

Example:
```yaml
controller:
  class: icepap
  host: icebcu2
  switches:
    - name: icebcu2_switch
      include-rack: [0,1]
      external-connector-id: 0
  axes:
    - name: mot1
      address: 1
      steps_per_unit: 200
      velocity: 2
      acceleration: 1
      encoder: $mot1enc
    - name: mot2
      address: 2
      steps_per_unit: 200
      velocity: 10
      acceleration: 10
      unit: deg
    - name: mot3
      sign: -1
      address: 3
      steps_per_unit: 100
      velocity: 1
      acceleration: 1
    - name: mot4
      address: 4
      steps_per_unit: -100
      velocity: 1
      acceleration: 1
  encoders:
    - name: mot1enc
      address: 1
      type: ENCIN
      steps_per_unit: 200
      tolerance: 0.025
```

Usage:

```python
icebcu2_switch.pmux_reset()
icebcu2_switch.pmux_reset()
icebcu2_switch._states_list()
```

Such a multiplexer can be used manually, but it is more likely used in
combination with a MUSST card.

Example of such usage can be found in [Musst Switch
section"](config_musst.md#switch).


## Usage

In addition to common Bliss Axis features, an Icepap axis offers specific ones:

### blink

Use the `blink(second=3.0)` command to identify an IcePaP driver.
```python
DEMO [15]: bcumot1.blink(second=5)   # non-blocking function
DEMO [16]:
```


# Eurotherm 3000 series used with **Regulation plugin**:

## YAML configuration file example

```YAML
- class: Eurotherm3000
  plugin: regulation
  module: temperature.eurotherm.eurotherm3000
  timeout: 3
  tcp:
    url: bm16eurohp.esrf.fr:502
  inputs:
    - name: eurohp_in
      unit: 'bar'

    - name: eurohp_in2
      channel: 2
      unit: 'bar'

  outputs:
    - name: eurohp_out
      unit: '%'
    - name: eurohp_out1
      channel: 1
      unit: '%'
    - name: eurohp_out2
      channel: 2
      unit: '%'
    
  ctrl_loops:
    - name: eurohp_loop
      input: $eurohp_in
      output: $eurohp_out

    - name: eurohp_loop2
      channel: 2
      input: $eurohp_in2
      output: $eurohp_out2
```

Opionnaly, `unit` can be specified for each input/output channel.

## Usage

The usage is the same than the Eurotherm **2000**.

In the Bliss session import the Loop object (ex: `eurohp_loop`) and type the name in the bliss shell to print informations.

Access the associated input and output with `eurohp_loop.input` and `eurohp_loop.output`.

Ramp to a given setpoint temperature with `eurohp_loop.setpoint = 23`.

Change the ramp rate with `eurohp_loop.ramprate = 10`.

If ramprate is set to zero (`eurohp_loop.ramprate = 0`), the controller will reach the setpoint temperature as fast as possible.

Get current input temperature with `eurohp_loop.input.read()`.

Perform a scan with the regulation loop as an axis with `eurohp_loop.axis`.

Access the controller with `eurohp_loop.controller` and all expert commands with `eurohp_loop.controller.cmds`.

All current values of the controller parameters can be retrieved with `eurohp_loop.controller.dump_all_cmds`.


## Status Information

In a Bliss session, type the name of the loop to show information about the Loop, its controller and associated input and output.

Use `eurohp_loop.controller.status` or `eurohp_loop.controller.state` to retrieve information about the controller.


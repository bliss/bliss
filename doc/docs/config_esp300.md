# Newport ESP300 motor controller

Bliss controller for Newport ESP300 / ESP301 motor controller.

The controller communicates via serial line. Baudrate is configured on the controller. Check it before configuring in bliss.

For newport ESP compatible stage, motor is automatically configured on controller reset.

## Supported features

Encoder | Shutter | Trajectories
------- | ------- | ------------
No      | No      | No


## Configuration


Example:
```YAML
controller:
  class: NewportESP300
  module: newport
  serial:
    url: tango://id00/serial_001/1
    baudrate: 19200
  axes:
    - name: esp1
      steps_per_unit: 1
      velocity: 12.5
      acceleration: 50.0
      tolerance: 0.001

```

## Specific methods

Read stage identification:
```
NEWPORT [3]: esp1.read_stage()
STAGE : VP-25XA, SNB147328
```

Read motor type:
```
NEWPORT [7]: esp1.read_motor_type()
Motor type : DC servo motor (single analog channel)
```

Read hardware configured limits:
```
NEWPORT [4]: esp1.read_limits()
    Out [4]: ('-12.5000', '12.5000') 
```
These limits can be set to bliss software limits using `smc1.configure_limits()`.

Read PID related  parameters:

```
NEWPORT [8]: esp1.read_pid()
    Out [8]: {'dc_proportional_gain': '136.000', 'dc_integral_gain': '341.000', 'dc_derivative_gain': '1365.00', 'dc_following_error': '0.1000'}

NEWPORT [9]:
```

To change this parameters, use `smc1.set_pid(dict_of_pid_parameters)`.


At controller level, two methods can be used to reset and/or clear_errors:
```
esp1.controller.clear_errors()
esp1.controller.reset()
```

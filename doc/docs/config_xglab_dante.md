![XGLAB DANTE](img/xglab_dante.jpg)
# XGLAB DANTE

DANTE is a Digital Pulse Processor specifically designed for X-ray spectroscopy (https://www.xglab.it/products/dante/).
It can control up to 8 detector channels on one controller. Configuration is 
performed using windows software provided by XGLAB. Configuration files
are HDF5 file (do not use the YML ones) which can be saved after performing
an acquisition.

Bliss access the controller using an ethernet address. To set it up, you will
need to connect the controller via USB on a windows PC. Then, using the DANTE 
software, you can assign it an IP address. 

The XGLabDante controller in bliss has support for all MCA triggering modes 
(soft, trigger, gate). DANTE supports a mapping mode which can be used in 
continuous scans.

## Bliss YAML configuration
```yaml
- name: xglab
  module: mca
  class: XGLabDante
  url: 192.168.1.120
  configuration_directory: /users/opid00/mca_config
```

Above example works in case there is a \_\_init__.yml in the same directory
containing:
```yaml
    plugin: bliss
```

## Loading DANTE configuration file

Getting the list of available configurations:
```python
XGLABTEST [2]: xglab.available_configurations()
      Out [2]: ['Config_ch3478.hdf', '220926_test2_8chs_pulser10k.hdf', '220926_test1_pulser10k.hdf', 'Config_ch12.hdf', 'config.hdf']

XGLABTEST [3]:
```

Loading a configuration:
```python
XGLABTEST [3]: xglab.load_configuration("Config_ch12.hdf")
XGLabDante[xglab] Loading configuration [Config_ch12.hdf] ...
XGLabDante[xglab] 2 boards configured
XGLABTEST [4]:
```

Checking current configuration:
```python
XGLABTEST [4]: xglab.current_configuration
      Out [4]: 'Config_ch12.hdf'

XGLABTEST [5]:

```
Bliss keeps the current configuration as a permanent settings. It will be
automatically reloaded at each bliss restart.

Standard info also prints out current configuration and active channels:
```pyhton
XGLABTEST [1]: xglab                                                                                                         
      Out [1]: MCA:                                                                                                          
                   object: <class 'bliss.controllers.mca.xglab_dante.XGLabDante'>                                            
                   Detector brand : XGLAB                                                                                    
                   Detector type  : DANTE                                                                                    
                   Acquisition mode : MCA                                                                                    
                   Spectrum size    : 4096                                                                                   
                                                                                                                             
                   trigger mode   : SOFTWARE                                                                                 
                   preset mode    : REALTIME   value=0.1                                                                     
                                                                                                                             
               ROIS:                                                                                                         
                   Name      start    end                                                                                    
                   ------  -------  -----                                                                                    
                   roi1          0    512                                                                                    
                   roi2          0   1024                                                                                    
                                                                                                                             
               DANTE:                                                                                                        
                   configuration directory : /users/opid00/mca_config                                                        
                   current configuration   : Config_ch3478.hdf                                                               
                   active channels         : [2, 3, 6, 7]                                                                    
```

## Accessing DPP configuration from bliss

From the bliss shell, you can access DPP parameters for each channel:
```python
XGLABTEST [5]: xglab.dppconfig                                                                                               
      Out [5]: DPP CONFIG for channels : [0, 1]                                                                              
                                                                                                                             
                - access one board config using dppconfig[channel]                                                           
                - set parameter to all boards using dppconfig.set(name, value)                                               
                                                                                                                             
                                                                                                                             
XGLABTEST [6]: xglab.dppconfig[1]                                                                                            
      Out [6]: DPP CONFIG on channel #1:                                                                                     
               .base_offset          = 0                                                                                     
               .baseline_samples     = 64                                                                                    
               .edge_flat_top        = 1                                                                                     
               .edge_peaking_time    = 4                                                                                     
               .energy_filter_thr    = 25                                                                                    
               .fast_filter_thr      = 50                                                                                    
               .flat_top             = 7                                                                                     
               .gain                 = 1.0                                                                                   
               .inverted_input       = False                                                                                 
               .max_peaking_time     = 0                                                                                     
               .max_risetime         = 30.0                                                                                  
               .other_param          = 0                                                                                     
               .overflow_recovery    = 0                                                                                     
               .peaking_time         = 25                                                                                    
               .reset_recovery_time  = 300                                                                                   
               .reset_threshold      = 1000                                                                                  
               .tail_coefficient     = 0.0                                                                                   
               .time_constant        = 0.0                                                                                   
               .zero_peak_freq       = 5.0
```

You can access or modify parameter on each channel individually:
```python
XGLABTEST [7]: xglab.dppconfig[1].peaking_time = 20
XGLABTEST [8]: xglab.dppconfig[1].peaking_time
      Out [8]: 20
XGLABTEST [9]: xglab.dppconfig[0].peaking_time = 25
```

You can also set the same parameter value on all channels:
```python
XGLABTEST [9]: xglab.dppconfig.set("peaking_time", 25)
```

After modifying one or more parameters, you need to apply these parameters on the controller. This is not done automatically.
```python
XGLABTEST [10]: xglab.apply_dppconfig()
Apply dpp configuration on board #0
Apply dpp configuration on board #1
XGLABTEST [11]:
```


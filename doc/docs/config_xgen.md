# X-Ray Generator

## Comet 

Comet provides a wide range of indutrial X-ray generators.

### Usage

!!! note

    The generator needs to be turned to Remote control (Mode 400): Change the mode on the X-ray generator controller: Press CTRL+4, the “mode” on the top left of the controller screen changes from 000 to 400.

    Turn the key back to the ~ sign, then to the lightning bolt sign. Press 1 to launch the X-rays, the system is now awaiting commands from BLISS / SPEC. Repeat this sequence each time you performed a new search.


| Function | Documentation |
| -------- | ------------- |
| `comet.status` | Check status |
| `comet.current_setpoint = XX` in mA / `comet.voltage_setpoint = XX` in kV | Set voltage / current setpoint |
| `comet.current` / `comet.voltage` | Check current values of voltage, current |
| `comet.focus = Focus.standard` / `comet.focus = Focus.small` | Set focal point size where Standard is 5.5 mm2 and Small 1 mm2 |
| `comet.focus` | Check the current status of focal spot size |
| `comet.hvoff_time_disable()` | Unset exposure time |
| `comet.hvoff_time=xx [s]` | Set exposure time (minimum 2 s) |
| `comet.hv = True | False` | Switch beam ON / OFF |

### yml configuration example

Comet accessible through serial communication:

```yaml
- class: Comet
  module: xgen.comet
  name: comet
  serial:
    url: rfc2217://xgenhost:28000
    baudrate: 9600
    bytesize: 8
  counters:
    - name: xr_comet_cv
      tag: voltage
    - name: xr_comet_ci
      tag: current
   axes:
     - name: xr_comet_mv
       tag: voltage
     - name: xr_comet_mi
       tag: current
```

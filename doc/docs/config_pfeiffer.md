# Keller RVC 300, Control unit gas regulating valves

## Yaml sample configuration
```YAML
- name: rvc300
  plugin: generic
  module: pfeiffer_rvc
  class: Rvc300
  pressure_over_temp_table_file: /home/mauro/tmp/lakeshore336_pfeiffer.dat # optional
  serial:
    url: /dev/ttyS5
    baudrate: 9600
```
At ID12, the *pressure_over_temp_table_file* is optionaly used for setting a pressure and pid according a temperature with a Lakeshore 336. The *get_list_table* method could display the values read from the file.

It is possible to read and set : *Kp, Ki, Kd* and the set pressure with *set_pressure* from the RVC 300 controller.
It is also possible to read the current pressure with *current_pressure* from the RVC 300 controller.

```python
LAKESHORE_SESSION [3]: rvc300
              Out [3]: 
                       Gathering information from rvc300:
                       	Version:          3.10
                       	Setpoint:         2.00E+00mbar
                       	Operating mode:   PRESS
                       	Actual value:     9.96E+02mbar
                       	Pressure Sensor:  CMR 1000
                       	Pidc controller:  0
                       	Kp:               13.4
                       	Ki                0.4
                       	Kd:               1.8
LAKESHORE_SESSION [4]: rvc300.kp = 13.3
LAKESHORE_SESSION [5]: rvc300.kp
              Out [5]: 13.3
LAKESHORE_SESSION [6]: rvc300.ki = 0.5
LAKESHORE_SESSION [7]: rvc300.ki
              Out [7]: 0.5
LAKESHORE_SESSION [8]: rvc300.kd = 1.9
LAKESHORE_SESSION [9]: rvc300.kd
              Out [9]: 1.9
LAKESHORE_SESSION [10]: rvc300.set_pressure = 3
LAKESHORE_SESSION [11]: rvc300.set_pressure
              Out [11]: 3.0
LAKESHORE_SESSION [12]: rvc300.current_pressure
              Out [12]: 999.0
```

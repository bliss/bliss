


bliss/scanning/scan_progress.py




!!! note
    `ScanProgress` replaces deprecated `DataWatchCallback`.


One of the goals of `ScanProgress` object is to decouple scan display from
acquisition loop to prevent to slow it down.

It is internaly used by standard scans and must be tailored to the user's needs
for custom scans.


This object can be passed to a Scan object (see 'scan_progress') in order to
store and access information about the progress of the scan and all acquisition
objects that emit the 'scan_progress' signal (see
'AcquisitionObject.emit_progress_signal').

In addition, an optional argument 'tracked_channels' can be passed in order to
ask to store the last data emitted by specific channels.

tracked_channels is a list of names that should match the name of the channels
involved in the scan.

`all` parameter can be used to listen all the 0D channels of a scan:
```
tracked_channels = 'all'
```


* `progress_callback()`
* `scan_new_callback()`
* `scan_end_callback()`


# Example

`bliss.controllers.lima.limatools.LimaTakeDisplay`





Bronkhorst Mass Flow Controller

https://www.bronkhorst.com/en-gb/


Bronkhorst MFC can be used with various communication protocol.
Only the flowbus RS232 version is considered here.



# Control design

The BLISS controller is designed to be used with a Python Tango Device Server

Flowbus Python Tango DS 1.0.0: https://gitlab.esrf.fr/bliss/bronkhorstds

This DS is based on `propar` library provided by Bronkhorst and the package is
available on http://bcu-ci.esrf.fr/stable/


# Installation

```
conda install -c esrf-bcu bronkhorst-propar
conda install -c esrf-bcu bronkhorst-flowbus-ds
```

Configuration example:
```yaml
controller:
  - class: Bronkhorst
    plugin: bliss
    name: bronkignol
    tango_url: tango://id42:20000/id42/flowbus/bronkhorst
    tango_timeout: 10
    tango:
    nodes:
      SNM23210842F03:
        name: bron_evap
        format: "%6.5f"
        channel: 2
      SNM23210842A:
        name: bron_co2
        channel: 3
      SNM23210842C:
        name: bron_liq
        channel: 8
      SNM23210842B:
        name: bron_n2
        channel: 9
```

Only master device object (`bronkignol` in previous example) can be imported in a session.


* `channel` can be configured via the rotating switch on each device.
* `channel` can be configured by software if rotating switch is in position `0`.
  - default software channel is `3`

# usage


```python
DEMO [1]: bronkignol
 Out [1]:                  bron_n2  bron_evap  bron_liq  bron_co2
          Fluid            N2       Temp.      H2O       CO2
          Setpoint (TODO)  0.00     0.00       0.00      0.00
          Measure (Bar)    0.00     23.46      0.00      0.00

DEMO [3]: bronkignol.bron_co2.setpoint
 Out [3]: 0.0

DEMO [7]: bronkignol.bron_co2.setpoint = 1
DEMO [8]: bronkignol.bron_co2
 Out [8]: Fluid   : CO2
          Setpoint: 1.00 %
          Measure : 0.00 %

EXP [8]: bronkignol.bron_co2.measure
Out [8]: 0.0

DEMO [11]: bronkignol.bron_co2.fluid  # internally configured ?
 Out [11]: 'CO2'

DEMO [12]: bronkignol.bron_co2.fluid_temperature   ???
 Out [12]: 20.0

DEMO [13]: bronkignol.bron_co2.orifice  ???
 Out [13]: 0.05000000074505806

DEMO [14]: bronkignol.bron_co2.pressure_inlet
 Out [14]: 12.5

DEMO [15]: bronkignol.bron_co2.pressure_outlet
 Out [15]: 7.5


DEMO [23]: ct(1, bronkignol)
              bron_co2  =        0.00000     (       0.00000      /s)  bron_co2
             bron_evap  =       23.4688      (      23.4688       /s)  bron_evap
              bron_liq  =        0.00000     (       0.00000      /s)  bron_liq
               bron_n2  =        0.00000     (       0.00000      /s)  bron_n2
 Out [23]: Scan(name=ct, path='not saved')

```


Config and debug information
```python
DEMO [19]: bronkignol.bron_co2._proxy
 Out [19]: DeviceProxy(id22/flowbus/snm23210822a,139918181115648)

DEMO [20]: bronkignol.bron_co2.type
 Out [20]: 3
```

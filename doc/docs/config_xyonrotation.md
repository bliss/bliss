Virtual X and Y axis which stay on top of a rotation


## Config example


```yaml
class: XYOnRotation

# if rotation is inverted
# optional, default is False
inverted: False

# if rotation angle is in radian
# optional, default is False == degree
# deprecated, prefer setting unit "deg"/"rad" to the rotation axis
radian: False           # optional

# Use dial instead of user position for the rotation
# optional, default is False
use_dial_rot: True

axes:
    - name: $rot
      tags: real rot
    - name: $px
      tags: real rx     # real X
    - name: $py
      tags: real ry     # real Y
    - name: sampx
      tags: x
    - name: sampy
      tags: y
```

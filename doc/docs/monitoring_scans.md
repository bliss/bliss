# Monitoring scans

Monitoring scans are special scans that can be launched in the background to monitor some counters.
They inherit from the standard scan of BLISS, so data will transit through REDIS (so available via BlissData) and can be saved to files.

Inside one BLISS session, monitoring scans are automatically stopped when a standard scan is started.
When the last standard scan terminates, monitoring scans are automatically restarted.

If monitoring scans are launched in a dedicated BLISS session, standard scans started in another session will not stop the monitoring scans.

Flint will identify a monitoring scan and provides a dedicated display.

For the management of the monitoring scans, BLISS provides the following helpers:

- `start_monitoring`: start one monitoring scan (will run in background)
- `stop_monitoring`: stop one monitoring scan
- `stop_all_monitoring`: stop all running monitoring scans
- `start_all_monitoring`: re-start all previously stopped monitoring scans
- `clean_monitoring`: stop and delete all monitoing scans



Example:
```python
from bliss.scanning.monitoring import start_monitoring, stop_monitoring
from bliss.scanning.monitoring import stop_all_monitoring, start_all_monitoring
from bliss.scanning.monitoring import clean_monitoring

# Launch one monitoring scan named 'moni1' 
# (it will run in the background forever (npoints=0) or until stopped )
start_monitoring("moni1", 0.1, diode, sleep_time=1)

# Launch another one named 'moni2' 
# (data will be saved, using current SCAN_SAVING path)
start_monitoring("moni2", 0.1, temp1, sleep_time=2, save=True)

# Stop 'moni1'
stop_monitoring("moni1")

# restart all (so here 'moni1')
start_all_monitoring()

# stop all
stop_all_monitoring()

# stop and clear all
clean_monitoring()

```


```python
def start_monitoring(
    name: str,
    count_time: _float,
    *counters: _countables,
    sleep_time: _float = 1.0,
    npoints: _int = 0,
    save: bool = False,
    use_default_chain: bool = True,
    table: bool = False,
):
    """Launch a monitoring scan task in the background

    Args:
        name: monitoring scan name
        count_time: measurement time
        *counters: counters, measurement group or counters container
        sleep_time: sleeping time between two measurements (default = 0)
        npoints: number of measurements (default = 0, i.e never ending)
        save: enable data archiving (default = False)
        use_default_chain: enable usage of the same default chain as all step-by-step scans
        table: enable data display as a table instead of a curve

    """


def stop_monitoring(name: str):
    """Stop a monitoring scan.

    Args:
        name: name of the monitoring scan
    """
```




## More control with the monitoring scan manager

Eventually, you can import the `MONITORING_SCANS` manager which stores all the created monitoring scans
and acces one particular monitoring scan via its name.

```python
from bliss.scanning.monitoring import MONITORING_SCANS

start_monitoring("moni1", 0.1, diode, sleep_time=1)

m1 = MONITORING_SCANS["moni1"]
m1.stop()
m1.start()
m1.scan.scan_info

```

```python
class MScan:
    
    @property
    def name(self):

    @property
    def count_time(self):

    @property
    def counters(self):

    @property
    def sleep_time(self):

    @property
    def npoints(self):

    @property
    def saving_is_enabled(self):

    @property
    def use_default_chain(self):

    @property
    def scan(self):

    def start(self):

    def stop(self):
        
```
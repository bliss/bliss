## Configuring a PI stepper controller

This chapter explains how to configure a stepper controller from
Physical Instrument company.

This configuration should be common to the following models:

* PI C663

### YAML configuration file example
```yaml
controller:
  class: PI_C663
  serial:
     url: /dev/ttyRP42
  axes:
      - acceleration: 1.0
        backlash: 0
        high_limit: null
        low_limit: null
        name: c663m0
        offset: 0
        steps_per_unit: 1
        tolerance: 0.1
        velocity: 5
        tango_server: c663m0
```

## Starting BLISS demo processes

BLISS demonstration processes can be started with the commands that are available
in the demo environments (see [installation instructions](installation.md)). We will
assume here that these environments are all conda environments with the names
`bliss_env`, `lima_env` and `oda_env` (the later is optional).

Start Beacon and the simulated device servers

```bash
conda activate bliss_env
bliss-demo-servers --lima-environment=lima_env [--oda-environment=oda_env]
```

When `--oda-environment` is specified, visit http://localhost:5555 to
monitor the data processing.

Start the BLISS shell with the demo session

```bash
conda activate bliss_env
bliss-demo-session
```

### Launch blissterm

The demo session can be used with blissterm.

This can be setup the following way:

```
mamba create -n pnpm-env pnpm
cd blissterm
conda run -n pnpm-env pnpm install
conda run -n pnpm-env pnpm build
```

This will generate a content inside `src/blissterm/static`.

To setup and launch blissterm so extra libraries have to be installed
```
conda run bliss-env pip -e ./blissterm
```

Then the server can be launched, following the way the demo session can be started.

```
BEACON_HOST="lvalls:10001" TANGO_HOST="lvalls:10000" blissterm
```

Finally you should have access to a link like http://localhost:5000.

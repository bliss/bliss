
Table support

Configuration parameters:

* back: alias for real back leg axis
* front: alias for real front leg axis
* ttrans: Y-axis translation calculated axis alias
* trot: Z-axis rotation calculated axis alias


Top view (S=sample position)::
```
             ^  ttrans
             |                            \
             |                             \
    ------------------------------    trot ^\
        ^                  ^            __/__\
        |          S<-d1-> |
        |             ^    |
        |<---d2--->   |    |
       ^|             |d5  |
     d4||             |    |
       -|             -    |
      back(tyb)      front(tyf)
```



## Config example

```yaml
-
  controller:
    class: tabsup
    d1: 300
    d2: 1900
    axes:
        -
            name: $ttf
            tags: real front
        -
            name: $ttb
            tags: real back
        -
            name: ttrans
            tags: ttrans
            tolerance: 0.3
        -
            name: tzrot
            tags: trot
```

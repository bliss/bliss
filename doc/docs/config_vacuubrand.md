# Vacuubrand pressure transmitter DCP3000


## Yaml sample configuration
```YAML
- plugin: generic
  class: Dcp3000
  name: dcp1
  serial:
    url: /dev/ttyS4
    baudrate: 19200

      
  counters:
    - name: dcp1_c
```

- plugin name (mandatory = 'generic')
- class name (mandatory = 'Dcp3000')
- controller name (mandatory)
- serial line configuration (mandatory)
- serial line url (mandatory)
- serial line baudrate (optional)
- list of counters
    - counter name (mandatory)


# MOSCA MCA Controller


## Configuration


!!!example "YAML configuration for a MOSCA MCA controller"

    ```yaml
    - name: mca_sim
      class: McaSimulator
      module: mosca.simulator
      plugin: generic

      tango_name: id00/mosca/simulator

      roi_correction_formula: roi / (1-trigger_livetime/realtime) / iodet  #(optional)

      external_counters:   #(optional)
        iodet: $diode1     #(optional)

      # Valid variables for the 'roi_correction_formula' are:
      #  - 'roi'
      #  - a stat label (ex: 'icr', 'ocr', 'deadtime', ...)
      #  - an external_counter tag as declared below 'external_counters' (ex: 'iodet')

    ```


## Usage

```python
BLISS [10]: fx
  Out [10]: === MCA controller: id42/falconx/fx ===
             detector name:
             detector model:    XIA FalconX
             channels number:   2
             spectrum size:     4096
             trigger mode:      SOFTWARE
             preset mode:       REALTIME
             preset value:      1.0 s
             detector mode:     MCA
             refresh rate:      0.1000 s

             configuration file: c:\blissadm\falconx\config\id42/20241210-ID12FalconX2-MR-8keV_Iron_spec.ini
```

Counting for 120 ms with a 2-channels FalconX. By default, 8 statistics counters ares displayed.

```python
BLISS [13]: ct(0.120,fx)
ct:      state  elapsed
          DONE       0s

            events_det00  =        0.00000     (       0.00000      /s)  fx
            events_det01  =        0.00000     (       0.00000      /s)  fx
               icr_det00  =        0.00000     (       0.00000      /s)  fx
               icr_det01  =        0.00000     (       0.00000      /s)  fx
               ocr_det00  =        0.00000     (       0.00000      /s)  fx
               ocr_det01  =        0.00000     (       0.00000      /s)  fx
  trigger_livetime_det00  =        0.00000     (       0.00000      /s)  fx
  trigger_livetime_det01  =        0.00000     (       0.00000      /s)  fx
          deadtime_det00  =        1.00000     (       8.33333      /s)  fx
          deadtime_det01  =        1.00000     (       8.33333      /s)  fx
          realtime_det00  =        0.120000    (       1.00000      /s)  fx
          realtime_det01  =        0.120000    (       1.00000      /s)  fx
          triggers_det00  =        0.00000     (       0.00000      /s)  fx
          triggers_det01  =        0.00000     (       0.00000      /s)  fx
   energy_livetime_det00  =        0.00000     (       0.00000      /s)  fx
   energy_livetime_det01  =        0.00000     (       0.00000      /s)  fx
  Out [13]: Scan(name=ct, path='not saved')
```

Statistics counters can be individually selected:
```
BLISS [19]: ct(0.120, fx.counters.events)
ct:      state  elapsed
          DONE       0s

          events_det00  =        0.00000     (       0.00000      /s)  fx
          events_det01  =        0.00000     (       0.00000      /s)  fx
  Out [19]: Scan(name=ct, path='not saved')
```


### Configuration file

A new configuration file can be loaded:
* by setting the `configuration_file` property
* using `load_configuration(<filename>)` method
* using `load_configuration()` to select file in a list

```python

BLISS [2]: fx.configuration_file
  Out [2]: 'c:\\blissadm\\falconx\\config\\id42/20240915-ID42FalconX2-MR-8keV_CaK_spec.ini'

BLISS [3]: fx.load_configuration()

1 - 20240915-ID42FalconX2-MR-12keV_spec_singlechannel.ini
2 - 20240915-ID42FalconX2-MR-8keV_CaK_spec.ini
3 - 20241115-ID42FalconX2-LE-4keV_SK_spec.ini
4 - 20241127-ID42FalconX2-MR-8keV_spec_Sm.ini
5 - 20241210-ID42FalconX2-MR-8keV_Iron_spec.ini

Loading 20241210-ID42FalconX2-MR-8keV_Iron_spec.ini...

BLISS [4]: fx.configuration_file="20240915-ID42FalconX2-MR-12keV_spec_singlechannel.ini"

BLISS [5]: fx.load_configuration("20241127-ID12FalconX2-MR-8keV_spec_Sm.ini")
Loading 20241127-ID12FalconX2-MR-8keV_spec_Sm.ini...

```


### ROIs

Regions of interest can be set with:

* `set()` method
* `load_rois_from_file()` method
* `edit_rois()` method to define them graphically using Flint


Using `rois.set()` method.
```
BLISS [1]: fx.rois
  Out [1]:    name  channel   roi
            ------------------------

BLISS [2]: fx.rois.set("AuLa", (654, 785))

BLISS [3]: fx.rois
  Out [3]:    name  channel    roi
            --------------------------
              AuLa    all   (654, 785)
```



### ROIs corrections

ROIs can be corrected using a calculation formula and statics counters.

Example for dead time correction:
```
roi_correction_formula: roi / (1-trigger_livetime/realtime)
```

See https://gitlab.esrf.fr/-/snippets/249 for calculation details.


![Screenshot](img/scan_data_flow_path.svg)

Data produced by BLISS is published into [Redis](https://redis.io/) (RAM
storage). In Redis, data is stored for a limited period of time (1 day by default) and for a limited amount (1GB by default).

Two primary [Redis](https://redis.io/) subscribers are provided by BLISS

1. The [Nexus writer](config_nexus_writer.md) for writing [Nexus compliant](https://www.nexusformat.org/) HDF5 files.

2. [Flint](../flint/index.md) for online data visualization

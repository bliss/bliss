The bliss rest service provides a REST API for bliss that can be used to provide remote access to BLISS. The API documentation is available here: [Rest API documentation](bliss_api/index.html)

# Overview

![Architecture overview](bliss_api/rest-service-achitecture.png)

The service is created at the start of a BLISS session, and for each of them.

It exposes a set of entry points which can be used to interact with the BLISS session.

- Access to general information about the session
- Access to objects and change events (`roby`, `bsh`...)
- Access to available object types (`motor`, `shutter`...)
- Access to yaml file configuration
- Call functions

# Setup

## Declare REST configuration

First you have to declare that the session have to expose
a REST service.

For the demo session it's already the case, else you have to declare
such `rest` key in the yaml file of your bliss session.

```
- class: Session
  name: demo_session
  rest:
     port: auto
     cors: true
     iosecret: 'foobar2000'
     debug: false
```

## Launch demo session

Then you have to start the infrastructure and the bliss
session.

For the demo session in can be done this way

```
bliss-demo-servers
```

```
export BEACON_HOST="mycomputer:10001"
export TANGO_HOST="mycomputer:10000"
bliss -s demo_session
```

# Bliss Web UI

![blisswebui](img/blisswebui.png)

Bliss Web UI provides a mechanism to create simple user interfaces to monitor many hardware objects. Bliss Web UI is built using shared UI components from [daiquiri-ui](https://ui.gitlab-pages.esrf.fr/daiquiri-ui/) to provide a consistent user experience beteween applications.

## Installation

Bliss Web UI is provided in a separate package and can be installed with:

```
pip install blisswebui
```

## Configuration

Bliss Web UI is configured via beacon using the `blissterm` folder i.e. `beacon://blissterm/config.yml`. The root folder should contain an `__init__.yml` with `bliss_ignored: true`:

```yaml title="blissterm/__init__.yml"
bliss_ignored: true
```

This configuration file is used to define the monitor objects at the top of the interface:

```yaml title="blissterm/config.yml"
monitor:
  - comparator: in
    comparison:
      - OPEN
      - RUNNING
    name: Front End
    overlay: fe.status
    value: fe.state

  - name: Omega
    value: omega.position

  - comparator: ==
    comparison: OPEN
    name: Absorber
    overlay: abs1.status
    value: abs1.state
```

## Layouts

Layouts are defined in `beacon://blissterm/layouts.yml`. These use the [daiquiri-ui](https://ui.gitlab-pages.esrf.fr/daiquiri-ui/) &raquo; [Layout Manager](https://ui.gitlab-pages.esrf.fr/daiquiri-ui/layout-manager/) to render a user interface from a standardised YAML layout language. See the [Layout Manager](https://ui.gitlab-pages.esrf.fr/daiquiri-ui/layout-manager/) documentation for more information about the available wrapper components and how to use them.

!!! info
    blisswebui currently supports only the `hardwaregroup` component type.

For example:

```yaml title="blissterm/layouts.yml"
layouts:
  - name: Monitor
    acronym: monitor
    icon: fa-cog
    children:
      - type: row
        children:
          - type: col
            children:
              - type: panel
                title: Rotation
                children:
                - type: hardwaregroup
                  ids:
                    - id: omega
                    - id: robz
                      steps: [0.1, 0.5, 1]
                      step: 0.5

          - type: col
            children:
              - type: panel
                title: Detector
                children:
                  - type: hardwaregroup
                    ids:
                      - id: beamstop

      - type: row
        children:
          - type: col
            children:
              - type: hardwaregroup
                ids:
                  - id: samx
                  - id: samy
                  - id: samz
```

# Debug

The BLISS API can expose it's state from the BLISS shell.

## Check the actual objects

The actual exposed objects are stored in the `object_store`.

```
DEMO [1]: current_session.rest_service.object_store
 Out [1]: List of exposed objects
          robx
          roby
```

For each objects events are monitored and propagated, which could
take some resources.

The `object_store` allow to register and unregister objects on the fly.

## Check the actual tasks

The actual tasks are stored in the `async_tasks`.

```
DEMO [2]: current_session.rest_service.async_tasks
 Out [2]: List of tasks
          9a749eb9-4e24-4ee8-ad0c-b31a24c15d36  DONE     long_task(...)
          05a9e667-6f12-45c1-a34a-31524856194e  RUNNING  my_scan(...)
          c6b2bfb3-0fcb-45cd-9bbf-75d4f9c99f76  RUNNING  roby.move(...)
```

This service allow to check the status of the tasks.
In case of problem, the task can be manually killed with `async_tasks`.
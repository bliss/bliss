# Kempower power supply configuration

## Example configuration

```yaml
  class: Kempower
  module: powersupply.kempower
  plugin: bliss
  name: kem
  timeout: 10
  serial:
    url: ser2net://lid312:28000/dev/ttyRP27
    baudrate: 4800
  counters:
    - name: kemv
      tag: voltage
      mode: SINGLE
    - name: kemc
      tag: current
      mode: SINGLE
```

# Installation and configuration of XIA MCA

Installation of XIA devices is common to 3 XIA MCA electronics:

* Xmap
* Mercury
* FalconX

The devices are connected to a Windows (11 pro 64 bits) computer.

BLISS must be installed on that Windows PC to be able to run a BLISS RPC server
`bliss-handel-server`.

**Handel** is the library used to deal with XIA devices and data. It comes
with **ProSpect**, the Windows software provided by XIA for testing and
calibrating XIA devices.

There are 2 versions of ProSpect:

* ProSpect for Xmap and Mercury (merged with the deprecated *xManager*)
* ProSpect for FalconX


## Windows PC installation

!!! note "Windows version must be 11 pro 64 bits"
    Windows 7 and 10 are no more supported at ESRF.

### Installation of conda

* Download a conda installer:
    * [miniconda](https://repo.anaconda.com/miniconda/Miniconda3-latest-Windows-x86_64.exe)

!!!warning

    Run miniconda installer as administrator (right clic / run as administrator)

* Miniconda Installer:
    * `Next`  /  `I agree` / tick `Install for All users`  / `Next`
    * "Destination Folder:" `C:\ProgramData\Miniconda3` / `Next`
    * "Advanced Installation Options":
        - Tick `Create shorcuts (supported packages only)`
        - Tick `Register miniconda3 as the system python 3.XX`
        - Tick `Clear the package cache upon completion`
        - Click `Install`, wait a bit.
    * `Next`
    * "Completing Miniconda3":
        - Untick "Anaconda Individual Edition Tutorial"
        - Untick "Getting Started with Anaconda"
        - Click `Finish`
* Create a link from taskbar to anaconda powershell:
    * click on Windows Start Menu / type `anaconda` / Right click on `Anaconda Powershell Prompt`  ( not `Anaconda Prompt`) /  `Pin to taskbar`


### Install some software

* Copy into `C:\` the `blissadm\` directory (or sub-dirs if it already exists) found here (415 MB):

```
P:\\ISDD\SOFTWARE\Public\distrib\blissadm\
```

* Install Git
* Install what you can find interesting :)
  - GIT guis:
    * git-cola-4.7.1.tar.gz
    * Guitar-1.2.999-win32-installe.exe
    * TortoiseGit-2.16.0.0-64bit.msi
  - HashCheckInstall-2.1.11.exe tool to check hash of files
  - HxDSetup.exe: Hexadecimal editor
  - nmap-7.40-setup.exe: to debug network problems
  - npp.7.3.2.Installer.x64.exe: notepad++
  - windirstat1_1_2_setup.exe: find lost space on disk
  - WinMerge-2.16.12-Setup.exe: diff tool



### Creation of bliss conda environment

* Do not use `tango-controls` and `defaults` channels

* run anaconda powershell prompt (icon in the taskbar)

```
conda config --show channels

channels:
  - conda-forge
  - https://repo.anaconda.com/pkgs/main   <---- to remove
  - https://repo.anaconda.com/pkgs/r      <---- to remove
  - https://repo.anaconda.com/pkgs/msys2  <---- to remove
  - defaults          <---- to remove
  - tango-controls    <---- to remove
  - esrf-bcu
```

```


conda config --env --remove channels https://repo.anaconda.com/pkgs/main
conda config --env --remove channels https://repo.anaconda.com/pkgs/r
conda config --env --remove channels https://repo.anaconda.com/pkgs/msys2
conda config --env --remove channels tango-controls
conda config --env --remove channels defaults

conda config --env --add channels conda-forge
conda config --env --append channels esrf-bcu
```

* install mamba in base environment
```
conda activate base
conda install -y mamba -c conda-forge
```

* Create the `bliss` conda environment with python 3.10 and git:
```
conda create -n bliss -y python=3.10 gitpython
```


### Installation of Bliss 2.1

!!!note

    cannot use Makefile on Windows -> use conda package :)


```
conda create -n mosca python=3.10
conda activate mosca
mamba install bliss=2.1
```




* test bliss installation:

```
C:\ python
>>> import bliss
>>>
```


### Mosca environment

Temporary solution:

* copy the file `mosca_env.tgz` from `P:\\ISDD\SOFTWARE\Public\distrib\blissadm\` directory into `C:\blissadm\`
* exctract it (right click / extract all...)
* use a startup script like:

```
set CONDAPATH=C:\ProgramData\Miniconda3
set ACTIVATE=%CONDAPATH%\Scripts\activate.bat

set ENVNAME="C:\blissadm\mosca_env\mosca"
set ORBendPoint=giop:tcp:172.29.XX.YY:

set TANGO_HOST=idXX:20000
set BEACON_HOST=idXX:25000

call %ACTIVATE% %ENVNAME%

cd c:\blissadm
cd mosca.git
python.exe -m mosca.devices.falconx.FalconX lala

pause

```


### Installation of XIA software

!!!note "2022 versions. (latest available versions as of October 2024)"

    * *ProSpect* for Falconx
        * version 1.1.71
    * *ProSpect* for Mercury and Xmap
        * version 1.1.66
    * *Handel-all* library for Mercury/Xmap devices
        * version 1.2.28 64 bits
    * *Handel-sitoro* library for Falconx
        * version 1.1.22 64 bits


* Depending on the XIA device type, **FalconX** or **Xmap/Mercury**:
    * Install the corresponding *ProSpect*
    * Test connection to the device with *ProSpect*

!!!tip
    For Xmap/Mercury: Avoid to rename Prospect directory.
    There is a hard coded link ths the directory in config files. An update of the name implies an update of all config files.

* If available, install configuration file(s) in: `c:\blissadm\falconx\config\idXX\` or in `c:\blissadm\xmap\config\idXX\`

!!! note "For FalconX"
    * Connection to the falconX can be tested with a browser using address `http://192.168.200.201`
    * In firmware tab, check firmware version (latest as of oct 2024:`22.4.0` june 2023)




### Server Startup script

!!!note
    Examples are given for FalconX. Xmap/Mercury behaviour is similar.

!!!important
    To ensure a peaceful coexistence between conda and powershell, the shortcut
    `falconx/falconx-start IDXX` should point to the startup script
    `falconx/falconx-server_IDXX.ps1`. They need to be customized.


* Duplicate startup script (let it in falconx/ directory) and rename it:
    - `falconx\falconx-server_IDXX.ps1` -> `falconx\falconx-server_ID00.ps1`

* Customize it according to Beamline configuration (BEACON_HOST / Device Name):
    - right-clisk / edit

* Copy and rename the shortcut `falconx\falconx-start IDXX` on the desktop:
    - drag'n drop with right mouse button OR `ctrl-c` `ctrl-v`
    - rename the desktop shortcut `falconx-start ID00` (right click / rename)

* Customize the desktop shortcut:
    - 'right-click' / 'properties' ;  Change end of "Traget" line: `IDXX` -> `ID00`



!!!note "Developer's details"
    The `bliss-handel-server` start-up script is created at installation time using
    the **entry_points** definitions in `setup.py` of the BLISS repository.
    ```python
    entry_points={
          "console_scripts": [
              ...
          "bliss-handel-server = bliss.controllers.mca.handel.server:main",
          ] }
    ```
    The wrapping of the Handel library is made with cffi.
    see: `bliss/controllers/mca/handel/_cffi.py`



## BLISS Configuration in Beacon

Example for mercury:
```yaml
- name: mercury
  module: mca
  plugin: bliss
  class: Mercury
  url: tcp://wfameid42xia:8000
  configuration_directory: C:\\blissadm\\mercury\\config\\BM16
  default_configuration: Vortex3_Mercury4ch_05us_Hg.ini
```

Example for FalconX:
```yaml
- name: fxid42
  module: mca
  plugin: bliss
  class: FalconX
  url: tcp://wid421:8000
  configuration_directory: C:\\blissadm\\falconx\\config\\ID42
  default_configuration: falconxn.ini
```

Example for Xmap:
```yaml
- name: xmap42
  module: mca
  plugin: bliss
  class: XMAP
  url: tcp://wid421:8000
  configuration_directory: C:\\blissadm\\xmap\\config\\ID42
  default_configuration: xmap.ini

```





# Keithley 2450 series


## Models

The keithley 2450 series replaces the 2410 series : measurement of current/voltage and source of current/voltage.
Known models are:

- 2450 : source up to 200V / 1A
- 2460 : source up to 100V / 7A
- 2470 : source up to 1.1kV / 1A

These controllers are interfaced by ethernet. They can be configured in DHCP. Check out MAC address, or fix an IP address using `Menu > System > Communications > LAN tab`. Once configured on the network, you can access a web server at *my_ip_name.esrf.fr* that allows to control the instrument remotely. In particular, you can use the *virtual front panel* that allow you to interact as if you were in front of the instrument.


## Configuration

```YAML
plugin: keithley
keithleys:
- model: 2450
  tcp:
    url: k2450bcu1:5025
  sensors:
    - name: keith2450
      address: 1
      meas_func: CURR
      auto_range: False
      range: 1e-3
      source_func: VOLT 
      source_value: 0           
      source_enable: False 
      source_range: 2  
      source_auto_range: False
      source_limit: 1e-3
```

Configuration keys for measurement:

- **meas_func** : can be CURR (current) / VOLT (voltage) / RES (resistance)
- **auto_range** : turn ON/OFF measurement auto range
- **range** : fix range. Avaliable ranges depends on models. Get the list through *keith2450.possible_ranges*

Configuration keys for source:

- **source_func** : can be CURR (current) or VOLT (voltage)
- **source_value** : set the output source value
- **source_enable** : enable / disable source output
- **source_auto_range** : turn on/off auto range (no output limit)
- **source_range** : fix a range for the output. Will limit output value ie. if source_func=VOLT and source_range=20, output voltage will not exceed 20V. Available source range depends on model and can be retrieved by *keith2450.source_possible_ranges*.
- **source_limit** : when sourcing voltage, limit output current. When sourcing current, limit output voltage. The source_limit cannot exceed the measurement range.

All these configuration keys can be omitted. They are all bliss settings. You can set them once the instrument is accessible in bliss and rely on the settings only.

!!! Note
    By default, all errors appears as a popup window on the instrument screen. This popup stays on the screen until someone click on it. To avoid this you can disable the error popup in `Menu > System > Event Log > Log Settings tab > Popups`. In anycase, when setting source parameters, these errors are read from bliss and reported as a warning message.

##Compliance limits

Compliance limits that exists on 2410 serie is now called *source_limit* on the 2450 series. Check out the documentation on [keithley 2410](config_keithley_2410.md) compliance for more info.


## Source as an axis

The source can be used as an axis. To create this software axis, just call `mykeithley.source_axis`. This will create an axis visible in all where motor commands. Axis name will be set to **yourkeithleyname_axis**. 

You can by configuration enable the creation of that axis by adding this in the yml file:

```YAML
      export_axis = True	# or False
```


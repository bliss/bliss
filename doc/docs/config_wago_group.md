
# Wago groups

For convenience it's possible to group keys from different wagos to a WagoGroup
object in config.

## Configuration

```yaml
- name: wago_group
  plugin: bliss
  module: wago.wagogroup
  class: WagoGroup
  wago:
    - name: $wago_simu
      logical_keys: foh2ctrl, foh2pos, esTr1, esTr2, o10v1
    - name: $wago2
```

In this example a `wago_group` object can be obtained with:

* Keys `foh2ctrl`, `foh2pos`, `esTr1`, `esTr2`, `o10v1` from `wago_simu`
* All keys from `wago2`

!!! warning
    Duplicate logical key names between wagos are not allowed.

!!! note
    If `logical_keys` is not specified, all keys for the wago are imported.

!!! info
    `logical_keys` defined as counters in the wagos are also `counters` in the wago group.

## Usage

```python
DEMO [1]: wago_simu
 Out [1]:
 logical device  chan num   module_type           module description
 ---------------- ---------  -------------  ----------------------------------
     foh2ctrl          4      750-504          4 Channel Digital Output
     foh2pos           4      750-408          4 Channel Digital Input
      sain2            1      750-408          4 Channel Digital Input
      sain4            1      750-408          4 Channel Digital Input
      sain6            1      750-408          4 Channel Digital Input
      sain8            1      750-408          4 Channel Digital Input
       pres            1      750-408          4 Channel Digital Input
      esTf1            1      750-469     2 Channel Ktype Thermocouple Input
      esTf2            1      750-469     2 Channel Ktype Thermocouple Input
      esTf3            1      750-469     2 Channel Ktype Thermocouple Input
      esTf4            1      750-469     2 Channel Ktype Thermocouple Input
      esTr1            1      750-469     2 Channel Ktype Thermocouple Input
      esTr2            1      750-469     2 Channel Ktype Thermocouple Input
      esTr3            1      750-469     2 Channel Ktype Thermocouple Input
      esTr4            1      750-469     2 Channel Ktype Thermocouple Input
     intlckf1          1      750-517         2 Changeover Relay Output
     intlckf2          1      750-517         2 Changeover Relay Output
      o10v1            1      750-554          2 Channel 4/20mA Output
      o10v2            1      750-554          2 Channel 4/20mA Output
    double_out         2      750-517         2 Changeover Relay Output

 Given mapping does match Wago attached modules

DEMO [2]: wago_group
 logical device   current value  wago name              description
 --------------  --------------  -----------   --------------------------------
  foh2ctrl       [1, 1, 0, 1]    wago_simu        4 Channel Digital Output
  foh2pos        [0, 0, 0, 0]    wago_simu        4 Channel Digital Input
   esTr1            -496.4       wago_simu     2 Channel Ktype Thermocouple Input
   esTr2            -2765.5      wago_simu     2 Channel Ktype Thermocouple Input
   o10v1         1.0517578125    wago_simu        2 Channel 4/20mA Output

DEMO [3]: wago_group.logical_keys
 Out [3]: ['foh2ctrl', 'foh2pos', 'esTr1', 'esTr2', 'o10v1']

DEMO [4]: wago_group.cnt_names
 Out [4]: ['esTr1', 'esTr2']

DEMO [5]: wago_group.counters
 Out [5]: namespace(esTr1=<bliss.controllers.wago.wago.WagoCounter>,
                    esTr2=<bliss.controllers.wago.wago.WagoCounter>)

```

Groups values can be set and get as on the underlying wagos.

```python
DEMO [8]: wago_group.get('foh2ctrl', 'esTr1', 'o10v1', 'foh2pos')
         Out [8]: [1, 1, 0, 1, -496.4, 1.0517578125, 0, 0, 0, 0]

DEMO [9]: wago_group.set('foh2ctrl', 1, 1, 1, 1, 'o10v1', 3.14)

DEMO [10]: wago_group.get('foh2ctrl')
         Out [10]: [1, 1, 1, 1]

DEMO [11]: wago_group.get('o10v1')
         Out [11]: 3.14013671875

DEMO [12]: wago_group
 Out [12]:
 logical device    current value   wago name                description
 ---------------  ---------------  ----------  ----------------------------------
    foh2ctrl       [1, 1, 1, 1]    wago_simu       4 Channel Digital Output
    foh2pos        [0, 0, 0, 0]    wago_simu       4 Channel Digital Input
     esTr1            -496.4       wago_simu   2 Channel Ktype Thermocouple Input
     esTr2            -2765.5      wago_simu   2 Channel Ktype Thermocouple Input
     o10v1         3.14013671875   wago_simu       2 Channel 4/20mA Output
```


Aalborg Mass Flow Controller

https://www.aalborg.com/

Supported Model:

* TD9704M

## Communication

RS232: 9600 bauds, bytesize: 8, No parity, 2 stopbits.

## Yaml configuration example

```
service:
- class: Aalborg
  plugin: generic
  name: flowctl
  serial:
    url: tango://id42/serial_aal/1
    baudrate: 9600
    bytesize: 8
    eol: "\r\n"
    parity: "N"
    stopbits: 2

  # Full-Scale Flow for 4 channels in l/min
  full_scale_flow: [50.0, 50.0, 2.0, 50.0]

  # K-factor for all 4 channels:
  # 1 means we use air or nitrogen, 1.454 for He<10L/min, 2.05 for He>10-50L/min??
  k_factor: [1, 1, 1, 1]

  # Gas density in g/l (= air/nitrogen)
  gas_density: [1.293, 0.1786, 0.1786, 0.1786]
```


# MAESTRIO controller

![MAESTRIO front panel](img/maestrio.png)

**MAESTRIO** stands for **M** ultipurpose **A** dvanced **E** lectronics for **S** equencing, **T** riggering and **R** econfigurable **I/O**. This board has been developped by ESRF Electronics Group to cover beamline needs for:

* sequencing and synchronization under the control of user-defined programs
* counting and integration of input pulses, analog inputs or absolute encoders
* data acquisition capabilities with internal storage
* user programmable logic (not in first release)
* interface to the accelerator timing system (white rabbit interface)

This board covers the functionnality of previous electronics : MUSST, OPIOM, P201, BCDU8.

Control and data acquisition communication is via a GbE interface.

## Input/Output signals

Following is a short description of maestrio's front panel connections:

* Inputs :

    * extended inputs **I1 - I4** : counting (max freq = 200MHz) or analog input (ADC 16-bit bipolar 5V)
    * standard inputs **I5 - I20** : pulse counting (max freq = 200MHz)

* Outputs :

    * extended outputs **O1 - O4** : logic level or analog output (DAC 16-bit unipolar 5V)
    * standard outputs **O5 - O8** : logic level

* Channels :

    * extended channels **C1 - C2** : encoder input, up/down counting or analog input (ADC 16-bit bipolar 10V, 5V, 2V or 1V)
    * standard channels **C3 - C6** : encoder input or up/down counting

* Triggers :

    * logic inputs : **DI1 - DI2**
    * logic outputs : **DO1 - DO2**

* General purpose I/O :

    * logic inputs **PI1 - PI8**
    * logic outputs **PO1 - PO8**
    * encoder output **EOUT**

![MAESTRIO connections](img/maestrio_connections.png)


## Configuration

Example of simple YAML configuration file:

```yaml

- name: maestrioeu6
  plugin: bliss
  class: Maestrio
  host: maestrioeu6
  channels:
  # --- input counters
  - channel: IN1
    counter_name: in1_cnt
  - channel: IN2
    counter_name: in2_cnt
  - channel: IN3
    counter_name: in3_cnt

  # --- encoder inputs
  - channel: CH1
    label: mot1
    steps_per_unit: 200
    offset: 0.

  - channel: CH2
    switch: $icebcu2_switch

  - channel: CH3
    counter_name: ch3_enc

```

For counters, mandatory configuration keys are:

- **channel** : define which input counter is used or input channel (ie encoder)
- **counter_name** : name of bliss counter for that input

The counter associated with an input will be either a bliss sampling or integrating counter. This depends on the maestrio configuration of the associated input. Once counters has been configured, we can use ct/scans on maestrio:

```python

FSCANTEST [10]: ct(.1, maestrioeu6)
ct: elapsed 0.190 s  (abort with Ctrl-c)
               in1_cnt  =        2           (      20.0000       /s)  maestrioeu6
               in2_cnt  =     1500           (   15000.0          /s)  maestrioeu6
               in3_cnt  =        0           (       0.00000      /s)  maestrioeu6
               ch3_enc  =        0           (       0.00000      /s)  maestrioeu6
      Out [10]: Scan(name=ct, path='not saved')

```

For encoder channels, following configuration keys can be used:

- **channel** : define which channel input is used (CH1 to CH6)
- **counter_name** : if encoder input has to be exported as a counter
- **label** : name to reference this channel. Usually the associated motor name is used.
- **switch** : if a switch is connected to the channel, give a reference to the bliss switch object (icepap or pi712 switch for example)

Additiionnal configuration related to encoder conversion can be added. Those configuration are presently not used in simple counting, but they are used by continuous scans when encoder has a different resolution than the associated motor. These parameters are:

- **steps_per_unit** : steps per unit of the input encoder
- **offset** : offset of the input encoder (this is not the motor offset)
- **modulo** : if the encoder works on a modulo base. For example, absolute encoder on an infinite rotation which delivers position in 360 range only.
- **sign** : sign of the encoder (may be opposite to linked motor direction)
- **unit** : unit string used for display


## usage

### global status

The default *info* string gives basic information about encoder channels, programs loaded and sequencer state:

```python

FSCANTEST [11]: maestrioeu6
      Out [11]: MAESTRIO board: MAESTRIO - 0.1
                TCP COMMAND:  host=maestrioeu6 port=5000

                CHANNELS:
                    C1 :   2.000000 - QUAD
                    C2 : 32160.000000 - QUAD
                    C3 :     UNUSED - UNUSED
                    C4 :     UNUSED - UNUSED
                    C5 :     UNUSED - UNUSED
                    C6 :   0.000000 - QUAD
                PROGRAMS UPLOADED:
                    FSCAN        : uploaded on 24/06/14 16:36:39
                    FTIMESCAN    : uploaded on 24/06/13 17:09:39
                SEQUENCERS:
                    SEQ1 :     STOP #ACQ       - RES: ALL
                    SEQ2 :     STOP UNKNOWNPROG - RES: NONE
                    SEQ3 :   NOPROG None       - RES: NONE
```

### channel access

Encoder channels can be accessed by channel number (1 to 6) or using the **label** name defined in configuration:

```python

FSCANTEST [12]: maestrioeu6.get_channel(1)
      Out [12]: Channel #1
                Config : CHCFG C1 QUAD INV UNIT 1
                         QUADCFG C1 X4
                Value  : 2.000000
                Encoder conversion :
                  steps_per_unit  = 200
                  offset          = 0.0


FSCANTEST [13]: maestrioeu6.get_channel_by_name("mot1")
      Out [13]: Channel #1
                Config : CHCFG C1 QUAD INV UNIT 1
                         QUADCFG C1 X4
                Value  : 2.000000
                Encoder conversion :
                  steps_per_unit  = 200
                  offset          = 0.0
```

To synchronize encoder to a given value, use the *sync()* method:

```
FSCANTEST [14]: c = maestrioeu6.get_channel_by_name("mot1")
FSCANTEST [15]: c.value
      Out [15]: '2.000000'
FSCANTEST [17]: c.sync(15)
FSCANTEST [18]: c.value
      Out [18]: '15.000000'
```

This method sets the encoder current value on incremental encoders and takes care of the smart accumulator configuration for both incremental and absolute encoder. This is needed if you want to use that channel as a target input in a program or just record it as data.

### program loading

On maestrio, this operation is done in 2 steps:

- upload the program to the board flash memory
- load the program on a sequencer to be able to execute it (compilation takes place at this stage)

Methods availables to upload programs are:
```python
mymaestrio.upload_program(progname, program_source, force=False)
mymaestrio.upload_program_file(progname, filename, force=False)
```
On upload, a checksum is computed. Unless, *force* flag is set to *True*, program is uploaded only if it has changed since last upload.

To load a program on a sequencer, use:
```python
mymaestrio.load_program(prog_name, seq_id=None, seq_res=None)
```
Parameters are:

- **prog_name** : program name which has been already uploaded
- **seq_id** : sequencer ID (1 to 3). If not specified, use sequencer #1 unless a default sequencer has been configured.
- **seq_res** : sequencer resources. If not specified, use *ALL* resources for that sequencer unless default resources has been configured.

To configure default sequencer for a program, a specific section can be added in configuration file:

```yaml
  program_defaults:
  - program_name: fscan
    sequencer_id: 1
    sequencer_resources: C1 C2
```

In that example, *fscan* program will by default be loaded on sequencer #1 and encoder input 1 and 2 resources attributed to it.

### program variables

Two kinds of parameters exists for maestrio programs:

- macro definition : they have to be set before loading program to the sequencer (needs compilation)
- program variables : variables can be set once the program has been loading

To access macro definition, use:
```python
FSCANTEST [25]: maestrioeu6.get_program_mac_values("fscan")
      Out [25]: {'OUT_ATRIG': 'OUT7', 'OUT_BTRIG': 'OUT8', 'OUT_CTRIG': 'OUT6', 'CH_MOT1': 'CH2', 'DATA_STORE': 'CH2'}
FSCANTEST [27]: maestrioeu6.set_program_mac_values("fscan", CH_MOT1="CH3", DATA_STORE="CH3")
```

To access program variables, use:
```python
FSCANTEST [32]: maestrioeu6.get_program_var_values("fscan")
      Out [32]: {'SCANMODE': 0, 'TRIGTIME': 0, 'POSGATE': 0, 'CUMERR': 0, 'RETCODE': 0, 'ACCTIME': 0, 'POSCORR': 0, 'GATEWIDTH': 100, 'POSDELTA': 0, 'GATEMODE': 0, 'IACC': 0, 'RETVAL': 0, 'NPULSES': 2, 'TRIGSTART': 200, 'POS_TG': 0, 'SCANDIR': 1, 'POSERR': 0, 'ACCNB': 0, 'NPOINTS': 0, 'TRIGDELTA': 150}
FSCANTEST [33]: maestrioeu6.set_program_var_values("fscan", NPOINTS=10, TRIGSTART=100)
```

### running a program

Typical loading workflow to execute a program is:
```python
mymaestrio.upload_program("myprog", myprog_source)
mymaestrio.set_program_mac_values("myprog", **mymacrodefs)
mymaestrio.load_program("myprog")
mymaestrio.set_program_var_values("myprog", **myvariables)
mymaestrio.run_program("myprog")
```

To get current program state, use:
```python
FSCANTEST [34]: maestrioeu6.get_program_state("fscan")
      Out [34]: <PROGRAM_STATE.STOP: 4>

FSCANTEST [36]: maestrioeu6.PROGRAM_STATE
      Out [36]: <enum 'PROGRAM_STATE'>
FSCANTEST [37]: dir(maestrioeu6.PROGRAM_STATE)
      Out [37]: ['BAD', 'BREAK', 'IDLE', 'NOPROG', 'RUN', 'STOP', 'UNLOAD', '__class__', '__doc__', '__members__', '__module__']
```

Stopping program execution is done via:
```python
FSCANTEST [38]: maestrioeu6.stop_program("fscan")
```

### raw commands

Sending raw commands to the board is possible as follow:
```python
FSCANTEST [40]: maestrioeu6.putget("?CHCFG C1")
      Out [40]: 'C1 QUAD INV UNIT 1'
```

### getting board configuration

The maestrio bliss object does not provide board configuration methods (can be done using putget only). Configuration can be complex and is usually performed by electronics group. Some methods are nevertheless provided to inspect those confoguration:

```python
FSCANTEST [45]: maestrioeu6.dump_config()
CHANNELS configuration :
------------------------
CHCFG C1 QUAD INV UNIT 1
QUADCFG C1 X4
CHCFG C2 QUAD NORMAL UNIT 1
QUADCFG C2 X4
CHCFG C3 UNUSED
CHCFG C4 UNUSED
CHCFG C5 UNUSED
CHCFG C6 QUAD NORMAL UNIT 1
QUADCFG C6 X4

INPUTS configuration :
----------------------
INCFG I1 NIM NORMAL NOFILT
INCFG I2 NIM NORMAL NOFILT
INCFG I3 NIM NORMAL NOFILT
INCFG I4 NIM NORMAL NOFILT
INCFG I5 NIM NORMAL NOFILT
INCFG I6 NIM NORMAL NOFILT
INCFG I7 NIM NORMAL NOFILT
INCFG I8 NIM NORMAL NOFILT
INCFG I9 NIM NORMAL NOFILT
INCFG I10 NIM NORMAL NOFILT
INCFG I11 NIM NORMAL NOFILT
INCFG I12 NIM NORMAL NOFILT
INCFG I13 NIM NORMAL NOFILT
INCFG I14 NIM NORMAL NOFILT
INCFG I15 NIM NORMAL NOFILT
INCFG I16 NIM NORMAL NOFILT
INCFG I17 NIM NORMAL NOFILT
INCFG I18 NIM NORMAL NOFILT
INCFG I19 NIM NORMAL NOFILT
INCFG I20 NIM NORMAL NOFILT

OUTPUTS configuration :
-----------------------
OUTCFG O1 LEVEL POS 5ms SRC OUT1
OUTCFG O2 LEVEL POS 5ms SRC OUT2
OUTCFG O3 LEVEL POS 5ms SRC OUT3
OUTCFG O4 LEVEL POS 5ms SRC OUT4
OUTCFG O5 LEVEL POS 5ms SRC OUT5
OUTCFG O6 PULSE POS 5us SRC OUT6
OUTCFG O7 PULSE POS 5us SRC OUT7
OUTCFG O8 LEVEL POS 200ns SRC OUT8

TRIGGER IN configuration :
--------------------------
DI1 TTL   NORMAL
DI2 TTL   NORMAL
```

Or use one of the individual type methods : *dump_channel_configuration()*, *dump_input_configuretion()*/...


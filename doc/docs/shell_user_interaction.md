# User interaction helpers

Some basic user interaction can be scripted with the `getval_` module.

It provides a set of functions to prompt user to enter interactively
various types of inputs: int, float, string, yes/no answer to a question etc.

## getval_yes_no

`getval_yes_no(message, default=True)`

```python
from bliss.shell.getval import getval_yes_no

if getval_yes_no("Do you want to open shutter ?", default="no"):
    print("ok, openning...")
else:
    print("abort...")
```

User is prevented to type other input than `yes` or `no` or `y` or `n`.

Executions example:

```
Do you want to open shutter ? [y/N]: bof
The input have to be on of [y]es or [n]o
```

On Enter:

```
Do you want to open shutter ? [y/N]:
abort...
```

On Ctrl-C:

```
Do you want to open shutter ? [y/N]:
!!! === KeyboardInterrupt:  === !!! ( for more details type cmd 'last_error()' )
```

```
Do you want to open shutter ? [y/N]: y
ok, openning...
```

## getval_name

`getval_name` prompt for a string without special char or spaces.

```
DEMO [8]: getval.getval_name("Enter a valid name")
Enter a valid name : fdsf
```


## getval_int_range

`getval_int_range(message, minimum, maximum, default)`:
   Prompt user for an int number in interval [min, max]


```python
from bliss.shell.getval import getval_int_range
getval_int_range("Please enter beamline number", minimum=1, maximum=32, default=1)
```
execution:
```
Please enter beamline number [1]: f
Error: f is not a valid integer
Please enter beamline number [1]: 99
Error: 99 is not in the valid range of 1 to 32.
Please enter beamline number [1]: 5
Out[2]: 5
```

## getval_idx_list

`getval_idx_list()`: Return the index and string chosen by user in list of N strings.
    Selection is done by index in [1..N].

Example:

```python
from bliss.shell.getval import getval_idx_list

dspacing_list = ["111", "311", "642"]
print(getval_idx_list(dspacing_list,"Choose the index of dspacing you want to use"))
```

Execution of previous example:

```
1 - 111
2 - 311
3 - 642
Choose the index of dspacing you want to use [1]: 2
311
```

## getval_char_list


```python
from bliss.shell.getval import getval_char_list

actions_list= [("a", "add a roi"), ("r", "remove a roi"), ("m", "modify a roi")]
getval_char_list(actions_list, "Action to do")
```

Execution of previous example:
```
a - add a roi
r - remove a roi
m - modify a roi
Action to do (a, r, m):a
  Out [11]: ('a', 'add a roi')
```

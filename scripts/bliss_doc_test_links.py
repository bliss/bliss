"""
Ugly script to test links of BLISS documentation
"""

import urllib.request

filename = "bliss_doc_links.txt"
# filename = "bliss_doc_links_short.txt"

with open(filename, mode="r") as links_file:
    for link in links_file.readlines():
        try:
            urllib.request.urlopen(link).getcode()
            print(f" OK     url: {link.strip()}")
        except urllib.error.HTTPError:
            print(f" ERROR  url: {link.strip()}")
        except Exception:
            print(f" ERROR     url: {link.strip()}")


"""
[(bliss) guilloud@pcsht]$ python ./bliss_doc_test_links.py | grep ERROR
 ERROR  url: https://black.readthedocs.io/en/stable/editor_integration.html
 ERROR     url: http://silx.org
 ERROR     url: https://www.embl.fr/instrumentation/cipriani/downloads/md2_pdf.pdf
 ERROR  url: https://www.esrf.eu/UsersAndScience/Experiments/MX/How_to_use_our_beamlines/Trouble_Shooting/id29-microdiffractometer-troubleshooting
 ERROR  url: https://nhnent.github.io/tui.editor/api/latest/tutorial-example01-basic.html
 ERROR  url: https://squidfunk.github.io/mkdocs-material/extensions/admonition/
 ERROR  url: http://support.xia.com/default.asp?W381
 ERROR     url: http://silx.org
 ERROR     url: https://countrymeters.info/en/World
 ERROR  url: https://financialmodelingprep.com/api/v3/forex
 ERROR  url: http://support.xia.com/default.asp?W882
 ERROR  url: https://bliss.gitlab-pages.esrf.fr/bliss/master/flint_scan_info.html
"""

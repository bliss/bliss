import numpy
import time
import h5py

try:
    import XGL_DPP as xgl
except ImportError:
    xgl = None


class XGLabDante(object):

    InputModes = ["DC_RinHighImp", "DC_RinLowImp", "AC_Slow", "AC_Fast"]
    TrigModes = [
        "FreeRunning",
        "TriggerRising",
        "TriggerFalling",
        "TriggerBoth",
        "GatedHigh",
        "GatedLow",
    ]

    def __init__(self, ipaddress):
        self.__master_id = None
        self.__spectrum_size = 4096

        # --- init library
        xgl.pyInitLibrary()

        # --- discover devices
        ret = xgl.pyadd_to_query(ipaddress)
        if ret is False:
            raise RuntimeError(f"failed to add IP [{ipaddress}]")

        xgl.pyautoScanSlaves(True)
        time.sleep(2.0)

        idx = 0
        while idx < 10:
            (ret, ndev) = xgl.pyget_dev_number()
            if ret is False:
                raise RuntimeError("failed to get device number")
            if ndev > 0:
                break
            idx += 1
            time.sleep(1.0)

        if ndev == 0:
            raise RuntimeError("could not found any device")
        if ndev != 1:
            raise RuntimeError("can manage only one master device !!")

        (ret, master_id) = xgl.pyget_ids(0, 64)
        if ret is False:
            raise RuntimeError("failed to get board ID")
        self.__master_id = master_id

        print(f"\nConnected to [{master_id}]")

        while idx < 10:
            (ret, nboards) = xgl.pyget_boards_in_chain(self.__master_id)
            if ret is False:
                raise RuntimeError("failed to get number of boards")
            if nboards > 0:
                break
            idx += 1
            time.sleep(1.0)

        if nboards == 0:
            raise RuntimeError("No boards found !!")

        self.__nboards = nboards

        print(f"{nboards} boards in chain.")

        xgl.pyautoScanSlaves(False)

        # --- connect callback
        self.__answers = dict()
        ret = xgl.pyregister_callback(self._xgl_callback)
        if ret is False:
            raise RuntimeError("failed to register callback")

    def close(self):
        xgl.pyCloseLibrary()

    # ---
    # communication callback / errors
    # ---

    def _xgl_callback(self, call_type, call_id, length, data):
        self.__answers[call_id] = (call_type, data)

    def _xgl_wait_reply(self, call_id, cmdmsg, wait=0.1):
        if call_id == 0:
            errcode = self._xgl_last_error()
            raise RuntimeError(f"{cmdmsg} calling failed [err={errcode}]")
        while True:
            if call_id in self.__answers:
                (call_type, data) = self.__answers.pop(call_id)
                if call_type == 0:
                    errcode = self._xgl_last_error()
                    raise RuntimeError(f"{cmdmsg} reply error [err={errcode}]")
                elif call_type == 2:
                    if data[0] != 1:
                        errcode = self._xgl_last_error()
                        raise RuntimeError(f"{cmdmsg} command failed [err={errcode}]")
                return data
            time.sleep(wait)

    def _xgl_last_error(self):
        (ret, err) = xgl.pygetLastError()
        if ret:
            return err
        return 0

    # ---
    # configuration
    # ---
    def _read_configuration_file(self, filename):
        hfile = h5py.File(filename, "r")
        nchan = len(hfile["ChannelID"])
        hconf = hfile["Configuration"]

        # --- input mode
        input_config = [self.InputModes[int(val)] for val in hconf["InputMode"]]

        # --- input offset
        offset_config = list()
        for val1, val2 in zip(hconf["Input_Offset1"], hconf["Input_Offset2"]):
            offsets = xgl.configuration_offset()
            offsets.offset_val1 = int(val1)
            offsets.offset_val2 = int(val2)
            offset_config.append(offsets)

        # --- dpp parameters
        MapConf2HdfName = [
            ("base_offset", "Exponential_Offset", int),
            ("baseline_samples", "Baseline_Samples", int),
            ("edge_flat_top", "FastFilter_FlatTop", int),
            ("edge_peaking_time", "FastFilter_PeakTime", int),
            ("energy_filter_thr", "EnergyFilter_Th", int),
            ("fast_filter_thr", "FastFilter_Th", int),
            ("flat_top", "EnergyFilter_FlatTop", int),
            ("gain", "Gain", float),
            ("inverted_input", "InputInverted", int),
            ("max_peaking_time", "EnergyFilter_MaxPeakTime", int),
            ("max_risetime", "MaxRiseTime", int),
            ("other_param", None, None),
            ("overflow_recovery", None, None),
            ("peaking_time", "EnergyFilter_PeakTime", int),
            ("reset_recovery_time", "Recovery_Time", int),
            ("reset_threshold", "Reset_Th", int),
            ("tail_coefficient", None, None),
            ("time_constant", "Exponential_TimeConstant", float),
            ("zero_peak_freq", "ZeroPeakRate", float),
        ]

        dpp_config = list()
        for idx in range(nchan):
            chanconf = xgl.configuration()
            for (cname, hname, conv) in MapConf2HdfName:
                if hname is not None:
                    value = conv(hconf[hname][idx])
                    setattr(chanconf, cname, value)
            dpp_config.append(chanconf)

        return nchan, input_config, offset_config, dpp_config

    def load_configuration(self, filename):
        # --- read config file
        (nchan, modes, offsets, dpps) = self._read_configuration_file(filename)
        if nchan != self.__nboards:
            raise ValueError(f"File contains config for only {nchan} boards")

        print(f"Loading configuration [{filename}] ...")

        # --- set input config
        print("Configure input mode")
        for idx, mode in enumerate(modes):
            call_id = xgl.pyconfigure_inputmode(self.__master_id, idx, mode)
            self._xgl_wait_reply(call_id, f"configure input on board #{idx}")

        # --- set input offset
        print("Configure input offset")
        for idx, offset in enumerate(offsets):
            call_id = xgl.pyconfigure_offset(self.__master_id, idx, offset)
            self._xgl_wait_reply(call_id, f"configure offset on board #{idx}")

        # --- set dpp config
        for idx, chanconf in enumerate(dpps):
            print(f"configure dpp on board #{idx}")
            call_id = xgl.pyconfigure(self.__master_id, idx, chanconf)
            self._xgl_wait_reply(call_id, f"configure dpp on board #{idx}")

        print("All boards configured.")

    # ---
    # acquisition
    # ---
    def set_trigger_mode(self, mode):
        if mode not in self.TrigModes:
            raise ValueError("Invalid trigger mode")
        # call_id = xgl.pyconfigure_gating(self.__master_id, mode, 0xff)
        # data = self._xgl_wait_reply(call_id, f"set trigger mode")
        for idx in range(self.__nboards):
            call_id = xgl.pyconfigure_gating(self.__master_id, mode, idx)
            self._xgl_wait_reply(call_id, "set trigger mode")
        print(f"gating set to {mode}")

    def single_acquisition(self, expo_time):
        self.set_trigger_mode("FreeRunning")

        # --- start acquisition
        call_id = xgl.pystart(self.__master_id, expo_time, self.__spectrum_size)
        data = self._xgl_wait_reply(call_id, "start acquisition")

        print("single acquisition started")

        # --- wait end of acquisition
        time.sleep(0.9 * expo_time)
        received = [False] * self.__nboards
        while not all(received):
            for idx in range(self.__nboards):
                if received[idx] is False:
                    (ret, last) = xgl.pyisLastDataReceived(self.__master_id, idx)
                    if ret and last:
                        received[idx] = True
            print("received", received)
            time.sleep(0.05)

        print("acquisition finished")

        # --- read data
        spectra = {}
        statistics = {}
        for idx in range(self.__nboards):
            (ret, data, idacq, stat) = xgl.pygetData(
                self.__master_id, idx, self.__spectrum_size
            )
            if ret is False:
                raise RuntimeError(f"Failed to read data for board #{idx}")
            spectra[idx] = numpy.array(data, dtype=numpy.uint64)
            spectra[idx] = spectra[idx][0 : self.__spectrum_size]
            statistics[idx] = {
                "realtime": stat.real_time / 1.0e6,
                "livetime": stat.live_time / 1.0e6,
                "ICR": stat.ICR / 1000.0,
                "OCR": stat.OCR / 1000.0,
            }
            total = spectra[idx].sum()
            real = statistics[idx]["realtime"]
            live = statistics[idx]["livetime"]
            print(f"Board #{idx} > real {real} live {live} total {total}")

        print("all data read")
        self.stop_acquisition()

    def map_acquisition(self, mode, npoints, expotime):
        self.set_trigger_mode(mode)
        call_id = xgl.pystart_map(
            self.__master_id, int(1000.0 * expotime), npoints, self.__spectrum_size
        )
        self._xgl_wait_reply(call_id, "start acquisition")
        print(f"map acquisition started for {npoints} points")

        self.poll_data()
        self.stop_acquisition()

    def stop_acquisition(self):
        call_id = xgl.pystop(self.__master_id)
        self._xgl_wait_reply(call_id, "start acquisition")
        print("acquisition stopped")

    def poll_data(self):
        finished = [False] * self.__nboards
        npoint_read = [0] * self.__nboards
        read_spectras = dict()
        read_stats = dict()

        while not all(finished):
            for idx in range(self.__nboards):
                if finished[idx] is False:
                    (ret, last) = xgl.pyisLastDataReceived(self.__master_id, idx)
                    if ret and last:
                        finished[idx] = True
            print(f"finished {finished}")

            for idx in range(self.__nboards):
                (ret, available) = xgl.pygetAvailableData(self.__master_id, idx)
                if not ret or not available:
                    continue
                print(f"board #{idx} available = {available}")
                (ret, spectra, spectraID, stats, adv_stats) = xgl.pygetAllData(
                    self.__master_id, idx, self.__spectrum_size, available
                )
                # -- unpack data
                ipoint = npoint_read[idx]
                for iread in range(available):
                    point_spectras = read_spectras.setdefault(ipoint + iread, {})
                    point_spectras[idx] = numpy.array(
                        spectra[iread * 4096 : iread * 4096 + 4096]
                    )

                    point_stats = read_stats.setdefault(ipoint + iread, {})
                    point_stats[idx] = {
                        "realtime": stats[iread * 4] / 1e6,
                        "livetime": stats[iread * 4 + 1] / 1e6,
                        "ICR": stats[iread * 4 + 2] / 1000.0,
                        "OCR": stats[iread * 4 + 3] / 1000.0,
                    }
                    total = point_spectras[idx].sum()
                    real = point_stats[idx]["realtime"]
                    live = point_stats[idx]["livetime"]
                    print(
                        f"Board #{idx} > ID {spectraID[iread]} real {real} live {live} total {total}"
                    )

                npoint_read[idx] += available
                print(f"Board #{idx} > total = {npoint_read[idx]} spectrum read")

            time.sleep(0.2)


if __name__ == "__main__":
    mca = XGLabDante("192.168.1.120")
    mca.load_configuration("./config.hdf")

    mca.single_acquisition(1.5)
    # mca.map_acquisition("FreeRunning", 10, 0.1)
    mca.map_acquisition("GatedHigh", 10, 0.2)
    # mca.map_acquisition("TriggerRising", 10, .5)

    mca.close()

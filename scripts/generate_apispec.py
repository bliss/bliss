import json
from bliss.rest_service.rest_service import RestService


rest_service = RestService(None)
app, socketio = rest_service._create_server()

with open("openapi.json", "w", encoding="utf-8") as file:
    json.dump(app.api_doc, file, indent=4)

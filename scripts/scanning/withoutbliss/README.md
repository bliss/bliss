# Bliss data: publish, subscribe, plot and save

The Bliss acquisition control system comes with independent python projects
that can be used without installing `bliss` itself:

 * `blissdata`: library to publish and subscribe to scan data in Redis
 * `blisswriter`: subscriber that saves data in NeXus-compliant HDF5 files
 * `flint`: subscriber that plots data in a Qt application

These are python packages available on `pypi` and can be installed in
any python environment >=3.8 and <3.10

The way python environments are organized in the instructions is just
an example and can be adapt to your needs. Here we assume the general
case where each process could be running on a different machine.

## Example scripts

The example scripts for data publishing, plotting and saving are currently
in the bliss repository

```
git clone https://gitlab.esrf.fr/bliss/bliss.git
```

## Redis server

Install Redis using in a conda environment

```
mamba create --name redis_env -c esrf-bcu -c conda-forge --yes "redis-server>=7" "redisearch>=2.6.5" "redisjson>=2.4.5" blissdata
```

See [here](https://gitlab.esrf.fr/dau/blissdata/-/tree/main/images) if you want a system installation instead.

In case you need to install `mamba` itself

```
curl -L -O "https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-$(uname)-$(uname -m).sh"
bash Miniforge3-$(uname)-$(uname -m).sh -b -p $HOME/miniforge3

source $HOME/miniforge3/etc/profile.d/conda.sh
conda config --add envs_dirs $HOME/virtualenvs/
```

With this particaly mamba installation, environments can be created in `$HOME/virtualenvs/`

```
source $HOME/miniforge3/etc/profile.d/conda.sh
source $HOME/miniforge3/etc/profile.d/mamba.sh

mamba create --name redis_env ...
```

Start the Redis server with the `blissdata` memory tracker

```
source $HOME/miniforge3/etc/profile.d/conda.sh
source $HOME/miniforge3/etc/profile.d/mamba.sh
mamba activate redis_env

./bliss/scripts/scanning/withoutbliss/startredis.sh
```

## Data Plotting

Install Flint

```
python -m venv $HOME/virtualenvs/flint_env
source $HOME/virtualenvs/flint_env/bin/activate

pip install bliss  # for now Flint is part of `bliss`
```

Start Flint

```
source $HOME/virtualenvs/flint_env/bin/activate

./bliss/scripts/scanning/withoutbliss/startflint.sh
```

![Data Plotting](./flint.png)

## Data Saving

Install the NeXus writer

```
python -m venv $HOME/virtualenvs/writer_env
source $HOME/virtualenvs/writer_env/bin/activate

pip install blisswriter
```

Start the NeXus writer

```
source $HOME/virtualenvs/writer_env/bin/activate

python ./bliss/scripts/scanning/withoutbliss/startwriter.py
```

![Data Saving](./blisswriter.png)

## Custom Subscriber

Data plotting and saving are two applications of Bliss data subscribers.
Custom data subscribers can be created with the `blissdata` python API.

Install data client dependencies

```
python -m venv $HOME/virtualenvs/monitor_env
source $HOME/virtualenvs/monitor_env/bin/activate

pip install blissdata
```

Start a custom client that prints data statistics

```
source $HOME/virtualenvs/monitor_env/bin/activate

python ./bliss/scripts/scanning/withoutbliss/startmonitor.py
```

The output looks like this

```
loopscan 250 0.02 (#1)
 File URL: silx:///tmp/RAW_DATA/collection/collection_0001/collection_0001.h5?path=1.1
 Scan duration: 0:00:05
 Channel           Dim        Shape    Dtype   Points   Volume Data Rate Scan Rate
 --------------- ----- ------------ -------- -------- -------- --------- ---------
 epoch               0           ()  float64      250    8.0B  381.6B /s 400.0B /s
 external_images     2  (1024, 512)   uint32      250    2.0MB  95.3MB/s 100.0MB/s
 images              2 (1024, 1024)   uint16      250    2.0MB  95.3MB/s 100.0MB/s
 scalars             0           ()  float64      250    8.0B  381.6B /s 400.0B /s
 vectors             1      (4096,)    int32      250   16.0KB 763.3KB/s 800.0KB/s
Scan ended with SUCCESS in 0:00:05.281415

loopscan 250 0.02 (#2)
 File URL: silx:///tmp/RAW_DATA/collection/collection_0001/collection_0001.h5?path=2.1
 Scan duration: 0:00:05
 Channel           Dim        Shape    Dtype   Points   Volume Data Rate Scan Rate
 --------------- ----- ------------ -------- -------- -------- --------- ---------
 epoch               0           ()  float64      250    8.0B  381.8B /s 400.0B /s
 external_images     2  (1024, 512)   uint32      250    2.0MB  95.3MB/s 100.0MB/s
 images              2 (1024, 1024)   uint16      250    2.0MB  95.3MB/s 100.0MB/s
 scalars             0           ()  float64      250    8.0B  381.8B /s 400.0B /s
 vectors             1      (4096,)    int32      250   16.0KB 763.5KB/s 800.0KB/s
Scan ended with SUCCESS in 0:00:05.261558

loopscan 250 0.02 (#3)
 File URL: silx:///tmp/RAW_DATA/collection/collection_0001/collection_0001.h5?path=3.1
 Scan duration: 0:00:05
 Channel           Dim        Shape    Dtype   Points   Volume Data Rate Scan Rate
 --------------- ----- ------------ -------- -------- -------- --------- ---------
 epoch               0           ()  float64      129    8.0B  380.8B /s 400.0B /s
 external_images     2  (1024, 512)   uint32      129    2.0MB  94.9MB/s 100.0MB/s
 images              2 (1024, 1024)   uint16      129    2.0MB  95.0MB/s 100.0MB/s
 scalars             0           ()  float64      129    8.0B  380.9B /s 400.0B /s
 vectors             1      (4096,)    int32      129   16.0KB 761.7KB/s 800.0KB/s
Scan ended with USER_ABORT in 0:00:02.717543
```

For all channels, the data is streamed through Redis. Except for the `external_images`
channels for which image references are streamed through Redis and the camera server
is doing the saving.

The rates from `images` and `external_images` channels are identical which means neither
`blissdata` nor Redis are a limiting factor for the data rate that can be handled. The
memory available to the Redis server in combination with the memory cleanup rate put an 
upper limit on the data rate (both are configurable).

## Data Publishing

Install data publishing dependencies

```
python -m venv $HOME/virtualenvs/scan_env
source $HOME/virtualenvs/scan_env/bin/activate

pip install blissdata
```

Run scans and see the data plotted in `Flint`
and saved in HDF5 in `/tmp/RAW_DATA`

```
source $HOME/virtualenvs/scan_env/bin/activate

python ./bliss/scripts/scanning/withoutbliss/runscans.py [--nscans 10] [--saving]
```

The `--save` option saves images for the `external_images` channel. Other options
are available for the scan time and count time per point.

The output looks like this

```
loopscan 250 0.02 (#1)
 File URL: silx:///tmp/RAW_DATA/collection/collection_0001/collection_0001.h5?path=1.1
 Scan duration: 0:00:05
 Data Volume  = 1003.9MB, Data Rate  = 200.8MB/s
 Redis Volume = 503.9MB, Redis Rate = 100.8MB/s
 Scan point 250
Scan ended with SUCCESS in 0:00:05.281415

loopscan 250 0.02 (#2)
 File URL: silx:///tmp/RAW_DATA/collection/collection_0001/collection_0001.h5?path=2.1
 Scan duration: 0:00:05
 Data Volume  = 1003.9MB, Data Rate  = 200.8MB/s
 Redis Volume = 503.9MB, Redis Rate = 100.8MB/s
 Scan point 250
Scan ended with SUCCESS in 0:00:05.261558

loopscan 250 0.02 (#3)
 File URL: silx:///tmp/RAW_DATA/collection/collection_0001/collection_0001.h5?path=3.1
 Scan duration: 0:00:05
 Data Volume  = 1003.9MB, Data Rate  = 200.8MB/s
 Redis Volume = 503.9MB, Redis Rate = 100.8MB/s
 Scan point 130^C
Scan ended with USER_ABORT in 0:00:02.717543
```

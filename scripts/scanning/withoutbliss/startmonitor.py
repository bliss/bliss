#!/usr/bin/env python
import math
import time
import logging
from datetime import datetime, timedelta
from collections.abc import Generator

import numpy

from blissdata.redis_engine.scan import Scan
from blissdata.redis_engine.scan import ScanState
from blissdata.redis_engine.store import DataStore
from blissdata.redis_engine.exceptions import NoScanAvailable
from blissdata.redis_engine.exceptions import EndOfStream
from blissdata.streams.base import Stream, BaseView, CursorGroup


YIELD_PERIOD = 1


def main(redis_url: str) -> None:
    data_store = DataStore(redis_url)
    for scan in _iterate_new_scans(data_store, "example_session"):

        # Wait until all scan start metadata has been published
        while scan.state < ScanState.PREPARED:
            scan.update(timeout=YIELD_PERIOD)
        _print_scan_info(scan.info)

        # Monitor the scan (could be done in a different thread or process)
        # Here we simply print the number of data points recieved per channel
        _process_scan_data(scan)

        # Wait until all scan end metadata has been published
        while scan.state < ScanState.CLOSED:
            scan.update(timeout=YIELD_PERIOD)
        _print_finished(scan.info)


def _iterate_new_scans(
    data_store: DataStore, session: str
) -> Generator[Scan, None, None]:
    """Yield new scans of a session indefinitely"""
    since = data_store.get_last_scan_timetag()
    while True:
        try:
            since, key = data_store.get_next_scan(since=since, timeout=YIELD_PERIOD)
            scan = data_store.load_scan(key)
            if scan.session == session:
                yield scan
        except NoScanAvailable:
            pass


def _process_scan_data(scan: Scan) -> None:
    """Handle scan data: print statistics in this example"""
    cursor_group = CursorGroup(scan.streams)
    nlast_lines = 0
    start_time = time.time()
    count_time = scan.info["count_time"]
    channels = dict()
    while True:
        try:
            views = cursor_group.read(timeout=YIELD_PERIOD)
        except EndOfStream:
            break
        seconds_since_start = time.time() - start_time
        channels.update(_extract_channel_info(views, seconds_since_start, count_time))
        nlast_lines = _print_scan_progress(channels, nlast_lines)


def _extract_channel_info(
    views: dict[Stream, BaseView],
    seconds_since_start: float,
    count_time: float,
) -> dict[str, dict[str, str]]:
    """Extract channel data statistics"""
    channels = dict()
    for stream, view in views.items():
        if stream.kind == "array":
            # 'array' streams all have dtype and shape in info
            dtype = numpy.dtype(stream.info["dtype"])
            shape = stream.info["shape"]
            ndim = len(shape)
            npoints = view.index + len(view)
        else:
            raise ValueError(f"unknown stream kind '{stream.kind}'")

        point_byte_size = dtype.itemsize
        if shape:
            point_byte_size *= numpy.prod(shape, dtype=int)

        transferred_volume_per_sec = point_byte_size * npoints / seconds_since_start

        scan_volume_per_sec = point_byte_size / count_time

        chaninfo = {
            "Channel": stream.name,
            "Dim": str(ndim),
            "Shape": str(shape),
            "Dtype": str(dtype),
            "Points": str(npoints),
            "Volume": _pretty_print_byte_size(point_byte_size),
            "Data Rate": _pretty_print_byte_size(transferred_volume_per_sec) + "/s",
            "Scan Rate": _pretty_print_byte_size(scan_volume_per_sec) + "/s",
        }

        channels[stream.name] = chaninfo
    return channels


def _print_scan_info(info: dict) -> None:
    title = info["title"]
    filename = info["filename"]
    npoints = info["npoints"]
    count_time = info["count_time"]
    scan_number = info["scan_nb"]

    print(f"\n{title} (#{scan_number})")
    print(f" File URL: silx://{filename}?path={scan_number}.1")
    duration = timedelta(seconds=npoints * count_time)
    print(f" Scan duration: {duration}")


def _print_finished(info: dict) -> None:
    end_reason = info.get("end_reason")
    if end_reason:
        start_time = datetime.fromisoformat(info["start_time"])
        end_time = datetime.fromisoformat(info["end_time"])
        print("Scan ended with", end_reason, "in", end_time - start_time)
    else:
        print("Scan did not start")


def _pretty_print_byte_size(byte_size: int) -> str:
    if byte_size == 0:
        return "0B "
    size_name = ("B ", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
    i = int(math.floor(math.log(byte_size, 1024)))
    value = byte_size / math.pow(1024, i)
    unit = size_name[i]
    return f"{value:5.1f}{unit}"


_COLUMN_FMT = {
    "Channel": "{:<15}",
    "Dim": "{:>5}",
    "Shape": "{:>12}",
    "Dtype": "{:>8}",
    "Points": "{:>8}",
    "Volume": "{:>8}",
    "Data Rate": "{:>8}",
    "Scan Rate": "{:>8}",
}


def _print_scan_progress(channels: dict[str, dict[str, str]], nlast_lines: int) -> int:
    if not channels:
        return nlast_lines

    up = f"\x1B[{nlast_lines}A"
    down = "\x1B[0K"
    sep = " "

    lines = [
        sep
        + sep.join([fmt.format(row[colname]) for colname, fmt in _COLUMN_FMT.items()])
        for _, row in sorted(channels.items())
    ]

    ncurrent_lines = len(lines)

    if nlast_lines == 0:
        header = [fmt.format(colname) for colname, fmt in _COLUMN_FMT.items()]
        separator = ["-" * len(s) for s in header]
        print(sep + sep.join(header))
        print(sep + sep.join(separator))

        print("\n" * (ncurrent_lines - 1))
        up = f"\x1B[{ncurrent_lines}A"
    elif nlast_lines > ncurrent_lines:
        print(f"{up}{down}")
        for _ in range(1, nlast_lines):
            print(down)

    print(f"{up}{lines[0]}{down}")
    for r in range(1, ncurrent_lines):
        print(f"{lines[r]}{down}")

    return ncurrent_lines


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Start Data Monitor")

    parser.add_argument(
        "--redis", default="redis://localhost:6379", type=str, help="Redis URL"
    )

    parser.add_argument(
        "--log",
        default="INFO",
        type=str.upper,
        dest="level",
        help="Log level",
        choices=["CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG", "NOTSET"],
    )

    args = parser.parse_args()
    logging.basicConfig(level=args.level)

    main(args.redis)

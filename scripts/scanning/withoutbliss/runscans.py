#!/usr/bin/env python

import os
import math
import logging
import typing
import time
import queue
import threading
import multiprocessing
from typing import Optional
from contextlib import ExitStack
from datetime import datetime, timedelta

import h5py
import numpy


from blissdata.redis_engine.scan import Scan
from blissdata.redis_engine.scan import ScanState
from blissdata.redis_engine.store import DataStore
from blissdata.schemas.scan_info import ScanInfoDict
from blissdata.streams.base import Stream
from blissdata.streams.lima import LimaStream


def main(
    redis_url: str,
    nscans: int,
    count_time: float,
    scan_time: float,
    saving: bool = True,
) -> None:
    # Configure blissdata for a Redis database
    data_store = DataStore(redis_url)

    # Get the number of the last scan
    try:
        _, scan_key = data_store.get_last_scan()
    except Exception:
        last_scan_number = 0
    else:
        scan = data_store.load_scan(scan_key)
        last_scan_number = scan.number

    # Run new scans
    for i in range(nscans):
        scan_number = last_scan_number + i + 1
        run_scan(data_store, scan_number, count_time, scan_time, saving=saving)


def run_scan(
    data_store: DataStore,
    scan_number: int,
    count_time: float,
    scan_time: float,
    saving: bool = True,
) -> None:
    scan_name = "loopscan"
    npoints = max(int(scan_time / count_time), 1)

    # The identity model we use is ESRFIdentityModel in blissdata.redis_engine.models
    # It defines the json fields indexed by RediSearch
    scan_id = {
        "name": scan_name,
        "number": scan_number,
        "data_policy": "no_policy",
        "session": "example_session",
    }

    # Create scan in the database (name is required on creation for the writer)
    scan = data_store.create_scan(scan_id, info={"name": f"{scan_number}_{scan_name}"})

    try:
        streams = start_scan(scan, scan_name, npoints, count_time)
        publish_data(npoints, count_time, **streams, saving=saving)
        finish_scan(scan)
    except BaseException as error:
        cancel_scan(scan, error=error)
        raise
    finally:
        _print_finished(scan.info)


def start_scan(scan, scan_name: str, npoints: int, count_time: float):
    info = scan_info(scan, scan_name, npoints, count_time)
    _print_scan_info(info)

    streams = prepare_streams(scan, info["images_path"])
    _print_stream_info(streams, info)

    # gather some metadata before running the scan
    scan.info.update(info)

    # upload initial metadata and stream declarations
    scan.prepare()

    # Scan is ready to start, eventually wait for other processes.
    # Sharing stream keys to external publishers can be done there.
    scan.start()

    return streams


def validate_scan_info(scan_info: dict) -> ScanInfoDict:
    """Copy past of validator code from bliss.scanning.scan_info"""
    try:
        from pydantic import BaseModel, ConfigDict
    except ImportError:
        print("pydantic not available. Scan info validation skipped")
        return scan_info

    class Holder(BaseModel):
        model_config = ConfigDict(arbitrary_types_allowed=True)
        scan_info: ScanInfoDict

    valid_scan_info = typing.cast(ScanInfoDict, scan_info)
    Holder(scan_info=valid_scan_info)  # This trig the validation
    return valid_scan_info


def scan_info(scan, scan_name: str, npoints: int, count_time: float):
    # Files like at the ESRF (not required to do it like this)
    filename, masterfiles, images_path = file_info(scan.number)

    scan_info = {
        ##################################
        # Scan metadata
        ##################################
        "name": scan.name,
        "scan_nb": scan.number,
        "session_name": scan.session,
        "data_policy": scan.data_policy,
        "start_time": datetime.now().astimezone().isoformat(),
        "title": f"{scan_name} {npoints} {count_time}",
        "type": f"{scan_name}",
        "npoints": npoints,
        "count_time": count_time,
        ##################################
        # Device information
        ##################################
        "acquisition_chain": {
            "timer": {
                "top_master": "timer",
                # Should not be anymore needed, but expected by the schema
                "devices": ["timer", "scalars", "vectors", "images", "external_images"],
                # Should not be anymore needed, but expected by the schema
                "scalars": [],
                # Should not be anymore needed, but expected by the schema
                "spectra": [],
                # Should not be anymore needed, but expected by the schema
                "images": [],
                # Should not be anymore needed, but expected by the schema
                "master": {},
            },
        },
        "devices": {
            "timer": {
                "name": "timer",
                "channels": ["epoch"],
                "triggered_devices": [
                    "scalars",
                    "vectors",
                    "images",
                    "external_images",
                ],
            },
            "scalars": {"name": "scalars", "channels": ["scalars"]},
            "vectors": {"name": "vectors", "channels": ["vectors"]},
            "images": {"name": "images", "channels": ["images"]},
            "external_images": {
                "name": "external_images",
                "channels": ["external_images"],
            },
        },
        "channels": {
            "epoch": {"dim": 0},
            "scalars": {"dim": 0},
            "vectors": {"dim": 1},
            "images": {"dim": 2},
            "external_images": {"dim": 2},
        },
        ##################################
        # Plot metadata
        ##################################
        "display_extra": {"plotselect": [], "displayed_channels": []},
        "plots": [],
        ##################################
        # NeXus writer metadata
        ##################################
        "save": True,
        "filename": filename,
        "images_path": images_path,
        "publisher": "test",
        "publisher_version": "1.0",
        "data_writer": "external",
        "writer_options": {"chunk_options": {}, "separate_scan_files": False},
        "scan_meta_categories": [
            "positioners",
            "nexuswriter",
            "instrument",
            "technique",
        ],
        "nexuswriter": {
            "devices": {},
            "instrument_info": {"name": "esrf-id00a", "name@short_name": "id00"},
            "masterfiles": masterfiles,
            "technique": {},
        },
        "positioners": {
            "positioners_dial_start": {"robx": 0.0, "roby": 0.0, "robz": 0.0},
            "positioners_start": {"robx": 0.0, "roby": 0.0, "robz": 0.0},
            "positioners_units": {"robx": None, "roby": None, "robz": "mm"},
        },
        "instrument": {
            "@NX_class": "NXinstrument",
            "att1": {"@NX_class": "NXattenuator", "status": "in", "type": "Al"},
            "machine": {
                "@NX_class": "NXsource",
                "automatic_mode": True,
                "current": 67.0,
                "current@units": "mA",
                "filling_mode": "7/8 multibunch",
                "front_end": "",
                "message": "You are in Simulated Machine",
                "mode": "USM",
                "name": "ESRF",
                "refill_countdown": 84.0,
                "refill_countdown@units": "s",
                "type": "Synchrotron",
            },
            "primary_slit": {
                "@NX_class": "NXslit",
                "horizontal_gap": 10.0,
                "horizontal_offset": 0.0,
                "vertical_gap": 20.0,
                "vertical_offset": 0.0,
            },
        },
        ##################################
        # Mandatory by the schema
        ##################################
        "user_name": os.getlogin(),
    }
    # Add a default curve plot
    scan_info["plots"].append(
        {"kind": "curve-plot", "items": [{"kind": "curve", "x": "epoch"}]}
    )
    # Add NXdata plot
    scan_info["display_extra"]["displayed_channels"].append("scalars")
    validate_scan_info(scan_info)
    return scan_info


def file_info(scan_number: int):
    raw_data = "/tmp/RAW_DATA"
    beamline = "id00"
    proposal = "testproposal"
    collection = "collection"
    dataset = "0001"
    proposal_file = os.path.join(raw_data, f"{proposal}_{beamline}.h5")
    collection_file = os.path.join(raw_data, collection, f"{proposal}_{collection}.h5")
    dataset_file = os.path.join(
        raw_data, collection, f"{collection}_{dataset}", f"{collection}_{dataset}.h5"
    )

    while scan_exists(dataset_file, scan_number):
        dataset = f"{int(dataset)+1:04d}"
        dataset_file = os.path.join(
            raw_data,
            collection,
            f"{collection}_{dataset}",
            f"{collection}_{dataset}.h5",
        )

    images_path = os.path.join(
        raw_data, collection, f"{collection}_{dataset}", f"scan{scan_number:04d}"
    )
    masterfiles = {
        "dataset": dataset_file,
        "dataset_collection": collection_file,
        "proposal": proposal_file,
    }
    return dataset_file, masterfiles, images_path


def scan_exists(dataset_file: str, scan_number: int) -> bool:
    if not os.path.exists(dataset_file):
        return False
    with h5py.File(dataset_file, "r", locking=False) as nxroot:
        return f"{scan_number}.1" in nxroot


def finish_scan(scan):
    scan.stop()

    # gather end of scan metadata
    scan.info["end_reason"] = "SUCCESS"
    scan.info["end_time"] = datetime.now().astimezone().isoformat()

    # upload final metadata
    scan.close()


def cancel_scan(scan, error):
    if scan.state >= ScanState.CLOSED:
        return

    # gather end of scan metadata
    if isinstance(error, KeyboardInterrupt):
        scan.info["end_reason"] = "USER_ABORT"
    else:
        scan.info["end_reason"] = "FAILURE"
    scan.info["end_time"] = datetime.now().astimezone().isoformat()

    # upload final metadata
    scan.close()


def prepare_streams(scan: Scan, images_path: str) -> dict[str, Stream]:
    # 0D data in Redis
    stream_definition = Stream.make_definition(
        "scalars",
        dtype=numpy.float64,
        shape=(),
        info={"unit": "mV", "embed the info": "you want"},
    )
    scalar_stream = scan.create_stream(stream_definition)

    stream_definition = Stream.make_definition(
        "epoch",
        dtype=numpy.float64,
        shape=(),
        info={"unit": "s"},
    )
    epoch_stream = scan.create_stream(stream_definition)

    # 1D data in Redis
    stream_definition = Stream.make_definition(
        "vectors",
        dtype=numpy.int32,
        shape=(4096,),
        info={},
    )
    vector_stream = scan.create_stream(stream_definition)

    # 2D data in Redis
    stream_definition = Stream.make_definition(
        "images",
        dtype=numpy.uint16,
        shape=(
            1024,
            1024,
        ),
        info={},
    )
    image_stream = scan.create_stream(stream_definition)

    # 2D data in HDF5
    stream_definition = LimaStream.make_definition(
        "external_images",
        dtype=numpy.uint32,
        shape=(1024, 512),
        server_url="MOCKED",
        buffer_max_number=1352,
        frames_per_acquisition=100,
        acquisition_offset=0,
        saving={
            "file_path": os.path.join(images_path, "lima_simulator_%04d.h5"),
            "data_path": "/entry_0000/esrf-id00a/lima_simulator/data",
            "file_format": "hdf5",
            "file_offset": 0,
            "frames_per_file": 100,
        },
        info={},
    )
    hdf5ref_stream = scan.create_stream(stream_definition)

    return {
        "epoch_stream": epoch_stream,
        "scalar_stream": scalar_stream,
        "vector_stream": vector_stream,
        "image_stream": image_stream,
        "hdf5ref_stream": hdf5ref_stream,
    }


def publish_data(
    npoints,
    count_time,
    epoch_stream,
    scalar_stream,
    vector_stream,
    image_stream,
    hdf5ref_stream,
    saving: bool = False,
):
    """ "Publish data into streams (could be done in parallel)"""
    with ExitStack() as stack:
        if saving:
            ctx = _CameraClient(hdf5ref_stream.info)
            camera_client = stack.enter_context(ctx)
        else:
            camera_client = None

        const = 2 * numpy.pi / npoints
        for i in range(npoints):
            print(f"\r Scan point {i + 1}", end="")

            time.sleep(count_time)

            epoch_stream.send(time.time())

            t = const * i
            data = float(numpy.sin(50 * t**2) + numpy.sin(2 * t))
            scalar_stream.send(data)

            data = numpy.full(
                get_shape(vector_stream), i, dtype=get_dtype(vector_stream)
            )
            vector_stream.send(data)

            data = numpy.full(get_shape(image_stream), i, dtype=get_dtype(image_stream))
            image_stream.send(data)

            if camera_client:
                camera_client.trigger()
                data = camera_client.status
            else:
                data = {"last_index": i, "last_index_saved": i}
            if data:
                hdf5ref_stream.send(data)

    if camera_client:
        data = camera_client.status
        hdf5ref_stream.send(data)

    # close streams
    epoch_stream.seal()
    scalar_stream.seal()
    vector_stream.seal()
    image_stream.seal()
    hdf5ref_stream.seal()


def get_shape(stream):
    return stream.info["shape"]


def get_dtype(stream):
    return stream.info["dtype"]


def _print_scan_info(info: dict) -> None:
    title = info["title"]
    npoints = info["npoints"]
    filename = info["filename"]
    scan_number = info["scan_nb"]
    count_time = info["count_time"]

    print(f"\n{title} (#{scan_number})")
    print(f" File URL: silx://{filename}?path={scan_number}.1")
    duration = timedelta(seconds=npoints * count_time)
    print(f" Scan duration: {duration}")


def _print_stream_info(streams: dict[str, Stream], info: dict) -> None:
    npoints = info["npoints"]
    count_time = info["count_time"]
    point_byte_size = sum(_get_point_byte_size(stream) for stream in streams.values())
    scan_byte_size = point_byte_size * npoints
    scan_byte_rate = point_byte_size / count_time
    print(
        f" Data Volume  = {_convert_size(scan_byte_size)}, Data Rate  = {_convert_size(scan_byte_rate)}/s"
    )

    point_byte_size = sum(
        _get_point_byte_size(stream, redis_only=True) for stream in streams.values()
    )
    scan_byte_size = point_byte_size * npoints
    scan_byte_rate = point_byte_size / count_time
    print(
        f" Redis Volume = {_convert_size(scan_byte_size)}, Redis Rate = {_convert_size(scan_byte_rate)}/s"
    )


def _print_finished(info: dict) -> None:
    print()
    end_reason = info.get("end_reason")
    if end_reason:
        start_time = datetime.fromisoformat(info["start_time"])
        end_time = datetime.fromisoformat(info["end_time"])
        print("Scan ended with", end_reason, "in", end_time - start_time)
    else:
        print("Scan did not start")


def _get_point_byte_size(stream: Stream, redis_only: bool = False) -> int:
    if stream.kind == "array":
        if isinstance(Stream, LimaStream) and redis_only:
            return 32  # approx
        else:
            dtype = numpy.dtype(stream.info["dtype"])
            shape = tuple(stream.info["shape"])
            point_byte_size = dtype.itemsize
            if shape:
                point_byte_size *= numpy.prod(shape, dtype=int)
            return point_byte_size
    else:
        raise ValueError(f"unknown stream kind '{stream.kind}'")


def _convert_size(size_bytes: int) -> str:
    if size_bytes == 0:
        return "0B"
    size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
    i = int(math.floor(math.log(size_bytes, 1024)))
    value = size_bytes / math.pow(1024, i)
    unit = size_name[i]
    return f"{value:5.1f}{unit}"


class _CameraClient:
    """Emulate external camera with HDF5 saving"""

    def __init__(self, info: dict) -> None:
        self._info = info
        self._proc = None
        self._trigger_queue = None
        self._status_queue = None
        self._last_status = None

    def __enter__(self) -> "_CameraClient":
        ctx = multiprocessing.get_context()
        self._trigger_queue = ctx.Queue()
        self._status_queue = ctx.Queue()

        self._proc = ctx.Process(
            target=_camera_server,
            args=(self._trigger_queue, self._status_queue, self._info),
        )
        self._proc.start()
        return self

    def __exit__(self, *_) -> None:
        self._trigger_queue.put("STOP")
        self._proc.join()
        self._fetch_status()
        self._proc = None
        self._trigger_queue = None
        self._status_queue = None

    @property
    def status(self) -> Optional[dict[str, int]]:
        self._fetch_status()
        return self._last_status

    def trigger(self) -> None:
        self._trigger_queue.put("MEASURE")

    def _fetch_status(self) -> None:
        if self._status_queue is not None:
            while not self._status_queue.empty():
                self._last_status = self._status_queue.get()


def _camera_server(
    trigger_queue: multiprocessing.Queue,
    status_queue: multiprocessing.Queue,
    info: dict,
) -> None:
    """Emulate an external server which gets triggered, saves images and returns a status."""
    writer = None
    try:
        with _AsyncImageWriter(info) as writer:
            image_index_in_scan = -1
            while True:
                trigger = trigger_queue.get()
                if trigger == "STOP":
                    break
                if trigger != "MEASURE":
                    raise ValueError(f"unknown trigger {repr(trigger)}")

                # Measure scan point
                image_index_in_scan += 1
                value = image_index_in_scan

                # Yield status
                status = {
                    "last_index": image_index_in_scan,
                    "last_index_saved": writer.last_index_saved,
                }
                status_queue.put(status)

                # Save asynchronously
                writer.save(image_index_in_scan, value)
    except KeyboardInterrupt:
        pass
    finally:
        if writer is None:
            return
        status = {
            "last_index": image_index_in_scan,
            "last_index_saved": writer.last_index_saved,
        }
        status_queue.put(status)


class _AsyncImageWriter:
    """Asynchronous image writer (HDF5 saving in a separate thread)"""

    def __init__(self, info: dict) -> None:
        self._file_path = info["lima_info"]["file_path"]
        self._data_path = info["lima_info"]["data_path"]
        self._dtype = getattr(numpy, info["dtype"])

        self._frame_per_file = info["lima_info"]["frame_per_file"]
        image_shape = tuple(info["shape"])
        self._file_shape = (self._frame_per_file,) + image_shape

        self._h5file = None
        self._h5dset = None
        self._open_file_index = -1
        self._last_index_saved = -1

        self._save_thread = None
        self._save_queue = queue.Queue()

    def __enter__(self) -> "_AsyncImageWriter":
        self._last_index_saved = -1
        self._save_thread = threading.Thread(target=self._save_main)
        self._save_thread.start()
        return self

    def __exit__(self, *_) -> None:
        self._save_queue.put(None)
        self._save_thread.join()
        self._close_file()

    @property
    def last_index_saved(self) -> int:
        return self._last_index_saved

    def save(self, image_index_in_scan: int, value: int) -> None:
        """Asynchronous call for saving one image"""
        self._save_queue.put((image_index_in_scan, value))

    def _save_main(self):
        """Save images in a separate thread"""
        while True:
            save_args = self._save_queue.get()
            if save_args:
                self._save(*save_args)
            else:
                break

    def _save(self, image_index_in_scan: int, value: int) -> None:
        """Save image and update the last saved counter"""
        file_index = image_index_in_scan // self._frame_per_file
        self._open_file(file_index)

        image_index_in_file = image_index_in_scan % self._frame_per_file

        self._h5dset[image_index_in_file] = value
        self._h5file.flush()

        self._last_index_saved = image_index_in_scan

    def _open_file(self, file_index: int) -> None:
        if file_index == self._open_file_index:
            return
        self._close_file()
        filename = self._file_path % file_index
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        self._h5file = h5py.File(filename, "w")
        self._h5dset = self._h5file.create_dataset(
            self._data_path, shape=self._file_shape, dtype=self._dtype
        )
        self._open_file_index = file_index

    def _close_file(self) -> None:
        if self._h5file is None:
            return
        self._h5file.close()
        self._h5file = None
        self._h5dset = None
        self._open_file_index = -1


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Run scans")

    parser.add_argument(
        "--redis", default="redis://localhost:6379", type=str, help="Redis URL"
    )

    parser.add_argument("--nscans", default=1, type=int, help="Number of scans")

    parser.add_argument(
        "--count-time",
        default=0.02,
        type=float,
        help="Count time per scan point in seconds",
    )

    parser.add_argument(
        "--scan-time",
        default=5,
        type=float,
        help="Scan time in seconds",
    )

    parser.add_argument(
        "--saving",
        action="store_true",
        help="Enable external HDF5 image saving",
    )

    parser.add_argument(
        "--log",
        default="INFO",
        type=str.upper,
        dest="level",
        help="Log level",
        choices=["CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG", "NOTSET"],
    )

    args = parser.parse_args()
    logging.basicConfig(level=args.level)

    main(args.redis, args.nscans, args.count_time, args.scan_time, saving=args.saving)

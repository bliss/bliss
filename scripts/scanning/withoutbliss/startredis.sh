#!/bin/bash

function cleanup {
    if [ ! -z "$REDIS_PID" ];then
        kill $REDIS_PID
    fi
}

trap cleanup EXIT

redis-server  \
--loadmodule $CONDA_PREFIX/lib/librejson.so \
--loadmodule $CONDA_PREFIX/lib/redisearch.so \
--maxmemory 3gb \
--save "" &

REDIS_PID=$!

echo ""
echo "Wait redis-server online:"
timeout 10s bash -c "while ! nc -zv localhost 6379;do sleep 0.3;done"

echo ""
echo "Start memory tracker:"
memory_tracker --init-db --redis-url redis://localhost:6379 \
--monitoring-period 10 \
--protected-history 5 \
--cleaning-threshold 50

# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
Configuration shared in the BlissRepl
"""

typing_helper_active = True
"""
If true, the BLISS REPL try to auto complete the expression with parenthesis and
comma when the space bar is pressed.
"""

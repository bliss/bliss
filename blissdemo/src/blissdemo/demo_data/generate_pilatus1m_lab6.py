import os
import json
from numpy.typing import ArrayLike
import numpy
import h5py
import pyFAI.units
import pyFAI.azimuthalIntegrator
import pyFAI.calibrant


class Setup:
    def __init__(self, detector: str, energy: float, geometry: dict) -> None:
        self.detector = pyFAI.detectors.detector_factory(detector)
        self.energy = energy
        self.geometry = geometry

    @property
    def wavelength(self) -> float:  # Angstrom
        return pyFAI.units.hc / (self.energy * 1e10)

    def mask_image(self) -> ArrayLike:
        return self.detector.mask

    def calib_image(self, name: str) -> ArrayLike:
        calibrant = pyFAI.calibrant.get_calibrant(name)
        calibrant.set_wavelength(self.wavelength)
        # W = FWHM^2 in rad
        FWHM = 0.2  # deg
        W = numpy.radians(FWHM) ** 2
        diffraction = self.fake_calibration_image(
            calibrant, self.get_ai(), Imax=1000, W=W
        )
        return diffraction

    def get_ai(self) -> pyFAI.azimuthalIntegrator.AzimuthalIntegrator:
        return pyFAI.azimuthalIntegrator.AzimuthalIntegrator(
            detector=self.detector, **self.geometry
        )

    @staticmethod
    def fake_calibration_image(
        calibrant, ai, Imax=1.0, U=0, V=0, W=0.0001
    ) -> numpy.ndarray:
        """
        Generates a fake calibration image from an azimuthal integrator.

        :param calibrant: known powder sample
        :param ai: azimuthal integrator
        :param Imax: maximum intensity of rings
        :param U, V, W: width of the peak from Caglioti's law (FWHM^2 = Utan(th)^2 + Vtan(th) + W)
        """
        image_shape = ai.detector.shape

        tth = ai.twoThetaArray(image_shape)
        tth_min = tth.min()
        tth_max = tth.max()
        npix_diag = int(
            numpy.sqrt(
                image_shape[0] * image_shape[0] + image_shape[1] * image_shape[1]
            )
        )
        tth_1d = numpy.linspace(tth_min, tth_max, npix_diag)

        cte_sigma2_to_fwhm2 = 8.0 * numpy.log(2.0)

        tanth = numpy.tan(tth_1d / 2.0)
        fwhm2 = U * tanth**2 + V * tanth + W
        sigma2 = fwhm2 / cte_sigma2_to_fwhm2

        signal = numpy.zeros_like(sigma2)
        for tth_rreflection in calibrant.get_2th():
            if tth_rreflection >= tth_max:
                break
            gauss = numpy.exp(-((tth_1d - tth_rreflection) ** 2) / (2.0 * sigma2))
            signal += Imax * gauss

        fwhm2_bkg = 4 * (tth_max - tth_min) ** 2
        sigma2_bkg = fwhm2_bkg / cte_sigma2_to_fwhm2
        gauss = numpy.exp(-((tth_1d - tth_min) ** 2) / (2.0 * sigma2_bkg))
        bkg = Imax * 0.1 * gauss

        res = ai.calcfrom1d(
            tth_1d,
            signal + bkg,
            shape=image_shape,
            mask=ai.mask,
            dim1_unit="2th_rad",
            correctSolidAngle=True,
        )
        return res


def iter_images(n):
    n = max(1, n)

    distmin = 5e-2
    distmax = 20e-2
    if n > 1:
        shift_in_meter = int((distmax - distmin) / (n - 1) * 1000) / 1000
    else:
        shift_in_meter = 0

    # Detector size is approx. 0.18 x 0.18 m
    geometry = {
        "dist": distmin,  # cm
        "poni1": 10e-2,  # 10 cm
        "poni2": 10e-2,  # 10 cm
        "rot1": numpy.radians(10),  # 10 deg
        "rot2": 0,  # 0 deg
        "rot3": 0,  # 0 deg
    }
    setup = Setup(detector="Pilatus1M", energy=12, geometry=geometry)

    yield f"{n} images taken by moving the detector {shift_in_meter*100} cm backwards, starting from {setup.geometry['dist']*100} cm"
    data = json.dumps(
        {
            "energy": setup.energy,
            "geometry": setup.geometry,
        }
    )
    yield data
    yield setup.mask_image()
    yield setup.calib_image("LaB6"), setup.geometry["dist"]

    for _ in range(n - 1):
        geometry["dist"] += shift_in_meter
        yield setup.calib_image("LaB6")


def save_data(filename, n=1):
    with h5py.File(filename, mode="w") as f:
        f.attrs["NX_class"] = "NXroot"
        scan = f.create_group("scan1")
        scan.attrs["NX_class"] = "NXentry"
        instrument = scan.create_group("instrument")
        instrument.attrs["NX_class"] = "NXinstrument"

        others = scan.create_group("others")
        others.attrs["NX_class"] = "NXnote"
        others.attrs["type"] = "application/json"

        description, info, mask, *images = iter_images(n)
        print(filename, ":", description)

        images, positions = zip(*images)
        scan["mask"] = mask
        instrument["data"] = numpy.stack(images, axis=0)
        instrument["detz"] = numpy.array(positions) * 1e3
        instrument["detz"].attrs["units"] = "mm"
        others["description"] = description
        others["data"] = info


if __name__ == "__main__":
    filename = os.path.abspath(
        os.path.join(os.path.dirname(__file__), "pilatus1m_lab6.h5")
    )
    save_data(filename, n=1)

import os
import sys
import socket
from bliss.shell.main.step1 import main as _main


def main() -> int:
    os.environ["TANGO_HOST"] = "%s:%d" % (socket.gethostname(), 10000)
    os.environ["BEACON_HOST"] = "%s:%d" % (socket.gethostname(), 10001)
    return _main(argv=sys.argv + ["-s", "demo_session"])


if __name__ == "__main__":
    sys.exit(main())

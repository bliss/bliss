import subprocess
import gevent
from contextlib import contextmanager, ExitStack
from typing import NamedTuple
from collections.abc import Iterator


class ProcessInfo(NamedTuple):
    name: str
    process: subprocess.Popen

    def __str__(self):
        return f"Process(pid={self.process.pid}, name={self.name})"


@contextmanager
def start_process(local_name: str, args: list[str], **kw) -> Iterator[ProcessInfo]:
    proc = subprocess.Popen(args, **kw)
    pinfo = ProcessInfo(name=local_name, process=proc)
    print("Starting", pinfo, " ".join(args))
    try:
        yield pinfo
    except BaseException:
        print("Failed", pinfo)
        cleanup_process(pinfo)
        raise
    else:
        print("Started", pinfo)


def cleanup_process(pinfo: ProcessInfo) -> None:
    try:
        print("Stop", pinfo)
        pinfo.process.terminate()
        pinfo.process.wait(timeout=10)
        print("  - ok")
    except (Exception, KeyboardInterrupt):
        print("  - still running")


@contextmanager
def start_context() -> Iterator[tuple[ExitStack, list[gevent.Greenlet]]]:
    wait_tasks: list[gevent.Greenlet] = []

    with ExitStack() as stack:
        try:
            yield stack, wait_tasks
            if wait_tasks:
                gevent.joinall(wait_tasks, raise_error=True)
        except BaseException:
            if wait_tasks:
                gevent.killall(wait_tasks)
            raise

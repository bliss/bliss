import os
import sys
import socket
import tempfile
import logging
from contextlib import contextmanager, ExitStack
from typing import NamedTuple, Optional
from collections.abc import Iterator

import redis
import shutil
import gevent
from docopt import docopt
from blisswriter.utils import log_levels
from bliss.tango.clients import utils as tango_utils

import blissdemo
from blissdemo import demo_data
from blissdemo import demo_configuration
from .processes import ProcessInfo, start_process, start_context, cleanup_process

CONFIGURATION_ROOT = os.path.abspath(demo_configuration.__path__[0])
DEMO_ROOT = os.path.realpath(blissdemo.__path__[0])


CLI = """
Usage: demo-servers [--lima-environment=<arg>]
                    [--lima2-environment=<arg>]
                    [--mosca-environment=<arg>]
                    [--oda-environment=<arg>]
                    [--beacon-port=<arg>]
                    [--tango-port=<arg>]
                    [--redis-port=<arg>]
                    [--redis-data-port=<arg>]
                    [--homepage-port=<arg>]
                    [--timeout=<arg>]
                    [--log=<arg>]
                    [--no-detectors]

Options:
    --lima-environment=<arg>     Lima simulation Conda environment name
                                 (if unset uses env:LIMA_SIMULATOR_CONDA_ENV)
    --lima2-environment=<arg>    Lima2 simulation Conda environment name
                                 (if unset uses env:LIMA2_SIMULATOR_CONDA_ENV)
    --mosca-environment=<arg>    Mosca simulation Conda environment name
                                 (if unset uses env:MOSCA_SIMULATOR_CONDA_ENV)
    --oda-environment=<arg>      Online data processing Conda environment name
                                 (if unset uses env:DEMO_ODA_CONDA_ENV)
    --tango-port=<arg>           Tango database server port [default: 10000]
    --beacon-port=<arg>          Beacon server port [default: 10001]
    --redis-port=<arg>           Redis server for stats [default: 10002]
    --redis-data-port=<arg>      Redis server for data [default: 10003]
    --homepage-port=<arg>        Homepage server port [default: 10004]
    --timeout=<arg>              Timeout for a server to start [default: 60]
    --log=<arg>                  Log level [default: WARNING]
    --no-detectors               Do not start servers for Lima, Mosca...
"""


class BeaconPorts(NamedTuple):
    beacon_port: int
    tango_port: int
    redis_port: int
    redis_data_port: int
    homepage_port: int


def wait_redis(ports: BeaconPorts, timeout: float = 10) -> redis.Redis:
    conn = None
    exception = None
    try:
        with gevent.Timeout(timeout):
            while conn is None:
                try:
                    conn = redis.Redis(host=socket.gethostname(), port=ports.redis_port)

                    # disable .rdb files saving (redis persistence)
                    conn.config_set("SAVE", "")
                except Exception as e:
                    exception = e
                    gevent.sleep(1)
    except gevent.Timeout:
        raise RuntimeError("Redis did not start") from exception
    return conn


def wait_beacon(ports: BeaconPorts, timeout: float = 10) -> None:
    tango_utils.wait_tango_db(
        host="localhost",
        port=ports.tango_port,
        db=2,
        timeout=timeout,
    )
    wait_redis(ports, timeout=timeout)


def start_beacon(
    stack: ExitStack,
    wait_tasks: list[gevent.Greenlet],
    processes: list[ProcessInfo],
    demo_resources: str,
    cli_options: dict,
) -> None:
    redis_uds = os.path.join(demo_resources, "redis_demo.sock")
    redis_data_uds = os.path.join(demo_resources, "redis_data_demo.sock")
    log_path = os.path.join(demo_resources, "bliss_logs")
    os.makedirs(log_path, exist_ok=True)

    port_names = [
        "--beacon-port",
        "--tango-port",
        "--redis-port",
        "--redis-data-port",
        "--homepage-port",
    ]
    port_list = (int(cli_options[p]) for p in port_names)
    ports = BeaconPorts(*port_list)

    level = get_log_level_int()
    log_level = log_levels.beacon_log_level[level]
    tango_debug_level = log_levels.tango_cli_log_level[level]

    args = [
        sys.executable,
        "-m",
        "bliss.beacon.app.beacon_server",
        "--port=%d" % ports.beacon_port,
        "--redis-port=%d" % ports.redis_port,
        "--redis-socket=" + redis_uds,
        "--redis-data-port=%d" % ports.redis_data_port,
        "--redis-data-socket=" + redis_data_uds,
        "--db_path=" + CONFIGURATION_ROOT,
        "--tango-port=%d" % ports.tango_port,
        "--homepage-port=%d" % ports.homepage_port,
        "--log-level=%s" % log_level,
        "--log-output-folder=" + log_path,
        "--tango-debug-level=%s" % tango_debug_level,
        f"--key=DEMO_ROOT={DEMO_ROOT}",
        f"--key=DEMO_TMP_ROOT={demo_resources}",
    ]

    ctx = start_process("beacon", args)
    processes.append(stack.enter_context(ctx))

    wait_task = gevent.spawn(wait_beacon, ports, timeout=int(cli_options["--timeout"]))
    wait_tasks.append(wait_task)

    os.environ["TANGO_HOST"] = "%s:%d" % (socket.gethostname(), ports.tango_port)
    os.environ["BEACON_HOST"] = "%s:%d" % (socket.gethostname(), ports.beacon_port)
    os.environ["BEACON_REDIS_PORT"] = "%d" % ports.redis_port

    update_ewoks_config(demo_resources, socket.gethostname(), ports.redis_port)
    update_scan_saving_config(demo_resources)


def update_ewoks_config(demo_resources: str, redis_host: str, redis_port: int) -> None:
    filename = os.path.join(CONFIGURATION_ROOT, "ewoks", "config.yml")
    with open(filename + ".template", "r") as f:
        content = f.read()
    content = content.replace("{{redis_host}}", redis_host)
    content = content.replace("{{redis_port}}", str(redis_port))
    with open(filename, "w") as f:
        f.write(content)


def update_scan_saving_config(demo_resources: str) -> None:
    filename = os.path.join(CONFIGURATION_ROOT, "__init__.yml")
    with open(filename + ".template", "r") as f:
        content = f.read()
    content = content.replace("{{demo_resources}}", demo_resources)
    with open(filename, "w") as f:
        f.write(content)


def conda_cmd(env_name: str, cmd: list[str]) -> list[str]:
    conda = os.environ.get("CONDA_EXE", None)
    if env_name and conda:
        if not env_name:
            return list()
        if os.sep in env_name:
            return [conda, "run", "-p", env_name, "--no-capture-output", *cmd]
        else:
            return [conda, "run", "-n", env_name, "--no-capture-output", *cmd]
    else:
        return cmd


def lima_cmd(server_name: str, cli_options: dict) -> list[str]:
    """
    Create the command line to launch a Lima tango server

    Arguments:
        server_name: Name of the instance of the tango server
    """
    conda_env = cli_options["--lima-environment"]
    if conda_env is None:
        conda_env = os.environ.get("LIMA_SIMULATOR_CONDA_ENV")
    return conda_cmd(conda_env, ["LimaCCDs", server_name])


def lima2_cmd(server_name: str, cli_options: dict) -> list[str]:
    """
    Create the command line to launch a Lima2 tango server

    Arguments:
        server_name: Name of the instance of the tango server
    """
    conda_env = cli_options["--lima2-environment"]
    if conda_env is None:
        conda_env = os.environ.get("LIMA2_SIMULATOR_CONDA_ENV")
    return conda_cmd(
        conda_env,
        [
            "mpiexec",
            "-n",
            "1",
            "lima2_tango",
            server_name + "_ctl",
            "--log-level",
            "info",
            "--log-file-filename",
            "lima2_ctl_%N.log",
            ":",
            "-n",
            "1",
            "lima2_tango",
            "--log-level",
            "info",
            "--log-file-filename",
            "lima2_rcv1_%N.log",
            server_name + "_rcv1",
            ":",
            "-n",
            "1",
            "lima2_tango",
            "--log-level",
            "info",
            "--log-file-filename",
            "lima2_rcv2_%N.log",
            server_name + "_rcv2",
        ],
    )


def mosca_cmd(server_name: str, cli_options: dict) -> list[str]:
    """
    Create the command line to launch a Mosca tango server

    Arguments:
        server_name: Name of the instance of the tango server
    """
    conda_env = cli_options["--mosca-environment"]
    if conda_env is None:
        conda_env = os.environ.get("MOSCA_SIMULATOR_CONDA_ENV")
    return conda_cmd(conda_env, ["SimulSpectro", server_name])


def oda_conda_env(cli_options) -> Optional[str]:
    """Returns the name of the conda env defined for the ODA server, else None"""
    oda_env = cli_options["--oda-environment"]
    if oda_env is None:
        return os.environ.get("DEMO_ODA_CONDA_ENV")
    return oda_env


def oda_cmd(cmd: list[str], cli_options: dict) -> Optional[list[str]]:
    """
    Create the command line to launch an ODA server
    """
    return conda_cmd(oda_conda_env(cli_options), cmd)


class TangoDeviceDescription(NamedTuple):
    name: str
    cmd: list[str]
    server_name: str
    cwd: str
    env: Optional[dict] = None


def get_lima_env():
    return {
        "TANGO_HOST": os.environ["TANGO_HOST"],
        "BEACON_REDIS_PORT": os.environ["BEACON_REDIS_PORT"],
        "DEMO_ROOT": os.path.realpath(blissdemo.__path__[0]),
    }


def get_lima2_env():
    return {
        "TANGO_HOST": os.environ["TANGO_HOST"],
        "BEACON_REDIS_PORT": os.environ["BEACON_REDIS_PORT"],
        "DEMO_ROOT": os.path.realpath(blissdemo.__path__[0]),
    }


def get_mosca_env():
    return {
        "TANGO_HOST": os.environ["TANGO_HOST"],
        "BEACON_REDIS_PORT": os.environ["BEACON_REDIS_PORT"],
        "DEMO_ROOT": os.path.realpath(blissdemo.__path__[0]),
    }


def get_session_env():
    return {
        "BEACON_HOST": os.environ["BEACON_HOST"],
        "TANGO_HOST": os.environ["TANGO_HOST"],
    }


def get_oda_env():
    return {
        "BEACON_HOST": os.environ["BEACON_HOST"],
        "FLOWER_UNAUTHENTICATED_API": "true",
    }


def tango_servers(cli_options) -> list[TangoDeviceDescription]:
    """Return the list the tango server to be started"""
    from bliss.testutils.servers import machinfo_tg_server

    cwd = os.path.realpath(demo_data.__path__[0])
    desc: list[TangoDeviceDescription] = []

    desc += [
        TangoDeviceDescription(
            name="id00/bliss_nxwriter/demo_session",
            cmd=["NexusWriterService", "demo"],
            server_name="NexusWriter",
            cwd=cwd,
        )
    ]

    desc += [
        TangoDeviceDescription(
            name="id00/machinfo/1",
            cmd=["python", machinfo_tg_server.__file__, "machinfo"],
            server_name="machinfo_tg_server",
            cwd=cwd,
        )
    ]

    if cli_options["--no-detectors"]:
        return desc

    lima_env = get_lima_env()
    lima2_env = get_lima2_env()
    mosca_env = get_mosca_env()

    desc += [
        TangoDeviceDescription(
            name="id00/limaccds/simulator1",
            cmd=lima_cmd("simulator", cli_options),
            server_name="LimaCCDs",
            cwd=cwd,
            env=lima_env,
        ),
        TangoDeviceDescription(
            name="id00/limaccds/slits_simulator",
            cmd=lima_cmd("slits_simulator", cli_options),
            server_name="LimaCCDs",
            cwd=cwd,
            env=lima_env,
        ),
        TangoDeviceDescription(
            name="id00/limaccds/tomo_simulator",
            cmd=lima_cmd("tomo_simulator", cli_options),
            server_name="LimaCCDs",
            cwd=cwd,
            env=lima_env,
        ),
        TangoDeviceDescription(
            name="id00/limaccds/diff_simulator",
            cmd=lima_cmd("diff_simulator", cli_options),
            server_name="LimaCCDs",
            cwd=cwd,
            env=lima_env,
        ),
        TangoDeviceDescription(
            name="id00/limaccds/diff2_simulator",
            cmd=lima_cmd("diff2_simulator", cli_options),
            server_name="LimaCCDs",
            cwd=cwd,
            env=lima_env,
        ),
        TangoDeviceDescription(
            name="id00/limaccds/lab6_simulator",
            cmd=lima_cmd("lab6_simulator", cli_options),
            server_name="LimaCCDs",
            cwd=cwd,
            env=lima_env,
        ),
        TangoDeviceDescription(
            name="id00/lima2/simulator",
            cmd=lima2_cmd("simulator", cli_options),
            server_name="lima2_tango",
            cwd=cwd,
            env=lima2_env,
        ),
        TangoDeviceDescription(
            name="id00/mosca/simulator",
            cmd=mosca_cmd("mosca_simulator", cli_options),
            server_name="SimulSpectro",
            cwd=cwd,
            env=mosca_env,
        ),
    ]
    return desc


def start_tango_servers(
    stack: ExitStack,
    wait_tasks: list[gevent.Greenlet],
    processes: list[ProcessInfo],
    cli_options: dict,
) -> None:
    for description in tango_servers(cli_options):
        if not description.cmd:
            continue
        fqdn_prefix = f"tango://{os.environ['TANGO_HOST']}"
        # device_fqdn = f"{fqdn_prefix}/{device_name}"

        # Warning: this assumes that the personal name gets last on the command line
        personal_name = description.cmd[-1]
        admin_device_fqdn = (
            f"{fqdn_prefix}/dserver/{description.server_name}/{personal_name}"
        )
        ctx = start_process(
            admin_device_fqdn, description.cmd, cwd=description.cwd, env=description.env
        )
        processes.append(stack.enter_context(ctx))
        wait_task = gevent.spawn(
            tango_utils.wait_tango_device,
            admin_device_fqdn,
            timeout=int(cli_options["--timeout"]),
        )
        wait_tasks.append(wait_task)


def wait_oda_worker(timeout: float = 10) -> None:
    import celery

    os.environ["CELERY_LOADER"] = "ewoksjob.config.EwoksLoader"
    app = celery.Celery("ewoks")
    inspect = app.control.inspect()
    try:
        with gevent.Timeout(timeout):
            while not inspect.active():
                gevent.sleep(1)
    except gevent.Timeout:
        raise RuntimeError("ODA worker did not start")


def start_oda_processes(
    stack: ExitStack,
    wait_tasks: list[gevent.Greenlet],
    processes: list[ProcessInfo],
    cli_options: dict,
) -> None:
    oda_env = get_oda_env()

    # Worker to execute EWOKS workflows
    cmd = ["ewoksjob", "worker", "-l", get_log_level()]
    cmd = oda_cmd(cmd, cli_options)
    if cmd:
        ctx = start_process("oda-worker", cmd, env=oda_env)
        processes.append(stack.enter_context(ctx))
        wait_task = gevent.spawn(wait_oda_worker, timeout=int(cli_options["--timeout"]))
        wait_tasks.append(wait_task)

    # Monitor workflows on http://localhost:5555
    cmd = ["ewoksjob", "monitor"]
    cmd = oda_cmd(cmd, cli_options)
    if cmd:
        ctx = start_process("oda-monitor", cmd, env=oda_env)
        processes.append(stack.enter_context(ctx))

    # Trigger workflows based on scan_info
    cmd = ["blissoda", "workflows", "demo_session", "-l", get_log_level()]
    if oda_cmd(cmd, cli_options):
        ctx = start_process("oda-submitter", cmd)
        processes.append(stack.enter_context(ctx))


def bordered_text(text: str) -> str:
    lines = text.splitlines()
    lines = [line.strip() for line in lines]
    width = max([len(line) for line in lines])
    for i, line in enumerate(lines):
        before = (width - len(line)) // 2
        after = (width - len(line) + 1) // 2
        line = "# " + " " * before + line + " " * after + " #"
        lines[i] = line
    lines.insert(0, "#" * (width + 4))
    lines.append("#" * (width + 4))
    return "\n".join(lines)


def cmd_string(cmd: str, env: dict[str, str]) -> str:
    lst = [f"{k}={v}" for k, v in env.items()]
    lst.append(cmd)
    return " ".join(lst)


def print_start_message() -> None:
    env = get_session_env()
    env = " ".join([f'{k}="{v}"' for k, v in env.items()])

    text = f"""Start BLISS in another Terminal using

    > bliss-demo-session

    or

    > {env} bliss -s demo_session

    Press CTRL+C to quit this process
    """
    print(bordered_text(text))


def run(demo_resources: str, cli_options: dict):
    processes: list[ProcessInfo] = list()
    try:
        print(bordered_text("    Starting the demo servers    "))
        with start_context() as (stack, wait_tasks):
            start_beacon(stack, wait_tasks, processes, demo_resources, cli_options)
        with start_context() as (stack, wait_tasks):
            start_tango_servers(stack, wait_tasks, processes, cli_options)
            if oda_conda_env(cli_options):
                start_oda_processes(stack, wait_tasks, processes, cli_options)
        print_start_message()
        processes[0].process.wait()
    except KeyboardInterrupt:
        pass
    finally:
        print(bordered_text("    Stopping the demo servers    "))
        for pinfo in processes:
            cleanup_process(pinfo)


@contextmanager
def setup_resource_files() -> Iterator[tuple[str, str]]:
    """Setup the configuration files"""
    demo_resources = tempfile.mkdtemp(prefix="demo_resources")
    print("Demo resource directory:", demo_resources)
    try:
        yield demo_resources
    finally:
        shutil.rmtree(demo_resources)


def get_log_level_int() -> int:
    return logging.getLogger().getEffectiveLevel()


def get_log_level() -> str:
    return logging.getLevelName(get_log_level_int())


def set_log_level(level: str) -> None:
    logging.basicConfig(level=level.upper())


def main(argv=None):
    if argv is None:
        argv = sys.argv
    cli_options = docopt(CLI, argv=argv[1:])
    set_log_level(cli_options["--log"])
    with setup_resource_files() as demo_resources:
        run(demo_resources, cli_options)
        print(bordered_text("    Done    "))


if __name__ == "__main__":
    sys.exit(main())

import time
from bliss.common import plot
import psutil
import pprint
from .memutils import substract_count

MONITORING_DURTION = 5  # In minutes


def test_opengl(flint_session, lima_simulator):
    """
    Start flint and start image monitoring.

    Make sure the is no extra memory consumption.
    Else print a diagnostic.
    """
    session = flint_session
    lima_simulator = session.config.get("lima_simulator")
    flint = plot.get_flint()
    flint.set_silx_backend("opengl")
    pflint = psutil.Process(flint.pid)

    mems = []
    viewer = flint.get_live_plot(image_detector=lima_simulator.name)
    viewer.submit("setRefreshMode", 100)
    lima_simulator.start_live(0.01)
    time.sleep(5)
    t0 = time.time()
    obj0 = flint.test_count_python_objects()
    mem0 = pflint.memory_info().vms
    try:
        rate = 10
        for _ in range(int(MONITORING_DURTION * 60 // rate)):
            mem = pflint.memory_info().vms
            t = time.time()
            print(f"{t-t0} {mem / 1_000_000:.3f} MB")
            mems.append(mem)
            if mem > mem0 * 4:
                raise RuntimeError("Already too much memory consumption")
            time.sleep(rate)
    finally:
        obj1 = flint.test_count_python_objects()
        lima_simulator.stop_live()
        pprint.pprint(substract_count(obj1, obj0))

    mid = mems[len(mems) // 2]
    last = mems[-1]
    assert last < (mid + 1_000_000), "Too much memory consumption"

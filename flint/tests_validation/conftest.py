# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from __future__ import annotations
import atexit
import os
import pytest
import shutil
import subprocess
import sys
import traceback
import weakref
from collections import namedtuple
from contextlib import contextmanager
from pathlib import Path

import gevent
import redis
import redis.connection

import bliss
from bliss import global_log, global_map
from bliss.common import logtools, plot
from bliss.common import session as session_module
from bliss.common.data_store import set_default_data_store
from bliss.common.tango import ApiUtil
from bliss.common.utils import copy_tree, get_open_ports, rm_tree
from bliss.config import static
from bliss.config.conductor import client, connection
from bliss.config.conductor.client import get_default_connection
from bliss.controllers import simulation_diode, tango_attr_as_counter
from flint.client import proxy as flint_proxy
from bliss.scanning import scan_meta
from bliss.tango.clients.utils import wait_tango_db
from bliss.testutils.comm_utils import wait_tcp_online
from bliss.testutils.process_utils import wait_terminate
from bliss.testutils.controller_utils import lima_simulator_context

from blissdata.redis_engine.store import DataStore

pytest_plugins = ["bliss.testutils.optional_capsys"]

BLISS = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
BEACON = [sys.executable, "-m", "bliss.beacon.app.beacon_server"]
BEACON_DB_PATH = os.path.join(BLISS, "tests_validation", "beamline_configuration")


@pytest.fixture(autouse=True)
def clean_globals():
    orig_excepthook = sys.excepthook
    yield
    sys.excepthook = orig_excepthook
    global_log.clear()
    global_map.clear()
    # reset module-level globals
    simulation_diode.DEFAULT_CONTROLLER = None
    simulation_diode.DEFAULT_CONTROLLER = None
    simulation_diode.DEFAULT_INTEGRATING_CONTROLLER = None
    bliss._BLISS_SHELL_MODE = False
    session_module.sessions.clear()
    scan_meta.USER_SCAN_META = None

    # clean modif from shell.cli.repl
    logtools.elogbook.disable()

    tango_attr_as_counter._TangoCounterControllerDict = weakref.WeakValueDictionary()


@pytest.fixture(autouse=True)
def clean_tango():
    # close file descriptors left open by Tango (see tango-controls/pytango/issues/324)
    try:
        ApiUtil.cleanup()
    except RuntimeError:
        # no Tango ?
        pass


@pytest.fixture(scope="session")
def beacon_tmpdir(tmpdir_factory):
    tmpdir = str(tmpdir_factory.mktemp("beacon"))
    yield tmpdir


@pytest.fixture(scope="session")
def beacon_directory(beacon_tmpdir):
    beacon_dir = os.path.join(beacon_tmpdir, "test_configuration")
    shutil.copytree(BEACON_DB_PATH, beacon_dir)
    yield beacon_dir


@pytest.fixture(scope="session")
def log_directory(beacon_tmpdir):
    log_dir = os.path.join(beacon_tmpdir, "log")
    os.mkdir(log_dir)
    yield log_dir


@pytest.fixture(scope="session")
def ports(beacon_directory, log_directory):
    redis_uds = os.path.join(beacon_directory, ".redis.sock")
    redis_data_uds = os.path.join(beacon_directory, ".redis_data.sock")

    port_names = [
        "redis_port",
        "redis_data_port",
        "tango_port",
        "beacon_port",
        "logserver_port",
        "homepage_port",
    ]

    ports = namedtuple("Ports", " ".join(port_names))(*get_open_ports(6))
    os.environ["TANGO_HOST"] = f"localhost:{ports.tango_port}"
    os.environ["BEACON_HOST"] = f"localhost:{ports.beacon_port}"
    args = [
        f"--port={ports.beacon_port}",
        f"--redis-port={ports.redis_port}",
        f"--redis-socket={redis_uds}",
        f"--redis-data-port={ports.redis_data_port}",
        f"--redis-data-socket={redis_data_uds}",
        f"--db-path={beacon_directory}",
        f"--tango-port={ports.tango_port}",
        f"--log-server-port={ports.logserver_port}",
        f"--log-output-folder={log_directory}",
        "--log-level=WARN",
        "--tango-debug-level=0",
        "--key=TESTKEY=FOO",
    ]
    proc = subprocess.Popen(BEACON + args)
    wait_tcp_online("localhost", ports.beacon_port, timeout=10)

    try:
        wait_tango_db(port=ports.tango_port, db=2, timeout=10)
    except BaseException:
        # don't wait for all tests to fail if the tango db doesn't work
        pytest.exit(traceback.format_exc(), returncode=-1)

    # disable .rdb files saving (redis persistence)
    r = redis.Redis(host="localhost", port=ports.redis_port)
    r.config_set("SAVE", "")
    del r

    yield ports

    atexit._run_exitfuncs()
    wait_terminate(proc)


@pytest.fixture
def blissdata(ports):
    redis_url = f"redis://localhost:{ports.redis_data_port}"

    red = redis.Redis.from_url(redis_url)
    red.flushall()
    try:
        data_store = DataStore(redis_url, init_db=True)
    except RuntimeError:
        # https://gitlab.esrf.fr/bliss/bliss/-/issues/4098
        # Memory tracker is part of beacon fixture which is session scoped.
        # Thus, it keeps running while this fixture restart, which may cause
        # the db to not be empty after flushall.
        red.delete("_MEMORY_TRACKING_")
        data_store = DataStore(redis_url, init_db=True)

    set_default_data_store(redis_url)
    try:
        yield data_store
    finally:
        data_store._redis.connection_pool.disconnect()
        data_store._redis = None


@pytest.fixture
def beacon(ports, beacon_directory, blissdata):
    redis_db = redis.Redis(port=ports.redis_port)
    redis_db.flushall()
    static.Config.instance = None
    client._default_connection = connection.Connection("localhost", ports.beacon_port)
    config = static.get_config()
    yield config
    config.close()
    client._default_connection.close()
    # Ensure no connections are created due to garbage collection:
    client._default_connection = None

    # Always restore beacon directory
    rm_tree(Path(beacon_directory))  # still keeps Unix socket "files"
    # Restore files from BEACON_DB_PATH
    copy_tree(Path(BEACON_DB_PATH), Path(beacon_directory))


@pytest.fixture
def beacon_host_port(ports):
    return "localhost", ports.beacon_port


@pytest.fixture
def redis_conn(beacon):
    cnx = get_default_connection()
    redis_conn = cnx.get_redis_proxy()
    yield redis_conn


@pytest.fixture
def redis_data_conn(beacon):
    cnx = get_default_connection()
    redis_conn = cnx.get_redis_proxy(db=1)
    yield redis_conn


@pytest.fixture
def scan_tmpdir(tmpdir):
    yield tmpdir


@pytest.fixture
def lima_simulator(ports):
    with lima_simulator_context("simulator", "id00/limaccds/simulator1") as fqdn_proxy:
        yield fqdn_proxy


@contextmanager
def flint_context(with_flint=True, stucked=False):
    """Helper to capture and clean up all new Flint processes created during the
    context.

    It also provides arguments to request a specific Flint state.
    """
    flint_singleton = flint_proxy._get_singleton()
    try:
        pids = set()

        def register_new_flint_pid(pid):
            nonlocal pids
            pids.add(pid)

        assert flint_singleton._on_new_pid is None
        flint_singleton._on_new_pid = register_new_flint_pid

        try:
            try:
                if with_flint:
                    flint = plot.get_flint()

                if stucked:
                    assert with_flint is True
                    try:
                        with gevent.Timeout(seconds=0.1):
                            # This command does not return that is why it is
                            # aborted with a timeout
                            flint.test_infinit_loop()
                    except gevent.Timeout:
                        pass

                yield
            finally:
                for pid in pids:
                    try:
                        wait_terminate(pid, timeout=10)
                    except gevent.Timeout:
                        # This could happen, if the kill fails, after 10s
                        pass
        finally:
            flint_singleton._on_new_pid = None
    finally:
        flint_singleton._proxy_cleanup()


@pytest.fixture
def flint_session(xvfb, beacon, scan_tmpdir):
    session = beacon.get("flint")
    session.setup()
    session.scan_saving.base_path = str(scan_tmpdir)
    with flint_context():
        yield session
    session.close()

def substract_count(d1: dict[str, int], d2: dict[str, int]):
    """Substract key count from dict containing count par keys"""
    d = {}
    for k in set(d1.keys()) | set(d2.keys()):
        v1 = d1.get(k, 0)
        v2 = d2.get(k, 0)
        v = v1 - v2
        if v <= 0:
            continue
        d[k] = v
    return d

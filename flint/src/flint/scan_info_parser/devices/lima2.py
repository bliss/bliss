# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from __future__ import annotations

from .lima import DeviceParser as LimaDeviceParser


class DeviceParser(LimaDeviceParser):
    pass

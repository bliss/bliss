# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from __future__ import annotations

import logging
import numpy
from silx.gui.plot.items import Curve, PointsBase


_logger = logging.getLogger(__name__)


class TimeCurveItem(Curve):
    """
    Curve item which feature a `firstDisplayedIndex` to tune the bouding item data.

    This actually allow to fit the y-range of the plot according to the duration time
    selected.
    """

    def __init__(self):
        Curve.__init__(self)
        self.firstDisplayedIndex: int = 0

    def _getBounds(self):
        if self.getXData(copy=False).size == 0:  # Empty data
            return None

        plot = self.getPlot()
        if plot is not None:
            xPositive = plot.getXAxis()._isLogarithmic()
            yPositive = plot.getYAxis()._isLogarithmic()
        else:
            xPositive = False
            yPositive = False

        if (xPositive, yPositive) not in self._boundsCache:
            # use the getData class method because instance method can be
            # overloaded to return additional arrays
            data = PointsBase.getData(self, copy=False, displayed=True)
            if len(data) == 5:
                # hack to avoid duplicating caching mechanism in Scatter
                # (happens when cached data is used, caching done using
                # Scatter._filterData)
                x, y, xerror, yerror = data[0], data[1], data[3], data[4]
            else:
                x, y, xerror, yerror = data

            if self.firstDisplayedIndex > 0:
                if self.firstDisplayedIndex < len(x):
                    x = x[self.firstDisplayedIndex :]
                    y = y[self.firstDisplayedIndex :]

            xmin, xmax = self._PointsBase__minMaxDataWithError(x, xerror, xPositive)
            ymin, ymax = self._PointsBase__minMaxDataWithError(y, yerror, yPositive)

            self._boundsCache[(xPositive, yPositive)] = tuple(
                [
                    (bound if bound is not None else numpy.nan)
                    for bound in (xmin, xmax, ymin, ymax)
                ]
            )
        return self._boundsCache[(xPositive, yPositive)]

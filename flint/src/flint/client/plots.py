# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.
"""
Provides plot helper class to deal with flint proxy.
"""

from flint.viewers.live_image.client import LiveImagePlot
from flint.viewers.live_scatter.client import LiveScatterPlot
from flint.viewers.live_curve.client import LiveCurvePlot
from flint.viewers.live_mca.client import LiveMcaPlot
from flint.viewers.live_onedim.client import LiveOneDimPlot

from flint.viewers.custom_time_curve.client import TimeCurvePlot
from flint.viewers.custom_spectro.client import SpectroPlot

from flint.viewers.custom_curve_stack.client import CurveStack
from flint.viewers.custom_image_stack.client import StackView
from flint.viewers.custom_image.client import ImageView
from flint.viewers.custom_scatter.client import ScatterView
from flint.viewers.custom_scatter3d.client import ScatterView3D

from flint.viewers.custom_plot1d.client import Plot1D
from flint.viewers.custom_plot2d.client import Plot2D
from flint.viewers.custom_plot3d.client import Plot3D

from flint.viewers.custom_grid_container.client import GridContainer

# Used by external code to create custom plots
from flint.client.base_plot import BasePlot  # noqa


CUSTOM_CLASSES = [
    Plot1D,
    Plot2D,
    Plot3D,
    ScatterView,
    ScatterView3D,
    ImageView,
    StackView,
    CurveStack,
    TimeCurvePlot,
    SpectroPlot,
    GridContainer,
]

LIVE_CLASSES = [
    LiveCurvePlot,
    LiveImagePlot,
    LiveScatterPlot,
    LiveMcaPlot,
    LiveOneDimPlot,
]

# For compatibility
CurvePlot = Plot1D
ImagePlot = Plot2D
ScatterPlot = ScatterView
HistogramImagePlot = ImageView
ImageStackPlot = StackView

# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.
"""
Package containing Qt widgets.

.. autosummary::
    :toctree:

    about
    color_picker
    ct_widget
    curve_plot
    colormap_widget
    delegates
    extended_dock_widget
    eye_check_box
    holder_widget
    image_plot
    live_window
    mca_plot
    one_dim_data_plot
    positioners_widget
    _property_tree_helper
    property_widget
    roi_selection_widget
    scan_status
    scatter_plot
    state_indicator
    viewer
"""

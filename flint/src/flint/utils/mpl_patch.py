"""Monkey-patch matplotlib to workaround memory leak"""

import functools
import logging
from matplotlib.ft2font import LOAD_NO_HINTING
from matplotlib import _api, _mathtext, mathtext
from matplotlib.font_manager import FontProperties

_logger = logging.getLogger(__name__)


@functools.lru_cache(100)
def _get_fontset(prop, output_type):
    from matplotlib.backends import backend_agg

    fontset_class = _api.check_getitem(
        MathTextParser._font_type_mapping, fontset=prop.get_math_fontfamily()
    )
    load_glyph_flags = {
        "vector": LOAD_NO_HINTING,
        "raster": backend_agg.get_hinting_flag(),
    }[output_type]
    return fontset_class(prop, load_glyph_flags)


class MathTextParser(mathtext.MathTextParser):
    """Override method to use cached fonset"""

    @functools.lru_cache(50)
    def _parse_cached(self, s, dpi, prop, antialiased):
        if prop is None:
            prop = FontProperties()
        fontset = _get_fontset(prop, self._output_type)

        # from matplotlib.backends import backend_agg
        # fontset_class = _api.check_getitem(
        #    self._font_type_mapping, fontset=prop.get_math_fontfamily())
        # load_glyph_flags = {
        #    "vector": LOAD_NO_HINTING,
        #    "raster": backend_agg.get_hinting_flag(),
        # }[self._output_type]
        # fontset = fontset_class(prop, load_glyph_flags)

        fontsize = prop.get_size_in_points()

        if self._parser is None:  # Cache the parser globally.
            self.__class__._parser = _mathtext.Parser()

        box = self._parser.parse(s, fontset, fontsize, dpi)
        output = _mathtext.ship(box)
        if self._output_type == "vector":
            return output.to_vector()
        elif self._output_type == "raster":
            return output.to_raster(antialiased=antialiased)


def patch_mathtext_memory_leak():
    """
    Fixes memory leak for matplotlib 3.9.2

    See https://gitlab.esrf.fr/bliss/bliss/-/issues/4539
    """
    if mathtext.MathTextParser is MathTextParser:
        return
    mathtext.MathTextParser = MathTextParser
    _logger.warning("matplotlib patched agaist mathtext memory leak")

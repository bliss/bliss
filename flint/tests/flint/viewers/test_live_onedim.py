"""Testing onedim plot."""

import pytest
import numpy
import weakref

from silx.gui import qt

from flint.viewers.live_onedim.viewer import OneDimDataPlotWidget
from flint.model import scan_model
from flint.model import flint_model
from flint.model import plot_model
from flint.model import plot_item_model
from flint.helper import style_helper


@pytest.fixture
def onedim_widget(local_flint, silxTestUtils):
    widget = OneDimDataPlotWidget()
    widget.setAttribute(qt.Qt.WA_DeleteOnClose)
    widget.show()
    yield widget

    widget.close()
    w = weakref.ref(widget)
    widget = None
    silxTestUtils.qWaitForDestroy(w)


def nbItems(widget: OneDimDataPlotWidget):
    silxPlot = widget._silxPlot()
    return len(silxPlot.getItems()) - 2


def create_scan():
    scan = scan_model.Scan()
    master = scan_model.Device(scan)
    master.setName("master")
    device = scan_model.Device(scan)
    device.setName("device")
    device.setMaster(master)
    channel = scan_model.Channel(device)
    channel.setName("chan1")
    channel.setType(scan_model.ChannelType.VECTOR)
    channel = scan_model.Channel(device)
    channel.setName("chan2")
    channel.setType(scan_model.ChannelType.VECTOR)
    scan.seal()
    return scan


def create_flint_model():
    flint = flint_model.FlintState()
    return flint


def create_plot_with_chan1():
    plot = plot_item_model.OneDimDataPlot()
    item = plot_item_model.CurveItem(plot)
    xchannel = plot_model.XIndexChannelRef(plot)
    ychannel = plot_model.ChannelRef(plot, "chan1")
    item.setYChannel(ychannel)
    item.setXChannel(xchannel)
    plot.addItem(item)
    flint = create_flint_model()
    styleStrategy = style_helper.DefaultStyleStrategy(flint)
    plot.setStyleStrategy(styleStrategy)
    return plot


def test_display_nothing(local_flint, onedim_widget, silxTestUtils):
    """Create a plot without scan"""
    silxTestUtils.allowedLeakingWidgets = 65
    plot = create_plot_with_chan1()
    flint = create_flint_model()

    onedim_widget.setFlintModel(flint)
    onedim_widget.setPlotModel(plot)
    onedim_widget.show()

    assert nbItems(onedim_widget) == 0


def test_display_data(local_flint, onedim_widget, silxTestUtils):
    silxTestUtils.allowedLeakingWidgets = 65
    # Create a plot with already existing data
    scan = create_scan()
    plot = create_plot_with_chan1()
    flint = create_flint_model()

    array = numpy.arange(4)
    data = scan_model.Data(scan, array)
    scan.getChannelByName("chan1").setData(data)

    onedim_widget.setFlintModel(flint)
    onedim_widget.setScan(scan)
    onedim_widget.setPlotModel(plot)

    silxTestUtils.qWait(1000)
    assert nbItems(onedim_widget) == 1

from __future__ import annotations

import sys
import os.path
import ruamel.yaml
from ..factory import ScanInfoFactory


_ALREADY_VALIDATED: dict[str, bool] = {}


def load_scan_info(resource: str) -> dict:
    filename = os.path.abspath(f"{__file__}/../../scans_description/{resource}")
    yaml = ruamel.yaml.YAML()
    with open(filename, "rt") as stream:
        info = yaml.load(stream)  # nosec

    major, minor, _micro = sys.version_info[:3]
    if (major, minor) == (3, 8):
        # Skip pydantic checks
        return info

    validated = _ALREADY_VALIDATED.get(resource, None)
    if validated is False:
        raise ValueError(f"Resource {resource} is not a valid BLISS scan info")

    if validated is None:
        from bliss.scanning.scan_info import ScanInfo

        scan_info = ScanInfo.normalize(info)
        try:
            scan_info.validate()
        except Exception:
            _ALREADY_VALIDATED[resource] = False
            raise
        else:
            _ALREADY_VALIDATED[resource] = True
    return info


def create_loopscan_scan_info(diode1=True, diode2=True):
    factory = ScanInfoFactory()
    factory.add_device(root_id="timer", device_id="timer")
    factory.add_channel(channel_id="timer:elapsed_time", dim=0, unit="s")
    factory.add_channel(channel_id="timer:epoch", dim=0, unit="s")
    factory.add_device(root_id="timer", device_id="diode", triggered_by="timer")
    if diode1:
        factory.add_channel(channel_id="diode:diode1", dim=0)
    if diode2:
        factory.add_channel(channel_id="diode:diode2", dim=0)
    factory["type"] = "loopscan"
    return factory.scan_info()


def create_ascan_scan_info(master_name, extra_name=None):
    factory = ScanInfoFactory()
    factory.add_device(root_id="ascan", device_id="master")
    factory.add_channel(channel_id=master_name, device_id="master", dim=0)

    factory.add_device(root_id="ascan", device_id="slave", triggered_by="master")
    factory.add_channel(
        channel_id="timer:elapsed_time", device_id="slave", dim=0, unit="s"
    )
    factory.add_channel(channel_id="timer:epoch", device_id="slave", dim=0, unit="s")
    factory.add_channel(channel_id="diode:diode1", device_id="slave", dim=0)
    factory.add_channel(channel_id="diode:diode2", device_id="slave", dim=0)
    if extra_name is not None:
        factory.add_channel(channel_id=extra_name, device_id="master", dim=0)
    factory["type"] = "ascan"
    return factory.scan_info()


def create_mesh_scan_info(setected_diode):
    factory = ScanInfoFactory()
    factory.add_device(root_id="timer", device_id="timer")
    factory.add_channel(channel_id="timer:elapsed_time", dim=0, unit="s")
    factory.add_channel(channel_id="timer:epoch", dim=0, unit="s")
    factory.add_device(root_id="timer", device_id="axis", triggered_by="timer")
    factory.add_channel(channel_id="axis:sx", dim=0)
    factory.add_channel(channel_id="axis:sy", dim=0)
    factory.add_channel(channel_id="axis:sxenc", dim=0)
    factory.add_channel(channel_id="axis:syenc", dim=0)
    factory.add_device(root_id="timer", device_id="diode", triggered_by="timer")
    factory.add_channel(channel_id="diode:diode1", dim=0)
    factory.add_channel(channel_id="diode:diode2", dim=0)
    factory["type"] = "mesh"
    factory["plots"] = [
        {
            "items": [
                {
                    "kind": "scatter",
                    "x": "axis:sx",
                    "y": "axis:sy",
                    "value": setected_diode,
                }
            ],
            "kind": "scatter-plot",
        }
    ]
    return factory.scan_info()


def create_lima_scan_info(include_roi2):
    """
    Simulate a scan containing a lima detector with ROIs.
    """
    factory = ScanInfoFactory()
    factory.add_device(root_id="timer", device_id="timer")
    factory.add_channel(channel_id="timer:elapsed_time", dim=0)
    factory.add_channel(channel_id="timer:epoch", dim=0)

    rois = {"roi1": {"kind": "rect", "x": 190, "y": 110, "width": 600, "height": 230}}
    if include_roi2:
        rois["roi2"] = {
            "kind": "arc",
            "cx": 487.0,
            "cy": 513.0,
            "r1": 137.0,
            "r2": 198.0,
            "a1": -172.0,
            "a2": -300.0,
        }
    factory.add_lima_device(
        device_id="beamviewer",
        root_id="timer",
        triggered_by="timer",
        image=True,
        rois=rois,
    )
    factory.add_channel(channel_id="beamviewer:roi_counters:roi1_sum", dim=0)
    if include_roi2:
        factory.add_channel(channel_id="beamviewer:roi_counters:roi2_sum", dim=0)

    scan_info = factory.scan_info()
    return scan_info

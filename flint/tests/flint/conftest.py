# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest


@pytest.fixture
def local_flint(qapp):
    """Registed expected things"""
    from flint import resources

    resources.silx_integration()
    yield

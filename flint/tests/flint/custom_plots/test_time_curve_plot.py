"""Testing time curve plot."""

import pytest

from silx.gui import qt

from flint.viewers.custom_time_curve import viewer
from flint.viewers.custom_time_curve.time_curve_item import TimeCurveItem


@pytest.fixture(scope="function")
def time_curve_plot_widget(local_flint):
    w = viewer.TimeCurvePlot()
    w.setAttribute(qt.Qt.WA_DeleteOnClose)
    w.setVisible(True)
    yield w
    w.close()


def test_time_curve_plot__creation(time_curve_plot_widget):
    """The widget can be implemented"""
    pass


def test_time_curve_plot__set_data(time_curve_plot_widget):
    """The setData of the widget can be used

    We expect curves to be displayed
    """
    w = time_curve_plot_widget
    w.addTimeCurveItem("value1")
    w.addTimeCurveItem("value2")
    w.setData(
        time=[0, 1, 2, 3, 4, 5], value1=[0, 1, 2, 3, 4, 5], value2=[0, 1, 2, 3, 4, 5]
    )
    plot = w.getPlotWidget()
    assert len(plot.getAllCurves()) == 2


def test_time_curve_plot__clear(time_curve_plot_widget):
    """Feed a plot and call `clear`

    We expect no curves to be displayed
    """
    w = time_curve_plot_widget
    w.addTimeCurveItem("value1")
    w.addTimeCurveItem("value2")
    w.setData(
        time=[0, 1, 2, 3, 4, 5], value1=[0, 1, 2, 3, 4, 5], value2=[0, 1, 2, 3, 4, 5]
    )
    w.clear()
    plot = w.getPlotWidget()
    assert len(plot.getAllCurves()) == 0


def test_time_curve_plot__append_data(time_curve_plot_widget):
    """Create a plot and feed data with `appendData`

    We expect the plot to contains curves witch grow up.
    """
    w = time_curve_plot_widget
    w.addTimeCurveItem("value1")
    w.addTimeCurveItem("value2")
    w.appendData(time=[0, 1, 2], value1=[0, 1, 2], value2=[0, 1, 2])
    plot = w.getPlotWidget()
    curve = plot.getAllCurves()[0]
    assert len(curve.getXData()) == 3
    w.appendData(time=[3, 4, 5], value1=[0, 1, 2], value2=[0, 1, 2])
    curve = plot.getAllCurves()[0]
    assert len(curve.getXData()) == 6


def test_time_curve_plot__drop_data(time_curve_plot_widget):
    """Create a plot with limited life time data duration and feed it with data

    We expect the plot to contain curves witch grow up on one side, and disappear
    on the other side.
    """
    w = time_curve_plot_widget
    w.setTtl(5)
    w.addTimeCurveItem("value1")
    w.addTimeCurveItem("value2")
    w.appendData(time=[0, 1, 2], value1=[0, 1, 2], value2=[0, 1, 2])
    w.appendData(time=[3, 4, 5], value1=[0, 1, 2], value2=[0, 1, 2])
    w.appendData(time=[6, 7, 8], value1=[0, 1, 2], value2=[0, 1, 2])
    plot = w.getPlotWidget()
    curve = plot.getAllCurves()[0]
    assert len(curve.getXData()) <= 5 + 2
    assert curve.getXData()[0] > 0
    assert curve.getXData()[-1] == 8


def test_time_curve_plot__limited_view(time_curve_plot_widget):
    """Create a plot with limited x-axis duration and feed it with data

    We expect the plot to contain more items data than what it is displayed.
    """
    w = time_curve_plot_widget
    w.setXDuration(5)
    w.setTtl(10)
    w.addTimeCurveItem("value1")
    w.addTimeCurveItem("value2")
    w.appendData(time=[0, 1, 2], value1=[0, 1, 2], value2=[0, 1, 2])
    w.appendData(time=[3, 4, 5], value1=[0, 1, 2], value2=[0, 1, 2])
    w.appendData(time=[6, 7, 8], value1=[0, 1, 2], value2=[0, 1, 2])
    plot = w.getPlotWidget()
    xAxis = plot.getXAxis()
    assert xAxis.getLimits()[0] > 0
    assert xAxis.getLimits()[1] == 8.0
    curve = plot.getAllCurves()[0]
    assert len(curve.getXData()) == 9
    assert curve.getXData()[0] == 0
    assert curve.getXData()[-1] == 8


def test_time_curve_item__bounds(qapp):
    item = TimeCurveItem()
    item.setData([0, 1, 2], [0, 1, 2])
    assert item.getBounds() == (0, 2, 0, 2)


def test_time_curve_item__modified_bounds(qapp):
    item = TimeCurveItem()
    item.firstDisplayedIndex = 1
    item.setData([0, 1, 2], [0, 1, 2])
    assert item.getBounds() == (1, 2, 1, 2)

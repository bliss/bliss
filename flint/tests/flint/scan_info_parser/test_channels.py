"""Testing scan info parser module."""

from flint.scan_info_parser.channels import iter_channels, ChannelInfo
from flint.scan_info_parser.channels import parse_channel_metadata
from flint.model import scan_model
from ..factory import ScanInfoFactory


def test_iter_channels():
    factory = ScanInfoFactory()
    factory.add_device(root_id="timer", device_id="timer")
    factory.add_device(root_id="timer2", device_id="timer2")
    factory.add_channel(channel_id="timer:elapsed_time", dim=0)
    factory.add_channel(channel_id="timer:epoch", dim=0)
    factory.add_device(root_id="timer", device_id="diode", triggered_by="timer")
    factory.add_device(root_id="timer2", device_id="opium", triggered_by="timer2")
    factory.add_device(root_id="timer2", device_id="lima", triggered_by="timer2")
    factory.add_channel(channel_id="diode:diode", dim=0)
    factory.add_channel(channel_id="opium:mca1", dim=1)
    factory.add_channel(channel_id="lima:image1", dim=2)
    scan_info = factory.scan_info()

    result = iter_channels(scan_info)
    result = [ChannelInfo(r.name, None, r.device, r.master) for r in result]
    expected = [
        ChannelInfo("diode:diode", None, "diode", "timer"),
        ChannelInfo("timer:elapsed_time", None, "timer", "timer"),
        ChannelInfo("timer:epoch", None, "timer", "timer"),
        ChannelInfo("opium:mca1", None, "opium", "timer2"),
        ChannelInfo("lima:image1", None, "lima", "timer2"),
    ]
    assert set(result) == set(expected)


def test_parse_channel_metadata__bliss_1_4():
    meta = {
        "start": 1,
        "stop": 2,
        "min": 3,
        "max": 4,
        "points": 5,
        "axis_points": 6,
        "axis_kind": "slow",
    }
    result = parse_channel_metadata(meta)
    expected = scan_model.ChannelMetadata(
        1, 2, 3, 4, 5, 1, 6, scan_model.AxisKind.FORTH, None, None, None, None
    )
    assert result == expected


def test_parse_channel_metadata():
    meta = {
        "start": 1,
        "stop": 2,
        "min": 3,
        "max": 4,
        "points": 5,
        "axis_id": 0,
        "axis_points": 6,
        "axis_kind": "backnforth",
        "dim": 3,
    }
    result = parse_channel_metadata(meta)
    expected = scan_model.ChannelMetadata(
        1, 2, 3, 4, 5, 0, 6, scan_model.AxisKind.BACKNFORTH, None, None, 3, None
    )
    assert result == expected


def test_parse_wrong_values():
    meta = {
        "start": 1,
        "stop": 2,
        "min": 3,
        "max": "foo",
        "points": 5,
        "axis_points": 6,
        "axis_kind": "foo",
        "foo": "bar",
    }
    result = parse_channel_metadata(meta)
    expected = scan_model.ChannelMetadata(
        1, 2, 3, None, 5, None, 6, None, None, None, None, None
    )
    assert result == expected

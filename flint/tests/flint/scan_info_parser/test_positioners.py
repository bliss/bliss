"""Testing scan info parser module."""

from flint.scan_info_parser.positioners import (
    get_all_positioners,
    PositionerDescription,
)

SCAN_INFO_POSITIONERS = {
    "positioners": {
        "positioners_start": {"slit_bottom": 1.0, "slit_top": -1.0},
        "positioners_end": {"slit_bottom": 2.0, "slit_top": -2.0},
        "positioners_dial_start": {"slit_bottom": 3.0, "slit_top": -3.0},
        "positioners_dial_end": {"slit_bottom": 4.0},
        "positioners_units": {"slit_bottom": "mm", "slit_top": None, "slit_foo": None},
    }
}


def test_get_all_positioners():
    positioners = get_all_positioners(SCAN_INFO_POSITIONERS)
    assert len(positioners) == 3
    assert positioners[0] == PositionerDescription("slit_bottom", 1, 2, 3, 4, "mm")
    assert positioners[1] == PositionerDescription("slit_top", -1, -2, -3, None, None)
    assert positioners[2] == PositionerDescription(
        "slit_foo", None, None, None, None, None
    )

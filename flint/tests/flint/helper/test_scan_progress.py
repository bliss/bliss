"""Testing scan progress module."""

import numpy
from flint.model import scan_model
from flint.scan_info_parser.scans import create_scan_model
from flint.helper import scan_progress
from bliss.scanning.scan_info import ScanInfo as BlissScanInfo


def test_progress_percent_curve():
    scan_info = {
        "npoints": 10,
        "acquisition_chain": {"main": {"top_master": "main", "devices": ["main"]}},
        "devices": {"main": {"name": "main", "channels": ["axis:roby"]}},
        "channels": {"axis:roby": {"dim": 0}},
    }
    scan = create_scan_model(scan_info)
    channel = scan.getChannelByName("axis:roby")
    res = scan_progress.get_scan_progress_percent(scan)
    assert res == 0.0

    data = scan_model.Data(scan, numpy.arange(5))
    channel.setData(data)
    res = scan_progress.get_scan_progress_percent(scan)
    assert res == 0.5

    data = scan_model.Data(scan, numpy.arange(10))
    channel.setData(data)
    res = scan_progress.get_scan_progress_percent(scan)
    assert res == 1.0


def test_progress_percent_scatter():
    scan_info = {
        "npoints1": 2,
        "npoints2": 5,
        "acquisition_chain": {"main": {"top_master": "main", "devices": ["main"]}},
        "devices": {"main": {"name": "main", "channels": ["axis:roby"]}},
        "channels": {"axis:roby": {"dim": 0}},
    }
    scan = create_scan_model(scan_info)
    channel = scan.getChannelByName("axis:roby")
    res = scan_progress.get_scan_progress_percent(scan)
    assert res == 0.0

    data = scan_model.Data(scan, numpy.arange(5))
    channel.setData(data)
    res = scan_progress.get_scan_progress_percent(scan)
    assert res == 0.5

    data = scan_model.Data(scan, numpy.arange(10))
    channel.setData(data)
    res = scan_progress.get_scan_progress_percent(scan)
    assert res == 1.0


def test_progress_percent_image():
    scan_info = {
        "npoints": 10,
        "acquisition_chain": {"main": {"top_master": "main", "devices": ["main"]}},
        "devices": {"main": {"name": "main", "channels": ["axis:roby"]}},
        "channels": {"axis:roby": {"dim": 2}},
    }
    scan = create_scan_model(scan_info)
    channel = scan.getChannelByName("axis:roby")
    res = scan_progress.get_scan_progress_percent(scan)
    assert res == 0.0

    image = numpy.arange(4).reshape(2, 2)
    data = scan_model.Data(scan, image, frameId=0)
    channel.setData(data)
    res = scan_progress.get_scan_progress_percent(scan)
    assert res == 0.1

    data = scan_model.Data(scan, image, frameId=9)
    channel.setData(data)
    res = scan_progress.get_scan_progress_percent(scan)
    assert res == 1.0


def test_progress_on_sequence():
    scan_info = BlissScanInfo()
    scan_info.set_sequence_info(scan_count=10)
    scan_info["is_scan_sequence"] = True
    sequence = create_scan_model(scan_info)

    scan_info2 = BlissScanInfo()
    scan_info2.set_scan_index(index_in_sequence=0, retry_nb=0)
    scan = create_scan_model(scan_info2)
    sequence.addSubScan(scan)

    assert scan_progress.get_scan_progress_percent(sequence) == 0.0
    scan._setState(scan_model.ScanState.FINISHED)
    assert scan_progress.get_scan_progress_percent(sequence) == 0.1


def test_progress_on_retry():
    scan_info = BlissScanInfo()
    scan_info.set_sequence_info(scan_count=10)
    scan_info["is_scan_sequence"] = True
    sequence = create_scan_model(scan_info)

    scan_info2 = BlissScanInfo()
    scan_info2.set_scan_index(index_in_sequence=0, retry_nb=0)
    scan = create_scan_model(scan_info2)
    scan._setState(scan_model.ScanState.FINISHED)
    sequence.addSubScan(scan)

    assert scan_progress.get_scan_progress_percent(sequence) == 0.1

    scan_info3 = BlissScanInfo()
    scan_info3.set_scan_index(index_in_sequence=0, retry_nb=1)
    scan = create_scan_model(scan_info3)
    scan._setState(scan_model.ScanState.FINISHED)
    sequence.addSubScan(scan)

    assert scan_progress.get_scan_progress_percent(sequence) == 0.1

    scan_info4 = BlissScanInfo()
    scan_info4.set_scan_index(index_in_sequence=1, retry_nb=0)
    scan = create_scan_model(scan_info4)
    scan._setState(scan_model.ScanState.FINISHED)
    sequence.addSubScan(scan)

    assert scan_progress.get_scan_progress_percent(sequence) == 0.2

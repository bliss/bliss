"""Testing style helper module."""

import itertools
from flint.helper import style_helper
from flint.model import scan_model
from flint.model import plot_model
from flint.model import plot_item_model
from flint.filters.negative import NegativeItem


def create_curve_item(plot, channel_name_x, channel_name_y):
    item = plot_item_model.CurveItem(plot)
    channel = plot_model.ChannelRef(plot, channel_name_x)
    item.setXChannel(channel)
    channel = plot_model.ChannelRef(plot, channel_name_y)
    item.setYChannel(channel)
    plot.addItem(item)
    return item


def create_sub_item(plot, source_item):
    item = NegativeItem()
    item.setSource(source_item)
    plot.addItem(item)
    return item


def styles_from_items_scans(style, items, scans):
    couples = itertools.product(items, scans)
    return [style.getStyleFromItem(c[0], c[1]) for c in couples]


def test_curve_plot__single_scan(local_flint):
    scan1 = scan_model.Scan()
    plot = plot_item_model.CurvePlot()
    item1 = create_curve_item(plot, "x", "y1")
    item2 = create_curve_item(plot, "x", "y2")
    item3 = create_curve_item(plot, "x", "y3")

    style = style_helper.DefaultStyleStrategy()
    style.setPlot(plot)
    style.setScans([scan1])
    styles1 = styles_from_items_scans(style, [item1, item2, item3], [scan1])
    colors = [s.lineColor for s in styles1]
    assert len(set(colors)) == 3


def test_curve_plot__single_scan_with_subitem(local_flint):
    """Create a plot with item and sub items.

    Only the main items use different colors.
    """
    scan1 = scan_model.Scan()
    plot = plot_item_model.CurvePlot()
    item1 = create_curve_item(plot, "x", "y1")
    item2 = create_curve_item(plot, "x", "y2")
    item3 = create_sub_item(plot, item2)

    style = style_helper.DefaultStyleStrategy()
    style.setPlot(plot)
    style.setScans([scan1])
    styles1 = styles_from_items_scans(style, [item1, item2, item3], [scan1])
    colors = [s.lineColor for s in styles1]
    assert len(set(colors)) == 2
    colors = [s.lineColor for s in styles1[1:]]
    assert len(set(colors)) == 1


def test_curve_plot__single_item_multi_sub_items(local_flint):
    """Create a plot with a single main item.

    Each items have to pick it's own color.
    """
    scan1 = scan_model.Scan()
    plot = plot_item_model.CurvePlot()
    item1 = create_curve_item(plot, "x", "y1")
    item2 = create_sub_item(plot, item1)
    item3 = create_sub_item(plot, item1)

    style = style_helper.DefaultStyleStrategy()
    style.setPlot(plot)
    style.setScans([scan1])
    styles1 = styles_from_items_scans(style, [item1, item2, item3], [scan1])
    colors = [s.lineColor for s in styles1]
    assert len(set(colors)) == 3


def test_curve_plot__multi_scans(local_flint):
    """Create plot.

    The first scan is normal, others are greyed.
    """
    scan1 = scan_model.Scan()
    scan2 = scan_model.Scan()
    plot = plot_item_model.CurvePlot()
    item1 = create_curve_item(plot, "x", "y1")
    item2 = create_curve_item(plot, "x", "y3")
    item3 = create_sub_item(plot, item1)

    style = style_helper.DefaultStyleStrategy()
    style.setPlot(plot)
    style.setScans([scan1, scan2])
    styles1 = styles_from_items_scans(style, [item1, item2, item3], [scan1])
    styles2 = styles_from_items_scans(style, [item1, item2, item3], [scan2])
    colors1 = [s.lineColor for s in styles1]
    assert len(set(colors1)) == 2
    colors2 = [s.lineColor for s in styles2]
    assert len(set(colors2)) == 1
    assert colors2[0] not in colors1


def test_curve_plot__multi_scans_single_item(local_flint):
    """Create plot with single item.

    Each scans will use a dedicated color.
    """
    scan1 = scan_model.Scan()
    scan1.setScanInfo({"scan_nb": 1})
    scan2 = scan_model.Scan()
    scan2.setScanInfo({"scan_nb": 2})
    scan3 = scan_model.Scan()
    scan3.setScanInfo({"scan_nb": 3})
    plot = plot_item_model.CurvePlot()
    item1 = create_curve_item(plot, "x", "y1")
    item2 = create_sub_item(plot, item1)

    style = style_helper.DefaultStyleStrategy()
    style.setPlot(plot)
    style.setScans([scan1, scan2, scan3])
    styles1 = styles_from_items_scans(style, [item1], [scan1, scan2, scan3])
    styles2 = styles_from_items_scans(style, [item2], [scan1, scan2, scan3])
    colors1 = [s.lineColor for s in styles1]
    colors2 = [s.lineColor for s in styles2]
    assert len(set(colors1)) == 3
    assert colors1 == colors2


def test_curve_plot__multi_scans_on_epoch(local_flint):
    """Create plot with epoch as x-axis.

    An item have the same style on any scans.
    """
    scan1 = scan_model.Scan()
    scan2 = scan_model.Scan()
    plot = plot_item_model.CurvePlot()
    item1 = create_curve_item(plot, "time:epoch", "y1")
    item2 = create_curve_item(plot, "time:epoch", "y3")

    style = style_helper.DefaultStyleStrategy()
    style.setPlot(plot)
    style.setScans([scan1, scan2])
    styles = styles_from_items_scans(style, [item1, item2], [scan1, scan2])
    colors = [s.lineColor for s in styles]
    assert len(set(colors)) == 2

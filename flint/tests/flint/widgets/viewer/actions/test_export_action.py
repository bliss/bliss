"""Testing for profile action"""

import numpy
from flint.model import plot_item_model
from flint.model import plot_model
from flint.model import scan_model
from flint.filters.derivative import DerivativeItem
from flint.filters.max import MaxCurveItem
from flint.widgets.viewer.actions import export_action


def test_export_csv(tmp_path):
    plot = plot_item_model.CurvePlot()
    plot.setScansStored(True)

    item = plot_item_model.CurveItem(plot)
    item.setXChannel(plot_model.ChannelRef(None, "x"))
    item.setYChannel(plot_model.ChannelRef(None, "y"))
    plot.addItem(item)

    item2 = DerivativeItem(plot)
    item2.setYAxis("right")
    item2.setSource(item)
    plot.addItem(item2)

    item3 = MaxCurveItem(plot)
    item3.setSource(item2)

    scan = scan_model.Scan()
    device = scan_model.Device(scan)
    channel = scan_model.Channel(device)
    channel.setType(scan_model.ChannelType.COUNTER)
    channel.setName("y")
    data = scan_model.Data(scan, numpy.arange(6) * 10)
    channel.setData(data)
    channel = scan_model.Channel(device)
    channel.setName("x")
    channel.setType(scan_model.ChannelType.COUNTER)
    data = scan_model.Data(scan, numpy.arange(5))
    channel.setData(data)
    scan.seal()

    filename = tmp_path / "data.csv"
    export_action.export_plot_as_csv(plot, scan, filename=filename)

    with open(filename, "rt") as f:
        txt = f.read()
    assert txt.startswith("x\ty\n")
    assert "2\t20\n" in txt
    assert txt.endswith("4\t40\n")

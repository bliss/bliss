"""Testing image plot."""

from flint.viewers.custom_plot1d import viewer


def test_customplot_logbook_send_data(local_flint):
    class IcatClientMockup:
        def __init__(self):
            self.data = None
            self.mimetype = None

        def send_binary_data(self, data: bytes, mimetype: str):
            self.data = data
            self.mimetype = mimetype

    plot = viewer.Plot1D()
    icatClient = IcatClientMockup()
    plot.exportToLogbook(icatClient)

    assert b"PNG" in icatClient.data
    assert icatClient.mimetype == "image/png"

    plot.deleteLater()

"""Testing image plot."""

import pytest
import os
from unittest.mock import MagicMock
from silx.gui.plot import PlotWindow
from flint.model import flint_model
from flint.manager.manager import ManageMainBehaviours
from flint.widgets.viewer.actions import export_action
from bliss.icat.client import DatasetId


def create_flint_model():
    flint = flint_model.FlintState()
    return flint


def create_manager():
    manager = ManageMainBehaviours()
    flintModel = create_flint_model()
    manager.setFlintModel(flintModel)
    return manager


@pytest.fixture
def icat_mock_client(mocker) -> MagicMock:
    """Enables the ICAT client and Elogbook for any Bliss session.
    The retured object can be used to check that all desired calls are being made."""
    mockedclass = mocker.patch("bliss.icat.client.IcatClient")

    datasetids: list[DatasetId] = []

    def store_dataset(path=None, **_):
        nonlocal datasetids
        datasetids.append(DatasetId(name=os.path.basename(path), path=path))

    def registered_dataset_ids(**_):
        nonlocal datasetids
        return None if datasetids == [] else datasetids

    mockedclass.return_value.store_dataset.side_effect = store_dataset

    mockedclass.return_value.registered_dataset_ids.side_effect = registered_dataset_ids

    return mockedclass


def test_logbook(local_flint, icat_mock_client):
    manager = create_manager()
    plot = PlotWindow()
    action = export_action.ExportToLogBookAction(plot, plot)
    action.setFlintModel(manager.flintModel())

    # The action is not available
    assert not action.isEnabled()

    # The action is now available
    kwargs = dict()
    kwargs["proposal"] = "id001234"
    kwargs["beamline"] = "id00"
    config = {"disable": False, "kwargs": kwargs}
    manager.setIcatClientConfig(config)
    assert action.isEnabled()

    # The action can use it
    action.trigger()

    action.deleteLater()
    plot.deleteLater()

    icat_mock_client.assert_called_once_with(beamline="id00", proposal="id001234")
    icat_mock_client.return_value.send_binary_data.assert_called_once()
    kwargs = icat_mock_client.return_value.send_binary_data.call_args[-1]
    assert {"data", "mimetype"} == set(kwargs)
    assert kwargs["mimetype"] == "image/png"
    assert b"PNG" in kwargs["data"]


def test_logbook_send_data(local_flint):
    class IcatClientMockup:
        def __init__(self):
            self.data = None
            self.mimetype = None

        def send_binary_data(self, data: bytes, mimetype: str):
            self.data = data
            self.mimetype = mimetype

    model = create_flint_model()
    client = IcatClientMockup()
    model.setIcatClient(client)
    plot = PlotWindow()
    action = export_action.ExportToLogBookAction(plot, plot)
    action.setFlintModel(model)

    assert action.isEnabled()
    action.trigger()

    assert b"PNG" in client.data
    assert client.mimetype == "image/png"

    action.deleteLater()
    plot.deleteLater()

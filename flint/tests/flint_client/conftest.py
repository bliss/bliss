import pytest
from flint.model import flint_model


def _get_real_flint(*args, **kwargs):
    """Replacement function for monkey patch of `bliss.common.plot`"""
    from flint import flint
    from silx.gui import qt
    from flint.client.proxy import FlintClient
    from flint.flint_api import FlintApi

    options = flint.parse_options([])
    settings = qt.QSettings()

    flint.initApplication([], options, settings)
    settings = qt.QSettings()
    flintModel = flint.create_flint_model(settings)

    liveWindow = flintModel.liveWindow()
    newWorkspace = flint_model.Workspace()
    liveWindow.feedDefaultWorkspace(flintModel, newWorkspace)
    liveWindow.updateFromWorkspace(newWorkspace)
    flintModel.setWorkspace(newWorkspace)
    flintApi = FlintApi(flintModel)
    flintModel.setFlintApi(flintApi)

    class FlintClientMock(FlintClient):
        def __init__(self):
            FlintClient.__init__(self, None)
            self._proxy = flintModel.flintApi()
            self._pid = -666
            self._old_run_method = self._proxy.run_method
            self._proxy.run_method = self._run_method

        def _run_method(self, plot_id, method, args, kwargs):
            result = self._old_run_method(plot_id, method, list(args), kwargs)

            def tuple_to_list(value):
                if isinstance(value, tuple):
                    return [tuple_to_list(v) for v in value]
                return value

            return tuple_to_list(result)

    flint_client = FlintClientMock()
    return flint_client


@pytest.fixture
def local_flint_client(qapp):
    return _get_real_flint()


@pytest.fixture
def rpc_flint_client(mocker):
    from bliss.common import plot

    session = mocker.Mock()
    session.name = "test"
    mocker.patch("flint.client.proxy.current_session", session)

    flint_client = plot.get_flint()
    return flint_client


@pytest.fixture
def flint_client(local_flint_client):
    return local_flint_client

"""Testing custom plots provided by Flint."""

import numpy


def test_image_view(flint_client):
    f = flint_client
    p = f.get_plot(plot_class="imageview", name="imageview")

    # Check the default data setter
    image = numpy.arange(10 * 10)
    image.shape = 10, 10
    p.set_data(image)
    vrange = p.get_data_range()
    assert vrange[0:2] == [[0, 10], [0, 10]]

    # Allow to setup the colormap
    p.set_colormap(lut="viridis", vmin=0, vmax=10)

    # Set none can be use to clear the data
    p.set_data(None)
    vrange = p.get_data_range()
    assert vrange[0:2] == [None, None]

    # Check deprecated API
    image = numpy.arange(9 * 9)
    image.shape = 9, 9
    p.add_data(image, field="image")
    p.select_data("image")
    vrange = p.get_data_range()
    assert vrange[0:2] == [[0, 9], [0, 9]]

    # Check the default way to clear data
    p.clear_data()
    vrange = p.get_data_range()
    assert vrange[0:2] == [None, None]

    assert p.yaxis_direction == "up"
    p.yaxis_direction = "down"
    assert p.yaxis_direction == "down"
    p.yaxis_direction = "up"
    assert p.yaxis_direction == "up"

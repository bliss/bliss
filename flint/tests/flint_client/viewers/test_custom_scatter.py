"""Testing custom plots provided by Flint."""

import numpy


def test_scatter_view(flint_client):
    f = flint_client
    p = f.get_plot(plot_class="scatter", name="scatterview")

    # Check the default data setter
    x = numpy.arange(11)
    y = numpy.arange(11)
    value = numpy.arange(11)
    p.set_data(x=x, y=y, value=value)
    vrange = p.get_data_range()
    assert vrange[0:2] == [[0, 10], [0, 10]]

    # Allow to setup the colormap
    p.set_colormap(lut="viridis", vmin=0, vmax=10)

    # Set none can be use to clear the data
    p.set_data(None, None, None)
    vrange = p.get_data_range()
    assert vrange[0:2] == [None, None]

    # Check deprecated API
    x = numpy.arange(9)
    y = numpy.arange(9)
    value = numpy.arange(9)
    p.add_data(x, field="x")
    p.add_data(y, field="y")
    p.add_data(value, field="value")
    p.select_data("x", "y", "value")
    vrange = p.get_data_range()
    assert vrange[0:2] == [[0, 8], [0, 8]]

    # Check the default way to clear data
    p.clear_data()
    vrange = p.get_data_range()
    assert vrange[0:2] == [None, None]

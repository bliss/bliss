"""Testing custom plots provided by Flint."""

import numpy


def test_curve_stack(flint_client):
    f = flint_client
    p = f.get_plot(plot_class="curvestack", name="curve-stack")

    curves = numpy.empty((10, 100))
    for i in range(10):
        curves[i] = numpy.sin(numpy.arange(100) / 30 + i * 6)
    x = numpy.arange(100) * 10

    p.set_data(curves=curves, x=x)
    vrange = p.get_data_range()
    assert vrange[0] == [0, 990]

    p.clear_data()
    vrange = p.get_data_range()
    assert vrange == [None, None]

"""Testing custom plots provided by Flint."""

import pytest
import gevent
import numpy
from bliss.controllers.lima import roi as lima_roi


def test_curveplot__create_empty(flint_client):
    flint = flint_client
    p = flint.get_plot(plot_class="curve", name="foo-empty", unique_name="foo-empty")
    assert flint.plot_exists("foo-empty")
    assert p is not None


def test_curveplot__bliss_1_8(flint_client):
    """Check custom plot curve API from BLISS <= 1.8"""
    flint = flint_client
    p = flint.get_plot(plot_class="curve", name="bliss-1.8")

    data1 = numpy.array([4, 5, 6])
    data2 = numpy.array([2, 5, 2])

    p.add_data({"data1": data1, "data2": data2})
    vrange = p.get_data_range()
    assert vrange == [None, None, None]

    p.select_data("data1", "data2")
    p.select_data("data1", "data2", color="green", symbol="x")
    vrange = p.get_data_range()
    assert vrange == [[4, 6], [2, 5], None]

    p.deselect_data("data1", "data2")
    vrange = p.get_data_range()
    assert vrange == [None, None, None]

    data = p.get_data("data1")
    assert data == pytest.approx(data1)

    p.remove_data("data1")
    data = p.get_data("data1")
    assert data == []


def test_curveplot__bliss_1_9(flint_client):
    """Check custom plot curve API from BLISS >= 1.9"""
    flint = flint_client
    p = flint.get_plot(plot_class="curve", name="bliss-1.9")
    p.submit("setRaiseOnException", True)

    y1 = numpy.array([4, 5, 6])
    y2 = numpy.array([2, 5, 2])
    x = numpy.array([1, 2, 3])

    # Setup the items
    p.add_curve_item("x", "y1", legend="item1", color="red", yaxis="left")
    p.add_curve_item("x", "y2", legend="item2", color="blue", yaxis="right")
    vrange = p.get_data_range()
    assert vrange == [None, None, None]

    # Update the data
    p.set_data(x=x, y1=y1, y2=y2)
    vrange = p.get_data_range()
    assert vrange == [[1, 3], [4, 6], [2, 5]]

    # Clear the data
    p.clear_data()
    vrange = p.get_data_range()
    assert vrange == [None, None, None]

    # Append the data
    for i in range(len(x)):
        p.append_data(x=x[i : i + 1], y1=y1[i : i + 1], y2=y2[i : i + 1])
    vrange = p.get_data_range()
    assert vrange == [[1, 3], [4, 6], [2, 5]]

    # Or change the item layout
    assert p.item_exists("item2")
    p.remove_item("item2")
    assert not p.item_exists("item2")
    vrange = p.get_data_range()
    assert vrange == [[1, 3], [4, 6], None]

    # Check transaction
    p.clear_data()
    with p.transaction(resetzoom=False):
        p.set_data(x=x, y=y1)
        vrange = p.get_data_range()
        assert vrange == [None, None, None]
        p.add_curve_item("x", "y", legend="item")
        vrange = p.get_data_range()
        assert vrange == [None, None, None]
    vrange = p.get_data_range()
    assert vrange == [[1, 3], [4, 6], None]


def test_curveplot__reuse_plot(flint_client):
    flint = flint_client
    p = flint.get_plot(plot_class="curve", unique_name="foo-reuse")
    cos_data = numpy.cos(numpy.linspace(0, 2 * numpy.pi, 10))
    p.add_data({"cos": cos_data})
    p2 = flint.get_plot(plot_class="curve", unique_name="foo-reuse")
    data = p2.get_data("cos")
    assert data == pytest.approx(cos_data)


def test_select_points(flint_client):
    flint = flint_client
    p = flint.get_plot("curve", unique_name="plot1d")
    context = []

    def active_gui():
        result = p.select_points(1)
        context.append(result)

    def do_actions():
        gevent.sleep(1)
        flint.test_mouse(
            p.plot_id, mode="click", position=(0, 0), relative_to_center=True
        )

    gevent.joinall([gevent.spawn(f) for f in [active_gui, do_actions]])
    assert len(context) == 1

    result = context[0]
    assert len(result) == 1
    assert len(result[0]) == 2


def test_select_shape(flint_client):
    flint = flint_client
    p = flint.get_plot("curve", unique_name="plot1d")
    context = []

    def active_gui():
        result = p.select_shape(shape="line")
        context.append(result)

    def do_actions():
        gevent.sleep(1)
        flint.test_mouse(
            p.plot_id, mode="click", position=(0, 0), relative_to_center=True
        )
        flint.test_mouse(
            p.plot_id, mode="click", position=(5, 5), relative_to_center=True
        )

    gevent.joinall([gevent.spawn(f) for f in [active_gui, do_actions]])
    assert len(context) == 1

    result = context[0]
    assert len(result) == 2
    assert len(result[0]) == 2
    assert len(result[1]) == 2


def test_select_shapes__rect(flint_client):
    flint = flint_client
    p = flint.get_plot("curve", unique_name="plot1d")
    context = []

    def active_gui():
        result = p.select_shapes()
        context.append(result)

    def do_actions():
        gevent.sleep(1)
        flint.test_mouse(
            p.plot_id, mode="press", position=(-5, -5), relative_to_center=True
        )
        flint.test_mouse(
            p.plot_id, mode="release", position=(5, 5), relative_to_center=True
        )
        flint.test_active(p.plot_id, qaction="roi-apply-selection")

    gevent.joinall([gevent.spawn(f) for f in [active_gui, do_actions]])
    assert len(context) == 1

    result = context[0]
    assert len(result) == 1
    roi = result[0]
    assert isinstance(roi, dict)
    expected_keys = set(["origin", "size", "label", "kind"])
    assert len(expected_keys - roi.keys()) == 0
    assert roi["kind"] == "Rectangle"


def test_select_shapes__arc(flint_client):
    flint = flint_client
    p = flint.get_plot("curve", unique_name="plot1d")
    context = []

    def active_gui():
        result = p.select_shapes(kinds=["lima-arc"])
        context.append(result)

    def do_actions():
        gevent.sleep(1)
        flint.test_mouse(
            p.plot_id, mode="press", position=(-5, -5), relative_to_center=True
        )
        flint.test_mouse(
            p.plot_id, mode="release", position=(5, 5), relative_to_center=True
        )
        flint.test_active(p.plot_id, qaction="roi-apply-selection")

    gevent.joinall([gevent.spawn(f) for f in [active_gui, do_actions]])
    assert len(context) == 1

    result = context[0]
    assert len(result) == 1
    roi = result[0]
    assert isinstance(roi, lima_roi.ArcRoi)


def test_select_shapes__rect_profile(flint_client):
    flint = flint_client
    p = flint.get_plot("curve", unique_name="plot1d")
    context = []

    def active_gui():
        result = p.select_shapes(kinds=["lima-vertical-profile"])
        context.append(result)

    def do_actions():
        gevent.sleep(1)
        flint.test_mouse(
            p.plot_id, mode="press", position=(-5, -5), relative_to_center=True
        )
        flint.test_mouse(
            p.plot_id, mode="release", position=(5, 5), relative_to_center=True
        )
        flint.test_active(p.plot_id, qaction="roi-apply-selection")

    gevent.joinall([gevent.spawn(f) for f in [active_gui, do_actions]])
    assert len(context) == 1

    result = context[0]
    assert len(result) == 1
    roi = result[0]
    assert isinstance(roi, lima_roi.RoiProfile)
    assert roi.mode == "vertical"


def test_select_shapes__initial_selection(flint_client):
    flint = flint_client
    p = flint.get_plot("curve", unique_name="plot1d")
    roi_dict = dict(origin=(1, 2), size=(3, 4), kind="Rectangle", label="roi_dict")
    roi_rect = lima_roi.Roi(0, 1, 2, 3, name="roi_rect")
    roi_arc = lima_roi.ArcRoi(0, 1, 2, 3, 4, 5, name="roi_arc")
    roi_profile = lima_roi.RoiProfile(0, 1, 2, 3, mode="vertical", name="roi_profile")

    result = None

    def active_gui():
        nonlocal result
        result = p.select_shapes(
            initial_selection=[roi_dict, roi_rect, roi_arc, roi_profile]
        )

    def do_actions():
        gevent.sleep(1)
        flint.test_active(p.plot_id, qaction="roi-apply-selection")

    gevent.joinall([gevent.spawn(f) for f in [active_gui, do_actions]])
    assert result is not None

    print(result)
    assert len(result) == 4
    assert result[0]["label"] == roi_dict["label"]
    assert result[1].name == roi_rect.name
    assert result[2].name == roi_arc.name
    assert result[3].name == roi_profile.name


def test_plot1d(flint_client):
    f = flint_client
    p = f.get_plot(plot_class="plot1d", name="plot1d")

    # Check the default data setter
    x = numpy.arange(11) * 2
    y = numpy.arange(11)
    y2 = numpy.arange(11) / 2
    p.add_curve(x=x, y=y, legend="c1", yaxis="left")
    p.add_curve(x=x, y=y2, legend="c2", yaxis="right")
    vrange = p.get_data_range()
    assert vrange == [[0, 20], [0, 10], [0, 5]]

    # Clear the data
    p.clear_data()
    vrange = p.get_data_range()
    assert vrange == [None, None, None]

    # Check deprecated API
    x = numpy.arange(11) * 2
    y = numpy.arange(11)
    y2 = numpy.arange(11) / 2
    p.add_data(x, field="x")
    p.add_data(y, field="y")
    p.add_data(y2, field="y2")
    p.select_data("x", "y", yaxis="left")
    p.select_data("x", "y2", yaxis="right")
    vrange = p.get_data_range()
    assert vrange == [[0, 20], [0, 10], [0, 5]]

    # Check the default way to clear data
    p.clear_data()
    vrange = p.get_data_range()
    assert vrange[0:2] == [None, None]

    # Check the scales
    assert p.xscale == "linear"
    assert p.yscale == "linear"
    p.xscale = "log"
    p.yscale = "log"
    assert p.xscale == "log"
    assert p.yscale == "log"


def test_select_points_to_remove(flint_client):
    """Execute and validate for coverage"""
    flint = flint_client
    p = flint.get_plot("curve", unique_name="foo")

    x = numpy.linspace(0, 10 * numpy.pi, 10)
    y = numpy.sin(x + numpy.pi * 1.5)
    p.add_curve_item("x", "y", legend="foo", linewidth=1)
    p.set_data(x=x, y=y)

    result = None

    def active_gui():
        nonlocal result
        result = p.select_points_to_remove("foo")

    def do_actions():
        gevent.sleep(1)
        flint.test_active(p.plot_id, qaction="interaction-validate")

    gevent.joinall([gevent.spawn(f) for f in [active_gui, do_actions]])

    assert result is True

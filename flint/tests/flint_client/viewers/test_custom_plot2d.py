"""Testing custom plots provided by Flint."""

import numpy


def test_plot2d(flint_client):
    f = flint_client
    p = f.get_plot(plot_class="plot2d", name="plot2d")

    # Check the default data setter
    image = numpy.arange(10 * 10)
    image.shape = 10, 10
    p.add_image(image)
    vrange = p.get_data_range()
    assert vrange[0:2] == [[0, 10], [0, 10]]

    p.add_colormap("col1", lut="viridis", vmin=0, vmax=10)
    p.add_image(image, legend="foo", colormap="col1")

    # Check the default way to clear data
    p.clear_data()
    vrange = p.get_data_range()
    assert vrange[0:2] == [None, None]

    # Check the default way to clear data
    p.clear_data()
    vrange = p.get_data_range()
    assert vrange[0:2] == [None, None]


def test_plot2d__bliss_1_9(flint_client):
    """Check deprecated select_data since BLISS 1.9"""
    f = flint_client
    p = f.get_plot(plot_class="plot2d", name="plot2d")

    # Check deprecated API
    image = numpy.arange(9 * 9)
    image.shape = 9, 9
    p.add_data(image, field="image")
    p.select_data("image")
    vrange = p.get_data_range()
    assert vrange[0:2] == [[0, 9], [0, 9]]

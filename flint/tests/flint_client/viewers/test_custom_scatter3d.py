"""Testing scatter3D plot provided by Flint."""

import numpy


def test_empty_scatter(flint_client):
    """Create and empty scatter3D"""
    flint = flint_client
    p = flint.get_plot("scatter3d")
    numpy.testing.assert_almost_equal(p.get_data_range(), [[0, 0, 0], [0, 0, 0]])


def test_scatter(flint_client):
    """Create a scatter 3D and use some API

    Check that the data range is right
    """
    flint = flint_client
    p = flint.get_plot("scatter3d")
    p.set_colormap(lut="cividis", vmin=0, vmax=10)
    p.set_marker(symbol="o")
    p.set_data([0, 1], [2, 3], [4, 5], [6, 7])
    numpy.testing.assert_almost_equal(p.get_data_range(), [[0, 2, 4], [1, 3, 5]])


def test_clear_scatter(flint_client):
    """Create a scatter 3D with data and clear it.

    Check that the plot is empty
    """
    flint = flint_client
    p = flint.get_plot("scatter3d")
    p.set_data([0, 1], [2, 3], [4, 5], [6, 7])
    p.clear_data()
    numpy.testing.assert_almost_equal(p.get_data_range(), [[0, 0, 0], [0, 0, 0]])

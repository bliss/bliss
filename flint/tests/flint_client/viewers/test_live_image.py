"""Testing custom plots provided by Flint."""

import pytest
import numpy


@pytest.fixture
def live_image_plot(flint_client):
    width = 100
    height = 100
    detector_name = "live_image"

    flint = flint_client

    # Create a checker board place holder
    y, x = numpy.mgrid[0:height, 0:width]
    data = ((y // 16 + x // 16) % 2).astype(numpy.uint8) + 2
    data[0, 0] = 0
    data[-1, -1] = 5

    channel_name = f"{detector_name}:image"
    flint.set_static_image(channel_name, data)
    plot_proxy = flint.get_live_plot(image_detector=detector_name)
    return plot_proxy


def test_image_plot__create_marker(live_image_plot):
    """Create a marker.

    Make sure the marker have a location in the plot
    """
    plot = live_image_plot
    plot.update_marker("foo", position=(10, 10))
    assert plot.marker_position("foo") == (10.0, 10.0)


def test_image_plot__update_marker_position(live_image_plot):
    """Create a marker and update it's location

    Make sure the marker have a location in the plot
    """
    plot = live_image_plot
    plot.update_marker("foo", position=(10, 10))
    plot.update_marker("foo", position=(20, 20))
    assert plot.marker_position("foo") == (20.0, 20.0)


def test_image_plot__update_marker_editable(live_image_plot):
    """Create a marker and update it's state

    Make sure the marker is still at the initial location
    """
    plot = live_image_plot
    plot.update_marker("foo", position=(10, 10))
    plot.update_marker("foo", editable=False)
    assert plot.marker_position("foo") == (10.0, 10.0)


def test_image_plot__remove_marker(live_image_plot):
    """Create a marker and remove it.

    Make sure the marker have a location in the plot
    """
    plot = live_image_plot
    plot.update_marker("foo", position=(10, 10))
    plot.remove_marker("foo")
    assert plot.marker_position("foo") is None

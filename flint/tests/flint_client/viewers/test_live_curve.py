"""Testing custom plots provided by Flint."""


def test_live_curve_axes_scale(flint_client):
    """Get the default curve plot

    Check the axes scale API
    """
    p = flint_client.get_live_plot("default-curve")

    # Check the scales
    assert p.xscale == "linear"
    assert p.yscale == "linear"
    p.xscale = "log"
    p.yscale = "log"
    assert p.xscale == "log"
    assert p.yscale == "log"
    assert p.xaxis_channel_name is None

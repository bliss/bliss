<!--
A new scriv changelog fragment.

Uncomment the section that is right (remove the HTML comment wrapper).
-->

### Removed

- Beacon server no longer accepts --redis-data-port=0 as a way to disable Redis data db

### Added

- Beacon server can run with external Redis instances (see new options: --redis-address and --redis-data-address)

<!--
### Changed

- A bullet item for the Changed category.

-->
<!--
### Deprecated

- A bullet item for the Deprecated category.

-->
<!--
### Fixed

- A bullet item for the Fixed category.

-->
<!--
### Security

- A bullet item for the Security category.

-->

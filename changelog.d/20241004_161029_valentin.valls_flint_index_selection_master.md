<!--
A new scriv changelog fragment.

Uncomment the section that is right (remove the HTML comment wrapper).
-->

<!--
### Removed

- A bullet item for the Removed category.

-->
### Added

- flint
    - Added `select_data_index` to the live scatter plot
    - Added `select_data_index` to the custom scatter plot

<!--
### Changed

- A bullet item for the Changed category.

-->
<!--
### Deprecated

- A bullet item for the Deprecated category.

-->

### Fixed

- flint
   - Fixed custom scatter plot interaction API

<!--
### Security

- A bullet item for the Security category.

-->

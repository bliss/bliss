import sys
import pip._vendor.tomli as tomllib

# CI utility used to extract black and flake8 versions from pyproject.toml

with open("pyproject.toml", "rb") as f:
    toml = tomllib.load(f)
    for dep in toml["project"]["optional-dependencies"]["dev"]:
        if dep.startswith(sys.argv[1]):
            print(dep)

#!/usr/bin/env python
import os
import sys
import pathlib
import pkg_resources
import pip._vendor.tomli as tomllib

bliss_root_path = pathlib.Path(__file__).parent.parent

if sys.platform == "win32":
    conda_requirements_path = bliss_root_path / "conda-requirements-win.txt"
else:
    conda_requirements_path = bliss_root_path / "conda-requirements.txt"

# extracting setup.cfg information
with open("pyproject.toml", "rb") as f:
    pptoml = tomllib.load(f)
install_requires = pptoml["project"]["dependencies"]
entry_points = [f"{k} = {v}" for k, v in pptoml["project"]["scripts"].items()]

# translation table for PyPI packages using a different name in Conda
pypi2conda = {"msgpack": "msgpack-python", "redis": "redis-py"}

# translate setup.cfg requirements to conda format
translated_pypi_reqs = []
for req in pkg_resources.parse_requirements(install_requires):
    # ignore "pyqt5" from PyPI because conda-requirements already has it with name "pyqt"
    if req.name == "pyqt5":
        continue
    if req.name in pypi2conda:
        req.name = pypi2conda[req.name]
    translated_pypi_reqs.append(str(req))

# Get conda-only requirements defined by the CI job
with open(conda_requirements_path, "r") as f:
    conda_reqs = [
        str(req) for req in pkg_resources.parse_requirements(f) if req.name != "python"
    ]


def format_list(list):
    return "\n".join([f"    - {val}" for val in list])


recipe = f"""\
package:
  name: {pptoml['project']['name']}
  version: {pptoml['project']['version']}
source:
  path: ./..
about:
  home: {pptoml['project']['urls']['Homepage']}
  license: LGPLv3
  license_family: LGPL
  summary: {pptoml['project']['description']}
build:
  entry_points:
{format_list(entry_points)}
  number: {os.environ.get("CI_PIPELINE_IID", 0)}
  script: python -m pip install . -vv --no-deps --no-build-isolation
test:
  imports:
    - bliss
requirements:
  host:
    - python={{{{ python_version }}}}
    - setuptools >40.8
    - wheel
    - pip
  run:
    # Pure conda packages:
    - python
{format_list(conda_reqs)}

    # Packages translated from PyPI to conda:
{format_list(translated_pypi_reqs)}"""

print(recipe)

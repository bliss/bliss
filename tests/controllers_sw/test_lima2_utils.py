# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""Unit test suite for bliss/controllers/lima2/utils.py"""

import pytest
from bliss.controllers.lima2.utils import lazy_init


def test_lazy_init():
    """Check the initialization behaviour of a @lazy_init class"""
    x = 0  # Some external factor, here a global variable

    @lazy_init
    class LazyCoffee:
        def __init__(self):
            self.done = False
            if x == 0:
                raise ValueError

        def make(self):
            self.done = True

    coffee = LazyCoffee()

    with pytest.raises(ValueError):
        # getattribute
        _ = coffee.done
    with pytest.raises(ValueError):
        # method call (goes through getattribute)
        coffee.make()
    with pytest.raises(ValueError):
        # setattr
        coffee.xyz = 42

    x = 1
    # coffee will be initialized on the next attribute access

    # Now we can use the instance normally
    assert not coffee.done
    coffee.xyz = 42
    assert coffee.xyz == 42
    coffee.make()
    assert coffee.done

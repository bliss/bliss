# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest
import mock
import numpy

from bliss.common.counter import SamplingMode
from bliss.common.tango import DevSource, DevFailed
from bliss.common.scans import loopscan
from bliss import global_map


def test_machinfo_counters_issue1793(machinfo_tango_server, session):
    machinfo = session.config.get("machinfo")
    mg = session.config.get("issue1793_mg")
    for cnt in machinfo.counters:
        assert cnt.fullname in mg.available


def test_machinfo_conn_issue2333(machinfo_tango_server, session):
    machinfo = session.config.get("machinfo")
    p1 = machinfo.proxy
    assert p1 is machinfo.proxy  # check always the same proxy instance
    # check the counter controller is using the same instance
    assert (
        machinfo.counters.current._counter_controller._proxy is p1
    )  # check the counter controller is using the same instance
    assert all(cnt.mode == SamplingMode.SINGLE for cnt in machinfo.counters)
    assert machinfo.proxy.get_source() == DevSource.CACHE_DEV


def test_machinfo_in_scan(machinfo_tango_server, session):
    diode = session.config.get("diode")
    machinfo = session.config.get("machinfo")
    scan_with_current = loopscan(1, 0.1, diode, machinfo.counters.current)
    scan_without_current = loopscan(1, 0.1, diode)

    # Machine metadata should be always there, whether part of the scan or not
    assert "machine" in scan_with_current.scan_info["instrument"]
    assert "machine" in scan_without_current.scan_info["instrument"]
    assert "current" in scan_with_current.scan_info["instrument"]["machine"]
    assert "current" in scan_without_current.scan_info["instrument"]["machine"]

    assert "machinfo:current" not in scan_without_current.streams
    assert "machinfo:current" in scan_with_current.streams


def test_broken_metadata_doesnt_prevent_scan(machinfo_tango_server, session, caplog):
    machinfo = session.config.get("machinfo")

    with mock.patch.object(machinfo, "scan_metadata", side_effect=RuntimeError("abc")):
        s = loopscan(1, 0.1, machinfo)
    expected = "Error in generating metadata for controller 'machinfo'\n"
    assert expected in caplog.text
    assert s.get_data()


def test_machinfo_issue_3029(beacon):
    session = beacon.get("test_issue_3029")

    # Errors can have many causes:
    # * Device does not exist
    # * Device exists but not exported
    # * Device exported but dead
    #
    # So I make a global test on RuntimeError.

    # the Tango server is not exported (at init of the object)
    with pytest.raises(RuntimeError):
        session.config.get("machinfo")

    # ensure it is not registered for metadata
    assert not list(global_map.instance_iter("controllers"))


def test_machinfo_issue_3308(machinfo_tango_server, session, log_context, caplog):
    """
    https://gitlab.esrf.fr/bliss/bliss/-/issues/3308
      Be tolerant in case of bad reading of the Machine Current, since we cannot
      be sure it will always reply diligently
    """
    _, machinfo_proxy = machinfo_tango_server
    machinfo = session.config.get("machinfo")

    # machinfo.counters contains: current, lifetime, refill, sbcurr

    machinfo_proxy.setSimulateError(True)
    try:
        s = loopscan(1, 0.1, machinfo)
    finally:
        machinfo_proxy.setSimulateError(False)

    assert numpy.isnan(s.get_data()[machinfo.counters.current])

    # same test, changing allow_failure.
    machinfo_proxy.setSimulateError(True)
    machinfo.counters.current.allow_failure = True
    with pytest.raises(DevFailed):
        s = loopscan(1, 0.1, machinfo)
    machinfo_proxy.setSimulateError(False)
    machinfo.counters.current.allow_failure = False
    assert "machinfo: 'SR_Current' reading failed, returned NaN." in caplog.text

    # test timeout
    machinfo_proxy.setExtraDelay(0.3)
    try:
        s = loopscan(1, 0.1, machinfo)
    finally:
        machinfo_proxy.setExtraDelay(0)
    assert numpy.isnan(s.get_data()[machinfo.counters.current])
    assert "machinfo: Communication timeout, all counters set to NaN." in caplog.text

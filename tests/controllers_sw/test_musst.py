# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import numpy
from bliss.controllers.musst import (
    MusstMock,
    MusstSamplingCounter,
    MusstIntegratingCounter,
)
from bliss.common.scans import loopscan
from bliss.common.counter import SamplingMode
from bliss.scanning.acquisition.counter import SamplingCounterAcquisitionSlave
from bliss.scanning.acquisition.musst import MusstDefaultAcquisitionMaster


def test_musst(default_session):
    musst = default_session.config.get("musst")
    assert isinstance(musst, MusstMock)

    musst_timer = musst.counters.musst_timer
    mu_cnt2 = musst.counters.mu_cnt2
    enc_samy = musst.counters.enc_samy

    # check that channel_type == "cnt" produces MusstIntegratingCounters
    assert isinstance(musst_timer, MusstIntegratingCounter)
    assert isinstance(mu_cnt2, MusstIntegratingCounter)
    # others channel_types produces MusstSamplingCounters
    assert isinstance(enc_samy, MusstSamplingCounter)

    sc = loopscan(3, 0.01, musst)
    data_musst_timer = sc.get_data("*musst_timer")
    data_mu_cnt2 = sc.get_data("*mu_cnt2")
    data_enc_samy = sc.get_data("*enc_samy")

    assert data_musst_timer.shape == (3,)
    assert data_mu_cnt2.shape == (3,)
    assert data_enc_samy.shape == (3,)

    # check that Musst counters data type is adapted to channel_type
    assert type(data_musst_timer[0]) is numpy.float64
    assert type(data_mu_cnt2[0]) is numpy.int64
    assert type(data_enc_samy[0]) is numpy.float64

    # check that SamplingCounter data type is always float64 whatever the sampling mode
    assert enc_samy.mode is SamplingMode.SINGLE
    assert enc_samy.data_dtype is numpy.float64
    assert type(data_enc_samy[0]) is enc_samy.data_dtype
    # set MEAN mode which will produce float data
    enc_samy.mode = "MEAN"
    assert enc_samy.mode is SamplingMode.MEAN
    assert enc_samy.data_dtype is numpy.float64
    sc = loopscan(3, 0.01, musst)
    assert type(sc.get_data("*enc_samy")[0]) is numpy.float64

    # check counters are properly handled by the expected acquisition object
    assert isinstance(sc.acq_chain.nodes_list[1], SamplingCounterAcquisitionSlave)
    assert isinstance(sc.acq_chain.nodes_list[2], MusstDefaultAcquisitionMaster)

    assert enc_samy in sc.acq_chain.nodes_list[1]._counters
    assert musst_timer in sc.acq_chain.nodes_list[2]._counters
    assert mu_cnt2 in sc.acq_chain.nodes_list[2]._counters

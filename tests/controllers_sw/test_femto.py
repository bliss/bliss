# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest


def approx(val, delta=0.005):
    return pytest.approx(val, delta)


def test_gain(default_session):
    """test a femto amplifier"""
    default_session.config.get("wago_femto_simulator")
    test_dev = default_session.config.get("femto_simulator")

    test_dev.range = "low"
    gain = 10000  # would read [0, 0, 0,] from the wago ;-)
    test_dev.gain = gain
    assert test_dev.gain == gain

    """
    The tests fail, as the femto.py code returns:
        E               TypeError: object of type 'numpy.uint8' has no len()
    However, this is the wago simulator, which doesn't return an array.
    """
    for gn in range(4, 11):
        gain = pow(10, gn)
        test_dev.gain = gain
        assert test_dev.gain == gain, "gain test failed"


def test_offset(default_session):
    default_session.config.get("wago_femto_simulator")
    test_dev = default_session.config.get("femto_simulator")
    test_dev.offset = 5
    # offset between 1 and 10
    assert test_dev.offset == approx(5), "offset test failed"


def test_range(default_session):
    default_session.config.get("wago_femto_simulator")
    test_dev = default_session.config.get("femto_simulator")
    test_dev.range = "high"
    assert test_dev.range == "high", "range test failed"


def test_coupling(default_session):
    default_session.config.get("wago_femto_simulator")
    test_dev = default_session.config.get("femto_simulator")
    # test a coupling ac/dc according to the simulator yml
    # some femto can have a coupling driven with wago channel
    test_dev.coupling = "ac"
    assert test_dev.coupling == "ac", "range test failed"

# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""Test WhiteBeamAttenuator object."""

import pytest


def test_white_beam_attenuator(session, caplog):
    """att1 MultiplePosition and att1z Axis object have to be configured."""

    wba = session.config.get("wba")
    att1z = session.config.get("att1z")

    assert wba.name == "wba"

    # test __info__()
    wba.__info__()

    # test to move to position
    wba.move(["att1", "Al40"])
    assert wba.position == ["att1", "Al40"]

    # test when move underlying motor
    assert wba.attenuators[0]["attenuator"].motor_objs[0] == att1z
    att1z.move(0.5)
    assert wba.position == ["att1", "Al"]

    # test with no wait
    wba.move(["att1", "Al40"], wait=False)
    wba.wait(["att1", "Al40"])
    assert wba.position == ["att1", "Al40"]

    # during first move from 0 to Al40 (1.5), indeed it did not start from HOME
    output = list(filter(None, caplog.text.split("\n")))
    assert len(output) == 1
    assert output[0].endswith("Axis att1z did not start from HOME position")


def test_faulty_attenuator(session, caplog):
    # set to faulty (home switches not activated)
    session.config.get_config("wba").update({"faulty": True})

    wba = session.config.get("wba")
    att1 = session.config.get("att1")

    att1.move("Al", wait=True)

    wba.move(["att1", "Al40"])

    assert "did not start from HOME" in caplog.text
    assert "did not stop on HOME" in caplog.text


def test_frontend_hook(session, dummy_tango_server):
    # set a hook to shutter

    fqdn, ds = dummy_tango_server
    session.config.get_config("wba").update({"frontend": "$safshut"})
    sh = session.config.get("safshut")
    wba = session.config.get("wba")
    att1z = session.config.get("att1z")

    wba.move("att1", "Al")

    sh.open()  # moving attenuator is not allowed
    with pytest.raises(RuntimeError):
        wba.move("att1", "Al40")
    sh.close()  # now is ok
    wba.move("att1", "Al40")
    assert len(att1z.motion_hooks) == 1


def test_frontend_hook_with_fe_close_option(session, dummy_tango_server):
    # set a hook to shutter

    fqdn, ds = dummy_tango_server
    session.config.get_config("wba").update(
        {"frontend": "$safshut", "close_fe_before_move": True}
    )
    sh = session.config.get("safshut")
    wba = session.config.get("wba")

    wba.move("att1", "Al")

    sh.open()
    wba.move("att1", "Al40")
    assert sh.is_open is True

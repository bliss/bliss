# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import glob
from bliss.common.scans import loopscan


def get_lima_expected_files(scan):
    image_stream = scan.streams["image"]
    all_refs = image_stream.get_references(slice(None, None))
    return {ref.file_path for ref in all_refs}


def get_actually_saved_files(scan):
    device_name = scan._scan_info["channels"]["lima_simulator:image"]["device"]
    device_info = scan._scan_info["devices"][device_name]
    saving_directory = device_info["metadata"]["acq_parameters"]["saving_directory"]
    return set(glob.glob(saving_directory + "/*"))


def test_LimaClient_1_frame_per_edf(default_session, lima_simulator):
    simulator = default_session.config.get("lima_simulator")
    scan = loopscan(5, 0.1, simulator, save=True)

    expected_files = get_lima_expected_files(scan)
    saved_files = get_actually_saved_files(scan)
    assert expected_files == saved_files
    assert len(expected_files) == 5


def test_LimaClient_2_frames_per_edf(default_session, lima_simulator):
    simulator = default_session.config.get("lima_simulator")

    fpf = simulator.saving.frames_per_file
    ff = simulator.saving.file_format
    mode = simulator.saving.mode

    simulator.saving.frames_per_file = 2
    simulator.saving.file_format = "EDF"
    simulator.saving.mode = simulator.saving.mode.ONE_FILE_PER_N_FRAMES

    scan = loopscan(5, 0.1, simulator, save=True, run=False)

    simulator.saving.frames_per_file = fpf
    simulator.saving.file_format = ff
    simulator.saving.mode = mode

    scan.run()

    expected_files = get_lima_expected_files(scan)
    saved_files = get_actually_saved_files(scan)
    assert expected_files == saved_files
    assert len(expected_files) == 3


def test_LimaClient_1_frame_per_hdf5(default_session, lima_simulator):
    simulator = default_session.config.get("lima_simulator")

    ff = simulator.saving.file_format
    simulator.saving.file_format = "HDF5"

    scan = loopscan(5, 0.1, simulator, save=True, run=False)

    simulator.saving.file_format = ff

    scan.run()

    expected_files = get_lima_expected_files(scan)
    saved_files = get_actually_saved_files(scan)
    assert expected_files == saved_files
    assert len(expected_files) == 5


def test_LimaClient_2_frames_per_hdf5(default_session, lima_simulator):
    simulator = default_session.config.get("lima_simulator")

    fpf = simulator.saving.frames_per_file
    ff = simulator.saving.file_format
    mode = simulator.saving.mode

    simulator.saving.frames_per_file = 2
    simulator.saving.file_format = "HDF5"
    simulator.saving.mode = simulator.saving.mode.ONE_FILE_PER_N_FRAMES

    scan = loopscan(5, 0.1, simulator, save=True, run=False)

    simulator.saving.frames_per_file = fpf
    simulator.saving.file_format = ff
    simulator.saving.mode = mode

    scan.run()

    expected_files = get_lima_expected_files(scan)
    saved_files = get_actually_saved_files(scan)
    assert expected_files == saved_files
    assert len(expected_files) == 3

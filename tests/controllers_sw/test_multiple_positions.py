# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest
import gevent
import gevent.event
from bliss.common import event
from bliss.config.channels import Channel


def test_multiple_positions(session):
    """Test MultiplePositions object.

    beamstop has 2 positions: IN and OUT
    """
    beamstop = session.config.get("beamstop")
    mot1 = beamstop.motors["bsy"]
    mot2 = beamstop.motors["bsz"]

    # Test moving by label to a position.
    beamstop.move("IN")
    assert beamstop.position == "IN"

    # Test configuration validity.
    for mot in beamstop.targets_dict["IN"]:
        mot_min = mot["destination"] - mot["tolerance"]
        mot_max = mot["destination"] + mot["tolerance"]
        print(mot_min, mot_max)
        assert mot_min < mot["axis"].position < mot_max
        assert beamstop._in_position(mot)

    # Test moving by label to a wrong position.
    with pytest.raises(RuntimeError):
        beamstop.move("TOTO")

    # Test moving real motors to an exact valid position.
    mot1.move(3.5)
    mot2.move(2.0)
    assert beamstop.position == "OUT"

    # Test status property (via __info__)
    print(beamstop.__info__())

    # Test state is READY after a move.
    assert beamstop.state == "READY"

    # Test state is not READY during a move.
    # TODO ???

    # Test moving real motors to an aproximative valid position
    # (half of the tolerance is added to each target).
    mot1.move(2.5 + beamstop.targets_dict["IN"][0]["tolerance"] / 2)
    mot2.move(1.0 + beamstop.targets_dict["IN"][1]["tolerance"] / 2)
    assert beamstop.position == "IN"

    # Test moving real motors to an invalid position.
    mot1.move(0.0)
    mot2.move(0.0)
    assert beamstop.position == "unknown"

    # Test timeout.
    beamstop.move("OUT", wait=False)
    with pytest.raises(TimeoutError) as info:
        beamstop.wait(0.05)
    assert str(info.value) == "Timeout while waiting for 'beamstop' to move"

    # Test stop.
    beamstop.move("OUT", wait=False)
    beamstop.stop()

    # Test various motors list properties.
    print(beamstop.motors)
    print(beamstop.motor_names)
    print(beamstop.motor_objs)


def test_multiple_positions_add_remove_update(session):
    """Test MultiplePositions object.

    beamstop has 2 positions: IN and OUT
    """
    beamstop = session.config.get("beamstop")
    mot1 = beamstop.motors["bsy"]
    mot2 = beamstop.motors["bsz"]

    assert len(beamstop.targets_dict) == 2

    # CREATE
    # Ok to create a new position.
    beamstop.create_position("HALF_IN", [(mot1, 4), (mot2, 5)], "half in half out")

    assert len(beamstop.targets_dict) == 3

    # Should not be able to create twice the same position.
    with pytest.raises(RuntimeError):
        beamstop.create_position("HALF_IN", [(mot1, 4), (mot2, 5)], "half in half out")

    assert len(beamstop.targets_dict) == 3

    # UPDATE
    # ok to update existing position.
    beamstop.update_position("HALF_IN", [(mot1, 4.1), (mot2, 5.1)], "half in half out")
    beamstop.update_position("HALF_IN", description="moit' moit'")

    # motors_destinations_list must be a list.
    with pytest.raises(TypeError):
        beamstop.update_position("HALF_IN", (mot1, 4.1), "half in half out")

    # REMOVE
    beamstop.remove_position("HALF_IN")
    assert len(beamstop.targets_dict) == 2

    # Cannot remove non existing position.
    with pytest.raises(RuntimeError):
        beamstop.remove_position("Totally_IN")


def test_multiple_positions_move_by_label(session):
    """Test label-defined move methods"""
    beamstop = session.config.get("beamstop")
    beamstop.IN()
    assert beamstop.position == "IN"
    beamstop.OUT()
    assert beamstop.position == "OUT"


def test_multiple_positions_label(session):
    """Test label-positioning"""
    att = session.config.get("att1")
    mot = att.motors["att1z"]

    mot.move(2.5)
    assert att.position == "Al200"

    info_str = att.__info__()
    star_count = info_str.count("*")
    assert star_count == 1


def test_multiple_positions_info(session):
    att = session.config.get("att1")

    info_string = att.__info__()
    assert isinstance(info_string, str)


@pytest.mark.parametrize("in_shell", (True, False))
def test_multiple_positions_move_events(session, in_shell, pt_test_context):
    """Test MultiplePositions object.

    test for movement events
    """
    import bliss

    bliss._BLISS_SHELL_MODE = in_shell

    beamstop = session.config.get("beamstop")
    beamstop.move("IN")

    ready_event_received = gevent.event.Event()

    def callback(val, *args, **kwargs):
        if val == "READY":
            ready_event_received.set()

    event.connect(beamstop, "state", callback)
    try:
        beamstop.move("OUT", wait=False)

        with gevent.Timeout(3):
            ready_event_received.wait()

        assert ready_event_received.is_set()
    finally:
        event.disconnect(beamstop, "state", callback)
        beamstop.stop()


@pytest.mark.parametrize("in_shell", (True, False))
def test_multiple_positions_channels(session, in_shell, pt_test_context):
    """Move the controller

    Make sure state and position are emitted as events
    """
    import bliss

    bliss._BLISS_SHELL_MODE = in_shell

    all_positions = []
    all_states = []

    def position_changed(pos):
        all_positions.append(pos)

    def state_changed(state):
        all_states.append(state)

    beamstop = session.config.get("beamstop")
    channels = [  # noqa F841
        Channel(f"{beamstop.name}:position", callback=position_changed),
        Channel(f"{beamstop.name}:state", callback=state_changed),
    ]

    beamstop.move("IN")

    assert all_states
    assert all_states[-1] == "READY"
    assert all_positions
    assert all_positions[-1] == "IN"


def test_multiple_positions_channels_from_slave(session):
    """Move axis used by the controller

    Make sure state and position are emitted as events
    """
    all_positions = []
    all_states = []

    def position_changed(pos):
        all_positions.append(pos)

    def state_changed(state):
        all_states.append(state)

    beamstop = session.config.get("beamstop")
    bsy = beamstop.motors["bsy"]
    bsz = beamstop.motors["bsz"]

    channels = [  # noqa F841
        Channel(f"{beamstop.name}:position", callback=position_changed),
        Channel(f"{beamstop.name}:state", callback=state_changed),
    ]

    bsy.move(2.5)
    bsz.move(1.0)

    assert beamstop.position == "IN"
    assert beamstop.state == "READY"

    assert all_states
    assert all_states[-1] == "READY"
    assert all_positions
    assert all_positions[-1] == "IN"


def test_multiple_positions_of_multiple_positions(session):
    """Test MultiplePositions object which has a multiple_position object as dependence.

    foils has 2 positions: Pyro30 and Empty
    """
    foils = session.config.get("attenuation_foils")
    beamstop = session.config.get("beamstop")
    max2 = session.config.get("max2")
    bsy = session.config.get("bsy")
    bsz = session.config.get("bsz")

    foils.Pyro30()
    assert beamstop.position == "IN"
    assert max2.position == pytest.approx(0.12, abs=max2.tolerance)
    assert bsy.position == pytest.approx(2.5, abs=bsy.tolerance)
    assert bsz.position == pytest.approx(1.0, abs=bsz.tolerance)

    foils.Empty()
    assert beamstop.position == "OUT"
    assert max2.position == pytest.approx(0.6, abs=max2.tolerance)
    assert bsy.position == pytest.approx(3.5, abs=bsy.tolerance)
    assert bsz.position == pytest.approx(2.0, abs=bsz.tolerance)

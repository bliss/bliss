# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from __future__ import annotations
import atexit
import logging
import os
import pytest
import shutil

import subprocess
import sys
import traceback
import weakref
import typing
from contextlib import contextmanager
from pathlib import Path

import gevent
import redis
import redis.connection

import bliss
from bliss import global_log, global_map
from bliss.common import logtools, plot
from bliss.common import session as session_module
from bliss.common.data_store import set_default_data_store
from bliss.common.tango import DeviceProxy, DevState
from bliss.common.utils import copy_tree, get_open_ports, rm_tree
from bliss.config import static
from bliss.config.conductor import client, connection
from bliss.config.conductor.client import get_default_connection
from bliss.controllers import simulation_diode, tango_attr_as_counter
from bliss.controllers.lima.roi import Roi
from bliss.controllers.wago.emulator import WagoEmulator
from bliss.controllers.wago.wago import ModulesConfig
from flint.client import proxy as flint_proxy
from bliss.scanning import scan_meta
from bliss.shell.log_utils import logging_startup
from bliss.tango.clients.utils import wait_tango_db
from bliss.testutils.comm_utils import wait_tcp_online
from bliss.testutils.process_utils import start_tango_server, wait_terminate
from bliss.testutils.controller_utils import (
    lima_simulator_context,
    mosca_simulator_context,
)

from blissdata.redis_engine.store import DataStore

pytest_plugins = [
    "bliss.testutils.optional_capsys",
    "bliss.testutils.shell_utils",
    "bliss.testutils.resources_utils",
    "bliss.testutils.icat_utils",
]

BLISS = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
BEACON = [sys.executable, "-m", "bliss.beacon.app.beacon_server"]
BEACON_DB_PATH = os.path.join(BLISS, "tests", "test_configuration")
IMAGES_PATH = os.path.join(BLISS, "tests", "images")


@pytest.fixture
def capsys(clear_pt_context, capsys):
    """Monkey patch capsys to make it compatible with prompt-toolkit

    capsys replace sys.stdout, then prompt toolkit creates a context on it.
    This mocked stdout is finally closed, but the prompt toolkit context
    still point to it. `clear_pt_context` force to drop the pt context.
    """
    yield capsys


@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item, call):
    # execute all other hooks to obtain the report object
    outcome = yield
    rep = outcome.get_result()

    # set a report attribute for each phase of a call, which can
    # be "setup", "call", "teardown"
    setattr(item, "rep_" + rep.when, rep)


@pytest.fixture(autouse=True)
def clean_globals():
    orig_excepthook = sys.excepthook
    yield
    sys.excepthook = orig_excepthook
    global_log.clear()
    global_map.clear()
    # reset module-level globals
    simulation_diode.DEFAULT_CONTROLLER = None
    simulation_diode.DEFAULT_CONTROLLER = None
    simulation_diode.DEFAULT_INTEGRATING_CONTROLLER = None
    bliss._BLISS_SHELL_MODE = False
    session_module.sessions.clear()
    scan_meta.USER_SCAN_META = None

    # clean modif from shell.cli.repl
    logtools.elogbook.disable()

    tango_attr_as_counter._TangoCounterControllerDict = weakref.WeakValueDictionary()


@pytest.fixture(scope="session")
def homepage_port(ports):
    yield ports.homepage_port


@pytest.fixture(scope="session")
def beacon_tmpdir(tmpdir_factory):
    tmpdir = str(tmpdir_factory.mktemp("beacon"))
    yield tmpdir


@pytest.fixture(scope="session")
def beacon_directory(beacon_tmpdir):
    beacon_dir = os.path.join(beacon_tmpdir, "test_configuration")
    shutil.copytree(BEACON_DB_PATH, beacon_dir)
    yield beacon_dir


@pytest.fixture(scope="session")
def log_directory(beacon_tmpdir):
    log_dir = os.path.join(beacon_tmpdir, "log")
    os.mkdir(log_dir)
    yield log_dir


@pytest.fixture(scope="session")
def images_directory(tmpdir_factory):
    images_dir = os.path.join(str(tmpdir_factory.getbasetemp()), "images")
    shutil.copytree(IMAGES_PATH, images_dir)
    yield images_dir


@pytest.fixture(scope="session")
def ports(beacon_directory, log_directory, pytestconfig):
    redis_uds = os.path.join(beacon_directory, ".redis.sock")
    redis_data_uds = os.path.join(beacon_directory, ".redis_data.sock")

    class Ports(typing.NamedTuple):
        redis_port: int
        redis_data_port: int
        tango_port: int
        beacon_port: int
        logserver_port: int
        homepage_port: int

    ports = Ports(*get_open_ports(6))
    mandatory_tango_port = pytestconfig.getoption("--tango-port")
    if mandatory_tango_port is not None:
        ports = ports._replace(tango_port=mandatory_tango_port)

    os.environ["TANGO_HOST"] = f"localhost:{ports.tango_port}"
    os.environ["BEACON_HOST"] = f"localhost:{ports.beacon_port}"
    args = [
        f"--port={ports.beacon_port}",
        f"--redis-port={ports.redis_port}",
        f"--redis-socket={redis_uds}",
        f"--redis-data-port={ports.redis_data_port}",
        f"--redis-data-socket={redis_data_uds}",
        f"--db-path={beacon_directory}",
        f"--tango-port={ports.tango_port}",
        f"--homepage-port={ports.homepage_port}",
        f"--log-server-port={ports.logserver_port}",
        f"--log-output-folder={log_directory}",
        "--log-level=WARN",
        "--tango-debug-level=0",
        "--key=TESTKEY=FOO",
    ]
    proc = subprocess.Popen(BEACON + args)
    wait_tcp_online("localhost", ports.beacon_port, timeout=10)

    try:
        wait_tango_db(port=ports.tango_port, db=2, timeout=10)
    except BaseException:
        # don't wait for all tests to fail if the tango db doesn't work
        pytest.exit(traceback.format_exc(), returncode=-1)

    # disable .rdb files saving (redis persistence)
    r = redis.Redis(host="localhost", port=ports.redis_port)
    r.config_set("SAVE", "")
    del r

    yield ports

    atexit._run_exitfuncs()
    wait_terminate(proc)


@pytest.fixture
def blissdata(ports):
    redis_url = f"redis://localhost:{ports.redis_data_port}"

    red = redis.Redis.from_url(redis_url)
    red.flushall()
    try:
        data_store = DataStore(redis_url, init_db=True)
    except RuntimeError:
        # https://gitlab.esrf.fr/bliss/bliss/-/issues/4098
        # Memory tracker is part of beacon fixture which is session scoped.
        # Thus, it keeps running while this fixture restart, which may cause
        # the db to not be empty after flushall.
        red.delete("_MEMORY_TRACKING_")
        data_store = DataStore(redis_url, init_db=True)

    set_default_data_store(redis_url)
    try:
        yield data_store
    finally:
        data_store._redis.connection_pool.disconnect()
        data_store._redis = None


@pytest.fixture
def beacon_directory_source(beacon_directory):
    """Allow to tune the beacon directory"""

    class BeaconDirectory:
        def __init__(self):
            self.source_directory: Path = Path(BEACON_DB_PATH)
            self.target_directory: Path = beacon_directory

        def replace_directory(self, directory: Path):
            self.source_directory = directory
            rm_tree(Path(beacon_directory))
            copy_tree(directory, Path(beacon_directory))

    return BeaconDirectory()


@pytest.fixture
def beacon(ports, beacon_directory, blissdata):
    redis_db = redis.Redis(port=ports.redis_port)
    redis_db.flushall()
    static.Config.instance = None
    client._default_connection = connection.Connection("localhost", ports.beacon_port)
    config = static.get_config()
    yield config
    config.close()
    client._default_connection.close()
    # Ensure no connections are created due to garbage collection:
    client._default_connection = None

    # Always restore beacon directory
    rm_tree(Path(beacon_directory))  # still keeps Unix socket "files"
    # Restore files from BEACON_DB_PATH
    copy_tree(Path(BEACON_DB_PATH), Path(beacon_directory))


@pytest.fixture
def beacon_host_port(ports):
    return "localhost", ports.beacon_port


@pytest.fixture
def redis_conn(beacon):
    cnx = get_default_connection()
    redis_conn = cnx.get_redis_proxy()
    yield redis_conn


@pytest.fixture
def redis_data_conn(beacon):
    cnx = get_default_connection()
    redis_conn = cnx.get_redis_proxy(db=1)
    yield redis_conn


@pytest.fixture
def scan_tmpdir(tmpdir):
    yield tmpdir


@pytest.fixture
def lima_simulator(ports):
    with lima_simulator_context("simulator", "id00/limaccds/simulator1") as fqdn_proxy:
        yield fqdn_proxy


@pytest.fixture
def lima_simulator2(ports):
    with lima_simulator_context("simulator2", "id00/limaccds/simulator2") as fqdn_proxy:
        yield fqdn_proxy


@pytest.fixture
def mosca_simulator(ports):
    with mosca_simulator_context(
        "mosca_simulator", "id00/mosca/simulator"
    ) as fqdn_proxy:
        yield fqdn_proxy


@pytest.fixture
def bliss_tango_server(ports, beacon):
    device_name = "id00/bliss/test"
    fqdn_prefix = f"tango://{os.environ['TANGO_HOST']}"
    device_fqdn = f"{fqdn_prefix}/{device_name}"
    admin_device_fqdn = f"{fqdn_prefix}/dserver/bliss/test"

    with start_tango_server(
        sys.executable,
        "-u",
        "-m",
        "bliss.tango.servers.bliss_ds",
        "test",
        device_fqdn=device_fqdn,
        admin_device_fqdn=admin_device_fqdn,
        state=DevState.STANDBY,
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


@pytest.fixture
def dummy_tango_server(ports, beacon):

    device_name = "id00/tango/dummy"
    device_fqdn = "tango://localhost:{}/{}".format(ports.tango_port, device_name)

    with start_tango_server(
        sys.executable,
        "-u",
        "-m",
        "bliss.testutils.servers.dummy_tg_server",
        "dummy",
        device_fqdn=device_fqdn,
        state=DevState.CLOSE,
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


@pytest.fixture
def dummy_tango_server2(ports, beacon):

    device_name = "id00/tango/dummy2"
    device_fqdn = "tango://localhost:{}/{}".format(ports.tango_port, device_name)

    with start_tango_server(
        sys.executable,
        "-u",
        "-m",
        "bliss.testutils.servers.dummy_tg_server",
        "dummy2",
        device_fqdn=device_fqdn,
        state=DevState.CLOSE,
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


@pytest.fixture
def dummy_axis_server(ports, beacon):

    device_name = "id00/tango/dummy_axis"
    device_fqdn = f"tango://localhost:{ports.tango_port}/{device_name}"

    with start_tango_server(
        sys.executable,
        "-u",
        "-m",
        "bliss.testutils.servers.dummy_axis",
        "dummy_axis",
        device_fqdn=device_fqdn,
        state=DevState.ON,
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


@pytest.fixture
def wago_tango_server(ports, default_session, wago_emulator):
    device_name = "1/1/wagodummy"
    device_fqdn = "tango://localhost:{}/{}".format(ports.tango_port, device_name)

    # patching the property Iphost of wago tango device to connect to the mockup
    wago_ds = DeviceProxy(device_fqdn)
    wago_ds.put_property({"Iphost": f"{wago_emulator.host}:{wago_emulator.port}"})

    with start_tango_server(
        "Wago", "wago_tg_server", device_fqdn=device_fqdn
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


@pytest.fixture
def machinfo_tango_server(ports, beacon):
    device_name = "id00/tango/machinfo"
    device_fqdn = "tango://localhost:{}/{}".format(ports.tango_port, device_name)

    with start_tango_server(
        sys.executable,
        "-u",
        "-m",
        "bliss.testutils.servers.machinfo_tg_server",
        "machinfo",
        device_fqdn=device_fqdn,
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


@pytest.fixture
def regulation_tango_server(ports, beacon):
    device_name = "id00/regulation/loop"
    fqdn_prefix = f"tango://{os.environ['TANGO_HOST']}"
    device_fqdn = f"{fqdn_prefix}/{device_name}"
    admin_device_fqdn = f"{fqdn_prefix}/dserver/regulation/test"

    with start_tango_server(
        "Regulation",
        "test",
        device_fqdn=device_fqdn,
        admin_device_fqdn=admin_device_fqdn,
        state=DevState.ON,
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


@pytest.fixture
def motor_tango_server(ports, beacon):
    device_name = "id00/tango/remo_ctrl"
    fqdn_prefix = f"tango://{os.environ['TANGO_HOST']}"
    device_fqdn = f"{fqdn_prefix}/{device_name}"
    admin_device_fqdn = f"{fqdn_prefix}/dserver/blissmotorcontroller/test"

    with start_tango_server(
        "BlissMotorController",
        "test",
        device_fqdn=device_fqdn,
        admin_device_fqdn=admin_device_fqdn,
        state=DevState.ON,
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


@pytest.fixture
def session(beacon, scan_tmpdir):
    session = beacon.get("test_session")
    session.setup()
    session.scan_saving.base_path = str(scan_tmpdir)
    yield session
    session.close()


@pytest.fixture
def default_session(beacon, scan_tmpdir):
    default_session = session_module.DefaultSession()
    default_session.setup()
    default_session.scan_saving.base_path = str(scan_tmpdir)
    yield default_session
    default_session.close()


def pytest_addoption(parser):
    """
    Add pytest options
    """
    parser.addoption("--pepu", help="pepu host name")
    parser.addoption("--ct2", help="ct2 address")
    parser.addoption("--axis-name", help="axis name")
    parser.addoption("--axis-name2", help="axis name2")
    parser.addoption("--axis-name3", help="axis name3")
    parser.addoption("--mythen", action="store", help="mythen host name")
    parser.addoption(
        "--wago",
        help="connection information: tango_cpp_host:port,domani,wago_dns\nExample: --wago bibhelm:20000,ID31,wcid31c",
    )

    # Allow to debug tango with external application, if needed
    parser.addoption(
        "--tango-port",
        type=int,
        default=None,
        help="Enfore the port of the Tango DB, default is dynamic",
    )


@pytest.fixture
def alias_session(beacon, lima_simulator, scan_tmpdir):
    session = beacon.get("test_alias")
    env_dict = dict()
    session.setup(env_dict)
    session.scan_saving.base_path = str(scan_tmpdir)

    ls = env_dict["lima_simulator"]
    rois = ls.roi_counters
    r1 = Roi(0, 0, 100, 200)
    rois["r1"] = r1
    r2 = Roi(100, 100, 100, 200)
    rois["r2"] = r2
    r3 = Roi(200, 200, 200, 200)
    rois["r3"] = r3

    env_dict["ALIASES"].add("myroi", ls.counters.r1_sum)
    env_dict["ALIASES"].add("myroi3", ls.counters.r3_sum)

    yield session

    session.close()


@pytest.fixture
def wago_emulator(beacon):
    config_tree = beacon.get_config("wago_simulator")
    modules_config = ModulesConfig.from_config_tree(config_tree)
    wago = WagoEmulator(modules_config)

    yield wago

    wago.close()


@contextmanager
def flint_context(with_flint=True, stucked=False):
    """Helper to capture and clean up all new Flint processes created during the
    context.

    It also provides arguments to request a specific Flint state.
    """
    flint_singleton = flint_proxy._get_singleton()
    try:
        pids = set()

        def register_new_flint_pid(pid):
            nonlocal pids
            pids.add(pid)

        assert flint_singleton._on_new_pid is None
        flint_singleton._on_new_pid = register_new_flint_pid

        try:
            try:
                if with_flint:
                    flint = plot.get_flint()

                if stucked:
                    assert with_flint is True
                    try:
                        with gevent.Timeout(seconds=0.1):
                            # This command does not return that is why it is
                            # aborted with a timeout
                            flint.test_infinit_loop()
                    except gevent.Timeout:
                        pass

                yield
            finally:
                for pid in pids:
                    try:
                        wait_terminate(pid, timeout=10)
                    except gevent.Timeout:
                        # This could happen, if the kill fails, after 10s
                        pass
        finally:
            flint_singleton._on_new_pid = None
    finally:
        flint_singleton._proxy_cleanup()


@pytest.fixture
def flint_session(xvfb, beacon, scan_tmpdir):
    session = beacon.get("flint")
    session.setup()
    session.scan_saving.base_path = str(scan_tmpdir)
    with flint_context():
        yield session
    session.close()


@pytest.fixture
def test_session_with_flint(xvfb, session):
    with flint_context():
        yield session


@pytest.fixture
def test_session_with_stucked_flint(xvfb, session):
    with flint_context(stucked=True):
        yield session


@pytest.fixture
def test_session_without_flint(xvfb, session):
    """This session have to start without flint, but can finish with"""
    with flint_context(False):
        yield session


@pytest.fixture
def log_context():
    """
    Initialize BLISS logging and restore previous logging state on exit
    """
    # Save the logging context
    old_handlers = list(logging.getLogger().handlers)
    old_logger_dict = dict(logging.getLogger().manager.loggerDict)

    # Bliss __init__ module loads the global_log which creates
    # "global" and "global.controllers" BlissLoggers
    # so they exist in old_logger_dict but with only a NullHandler handler

    logging_startup()
    # this modifies the 'global' logger:
    #   - adding a beacon handler
    #   - changing level to logging.INFO
    #   - turning propagation to False

    # revert logging_startup modifications of the 'global' logger for the test purposes
    logging.getLogger("global").propagate = True
    logging.getLogger("global").setLevel(logging.NOTSET)

    yield

    global_log.restore_initial_state()
    # this removes the beacon handler from the 'global' logger

    # logging.getLogger("bliss").setLevel(logging.NOTSET)
    # logging.getLogger("flint").setLevel(logging.NOTSET)

    # logging.getLogger("parso.python.diff").disabled = False
    # logging.getLogger("parso.cache").disabled = False

    # Restore the logging context
    logging.shutdown()
    logging.setLoggerClass(logging.Logger)
    logging.getLogger().handlers.clear()  # deletes all handlers
    logging.getLogger().handlers.extend(old_handlers)
    logging.getLogger().manager.loggerDict.clear()  # deletes all loggers
    logging.getLogger().manager.loggerDict.update(old_logger_dict)


@pytest.fixture
def nexus_writer_service(ports):
    device_name = "id00/bliss_nxwriter/test_session"
    device_fqdn = "tango://localhost:{}/{}".format(ports.tango_port, device_name)

    with start_tango_server(
        "NexusWriterService", "testwriters", "--log", "warning", device_fqdn=device_fqdn
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


@pytest.fixture
def simulation_monochromator(beacon):
    # adjust files directories
    tracker_cfg = beacon.get_config("tracker")
    for tracker in tracker_cfg["trackers"]:
        for params in tracker["parameters"]:
            table_file = params.get("table")
            if table_file:
                params["table"] = os.path.join(BEACON_DB_PATH, table_file)
    mono_xtals = beacon.get_config("simul_mlmono_xtals")
    for xtal in mono_xtals["xtals"]:
        ml_lab_file = xtal["ml_lab_file"]
        xtal["ml_lab_file"] = os.path.join(BEACON_DB_PATH, ml_lab_file)
    #
    bragg = beacon.get("sim_mono")
    bragg.move(10)
    mono = beacon.get("simul_mono")
    mono.xtal.change("Si220")
    yield mono
    mono._close()

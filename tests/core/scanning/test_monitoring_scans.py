import gevent
from bliss.scanning.monitoring import MScanManager, MScan
from bliss.scanning.monitoring import (
    start_monitoring,
    stop_monitoring,
    clean_monitoring,
)
from bliss.scanning.monitoring import stop_all_monitoring, start_all_monitoring
from bliss.scanning.scan import MonitoringScan


def test_monitoring_scans(default_session, monitoring_scans):
    regul = default_session.config.get("sample_regulation")
    diode = default_session.config.get("diode")
    diode1 = default_session.config.get("diode1")
    diode2 = default_session.config.get("diode2")

    # start_monitoring(name, count_time, *counters, *, sleep_time=1.0, npoints=0, save=False, use_default_chain=True, table=False)

    start_monitoring("moni1", 0.01, regul, sleep_time=0.1)
    start_monitoring("moni2", 0.02, diode, diode1, diode2, sleep_time=0.2, table=True)

    assert isinstance(monitoring_scans, MScanManager)
    assert list(monitoring_scans.keys()) == ["moni1", "moni2"]

    ms1 = monitoring_scans["moni1"]
    ms2 = monitoring_scans["moni2"]
    assert isinstance(ms1, MScan)
    assert isinstance(ms1, MScan)

    assert isinstance(ms1.scan, MonitoringScan)
    assert not ms1._task.dead
    assert ms1._task.name == "moniscan_moni1"
    assert ms1.name == "moni1"
    assert ms1.count_time == 0.01
    assert ms1.counters == (regul,)
    assert ms1.sleep_time == 0.1
    assert ms1.npoints == 0
    assert ms1.saving_is_enabled is False
    assert ms1.use_default_chain is True
    assert ms1._use_table is False

    assert isinstance(ms2.scan, MonitoringScan)
    assert not ms2._task.dead
    assert ms2._task.name == "moniscan_moni2"
    assert ms2.name == "moni2"
    assert ms2.count_time == 0.02
    assert ms2.counters == (
        diode,
        diode1,
        diode2,
    )
    assert ms2.sleep_time == 0.2
    assert ms2.npoints == 0
    assert ms2.saving_is_enabled is False
    assert ms2.use_default_chain is True
    assert ms2._use_table is True

    stop_monitoring("moni1")
    assert ms1._task.dead
    assert not ms2._task.dead

    ms1.start()
    assert not ms1._task.dead
    assert not ms2._task.dead

    stop_all_monitoring()
    assert ms1._task.dead
    assert ms2._task.dead

    start_all_monitoring()
    assert not ms1._task.dead
    assert not ms2._task.dead

    clean_monitoring()

    assert not monitoring_scans


def test_npoints(default_session, monitoring_scans):
    diode = default_session.config.get("diode")
    start_monitoring("moni1", 0.01, diode, sleep_time=0, npoints=10)
    ms1 = monitoring_scans["moni1"]
    with gevent.Timeout(0.2):
        ms1._task.get()
    len(ms1.scan.streams[f"{diode.fullname}"]) == 10

# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import gevent
import pytest

from bliss.common.scans import ascan
from bliss.controllers.simulation_diode import simulation_diode
from bliss.scanning.scan import Scan
from bliss.scanning.chain import AcquisitionChain, AcquisitionSlave
from bliss.scanning.acquisition.timer import SoftwareTimerMaster
from bliss.scanning.acquisition.motor import MotorMaster


def test_integrating_acquisition_slave_without_mastercc(default_session):
    # verify that IntegratingCounterAcquisitionSlave without master counter controller
    # still obtains its parent when put in the acquisition chain
    diode = simulation_diode("diode", {"integration": True})
    roby = default_session.config.get("roby")
    s = ascan(roby, 0, 1, 2, 0.01, diode)
    dao = s.acq_chain.nodes_list[2]
    assert dao.name == "simulation_diode_integrating_controller"
    assert dao.parent is s.acq_chain.nodes_list[1]
    # roby.tolerance == 0.0001
    assert s.scan_info["channels"]["axis:roby"]["decimals"] == 6


def test_scan_stops_when_reading_loop_fails(default_session):
    class FakeCtrl:
        def __init__(self, name):
            self.name = name

    class FaultySlave(AcquisitionSlave):
        def prepare(self):
            pass

        def start(self):
            pass

        def stop(self):
            pass

        def trigger(self):
            pass

        def reading(self):
            gevent.sleep(self.error_time)
            raise RuntimeError("Faulty slave did a bad thing")

    roby = default_session.config.get("roby")

    fast_npoints = 1
    fast_count_time = 0.01
    motion_time = 3

    chain = AcquisitionChain()
    mot_master = MotorMaster(roby, start=0, end=3, time=motion_time)
    fast_timer = SoftwareTimerMaster(
        fast_count_time, npoints=fast_npoints, name="fast_timer"
    )
    fast_slave = FaultySlave(FakeCtrl("FaultySlave"))
    fast_slave.error_time = 1

    chain.add(mot_master, fast_timer)
    chain.add(fast_timer, fast_slave)

    scan = Scan(chain, "test", save=False)

    with gevent.Timeout(motion_time):
        with pytest.raises(RuntimeError) as exc_info:
            scan.run()
        assert "Faulty slave did a bad thing" in str(exc_info.value)

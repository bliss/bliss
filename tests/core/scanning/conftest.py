import pytest


@pytest.fixture
def monitoring_scans():
    from bliss.scanning.monitoring import MONITORING_SCANS

    yield MONITORING_SCANS
    MONITORING_SCANS.clear()

import re
import itertools

from bliss.shell.standard import elog_print
from bliss.shell.standard import elog_prdef
from bliss.common import logtools


def test_elog_print(session, esrf_data_policy, icat_mock_client, elogbook_enabled):
    elog_print("message1")
    icat_mock_client.return_value.send_message.assert_called_once_with(
        "message1",
        beamline_only=None,
        msg_type="comment",
        tags=[session.name],
    )


def test_elog_prdef(session, esrf_data_policy, icat_mock_client, elogbook_enabled):
    elog_prdef(_test_function)

    expected_message_pattern = re.compile(
        r"'_test_function' is defined in:\n.*/test_elogbook\.py:\d+\n"
        r"<pre class=\"language-python\"><code>def _test_function\(\):\n"
        r"    return \"Hello\"\n"
        r"</code></pre>"
    )

    expected_kwargs = {
        "beamline_only": None,
        "mimetype": "text/html",
        "msg_type": "comment",
        "tags": [session.name],
    }

    icat_mock_client.return_value.send_message.assert_called_once()
    actual_args, actual_kwargs = icat_mock_client.return_value.send_message.call_args

    assert len(actual_args) == 1
    message = actual_args[0]
    assert expected_message_pattern.match(
        message
    ), f"Unexpected message argument: {message}"

    assert (
        actual_kwargs == expected_kwargs
    ), f"Unexpected keyword arguments: {actual_kwargs}"


def _test_function():
    return "Hello"


def test_electronic_logbook(
    session, esrf_data_policy, icat_mock_client, elogbook_enabled
):
    lst = [
        ("info", "info"),
        ("warning", "warning"),
        ("error", "error"),
        ("debug", "debug"),
        ("critical", "critical"),
        ("command", "command"),
        ("comment", "comment"),
        ("print", "comment"),
    ]
    beamline_only = [True, False, None]
    try:
        for param in itertools.product(lst, beamline_only, beamline_only):
            (method_name, msg_type), beamline_only, default_beamline_only = param
            logtools.elogbook.beamline_only = default_beamline_only
            msg = repr(method_name + " message")
            method = getattr(logtools.elogbook, method_name)
            if beamline_only is None:
                method(msg)
                effective_beamline_only = logtools.elogbook.beamline_only
            else:
                method(msg, beamline_only=beamline_only)
                effective_beamline_only = beamline_only
            try:
                icat_mock_client.return_value.send_message.assert_called_once_with(
                    msg,
                    beamline_only=effective_beamline_only,
                    msg_type=msg_type,
                    tags=[session.name],
                )
            except AssertionError as e:
                raise AssertionError(str(param)) from e
            finally:
                icat_mock_client.reset_mock()
    finally:
        logtools.elogbook.beamline_only = None

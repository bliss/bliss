import pytest
from bliss.config import static
from collections.abc import MutableMapping
from collections.abc import MutableSequence


def print_tree(obj, indent=0, deep=4):
    if deep <= 0:
        return
    if isinstance(obj, MutableMapping):
        for k, v in obj.items():
            print("    " * indent, k)
            print_tree(v, indent + 1, deep - 1)
    elif isinstance(obj, MutableSequence):
        print("    " * indent, "[")
        for i in obj:
            print_tree(i, indent + 1, deep - 1)
        print("    " * indent, "]")
    else:
        print("    " * indent, type(obj))


@pytest.fixture
def setup_beacon_configuration1(beacon_directory_source):
    root = beacon_directory_source.source_directory
    beacon_directory_source.replace_directory(
        root / ".." / "test_extra_configuration" / "root1"
    )


def test_dir_with_init(setup_beacon_configuration1, beacon):
    config = static.Config("structure")
    assert "dir_with_init" in config.root
    assert "value" in config.root["dir_with_init"]
    assert config.root["dir_with_init"]["value"] == 10


@pytest.fixture
def setup_beacon_configuration2(beacon_directory_source):
    root = beacon_directory_source.source_directory
    beacon_directory_source.replace_directory(
        root / ".." / "test_extra_configuration" / "root2"
    )


def test_dir_with_file(setup_beacon_configuration2, beacon):
    config = static.Config("structure")
    assert "dir_with_file" in config.root
    assert "value" in config.root["dir_with_file"]
    assert config.root["dir_with_file"]["value"] == 1


@pytest.fixture
def setup_beacon_configuration3(beacon_directory_source):
    root = beacon_directory_source.source_directory
    beacon_directory_source.replace_directory(
        root / ".." / "test_extra_configuration" / "root3"
    )


def test_dir_with_files(setup_beacon_configuration3, beacon):
    config = static.Config("structure")
    assert "dir_with_files" in config.root
    assert isinstance(config.root["dir_with_files"], MutableSequence)
    children = config.root["dir_with_files"]
    # There is no specified order
    assert {children[0]["value"], children[1]["value"]} == {1, 2}


@pytest.fixture
def setup_beacon_configuration4(beacon_directory_source):
    root = beacon_directory_source.source_directory
    beacon_directory_source.replace_directory(
        root / ".." / "test_extra_configuration" / "root4"
    )


def test_dir_with_files_and_dir(setup_beacon_configuration4, beacon):
    config = static.Config("structure")
    print_tree(config.root)
    assert "dir_with_files_and_dir" in config.root
    assert isinstance(config.root["dir_with_files_and_dir"], MutableMapping)
    assert "d" in config.root["dir_with_files_and_dir"]
    children = config.root["dir_with_files_and_dir"]["__children__"]
    # There is no specified order
    assert {children[0]["value"], children[1]["value"]} == {1, 2}


@pytest.fixture
def setup_beacon_configuration5(beacon_directory_source):
    root = beacon_directory_source.source_directory
    beacon_directory_source.replace_directory(
        root / ".." / "test_extra_configuration" / "root5"
    )


def test_dir_with_files_and_init(setup_beacon_configuration5, beacon):
    config = static.Config("structure")
    assert "dir_with_files_and_init" in config.root
    assert isinstance(config.root["dir_with_files_and_init"], MutableMapping)
    assert "value" in config.root["dir_with_files_and_init"]
    children = config.root["dir_with_files_and_init"]["__children__"]
    # There is no specified order
    assert {children[0]["value"], children[1]["value"]} == {1, 2}


@pytest.fixture
def setup_beacon_configuration6(beacon_directory_source):
    root = beacon_directory_source.source_directory
    beacon_directory_source.replace_directory(
        root / ".." / "test_extra_configuration" / "root6"
    )


def test_complex_structure(setup_beacon_configuration6, beacon):
    config = static.Config("structure")
    print_tree(config.root)
    assert "dir1" in config.root
    assert "dir2" in config.root
    assert "dir22" in config.root["dir2"]
    assert "dir221" in config.root["dir2"]["dir22"]
    assert "dir21" not in config.root

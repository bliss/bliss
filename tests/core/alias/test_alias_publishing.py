# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from bliss.common import scans


def test_alias_data_channel(alias_session):
    env_dict = alias_session.env_dict

    s = scans.a2scan(
        env_dict["robu"],
        0,
        1,
        env_dict["robzz"],  # alias of 'robz'
        0,
        1,
        3,
        0.001,
        env_dict["simu1"].counters.spectrum_det0,
        env_dict["dtime"],
        env_dict["lima_simulator"].counters.r1_sum,  # has an alias 'myroi'
        env_dict["lima_simulator"].counters.r2_sum,
        env_dict["myroi3"],
        save=True,
        return_scan=True,
    )

    dump1 = [
        "axis:robu robu",
        "axis:robzz robzz",
        "timer:elapsed_time elapsed_time",
        "timer:epoch epoch",
        "lima_simulator:roi_counters:myroi myroi",
        "lima_simulator:roi_counters:r2_sum r2_sum",
        "lima_simulator:roi_counters:myroi3 myroi3",
        "simu1:dtime dtime",
        "simu1:spectrum_det0 spectrum_det0",
    ]

    for name, stream in s.streams.items():
        display_name = s.scan_info["channels"][stream.name]["display_name"]
        assert f"{name} {display_name}" in dump1


def test_alias_default_lima_counters(alias_session):
    env_dict = alias_session.env_dict

    s = scans.a2scan(
        env_dict["robu"],
        0,
        1,
        env_dict["robzz"],
        0,
        1,
        3,
        0.001,
        env_dict["lima_simulator"],
        save=True,
    )

    dump1 = [
        "axis:robu robu",
        "axis:robzz robzz",
        "timer:elapsed_time elapsed_time",
        "timer:epoch epoch",
        "lima_simulator:image image",
        "lima_simulator:roi_counters:r1_avg r1_avg",
        "lima_simulator:roi_counters:r1_max r1_max",
        "lima_simulator:roi_counters:r1_min r1_min",
        "lima_simulator:roi_counters:r1_std r1_std",
        "lima_simulator:roi_counters:myroi myroi",
        "lima_simulator:roi_counters:r2_avg r2_avg",
        "lima_simulator:roi_counters:r2_max r2_max",
        "lima_simulator:roi_counters:r2_min r2_min",
        "lima_simulator:roi_counters:r2_std r2_std",
        "lima_simulator:roi_counters:r2_sum r2_sum",
        "lima_simulator:roi_counters:r3_avg r3_avg",
        "lima_simulator:roi_counters:r3_max r3_max",
        "lima_simulator:roi_counters:r3_min r3_min",
        "lima_simulator:roi_counters:r3_std r3_std",
        "lima_simulator:roi_counters:myroi3 myroi3",
    ]

    for name, stream in s.streams.items():
        display_name = s.scan_info["channels"][stream.name]["display_name"]
        assert f"{name} {display_name}" in dump1


def test_lima_counter_aliased_and_lima_counters(alias_session):
    # related to https://gitlab.esrf.fr/bliss/bliss/merge_requests/1455#note_34525
    env_dict = alias_session.env_dict

    s = scans.a2scan(
        env_dict["robu"],
        0,
        1,
        env_dict["robzz"],
        0,
        1,
        3,
        0.001,
        env_dict["myroi3"],
        env_dict["lima_simulator"],
        save=True,
    )

    expected = [
        "axis:robu robu",
        "axis:robzz robzz",
        "timer:elapsed_time elapsed_time",
        "timer:epoch epoch",
        "lima_simulator:image image",
        "lima_simulator:roi_counters:r1_avg r1_avg",
        "lima_simulator:roi_counters:r1_max r1_max",
        "lima_simulator:roi_counters:r1_min r1_min",
        "lima_simulator:roi_counters:r1_std r1_std",
        "lima_simulator:roi_counters:myroi myroi",
        "lima_simulator:roi_counters:r2_avg r2_avg",
        "lima_simulator:roi_counters:r2_max r2_max",
        "lima_simulator:roi_counters:r2_min r2_min",
        "lima_simulator:roi_counters:r2_std r2_std",
        "lima_simulator:roi_counters:r2_sum r2_sum",
        "lima_simulator:roi_counters:r3_avg r3_avg",
        "lima_simulator:roi_counters:r3_max r3_max",
        "lima_simulator:roi_counters:r3_min r3_min",
        "lima_simulator:roi_counters:r3_std r3_std",
        "lima_simulator:roi_counters:myroi3 myroi3",
    ]

    dump = []
    for name, stream in s.streams.items():
        display_name = s.scan_info["channels"][stream.name]["display_name"]
        dump.append(f"{name} {display_name}")
    assert sorted(expected) == sorted(dump)

    s2 = scans.a2scan(
        env_dict["robu"],
        0,
        1,
        env_dict["robzz"],
        0,
        1,
        3,
        0.001,
        env_dict["lima_simulator"],
        env_dict["myroi3"],
        save=True,
    )

    dump = []
    for name, stream in s2.streams.items():
        display_name = s2.scan_info["channels"][stream.name]["display_name"]
        dump.append(f"{name} {display_name}")
    assert sorted(expected) == sorted(dump)

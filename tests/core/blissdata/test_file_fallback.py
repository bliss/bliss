# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.
import pytest

from bliss.common.scans import ascan, loopscan
from blissdata.redis_engine.exceptions import IndexNoMoreThereError


def test_file_fallback(default_session):
    diode = default_session.config.get("diode")
    sim_ct_gauss = default_session.config.get("sim_ct_gauss")
    robz = default_session.config.get("robz")

    def test_stream(file_backed_stream):
        event_stream = file_backed_stream._event_stream
        length = len(event_stream)
        redis = event_stream._data_store._redis
        redis.xtrim(event_stream.key, minid=length - 1, approximate=False)

        file_backed_stream[0]
        with pytest.raises(IndexNoMoreThereError):
            event_stream[0]

    scan = loopscan(20, 0.01, diode)
    test_stream(scan.streams["diode"])
    test_stream(scan.streams["elapsed_time"])
    test_stream(scan.streams["epoch"])

    scan = ascan(robz, 0, 1, 20, 0.01, sim_ct_gauss)
    test_stream(scan.streams["sim_ct_gauss"])
    test_stream(scan.streams["elapsed_time"])
    test_stream(scan.streams["epoch"])
    test_stream(scan.streams["robz"])

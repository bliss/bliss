# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import requests
import os
import ast
import glob
import itertools
import shutil
import json


def test_home(beacon, homepage_port):
    r = requests.get(f"http://localhost:{homepage_port}")
    assert r.status_code == 200  # OK

    r = requests.get(f"http://localhost:{homepage_port}/config")
    assert r.status_code == 200  # OK


def test_tree_files(beacon, homepage_port):
    r = requests.get(f"http://localhost:{homepage_port}/config/tree/files")
    assert r.status_code == 200  # OK


def test_db_tree(beacon, homepage_port):
    """Test that the skipped config is part of the dbtree"""
    # Get files list from the webapp.
    url = f"http://localhost:{homepage_port}/config/db_tree"
    r = requests.get(url)
    assert r.status_code == 200

    data = json.loads(r.text)
    assert "skipped_config" in data
    assert len(data["skipped_config"]) == 3


def test_db_files(beacon, homepage_port, beacon_directory):
    """
    Compare config files list taken from:
    * web config application
    * config directory directly from file system
    """
    # Get files list from the webapp.
    url = f"http://localhost:{homepage_port}/config/db_files"
    r = requests.get(url)
    assert r.status_code == 200

    db_files = set(
        [os.path.join(beacon_directory, f) for f in ast.literal_eval(r.text)]
    )

    # Get files list from file system.
    fs_files = set(
        itertools.chain(
            *[
                glob.glob(os.path.join(dp, "*.yml"))
                for dp, dn, fn in os.walk(beacon_directory)
            ]
        )
    )
    fs_files = [f for f in fs_files if "skipped_config" not in f]

    # Compare the 2 lists.
    assert set(db_files) == set(fs_files)


def test_duplicated_key_on_new_file(beacon, beacon_directory, homepage_port):
    # copy the diode.yml to a new file
    diode_path = os.path.join(beacon_directory, "diode.yml")
    diode1_path = os.path.join(beacon_directory, "diode1.yml")

    shutil.copyfile(diode_path, diode1_path)
    r = requests.get(f"http://localhost:{homepage_port}/config/config/reload")
    os.remove(diode1_path)

    assert r.status_code == 200  # OK


def test_duplicated_key_on_same_file(beacon, beacon_directory, homepage_port):
    file_content = """\
-
  name: diode_test
  plugin: bliss
  class: simulation_diode
  independent: True
"""
    filename = "diode_duplicated_key.yml"
    filepath = os.path.join(beacon_directory, filename)
    with open(filepath, "w") as f:
        f.write(file_content)

    with open(filepath, "w") as f:
        f.write(file_content)
        f.write(file_content)  # write twice to raise key error

    r = requests.get(f"http://localhost:{homepage_port}/config/config/reload")
    os.remove(filepath)

    assert r.status_code == 200  # OK

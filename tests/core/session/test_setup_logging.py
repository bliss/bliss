# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import os
import pytest
import gevent
from bliss.common import logtools


@pytest.fixture
def beacon_with_logging(beacon, capsys, caplog, log_context):
    logtools.elogbook.enable()
    yield beacon
    logtools.elogbook.disable()


@pytest.fixture
def logging_session(beacon_with_logging):
    session = beacon_with_logging.get("test_logging_session")
    session.setup()
    yield session
    session.close()


@pytest.fixture
def beacon_with_logging_esrf(beacon_with_logging):
    scan_saving_cfg = beacon_with_logging.root["scan_saving"]
    scan_saving_cfg["class"] = "ESRFScanSaving"
    yield beacon_with_logging


@pytest.fixture
def logging_session_without_elogserver(beacon_with_logging_esrf, log_directory):
    logfile = os.path.join(log_directory, "test_logging_session.log")
    with open(logfile, "w"):
        pass
    session = beacon_with_logging_esrf.get("test_logging_session")
    session.setup()
    yield session
    session.close()


@pytest.fixture
def logging_session_with_elogserver(
    beacon_with_logging_esrf, icat_mock_client, log_directory
):
    logfile = os.path.join(log_directory, "test_logging_session.log")
    with open(logfile, "w"):
        pass
    session = beacon_with_logging_esrf.get("test_logging_session")
    session.setup()
    yield session
    session.close()


def check_scripts_finished(session):
    assert session.env_dict.get("setupfinished")
    assert session.env_dict.get("scriptfinished")


def check_user_logging(capsys, elog_offline=False, data_policy=True):
    captured = capsys.readouterr().err.split("\n")
    captured = [s for s in captured if s]
    nexpected = 3 + (elog_offline and data_policy)
    assert len(captured) == nexpected, captured
    i = 0
    if data_policy and elog_offline:
        expected = "The ICAT client is disabled in the beacon configuration. Datasets will not be registered and e-logbook messages are lost."
        assert expected in captured[i]
        i += 1
    expected = "LogInitController: Beacon error"
    assert expected in captured[i]
    i += 1
    expected = "test_logging_session.py: Beacon error"
    assert expected in captured[i]
    i += 1
    expected = "logscript.py: Beacon error"
    assert expected in captured[i]


def check_beacon_logging(caplog, logfile, expected_records):
    # Get lines from logging capture
    records = caplog.get_records("setup")
    nrecords = len(expected_records)
    assert len(records) == nrecords, records

    # Get lines from the log server file
    try:
        lines = []
        with gevent.Timeout(10):
            while len(lines) != nrecords:
                with open(logfile, "r") as f:
                    lines = [line.rstrip() for line in f]
                gevent.sleep(0.5)
    except gevent.Timeout:
        assert len(lines) == nrecords, lines

    for i, (level, expected) in enumerate(expected_records):
        assert records[i].levelname == level
        assert records[i].message == expected
        assert expected in lines[i]

    return records, lines


def check_elogging(icat_mock_client):
    # +1 for the "Proposal set to ..." message
    assert icat_mock_client.return_value.send_message.call_count == 4
    assert icat_mock_client.return_value.send_message.called_once_with(
        "LogInitController: E-logbook error",
        beamline_only=None,
        formatted=None,
        editable=None,
        msg_type="error",
        tags=["test_logging_session"],
    )
    assert icat_mock_client.return_value.send_message.called_once_with(
        "test_logging_session.py: E-logbook error",
        beamline_only=None,
        formatted=None,
        editable=None,
        msg_type="error",
        tags=["test_logging_session"],
    )
    assert icat_mock_client.return_value.send_message.called_once_with(
        "logscript.py: E-logbook error",
        beamline_only=None,
        formatted=None,
        editable=None,
        msg_type="error",
        tags=["test_logging_session"],
    )


def test_setup_logging_no_data_policy(
    logging_session, capsys, caplog, log_directory, icat_mock_client
):
    logfile = os.path.join(log_directory, logging_session.name + ".log")
    check_scripts_finished(logging_session)
    check_user_logging(capsys, data_policy=False)
    records, lines = check_beacon_logging(
        caplog,
        logfile,
        (
            (
                ("ERROR", "LogInitController: Beacon error"),
                ("ERROR", "test_logging_session.py: Beacon error"),
                ("ERROR", "logscript.py: Beacon error"),
            )
        ),
    )

    assert icat_mock_client.call_count == 0


def test_setup_logging_without_elogserver(
    logging_session_without_elogserver,
    capsys,
    caplog,
    log_directory,
    icat_mock_client,
    elogbook_enabled,
):
    logfile = os.path.join(
        log_directory, logging_session_without_elogserver.name + ".log"
    )
    check_scripts_finished(logging_session_without_elogserver)
    check_user_logging(capsys, elog_offline=True)
    records, lines = check_beacon_logging(
        caplog,
        logfile,
        (
            (
                (
                    "WARNING",
                    "The ICAT client is disabled in the beacon configuration. Datasets will not be registered and e-logbook messages are lost.",
                ),
                ("ERROR", "LogInitController: Beacon error"),
                ("ERROR", "test_logging_session.py: Beacon error"),
                ("ERROR", "logscript.py: Beacon error"),
            )
        ),
    )

    assert icat_mock_client.call_count == 0


def test_setup_logging_with_elogserver(
    logging_session_with_elogserver,
    capsys,
    caplog,
    log_directory,
    icat_mock_client,
):
    logfile = os.path.join(log_directory, logging_session_with_elogserver.name + ".log")
    check_scripts_finished(logging_session_with_elogserver)
    check_user_logging(capsys)
    records, lines = check_beacon_logging(
        caplog,
        logfile,
        (
            (
                ("ERROR", "LogInitController: Beacon error"),
                ("ERROR", "test_logging_session.py: Beacon error"),
                ("ERROR", "logscript.py: Beacon error"),
            )
        ),
    )

    check_elogging(icat_mock_client)

# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest

from prompt_toolkit.formatted_text import to_plain_text

from bliss.common.closed_loop import ClosedLoopState


def test_closed_loop_any_axis(robz):
    assert hasattr(robz, "closed_loop")
    assert robz.closed_loop is None


def test_closed_loop_config(servo1):
    # incorrect state
    servo1.closed_loop.config["state"] = "foo"
    with pytest.raises(KeyError):
        servo1.closed_loop.apply_config()

    # missing state
    del servo1.closed_loop.config["state"]
    with pytest.raises(RuntimeError) as excinfo:
        servo1.closed_loop.apply_config()
    assert "configuration must contain" in str(excinfo.value)

    servo1.closed_loop.config["state"] = "on"

    # missing param
    del servo1.closed_loop.config["ki"]
    with pytest.raises(RuntimeError) as excinfo:
        servo1.closed_loop.apply_config()
    assert "configuration must contain" in str(excinfo.value)


def test_closed_loop_sync_hard(servo1):
    ctrl = servo1.controller
    servo1.closed_loop.ki = 1.0
    assert servo1.closed_loop.ki == ctrl.get_closed_loop_param(servo1, "ki")

    # sync_hard closed loop
    servo1.controller.set_closed_loop_param(servo1, "ki", 2.0)
    assert servo1.closed_loop.ki != ctrl.get_closed_loop_param(servo1, "ki")
    servo1.closed_loop.sync_hard()
    assert servo1.closed_loop.ki == ctrl.get_closed_loop_param(servo1, "ki")

    # sync_hard axis
    servo1.controller.set_closed_loop_param(servo1, "ki", 3.0)
    assert servo1.closed_loop.ki != ctrl.get_closed_loop_param(servo1, "ki")
    servo1.sync_hard()
    assert servo1.closed_loop.ki == ctrl.get_closed_loop_param(servo1, "ki")


def test_closed_loop_on_off(servo1):
    assert servo1.closed_loop.config["state"] == "on"
    assert servo1.closed_loop.state == ClosedLoopState.ON

    servo1.closed_loop.off()
    assert servo1.closed_loop.state == ClosedLoopState.OFF
    assert servo1.controller.get_closed_loop_state(servo1) == ClosedLoopState.OFF

    for _ in range(2):
        servo1.closed_loop.on()
        assert servo1.closed_loop.state == ClosedLoopState.ON
        assert servo1.controller.get_closed_loop_state(servo1) == ClosedLoopState.ON


def test_closed_loop_apply_config(servo1):
    config_kp = servo1.closed_loop.kp

    servo1.closed_loop.kp += 1
    assert config_kp + 1 == servo1.closed_loop.kp
    servo1.closed_loop.apply_config()
    assert config_kp == servo1.closed_loop.kp

    # axis.apply_config implies closed_loop.apply_config
    servo1.closed_loop.kp += 1
    assert config_kp + 1 == servo1.closed_loop.kp
    servo1.apply_config()
    assert config_kp == servo1.closed_loop.kp

    # closed_loop.apply_config doesn't imply axis.apply_config
    config_acc = servo1.acceleration
    servo1.acceleration += 100
    servo1.closed_loop.apply_config()
    assert config_acc + 100 == servo1.acceleration


def test_info_axis(servo1, capsys):

    # servo1.__info__() returns FormattedText
    captured = to_plain_text(servo1.__info__())

    assert "CLOSED LOOP:" in captured
    assert "state: ON" in captured
    assert "kp: 1" in captured


def test_info_closed_loop(servo1, capsys):
    captured = servo1.closed_loop.__info__()

    output = "CLOSED LOOP:\n"
    output += "     state: ON\n"
    output += "     kp: 1\n"
    output += "     ki: 2\n"
    output += "     kd: 3\n"

    assert captured == output

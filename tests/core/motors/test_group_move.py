import pytest
import gevent

from bliss.common.standard import mv
from bliss.common.standard import move
from bliss.common.standard import _move as std_move
from bliss.common.soft_axis import SoftAxis


@pytest.fixture
def axes_list(beacon):
    bad = beacon.get("bad")
    roby = beacon.get("roby")
    maxee = beacon.get("mot_maxee")

    # load calc dependencies to force Louie disconnections at end of test
    oax1 = beacon.get("oax1")
    oax2 = beacon.get("oax2")
    oax3 = beacon.get("oax3")
    cmc1 = beacon.get("cmc1")
    cmc2 = beacon.get("cmc2")
    cmc3 = beacon.get("cmc3")

    calc_axes = [oax1, oax2, oax3, cmc1, cmc2, cmc3]
    axes = [bad, roby, maxee, cmc3, oax3]
    for ax in axes:
        ax.backlash = 0.5

    yield axes

    for ax in calc_axes:
        ax.close()


def check_discrepency(ax):
    dial_initial_pos = ax.dial
    hw_pos = ax._hw_position
    diff_discrepancy = abs(dial_initial_pos - hw_pos)
    if diff_discrepancy > ax.tolerance:
        raise RuntimeError(
            "%s: discrepancy between dial (%f) and controller position (%f)\n \
                diff=%g tolerance=%g => aborting movement."
            % (
                ax.name,
                dial_initial_pos,
                hw_pos,
                diff_discrepancy,
                ax.tolerance,
            )
        )


def test_axis_move(axes_list):
    for ax in axes_list:

        # move sync
        ax.move(0.5)
        assert ax.position == pytest.approx(0.5, abs=ax.tolerance)
        assert ax.is_moving is False

        # move async
        ax.move(0.2, wait=False)
        assert ax.is_moving is True
        ax.wait_move()
        assert ax.position == pytest.approx(0.2, abs=ax.tolerance)
        assert ax.is_moving is False

        # already in place
        ax.move(0.2)
        assert ax.position == pytest.approx(0.2, abs=ax.tolerance)
        assert ax.is_moving is False

        # rmove sync
        ax.rmove(0.5)
        assert ax.position == pytest.approx(0.7, abs=ax.tolerance)
        assert ax.is_moving is False

        # rmove async
        ax.rmove(-0.5, wait=False)
        assert ax.is_moving is True
        ax.wait_move()
        assert ax.position == pytest.approx(0.2, abs=ax.tolerance)
        assert ax.is_moving is False

        # move async and stop main motion
        ax.move(2, wait=False)
        assert ax.is_moving is True
        ax._group_move._move_prepared_event.wait()
        ax._group_move._move_started_event.wait()
        ax.stop()
        assert ax.is_moving is False
        assert ax._set_position == ax.position

        # rmove async and stop backlash motion
        ax.rmove(-1 * ax.sign, wait=False)
        assert ax.is_moving is True
        ax._group_move._backlash_started_event.wait()
        ax.stop()
        assert ax.is_moving is False
        assert ax._set_position == ax.position


def test_group_move(axes_list):

    # move sync
    pos = 0.5
    mdict = {ax: pos for ax in axes_list}
    grp, _ = std_move(mdict, wait=True, relative=False)
    grp.is_moving is False
    for ax, axpos in mdict.items():
        assert ax.position == pytest.approx(axpos, abs=ax.tolerance)
        assert ax.is_moving is False

    # move async
    pos = 0.2
    mdict = {ax: pos for ax in axes_list}
    grp, _ = std_move(mdict, wait=False, relative=False)
    grp.is_moving is True
    for ax in mdict.keys():
        assert ax.is_moving is True
    grp.wait_move()
    for ax, axpos in mdict.items():
        assert ax.position == pytest.approx(axpos, abs=ax.tolerance)
        assert ax.is_moving is False

    # already in place
    grp, _ = std_move(mdict, wait=True, relative=False)
    for ax, axpos in mdict.items():
        assert ax.position == pytest.approx(axpos, abs=ax.tolerance)
        assert ax.is_moving is False

    # rmove sync
    pos = 0.5
    rmdict = {ax: pos for ax in axes_list}
    grp, _ = std_move(rmdict, wait=True, relative=True)
    grp.is_moving is False
    for ax, axpos in rmdict.items():
        newpos = axpos + mdict[ax]
        assert ax.position == pytest.approx(newpos, abs=ax.tolerance)
        assert ax.is_moving is False
        mdict[ax] = newpos

    # rmove async
    grp, _ = std_move(rmdict, wait=False, relative=True)
    grp.is_moving is True
    for ax in rmdict.keys():
        assert ax.is_moving is True
    grp.wait_move()
    for ax, axpos in rmdict.items():
        assert ax.position == pytest.approx(axpos + mdict[ax], abs=ax.tolerance)
        assert ax.is_moving is False

    # move async and stop main motion
    pos = 0.8
    mdict = {ax: pos for ax in axes_list}
    grp, _ = std_move(mdict, wait=False, relative=False)
    grp.is_moving is True
    for ax in mdict.keys():
        assert ax.is_moving is True
    grp._group_move._move_prepared_event.wait()
    grp._group_move._move_started_event.wait()
    grp.stop()
    for ax in mdict.keys():
        assert ax.is_moving is False
        assert ax._set_position == ax.position

        ax.backlash = 0.5  # for next test
        mdict[ax] = -1 * ax.sign

    # rmove async and stop backlash motion
    grp, _ = std_move(mdict, wait=False, relative=True)
    grp.is_moving is True
    for ax in mdict.keys():
        assert ax.is_moving is True
    grp._group_move._backlash_started_event.wait()
    grp.stop()
    for ax in mdict.keys():
        assert ax.is_moving is False
        assert ax._set_position == ax.position


def test_failure_bad_initialization(axes_list):
    bad = axes_list[0]

    def mock(*args):
        raise RuntimeError("BOUM")

    bad._set_moving_state = mock
    set_pos = bad._set_position
    target_pos = 2
    assert set_pos != target_pos
    with pytest.raises(RuntimeError) as excinfo:
        with gevent.Timeout(seconds=1):
            bad.move(target_pos)
    assert "BOUM" in excinfo.value.args[0]
    assert bad._group_move._initialization_has_failed
    assert bad._set_position == set_pos
    assert bad.is_moving is False


def test_failure_bad_prepare(axes_list):
    pos = 100
    mdict = {ax: pos for ax in axes_list}
    bad = axes_list[0]
    bad.velocity = 1

    # prepare failure
    func = bad.controller.prepare_move

    def bad_prepare(motion):
        raise RuntimeError("BAD PREPARE")

    bad.controller.prepare_move = bad_prepare

    with pytest.raises(RuntimeError) as excinfo:
        grp, _ = std_move(mdict, wait=True, relative=False)
    assert "BAD PREPARE" in excinfo.value.args[0]

    for ax in mdict.keys():
        assert ax.is_moving is False
        check_discrepency(ax)
        assert ax.position == ax._set_position

    bad.controller.prepare_move = func


def test_failure_bad_start(axes_list):
    pos = 100
    mdict = {ax: pos for ax in axes_list}
    bad = axes_list[0]
    bad.velocity = 1

    # start failure
    bad.controller.bad_start = True

    with pytest.raises(RuntimeError) as excinfo:
        grp, _ = std_move(mdict, wait=True, relative=False)
    assert "BAD START" in excinfo.value.args[0]

    for ax in mdict.keys():
        assert ax.is_moving is False
        check_discrepency(ax)
        assert ax.position == ax._set_position

    bad.controller.bad_start = False


def test_failure_bad_state(axes_list):
    pos = 100
    mdict = {ax: pos for ax in axes_list}
    bad = axes_list[0]
    bad.velocity = 1

    # state failure
    bad.controller.bad_state = True

    with pytest.raises(RuntimeError) as excinfo:
        grp, _ = std_move(mdict, wait=True, relative=False)
    assert "BAD STATE" in excinfo.value.args[0]

    for ax in mdict.keys():
        assert ax.is_moving is False
        if ax is not bad:
            assert "READY" in ax.state
        check_discrepency(ax)
        assert ax.position == ax._set_position

    bad.controller.bad_state = False


def test_failure_bad_state_after_start(axes_list):
    pos = 100
    mdict = {ax: pos for ax in axes_list}
    bad = axes_list[0]
    bad.velocity = 1

    # after start state failure
    bad.controller.bad_state_after_start = True
    with pytest.raises(RuntimeError) as excinfo:
        grp, _ = std_move(mdict, wait=True, relative=False)
    assert "BAD STATE" in excinfo.value.args[0]

    for ax in mdict.keys():
        assert ax.is_moving is False
        if ax is not bad:
            assert "READY" in ax.state
        check_discrepency(ax)
        assert ax.position == ax._set_position

    gevent.sleep(bad.controller.state_recovery_delay)
    assert bad.controller.bad_state is False
    bad.controller.bad_state_after_start = False


def test_failure_bad_position(axes_list):
    pos = 1
    mdict = {ax: pos for ax in axes_list}
    bad = axes_list[0]
    bad.velocity = 1

    # bad position while moving
    grp, _ = std_move(mdict, wait=False, relative=False)
    grp._group_move._move_started_event.wait()
    for ax in mdict.keys():
        assert ax.is_moving is True
    with pytest.raises(RuntimeError) as excinfo:
        bad.controller.bad_position = True
        grp.wait_move()
    assert "BAD POSITION" in excinfo.value.args[0]
    assert "FAULT" in bad.state

    for ax in mdict.keys():
        assert ax.is_moving is False
        if ax is not bad:
            assert "READY" in ax.state
            check_discrepency(ax)
            assert ax.position == ax._set_position

    bad.controller.bad_position = False


def test_failure_bad_stop(axes_list):
    pos = 1
    mdict = {ax: pos for ax in axes_list}
    bad = axes_list[0]
    bad.velocity = 1

    # bad stop (all stop attempts fails, so it waits end of main motion before returning the stop error)
    bad.sync_hard()
    bad.controller.bad_stop = True
    with pytest.raises(RuntimeError) as excinfo:
        grp, _ = std_move(mdict, wait=False, relative=False)
        grp._group_move._move_started_event.wait()
        grp.stop()

    assert (
        "axis ['bad'] stopping cmd failed with exception: BAD STOP"
        in excinfo.value.args[0]
    )
    bad.controller.bad_stop = False

    for ax in mdict.keys():
        assert ax.is_moving is False
        check_discrepency(ax)
        assert ax.position == ax._set_position


def test_kill_spawned_motion(roby):
    roby.velocity = 1
    task = gevent.spawn(mv, roby, 100)
    gevent.sleep(0.1)
    assert roby.is_moving is True
    assert roby._group_move._interrupted_move is False
    assert roby._group_move._end_of_move_event.is_set() is False
    task.kill()
    assert roby.is_moving is False
    assert roby._group_move._interrupted_move is True
    assert roby._group_move._end_of_move_event.is_set() is True


def test_blocking_soft_axis(beacon):
    class SoftCalc:
        def __init__(self, ax1, ax2):
            self.ax1 = ax1
            self.ax2 = ax2

        @property
        def gap(self):
            return self.ax2.position - self.ax1.position

        @gap.setter
        def gap(self, value):
            dx = value / 2
            curpos = self.read()
            p1 = curpos - dx
            p2 = curpos + dx
            move(self.ax1, p1, self.ax2, p2)

        def move(self, value):
            dx = self.gap / 2
            p1 = value - dx
            p2 = value + dx
            move(self.ax1, p1, self.ax2, p2)

        def read(self):
            return self.ax1.position + self.gap / 2

    max1 = beacon.get("max1")
    max2 = beacon.get("max2")

    sc = SoftCalc(max1, max2)

    sc.gap = 0.1

    sax = SoftAxis(
        "sax",
        sc,
        position=sc.read,
        move=sc.move,
        stop=None,
        state=None,
        low_limit=float("-inf"),
        high_limit=float("+inf"),
        tolerance=None,
        export_to_session=True,
        unit=None,
        as_positioner=False,
        display_digits=None,
    )

    move(sax, 0.1)

    assert sax.position == pytest.approx(0.1)
    assert max1.position == pytest.approx(0.05)
    assert max2.position == pytest.approx(0.15)

    # Launch motion and check it can be stopped
    # even if the soft axis move callback is a blocking call
    max1.velocity = 1
    max2.velocity = 1
    move(sax, 10, wait=False)
    assert sax.is_moving is True
    gpm = sax._group_move
    assert gpm._initialization_event.is_set()
    gpm._move_started_event.wait()
    timeout = gpm.task_interruption_timeout + 1
    with gevent.Timeout(seconds=timeout):
        gpm.stop()

    for ax in [max1, max2, sax]:
        assert ax.is_moving is False
        check_discrepency(ax)
        assert ax.position == ax._set_position

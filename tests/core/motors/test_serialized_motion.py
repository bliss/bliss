import gevent
from bliss.common.standard import _move


def test_sequencer_controller(beacon):
    max1 = beacon.get("max1")
    max2 = beacon.get("max2")
    max3 = beacon.get("max3")

    seqx = beacon.get("seqx")
    seqy = beacon.get("seqy")
    seqz = beacon.get("seqz")

    leader = beacon.get("leader_axis")

    allaxes = [leader, seqx, seqy, seqz, max1, max2, max3]

    # === perform a blocking move and check final states and positions
    group, _ = _move({leader: 1}, wait=True)

    for ax in allaxes:
        assert ax.is_moving is False

    assert seqx.position == max1.position
    assert seqy.position == max2.position
    assert seqz.position == max3.position

    assert seqx.position == leader.position + 1
    assert seqy.position == leader.position + 2
    assert seqz.position == leader.position + 3

    # === perform a NOT blocking move and check states during motion
    # === max1, max2, max3 states should become MOVING one after the other
    # === whereas seqx, seqy, seqz states should be MOVING during the entire motion
    # === also leader state should be MOVING during the entire motion
    group, _ = _move({leader: 2}, wait=False)
    group._group_move._move_started_event.wait()

    max1._group_move._move_started_event.wait()
    while not max1._group_move._move_task.dead:
        assert "MOVING" in max1.state
        assert "READY" in max2.state
        assert "READY" in max3.state

        assert "MOVING" in seqx.state
        assert "MOVING" in seqy.state
        assert "MOVING" in seqz.state

        assert "MOVING" in leader.state
        assert not leader._group_move._move_task.dead
        gevent.sleep(0.01)

    max2._group_move._move_started_event.wait()
    while not max2._group_move._move_task.dead:
        assert "READY" in max1.state
        assert "MOVING" in max2.state
        assert "READY" in max3.state

        assert "MOVING" in seqx.state
        assert "MOVING" in seqy.state
        assert "MOVING" in seqz.state

        assert "MOVING" in leader.state
        assert not leader._group_move._move_task.dead
        gevent.sleep(0.01)

    max3._group_move._move_started_event.wait()
    while not max3._group_move._move_task.dead:
        assert "READY" in max1.state
        assert "READY" in max2.state
        assert "MOVING" in max3.state

        assert "MOVING" in seqx.state
        assert "MOVING" in seqy.state
        assert "MOVING" in seqz.state

        assert "MOVING" in leader.state
        assert not leader._group_move._move_task.dead
        gevent.sleep(0.01)

    group._group_move.wait()

    for ax in allaxes:
        assert ax.is_moving is False

    assert seqx.position == max1.position
    assert seqy.position == max2.position
    assert seqz.position == max3.position

    assert seqx.position == leader.position + 1
    assert seqy.position == leader.position + 2
    assert seqz.position == leader.position + 3

    # release louie connections
    leader.controller.close()

# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
Bipod calc controller tests.
Values taken from ID21 for inside center.
"""

import pytest
from bliss.shell.standard import mv


def test_bip_config(session):
    bip = session.config.get("bip")

    for tag, axis_name in {
        "lega": "bip_legU",
        "legb": "bip_legD",
        "tz": "bip_trans",
        "ry": "bip_rot",
    }.items():
        assert bip._tagged[tag][0].name == axis_name


def test_bip_position(session):
    lup = session.config.get("bip_legU")
    ldown = session.config.get("bip_legD")
    bip_trans = session.config.get("bip_trans")
    bip_rot = session.config.get("bip_rot")

    assert lup.position == 0.0
    assert ldown.position == 0.0
    assert bip_trans.position == 0.0
    assert bip_rot.position == 0.0

    mv(bip_rot, 0.022)
    assert lup.position == pytest.approx(0.009847, abs=0.00001)
    assert ldown.position == pytest.approx(-0.028, abs=0.00001)
    assert bip_trans.position == pytest.approx(0.0, abs=0.00001)
    assert bip_rot.position == pytest.approx(0.022, abs=0.00001)

    bip_trans.move(0.01)
    assert lup.position == pytest.approx(0.019847, abs=0.00001)
    assert ldown.position == pytest.approx(-0.018, abs=0.00001)
    assert bip_trans.position == pytest.approx(0.01, abs=0.00001)
    assert bip_rot.position == pytest.approx(0.022, abs=0.00001)

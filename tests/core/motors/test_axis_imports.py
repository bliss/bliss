# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.


def test_import_axis():
    from bliss.common.axis import Axis, lazy_init, DEFAULT_POLLING_TIME  # noqa: F401
    from bliss.common.axis import ModuloAxis, NoSettingsAxis, CalcAxis  # noqa: F401
    from bliss.common.axis import AxisState  # noqa: F401
    from bliss.common.axis import (  # noqa: F401
        AxisOnLimitError,
        AxisOffError,
        AxisFaultError,
    )
    from bliss.common.axis import Trajectory, CyclicTrajectory  # noqa: F401
    from bliss.common.axis import GroupMove  # noqa: F401
    from bliss.common.axis import Motion  # noqa: F401

# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest
import contextlib
from unittest import mock
from bliss.common.axis.state import AxisState
from bliss.common.axis.error import AxisOnLimitError
from bliss.common.event import connect, disconnect


@contextlib.contextmanager
def axis_stdout(axis):
    def print_msg(msg):
        print(msg)

    connect(axis, "msg", print_msg)
    try:
        yield
    finally:
        disconnect(axis, "msg", print_msg)


def test_axis_stdout(roby, capsys):
    move_user_msg = roby.get_motion(0.1).user_msg

    with axis_stdout(roby):
        roby.move(0.1)

    assert capsys.readouterr().out == move_user_msg + "\n"

    roby.position = 0

    assert (
        capsys.readouterr().out
        == "'roby` position reset from 0.1 to 0.0 ; offset changed from 0.000000 to -0.100000 (sign:1)\n"
    )

    roby.dial = 1

    assert (
        capsys.readouterr().out
        == "'roby` dial position reset from 0.1 to 1.0 ; offset changed from -0.100000 to -1.000000 (sign:1)\n"
    )

    roby.position = roby.dial = 2

    assert (
        capsys.readouterr().out
        == "'roby` position reset from 0.0 to 2.0 ; offset changed from -1.000000 to 1.000000 (sign:1)\n"
        + "'roby` dial position reset from 1.0 to 2.0 ; offset changed from 1.000000 to 0.000000 (sign:1)\n"
    )

    vel = roby.velocity
    roby.velocity = vel + 1

    assert (
        capsys.readouterr().out
        == f"'roby` velocity changed from {vel} to {roby.velocity}\n"
    )

    acc = roby.acceleration
    roby.acceleration = acc + 1

    assert (
        capsys.readouterr().out
        == f"'roby` acceleration changed from {acc} to {roby.acceleration}\n"
    )


def test_axis_stdout2(roby, bad_motor, capsys):
    """Ensure "Moving..." and "...stopped at..." messages are present."""
    with axis_stdout(roby):
        roby.move(0.1)

    out, err = capsys.readouterr()
    assert "Moving roby from 0.000000 to 0.100000\n" in out
    with axis_stdout(roby):
        roby.move(0.2, wait=False)
        roby.stop()

    out, err = capsys.readouterr()
    assert "Axis roby stopped at position" in out


def test_axis_stderr1(roby):
    with mock.patch.object(roby.controller, "state") as new_state:
        new_state.return_value = AxisState("READY", "LIMNEG")
        assert roby.state.LIMNEG
        roby.jog()
        with pytest.raises(
            AxisOnLimitError,
            match=r"roby: READY \(Axis is READY\) \| "
            r"LIMNEG \(Hardware low limit active\) at [0-9\.]*",
        ):
            roby.wait_move()


def test_axis_out_of_limits(default_session, capsys):
    """
    Test error message in case of out-of-limits movement.
    """
    simy = default_session.config.get("simy")

    # Ensure that a move outside limit raises a ValueError.
    with pytest.raises(ValueError) as val_err:
        simy.move(-5.0000001)

    # Ensure 'would exceed high limit' message is present.
    assert "would exceed low limit" in str(val_err)

    # Ensure that a move slightly outside limit print all significant digits.
    # cf. issue #3749
    assert "move to `-5.0000001' would exceed low limit" in str(val_err)

# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest
import gevent
import pickle as pickle
import base64
import mock

from tango.gevent import DeviceProxy
from bliss.common import event

from .conftest import wait_state


def decode_tango_eval(x):
    return pickle.loads(base64.b64decode(x))


def test_2_library_instances(bliss_tango_server, s1hg, s1f, s1b, ports):
    s1hg.dial = 1
    s1hg.position = 1
    assert s1f.position == 0.5
    assert s1b.position == 0.5
    assert s1hg.position == 1

    tango_s1hg = DeviceProxy(
        f"tango://localhost:{ports.tango_port}/id00/bliss_test/s1hg"
    )

    assert tango_s1hg.read_attribute("position").value == 1
    assert tango_s1hg.read_attribute("offset").value == 0

    s1f.velocity = 1
    s1b.velocity = 1

    _dev_name, proxy = bliss_tango_server
    eval_id = proxy.eval("(s1f.velocity, s1b.velocity)")
    res = proxy.get_result(eval_id)
    assert decode_tango_eval(res) == (1, 1)

    # Test move from external session
    ev_state = {"last_value": None, "count": 0}
    ev_position = {"last_value": None, "count": 0}

    def pos_callback(pos):
        nonlocal ev_position
        ev_position["last_value"] = pos
        ev_position["count"] += 1

    def state_callback(state):
        nonlocal ev_state
        ev_state["last_value"] = state
        ev_state["count"] += 1

    event.connect(s1hg, "state", state_callback)
    event.connect(s1hg, "position", pos_callback)
    try:
        # Start moving
        tango_s1hg.position = 2
        wait_state(s1hg, "MOVING")
        # Check final state
        wait_state(s1hg, "READY")
        assert s1hg.position == pytest.approx(2)
        s1f.velocity = 10
        s1b.velocity = 10

        assert ev_position["count"] > 0
        assert ev_position["last_value"] == pytest.approx(2)
        assert ev_state["count"] > 0
        assert "READY" in ev_state["last_value"]

        # Test move from local session

        s1hg.rmove(1)

        value = tango_s1hg.read_attribute("position").value
        assert pytest.approx(value) == 3
    finally:
        event.disconnect(s1hg, "state", state_callback)
        event.disconnect(s1hg, "position", pos_callback)


def test_remote_stop(bliss_tango_server, robz, ports):
    robz.position = 1
    tango_robz = DeviceProxy(
        f"tango://localhost:{ports.tango_port}/id00/bliss_test/robz"
    )
    assert tango_robz.position == 1

    tango_robz.position = 1000
    wait_state(robz, "MOVING")

    robz.stop()
    wait_state(robz, "READY")
    assert tango_robz.position == robz.position


def test_remote_jog(bliss_tango_server, robz, ports):
    tango_robz = DeviceProxy(
        f"tango://localhost:{ports.tango_port}/id00/bliss_test/robz"
    )

    tango_robz.JogMove(300)
    wait_state(robz, "MOVING")

    robz.stop()
    wait_state(robz, "READY")

    gevent.sleep(0.05)  # let some time for Tango server to go from MOVING to ON state
    assert "ON" in str(tango_robz.state())

    tango_robz.JogMove(100)
    wait_state(robz, "MOVING")
    assert robz.jog_velocity == 100

    robz.jog(0)
    wait_state(robz, "READY")
    assert tango_robz.position == robz.position


def test_issue_3546(bliss_tango_server, s1ho, ports):
    tango_s1ho = DeviceProxy(
        f"tango://localhost:{ports.tango_port}/id00/bliss_test/s1ho"
    )

    with mock.patch(
        "bliss.common.motor_settings.AxisSettings.set", wraps=s1ho.settings.set
    ) as mock_settings_set:
        tango_s1ho.position = 1
        wait_state(s1ho, "MOVING")
        wait_state(s1ho, "READY")
    mock_settings_set.assert_not_called()

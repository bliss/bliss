# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest
import gevent
import numpy
from unittest import mock

from bliss.common import event
from bliss.common.standard import Group
from bliss.common.axis.state import AxisState


def test_group_move(robz, roby):
    robz_pos = robz.position
    roby_pos = roby.position
    grp = Group(robz, roby)

    assert grp.state.READY

    target_robz = robz_pos + 10
    target_roby = roby_pos + 10

    grp.move(robz, target_robz, roby, numpy.array([target_roby]), wait=False)

    assert grp.state.MOVING
    assert robz.state.MOVING
    assert roby.state.MOVING

    grp.wait_move()

    assert robz.state.READY
    assert roby.state.READY
    assert grp.state.READY


def test_stop(roby, robz):
    grp = Group(robz, roby)
    grp.move(robz, 1, roby, 1)

    assert robz.state.READY
    assert roby.state.READY
    assert grp.state.READY

    grp.move({robz: 0, roby: 0}, wait=False)
    assert grp.state.MOVING

    grp._group_move._move_started_event.wait()

    # ensure stop is called only once. cf issue #3547
    with mock.patch.object(
        robz.controller,
        "stop",
        wraps=robz.controller.stop,
    ) as stop_motor:
        grp.stop()
        stop_motor.assert_called_once()

    assert grp.state.READY
    assert robz.state.READY
    assert roby.state.READY

    # ensure motors are stopped (only valid for mockup, of course)
    assert robz.controller._get_axis_motion(robz) is None
    assert roby.controller._get_axis_motion(roby) is None


def test_ctrlc(roby, robz):
    grp = Group(robz, roby)
    assert robz.state.READY
    assert roby.state.READY
    assert grp.state.READY

    grp.move({robz: -10, roby: -10}, wait=False)

    gevent.sleep(0.01)

    grp._group_move._move_task.kill(KeyboardInterrupt, block=False)

    with pytest.raises(KeyboardInterrupt):
        grp.wait_move()

    assert grp.state.READY
    assert robz.state.READY
    assert grp.state.READY


def test_position_reading(beacon, robz, roby):
    grp = Group(robz, roby)
    positions_dict = grp.position

    for axis, axis_pos in positions_dict.items():
        group_axis = beacon.get(axis.name)
        assert axis == group_axis
        assert axis.position == axis_pos


def test_move_done(roby, robz):
    grp = Group(robz, roby)
    res = {"ok": False}

    def callback(move_done, res=res):
        if move_done:
            res["ok"] = True

    roby_pos = roby.position
    robz_pos = robz.position

    event.connect(grp, "move_done", callback)

    grp.rmove({robz: 2, roby: 3})

    assert res["ok"] is True
    assert robz.position == robz_pos + 2
    assert roby.position == roby_pos + 3

    event.disconnect(grp, "move_done", callback)


def test_issue_2967(roby):
    grp = Group(roby)

    with pytest.raises(TypeError):
        grp.move(())
    with pytest.raises(TypeError):
        grp.move((roby, 2))
    with pytest.raises(ValueError):
        grp.move({roby: None})


def test_hardlimits_set_pos(robz, robz2):
    assert robz._set_position == 0
    grp = Group(robz, robz2)
    robz.controller.set_hw_limits(robz, -2, 2)
    robz2.controller.set_hw_limits(robz2, -2, 2)
    with pytest.raises(RuntimeError):
        grp.move({robz: 3, robz2: 1})
    assert robz._set_position == robz.position


def test_no_move(robz):
    robz.move(0)
    grp = Group(robz)
    with gevent.Timeout(1):
        grp.move(robz, 0)
    assert not grp.is_moving


def test_is_moving_prop(robz, robz2):
    # issue #1599
    group = Group(robz, robz2)
    robz.move(10, wait=False, relative=True)
    assert robz.is_moving
    assert "MOVING" in group.state
    assert group.is_moving
    robz.stop()
    assert "READY" in group.state
    group.move(robz, 10, wait=False, relative=True)
    assert "MOVING" in group.state
    assert group.is_moving
    assert robz.is_moving
    assert not robz2.is_moving
    group.stop()


def test_axes_with_reals_prop(calc_mot2, calc_mot1, roby, robz):
    g = Group(calc_mot2, robz)
    assert "calc_mot2" in g.axes
    assert "robz" in g.axes
    assert list(g.axes.values()) == [calc_mot2, robz]  # this is to check order
    assert "calc_mot2" in g.axes_with_reals
    assert "robz" in g.axes_with_reals
    assert "calc_mot1" in g.axes_with_reals
    assert "roby" in g.axes_with_reals
    assert list(g.axes_with_reals.values()) == [calc_mot2, robz, calc_mot1, roby]


def test_no_hardware_access_if_at_pos(robz, roby):
    g = Group(roby, robz)

    g.move(roby, 1, robz, 2)

    # roby
    with mock.patch.object(
        roby.controller, "read_position", return_value=1.0 * roby.steps_per_unit
    ) as read_position:
        g.move(roby, 1, robz, 2)
        read_position.assert_not_called()

    with mock.patch.object(
        robz.controller, "state", return_value=AxisState("READY")
    ) as read_state:

        g.move(roby, 1, robz, 2)
        read_state.assert_not_called()

    # robz
    with mock.patch.object(
        robz.controller, "read_position", return_value=1.0 * robz.steps_per_unit
    ) as read_position:
        g.move(roby, 1, robz, 2)
        read_position.assert_not_called()

    with mock.patch.object(
        robz.controller, "state", return_value=AxisState("READY")
    ) as read_state:

        g.move(roby, 1, robz, 2)
        read_state.assert_not_called()


def test_group_state(roby, robz):
    grp = Group(roby, robz)

    def fancy_state(_):
        st = AxisState()
        st.create_state("SCSTOP", "Move was stopped")
        st.set("SCSTOP")
        return st

    with mock.patch.object(roby.controller, "state", fancy_state):
        assert "READY" in grp.state
        assert "SCSTOP" in grp.state

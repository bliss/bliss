import pytest
from unittest import mock

from bliss.common.proxy import Proxy, ProxyWithoutCall


def test_obj_proxy():
    class A:
        def __call__(self):
            return self

        def test_method(self):
            return "testA"

    class B:
        def test_method(self):
            return "testB"

    obj = A()
    obj_factory = mock.Mock(
        return_value=obj
    )  # this allows to always get the same obj to test (proxy "factory" keyword arg), while being able to use Mock features like knowing how many times it was called for example
    pa = Proxy(obj_factory, init_once=True)
    assert pa() is obj
    assert pa.test_method() == "testA"
    assert obj_factory.call_count == 1
    assert "test_method" in dir(pa)

    obj_factory.reset_mock()
    pa = Proxy(obj_factory)
    assert pa() is obj
    assert pa.test_method() == "testA"
    assert obj_factory.call_count == 2

    obj_factory.reset_mock()
    obj_factory.return_value = B()
    assert pa.test_method() == "testB"

    pa = ProxyWithoutCall(A)
    with pytest.raises(TypeError):
        pa()

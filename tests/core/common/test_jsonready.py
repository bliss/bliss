import numpy
from bliss.common import jsonready
from bliss.physics.units import ur


def test_nan():
    result = jsonready.bliss_to_jsonready(float("nan"))
    assert result == {"__type__": "nan"}
    obj = jsonready.bliss_from_jsonready(result)
    assert numpy.isnan(obj)


def test_neginf():
    result = jsonready.bliss_to_jsonready(float("-inf"))
    assert result == {"__type__": "neginf"}
    obj = jsonready.bliss_from_jsonready(result)
    assert obj == float("-inf")


def test_posinf():
    result = jsonready.bliss_to_jsonready(float("inf"))
    assert result == {"__type__": "posinf"}
    obj = jsonready.bliss_from_jsonready(result)
    assert obj == float("inf")


def test_number():
    result = jsonready.bliss_to_jsonready(10.1)
    assert result == 10.1
    obj = jsonready.bliss_from_jsonready(result)
    assert obj == 10.1


def test_quantity():
    value = ur.Quantity(10.1, "mm")
    result = jsonready.bliss_to_jsonready(value)
    assert result == {
        "__type__": "quantity",
        "scalar": 10.1,
        "unit": "mm",
    }
    obj = jsonready.bliss_from_jsonready(result)
    assert obj == value


def test_neginf_quantity():
    value = ur.Quantity(float("+inf"), "mm")
    result = jsonready.bliss_to_jsonready(value)
    assert result == {
        "__type__": "quantity",
        "scalar": {"__type__": "posinf"},
        "unit": "mm",
    }
    obj = jsonready.bliss_from_jsonready(result)
    assert obj == value


def test_obj(session):
    robz = session.config.get("robz")
    result = jsonready.bliss_to_jsonready(robz)
    assert result == {
        "__type__": "object",
        "name": "robz",
    }
    obj = jsonready.bliss_from_jsonready(result)
    assert obj is robz


def test_obj_in_dict(session):
    robz = session.config.get("robz")
    data = {"robz": robz}
    result = jsonready.bliss_to_jsonready(data)
    assert result == {
        "robz": {
            "__type__": "object",
            "name": "robz",
        }
    }
    data2 = jsonready.bliss_from_jsonready(result)
    assert data2["robz"] is robz


def test_obj_in_list(session):
    robz = session.config.get("robz")
    data = [robz]
    result = jsonready.bliss_to_jsonready(data)
    assert result == [
        {
            "__type__": "object",
            "name": "robz",
        }
    ]
    data2 = jsonready.bliss_from_jsonready(result)
    assert data2 == [robz]

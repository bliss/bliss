from bliss.common import expand


def test_expandvars__env_var(mocker):
    mock_environ = mocker.patch("os.environ.get", return_value="FOO")
    result = expand.expandvars("T'es foooo ${ENV_VAR}")
    mock_environ.assert_called_once_with("ENV_VAR", None)
    assert result == "T'es foooo FOO"


def test_expandvars__beacon_key(mocker):
    mock_beacon = mocker.patch("bliss.common.expand.get_default_connection")
    mock_beacon.return_value.get_key.return_value = "FOO"
    result = expand.expandvars("T'es foooo ${BEACON:ENV_VAR}")
    mock_beacon.return_value.get_key.assert_called_once_with("ENV_VAR", None)
    assert result == "T'es foooo FOO"


def test_expandvars__missing_env_var(mocker):
    mock_environ = mocker.patch("os.environ.get", return_value=None)
    result = expand.expandvars("T'es foooo ${ENV_VAR}")
    mock_environ.assert_called_once_with("ENV_VAR", None)
    assert result == "T'es foooo ${ENV_VAR}"


def test_expandvars__missing_beacon_key(mocker):
    mock_beacon = mocker.patch("bliss.common.expand.get_default_connection")
    mock_beacon.return_value.get_key.return_value = None
    result = expand.expandvars("T'es foooo ${BEACON:ENV_VAR}")
    mock_beacon.return_value.get_key.assert_called_once_with("ENV_VAR", None)
    assert result == "T'es foooo ${BEACON:ENV_VAR}"

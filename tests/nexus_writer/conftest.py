# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.


import os
import logging
from contextlib import contextmanager

import gevent
import pytest

from bliss.common import measurementgroup
from bliss.common.tango import DevState, Database
from bliss.tango.clients.utils import wait_tango_device
from bliss.testutils.data_policies import set_esrf_config
from bliss.scanning.writer.base import FileWriter
from bliss.scanning.writer.internal import InternalNexusWriter

from blisswriter.parameters import all_cli_saveoptions
from blisswriter.utils import log_levels
from blisswriter.utils import patch_testing

from .helpers import nxw_test_utils


def pytest_addoption(parser):
    parser.addoption(
        "--nexus-writer",
        type=str,
        choices=[s for s in FileWriter.get_writer_names() if s != "null"],
        default="nexus",
        help="Type of NeXus writer to use when writing scan data",
    )


@pytest.fixture(scope="session")
def nexus_writer_type(request) -> str:
    return request.config.getoption("nexus_writer")


@pytest.fixture
def nexus_writer_session(
    beacon, lima_simulator, lima_simulator2, machinfo_tango_server
):
    """Writer sessions with lots of different detectors and scan types"""
    session = beacon.get("nexus_writer_session")
    session.setup()
    assert "autofwheel_cu" in session.env_dict
    mono = beacon.get("mono")
    mono.move(7)
    yield session
    session.close()


@pytest.fixture
def nexus_writer_session_policy(beacon, nexus_writer_session):
    """Writer sessions with lots of different detectors and scan types"""
    yield nexus_writer_session


@pytest.fixture
def nxw_dir(tmpdir):
    yield tmpdir
    # tmpdir.remove()


@pytest.fixture
def nexus_writer_base(nexus_writer_session_policy, nxw_dir, nexus_writer_type):
    """Writer session with a Nexus writer"""
    with nexus_writer(
        nexus_writer_session_policy,
        nxw_dir,
        config=False,
        alt=False,
        policy=True,
        writer_type=nexus_writer_type,
    ) as info:
        yield info


@pytest.fixture
def nexus_writer_base_nopolicy(nexus_writer_session, nxw_dir, nexus_writer_type):
    """Writer session with a Nexus writer"""
    with nexus_writer(
        nexus_writer_session,
        nxw_dir,
        config=False,
        alt=False,
        policy=False,
        writer_type=nexus_writer_type,
    ) as info:
        yield info


@pytest.fixture
def nexus_writer_base_alt(nexus_writer_session_policy, nxw_dir, nexus_writer_type):
    """Writer session with a Nexus writer"""
    with nexus_writer(
        nexus_writer_session_policy,
        nxw_dir,
        config=False,
        alt=True,
        policy=True,
        writer_type=nexus_writer_type,
    ) as info:
        yield info


@pytest.fixture
def nexus_writer_config(nexus_writer_session_policy, nxw_dir, nexus_writer_type):
    """Writer session with a Nexus writer"""
    with nexus_writer(
        nexus_writer_session_policy,
        nxw_dir,
        config=True,
        alt=False,
        policy=True,
        writer_type=nexus_writer_type,
    ) as info:
        yield info


@pytest.fixture
def nexus_writer_config_capture(
    nexus_writer_session_policy, nxw_dir, nexus_writer_type
):
    """Writer session with a Nexus writer"""
    with nexus_writer(
        nexus_writer_session_policy,
        nxw_dir,
        config=True,
        alt=False,
        policy=True,
        capture=True,
        writer_type=nexus_writer_type,
    ) as info:
        yield info


@pytest.fixture
def nexus_writer_limited_disk_space(
    nexus_writer_session_policy, nxw_dir, nexus_writer_type
):
    """Like nexus_writer_config but require more disk space
    than available.
    """
    statvfs = os.statvfs(nxw_dir)
    free_space = statvfs.f_frsize * statvfs.f_bavail / 1024**2
    with nexus_writer(
        nexus_writer_session_policy,
        nxw_dir,
        config=True,
        alt=False,
        policy=True,
        required_disk_space=free_space * 10,
        writer_type=nexus_writer_type,
    ) as info:
        yield info


@pytest.fixture
def nexus_writer_config_slow_disk(
    nexus_writer_session_policy, nxw_dir, nexus_writer_type
):
    """Writer session with a Nexus writer and slow disk"""
    with nexus_writer(
        nexus_writer_session_policy,
        nxw_dir,
        config=True,
        alt=False,
        policy=True,
        patches=["slowdisk"],
        writer_type=nexus_writer_type,
    ) as info:
        yield info


@pytest.fixture
def nexus_writer_config_nopolicy(nexus_writer_session, nxw_dir, nexus_writer_type):
    """Writer session with a Nexus writer"""
    with nexus_writer(
        nexus_writer_session,
        nxw_dir,
        config=True,
        alt=False,
        policy=False,
        writer_type=nexus_writer_type,
    ) as info:
        yield info


@pytest.fixture
def nexus_writer_config_alt(nexus_writer_session_policy, nxw_dir, nexus_writer_type):
    """Writer session with a Nexus writer"""
    with nexus_writer(
        nexus_writer_session_policy,
        nxw_dir,
        config=True,
        alt=True,
        policy=True,
        writer_type=nexus_writer_type,
    ) as info:
        yield info


@contextmanager
def nexus_writer(
    session,
    tmpdir,
    config=True,
    alt=False,
    policy=True,
    capture=False,
    patches=None,
    required_disk_space=None,
    writer_type: str = "nexus",
):
    """Nexus writer for this session

    :param session:
    :param tmpdir:
    :param bool policy:
    :param bool config:
    :param bool alt:
    :param bool capture:
    :param list patches:
    :param num required_disk_space:
    :returns dict:
    """
    info = {
        "session": session,
        "tmpdir": tmpdir,
        "config": config,
        "alt": alt,
        "policy": policy,
        "required_disk_space": required_disk_space,
    }
    prepare_objects(**info)
    prepare_scan_saving(writer_type=writer_type, **info)
    if writer_type == "nexus":
        writer_ctx = writer_tango
    elif writer_type == "external":
        writer_ctx = writer_process
    elif writer_type == "hdf5":
        writer_ctx = writer_internal
    else:
        raise NotImplementedError("initialization of this writer for tests is missing")
    with writer_ctx(capture=capture, patches=patches, **info) as writer:
        info["writer"] = writer
        yield info


def prepare_objects(session=None, **_):
    att1 = session.env_dict["att1"]
    att1.Al()
    beamstop = session.env_dict["beamstop"]
    beamstop.IN()


def prepare_scan_saving(
    session=None,
    tmpdir=None,
    policy: bool = True,
    writer_type: str = "nexus",
    **_,
):
    """Initialize scan saving so the tests save in `tmpdir`
    with or without policy.

    :param session:
    :param tmpdir:
    :param policy:
    """
    if policy:
        session.enable_esrf_data_policy()
        scan_saving = session.scan_saving
        set_esrf_config(scan_saving, str(tmpdir / session.name))
        assert scan_saving.writer == "nexus"
        scan_saving.writer = writer_type
        scan_saving.proposal_name = "testproposal"
        scan_saving.proposal.all.definition = "test"
        measurementgroup.set_active_name("testMG")
    else:
        session.disable_esrf_data_policy()
        scan_saving = session.scan_saving
        assert scan_saving.writer == "hdf5"
        scan_saving.writer = writer_type
        scan_saving.base_path = str(tmpdir)
        scan_saving.data_filename = "{a}_{b}"
        scan_saving.add("a", "default")
        scan_saving.add("b", "filename")
        measurementgroup.set_active_name("testMG")


@contextmanager
def writer_tango(
    session=None,
    tmpdir=None,
    config=True,
    alt=False,
    capture=False,
    patches=None,
    required_disk_space=None,
    **_,
):
    """
    Run external writer as a Tango server

    :param session:
    :param tmpdir:
    :param bool config:
    :param bool alt:
    :param bool capture:
    :param list patches:
    :param num required_disk_space:
    :returns PopenGreenlet:
    """
    env = writer_env()
    server_instance = "testwriters"
    cliargs = (
        ("NexusWriterService", server_instance)
        + writer_cli_logargs(tmpdir)
        + patch_testing.popen_args(patches)
    )
    # Register another writer with the TANGO database (testing concurrency):
    # device_fqdn = register_service.ensure_existence(
    #    session.name, instance=server_instance, family="dummy", use_existing=False
    # )
    # Rely on beacon registration from YAML description:
    device_name = "id00/bliss_nxwriter/" + session.name
    device_fqdn = "tango://{}/{}".format(env["TANGO_HOST"], device_name)
    properties, attributes = writer_options(
        writer="tango", config=config, alt=alt, required_disk_space=required_disk_space
    )
    db = Database()
    db.put_device_property(device_name, properties)
    exception = None
    for i in range(3):
        with nxw_test_utils.popencontext(cliargs, env=env, capture=capture) as greenlet:
            try:
                dev_proxy = wait_tango_device(
                    device_fqdn=device_fqdn, state=DevState.ON
                )
            except Exception as e:
                exception = e
                if i == 2:
                    raise RuntimeError(
                        f"Could not start {device_fqdn} or started but failed immediately"
                    ) from exception
                continue
            # Changing attributes does not need Init
            for attr, value in attributes.items():
                dev_proxy.write_attribute(attr, value)

            # DEBUG(1), INFO(2), WARNING(3), ...
            log_level_idx = int(get_log_level()) // 10
            assert int(dev_proxy.writer_log_level) == log_level_idx
            assert int(dev_proxy.tango_log_level) == log_level_idx

            greenlet.proxy = dev_proxy
            try:
                yield greenlet
            finally:
                break


@contextmanager
def writer_process(
    session=None,
    tmpdir=None,
    config=True,
    alt=False,
    capture=False,
    patches=None,
    **_,
):
    """
    Run external writer as a python process

    :param session:
    :param tmpdir:
    :param bool config:
    :param bool alt:
    :param bool capture:
    :param list patches:
    :returns PopenGreenlet:
    """
    env = writer_env()
    semaphore_file = tmpdir / "start_file.log"
    cliargs = (
        ("NexusSessionWriter", session.name)
        + writer_cli_logargs(tmpdir)
        + writer_options(
            writer="process", config=config, alt=alt, semaphore_file=semaphore_file
        )
        + patch_testing.popen_args(patches)
    )
    with nxw_test_utils.popencontext(cliargs, env=env, capture=capture) as greenlet:
        with gevent.Timeout(10, RuntimeError("Nexus Writer not running")):
            while not semaphore_file.exists():
                gevent.sleep(0.1)
        yield greenlet


@contextmanager
def writer_internal(config=True, alt=False, required_disk_space=None, **_):
    options = writer_options(
        config=config, alt=alt, required_disk_space=required_disk_space
    )
    InternalNexusWriter.update_parameters(options)
    try:
        yield None
    finally:
        InternalNexusWriter.clear_parameters()


def writer_options(
    writer=None, config=True, alt=False, required_disk_space=None, semaphore_file=None
):
    """
    :param bool tango: launch writer as process/tango server
    :param bool config: writer uses/ignores extra Redis info
    :param bool alt: anable all options (all disabled by default)
    :param num required_disk_space:
    :patam str semaphore_file:
    """
    fixed = (
        "copy_non_external",
        "resource_profiling",
        "noconfig",
        "disable_external_hdf5",
        "required_disk_space",
        "recommended_disk_space",
        "start_semaphore_file",
    )
    options = all_cli_saveoptions(configurable=config)
    resource_profiling = options["resource_profiling"]["default"]
    if required_disk_space is None:
        # Allow tests to run on very low disk space but still
        # provide a value greater than 0 to ensure disk checks
        # are being done
        required_disk_space = 1

    if writer == "tango":
        properties = {"copy_non_external": True}
        attributes = {}
        properties["noconfig"] = not config
        properties["required_disk_space"] = required_disk_space
        attributes["resource_profiling"] = int(resource_profiling) - 1  # to tango enum
        properties.update({k: alt for k in options if k not in fixed})
        return properties, attributes

    if writer == "process":
        cliargs = ["--copy_non_external"]
        if not config:
            cliargs.append("--noconfig")
        cliargs += ["--required_disk_space", str(required_disk_space)]
        cliargs += ["--resource_profiling", resource_profiling.name]
        if alt:
            cliargs += [f"--{k}" for k in options if k not in fixed]
        if semaphore_file:
            cliargs += ["--start_semaphore_file", str(semaphore_file)]
        return tuple(cliargs)

    roptions = {
        "copy_non_external": True,
        "noconfig": not config,
        "required_disk_space": required_disk_space,
        "resource_profiling": resource_profiling,
    }
    roptions.update({k: alt for k in options if k not in fixed})

    # Convert CLI options to keyword parameters
    ret = dict()
    for k, v in roptions.items():
        option = options[k]
        k = option["dest"]
        if option.get("action") == "store_false":
            v = not v
        ret[k] = v
    return ret


def writer_cli_logargs(tmpdir):
    log = log_levels.log_level_name[get_log_level()]
    return (
        f"--log={log}",  # applies to log_tango as well (abbreviations allowed)
        # "--redirectstdout",
        # "--redirectstderr",
        # "--logfileout={}".format(tmpdir.join("writer.stdout.log")),
        # "--logfileerr={}".format(tmpdir.join("writer.stderr.log")),
    )


def writer_env():
    env = {k: str(v) for k, v in os.environ.items()}
    # env["GEVENT_MONITOR_THREAD_ENABLE"] = "true"
    # env["GEVENT_MAX_BLOCKING_TIME"] = "1"
    return env


def get_log_level():
    return logging.getLogger().getEffectiveLevel()

# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import gevent
import logging
import numpy
from packaging.version import Version
from importlib.metadata import version as get_version
from silx.io import h5py_utils
from bliss.common import scans
from bliss.scanning.group import Sequence
from bliss.scanning.chain import AcquisitionChannel
from tests.nexus_writer.helpers import nxw_test_utils
from tests.nexus_writer.helpers import nxw_test_data


_logger = logging.getLogger(__name__)


def test_nxw_scansequence(nexus_writer_config):
    _test_nxw_scansequence(**nexus_writer_config)


def test_nxw_scansequence_alt(nexus_writer_config_alt):
    _test_nxw_scansequence(**nexus_writer_config_alt)


def test_nxw_scansequence_nopolicy(nexus_writer_config_nopolicy):
    _test_nxw_scansequence(**nexus_writer_config_nopolicy)


def test_nxw_scansequence_base(nexus_writer_base):
    _test_nxw_scansequence(**nexus_writer_base)


def test_nxw_scansequence_base_alt(nexus_writer_base_alt):
    _test_nxw_scansequence(**nexus_writer_base_alt)


def test_nxw_scansequence_base_nopolicy(nexus_writer_base_nopolicy):
    _test_nxw_scansequence(**nexus_writer_base_nopolicy)


@nxw_test_utils.writer_stdout_on_exception
def _test_nxw_scansequence(session=None, tmpdir=None, writer=None, **kwargs):
    try:
        session.scan_saving.dataset.all.definition = "none"
    except AttributeError:
        pass  # data policy disabled
    npoints = 10
    detector1 = session.env_dict["diode3"]
    detector2 = session.env_dict["diode4"]
    motor = session.env_dict["robx"]

    user_scan_info = {"technique": {"foo": 10}}
    seq = Sequence(scan_info=user_scan_info)
    seq.add_custom_channel(AcquisitionChannel("customdata", float, ()))
    seq.add_custom_channel(AcquisitionChannel("diode34", float, ()))
    with gevent.Timeout(30):
        with seq.sequence_context() as scan_seq:
            scan1 = scans.loopscan(npoints, 0.1, detector1, run=False)
            scan2 = scans.ascan(motor, 0, 1, npoints - 1, 0.1, detector2, run=False)
            g1 = nxw_test_utils.run_scan(scan1, runasync=True)
            data = numpy.arange(npoints // 2, dtype=float)
            seq.custom_channels["customdata"].emit(data)
            g2 = nxw_test_utils.run_scan(scan2, runasync=True)
            data = numpy.arange(npoints // 2, npoints, dtype=float)
            seq.custom_channels["customdata"].emit(data)
            gevent.joinall([g1, g2], raise_error=True)
            diode34 = scan1.get_data()["diode3"] + scan2.get_data()["diode4"]
            seq.custom_channels["diode34"].emit(diode34)

            # Check that the technique is already saved
            technique_foo = None
            for _ in range(10):
                filename = seq.scan.writer.get_filename()
                try:
                    if session.scan_saving.writer == "hdf5":
                        # file is already open in this process
                        kw = {"mode": "a"}
                        if Version(get_version("h5py")) >= Version("3.5.0"):
                            kw["locking"] = True
                    else:
                        kw = {"mode": "r"}

                    with h5py_utils.File(filename, **kw) as h5:
                        technique_foo = h5["1.1/technique/foo"][()]
                        break
                except Exception:
                    _logger.error("Error while reading HDF5 file", exc_info=True)
                    gevent.sleep(0.1)
            else:
                raise AssertionError("User technique was not reachable")
            assert technique_foo == 10

            scan_seq.add(scan1)
            scan_seq.add(scan2)
        scan_seq.wait_all_subscans()

    nxw_test_utils.wait_scan_data_finished(
        [scan1, scan2, scan_seq.sequence.scan], writer=writer
    )
    nxw_test_data.assert_scan_data(
        scan1,
        scan_shape=(npoints,),
        positioners=[["elapsed_time", "epoch"]],
        detectors=["diode3"],
        **kwargs
    )
    nxw_test_data.assert_scan_data(
        scan2,
        scan_shape=(npoints,),
        positioners=[["robx"]],
        detectors=["diode4"],
        softtimer="detector",
        **kwargs
    )
    nxw_test_data.assert_scansequence_data(scan_seq.sequence, **kwargs)

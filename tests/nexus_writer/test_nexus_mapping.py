from copy import deepcopy

import h5py

from bliss.common import scans
from bliss.common.data_store import get_default_data_store

from blisswriter.nexus.devices import device_info
from blisswriter.nexus.mapping import get_primary_dataset_path

from tests.nexus_writer.helpers import nxw_test_utils


def test_nexus_mapping(nexus_writer_config):
    bliss_scan = _run_scan(**nexus_writer_config)

    data_store = get_default_data_store()
    _, key = data_store.get_last_scan()
    blissdata_scan = data_store.load_scan(key)

    bliss_devices = bliss_scan.scan_info["nexuswriter"]["devices"]
    writer_devices = device_info(deepcopy(bliss_devices), bliss_scan.scan_info)
    subscan_devices = writer_devices["axis"]

    # Device info on the Bliss side is a subset of device info on the writer side
    subset = bliss_devices["simu1:deadtime_det3"]
    superset = subscan_devices["simu1:deadtime_det3"]
    assert len(subset) < len(superset)
    assert all(item in superset.items() for item in subset.items())

    # Device info on the writer side defines the NeXus structure.
    # Check the primary dataset paths.
    data_paths = [
        get_primary_dataset_path(stream.name, subscan_devices)
        for stream in blissdata_scan.streams.values()
    ]
    filename = bliss_scan.scan_info["filename"]
    scan_nb = bliss_scan.scan_info["scan_nb"]
    _assert_primary_dataset_paths(filename, scan_nb, data_paths)


def _assert_primary_dataset_paths(
    filename: str, scan_nb: str, data_paths: list[str]
) -> None:
    with h5py.File(filename, "r") as nxroot:
        # Check that all data paths exist in the scan
        scan = nxroot[f"/{scan_nb}.1"]
        for data_path in data_paths:
            try:
                assert len(scan[data_path]) == 10, data_path
            except KeyError:
                raise KeyError(f"Dataset {data_path} not found") from None
        # Check that we have all datasets
        assert len(scan["measurement"]) == len(data_paths)


@nxw_test_utils.writer_stdout_on_exception
def _run_scan(session=None, writer=None, **_):
    scan_shape = (10,)
    scan = scans.ascan(
        session.env_dict["robx"], 0, 1, scan_shape[0] - 1, 0.1, run=False
    )
    nxw_test_utils.run_scan(scan)
    nxw_test_utils.wait_scan_data_finished([scan], writer=writer)
    return scan

# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import gevent
import gevent.event
import pytest

from bliss.common.data_store import get_default_data_store
from blissdata.streams.base import CursorGroup
from blissdata.redis_engine.scan import ScanState
from blissdata.redis_engine.exceptions import EndOfStream

from tests.nexus_writer.helpers import nxw_test_utils
from tests.nexus_writer.helpers import nxw_test_data


def test_nxw_timescan(nexus_writer_config):
    _test_nxw_timescan(**nexus_writer_config)


# VDS of MCA raises exception when npoints not equal
# so scan writer is in FAULT state.
@pytest.mark.skip("skip until timescan has same npoints for each channel")
def test_nxw_timescan_alt(nexus_writer_config_alt):
    _test_nxw_timescan(**nexus_writer_config_alt)


def test_nxw_timescan_nopolicy(nexus_writer_config_nopolicy):
    _test_nxw_timescan(**nexus_writer_config_nopolicy)


def test_nxw_timescan_base(nexus_writer_base):
    _test_nxw_timescan(**nexus_writer_base)


@pytest.mark.skip("skip until timescan has same npoints for each channel")
def test_nxw_timescan_base_alt(nexus_writer_base_alt):
    _test_nxw_timescan(**nexus_writer_base_alt)


def test_nxw_timescan_base_nopolicy(nexus_writer_base_nopolicy):
    _test_nxw_timescan(**nexus_writer_base_nopolicy)


@nxw_test_utils.writer_stdout_on_exception
def _test_nxw_timescan(session=None, tmpdir=None, writer=None, **kwargs):
    redis_data = {}
    listening = gevent.event.Event()
    started = gevent.event.Event()
    nminevents = 5

    def listenscan(scan):
        print(f"Listen to scan {scan.info['title']}")
        while scan.state < ScanState.PREPARED:
            scan.update()

        cursor_group = CursorGroup(scan.streams)
        started.set()

        while True:
            try:
                output = cursor_group.read(timeout=0.1)
            except EndOfStream:
                break

            for stream, view in output.items():
                name = stream.name
                n = len(view)
                if name in redis_data:
                    redis_data[name] += n
                else:
                    redis_data[name] = n

    def listensession(data_store):
        listeners = []
        try:
            since = data_store.get_last_scan_timetag()
            listening.set()
            while True:
                since, key = data_store.get_next_scan(since=since)
                listeners.append(gevent.spawn(listenscan, data_store.load_scan(key)))
        finally:
            for g in listeners:
                g.kill()

    def has_enough_data():
        if redis_data:
            names = list(redis_data)
            values = list(redis_data.values())

            nmin = min(values)
            nmax = max(values)
            namemin = names[values.index(nmin)]
            namemax = names[values.index(nmax)]
        else:
            nmin = nmax = namemin = namemax = 0

        print("Events per channel:")
        print(f"  MIN: N={nmin}, NAME={namemin}")
        print(f"  MAX: N={nmax}, NAME={namemax}")
        print(f"Wait until all channels have at least {nminevents} data events ...")

        return len(redis_data) == 171 and all(
            v > nminevents for v in redis_data.values()
        )

    data_store = get_default_data_store()
    glisten = gevent.spawn(listensession, data_store)
    listening.wait(10)
    try:
        with nxw_test_utils.timescan_context(session) as (scan, gscan):
            with gevent.Timeout(100):
                print("Wait for the Redis stream client to start ...")
                started.wait()
                while not has_enough_data():
                    if not gscan:
                        # timescan_context will re-raise the scan error
                        break

                    gevent.sleep(1)
                print(
                    f"All {len(redis_data)} channels have at least {nminevents} data events."
                )
    finally:
        glisten.kill()

    # Verify data
    print("Verify data ...")
    nxw_test_utils.wait_scan_data_finished([scan], writer=writer)
    nxw_test_data.assert_scan_data(
        scan, scan_shape=(0,), positioners=[["elapsed_time", "epoch"]], **kwargs
    )

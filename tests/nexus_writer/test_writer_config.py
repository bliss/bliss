# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest
from blisswriter.utils import config_utils
from blisswriter.utils import scan_utils


@pytest.mark.parametrize("separate_scan_files", (True, False))
def test_config_withoutpolicy(separate_scan_files, nexus_writer_config_nopolicy):
    session = nexus_writer_config_nopolicy["session"]
    tmpdir = nexus_writer_config_nopolicy["tmpdir"]
    writer_object = session.scan_saving.writer_object
    writer_object.separate_scan_files = separate_scan_files
    assert config_utils.beamline() == "id00"
    assert config_utils.institute() == "ESRF"
    assert config_utils.instrument() == "esrf-id00a"
    filenames = scan_utils.session_filenames(config=True)
    expected_filenames = {"dataset": tmpdir.join(session.name, "default_filename.h5")}
    assert filenames == expected_filenames


@pytest.mark.parametrize("separate_scan_files", (True, False))
def test_config_withpolicy(separate_scan_files, nexus_writer_config):
    session = nexus_writer_config["session"]
    tmpdir = nexus_writer_config["tmpdir"]
    writer_object = session.scan_saving.writer_object
    writer_object.separate_scan_files = separate_scan_files
    assert config_utils.beamline() == "id00"
    assert config_utils.institute() == "ESRF"
    assert config_utils.instrument() == "esrf-id00a"
    filenames = scan_utils.session_filenames(config=True)
    proposal_session_name = session.scan_saving.proposal_session_name
    expected_filenames = dict()
    expected_filenames["dataset"] = tmpdir.join(
        session.name,
        "fs1",
        "id00",
        "tmp",
        "testproposal",
        "id00",
        proposal_session_name,
        "RAW_DATA",
        "sample",
        "sample_0001",
        "sample_0001.h5",
    )
    expected_filenames["dataset_collection"] = tmpdir.join(
        session.name,
        "fs1",
        "id00",
        "tmp",
        "testproposal",
        "id00",
        proposal_session_name,
        "RAW_DATA",
        "sample",
        "testproposal_sample.h5",
    )
    expected_filenames["proposal"] = tmpdir.join(
        session.name,
        "fs1",
        "id00",
        "tmp",
        "testproposal",
        "id00",
        proposal_session_name,
        "RAW_DATA",
        "testproposal_id00.h5",
    )
    assert filenames == expected_filenames

# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.


from tests.nexus_writer.helpers import nxw_test_utils
from tests.nexus_writer.helpers import nxw_test_data


def test_nxw_autofilter(nexus_writer_config):
    _test_nxw_autofilter(**nexus_writer_config)


def test_nxw_autofilter_alt(nexus_writer_config_alt):
    _test_nxw_autofilter(**nexus_writer_config_alt)


def test_nxw_autofilter_nopolicy(nexus_writer_config_nopolicy):
    _test_nxw_autofilter(**nexus_writer_config_nopolicy)


def test_nxw_autofilter_base(nexus_writer_base):
    _test_nxw_autofilter(**nexus_writer_base)


def test_nxw_autofilter_base_alt(nexus_writer_base_alt):
    _test_nxw_autofilter(**nexus_writer_base_alt)


def test_nxw_autofilter_base_nopolicy(nexus_writer_base_nopolicy):
    _test_nxw_autofilter(**nexus_writer_base_nopolicy)


@nxw_test_utils.writer_stdout_on_exception
def _test_nxw_autofilter(session=None, tmpdir=None, writer=None, **kwargs):
    sim = session.env_dict["sim_autofilter1_ctrs"]
    lima_simulator = session.env_dict["lima_simulator"]
    scan = sim.auto_filter.ascan(
        sim.axis,
        0,
        1,
        sim.npoints,
        0.1,
        *sim.detectors,
        lima_simulator,
        lima_simulator.bpm,
        run=False,
        save=True,
    )

    detectors = [
        "curratt",
        "ratio",
        "sim_autofilter1_det",
        "sim_autofilter1_det_corr",
        "sim_autofilter1_mon",
        "transm",
        "lima_simulator",
    ]

    nxw_test_utils.run_scan(scan)
    nxw_test_utils.wait_scan_data_finished([scan], writer=writer)
    nxw_test_data.assert_scan_data(
        scan,
        positioners=[["sim_autofilter1_ctrs_axis"]],
        scan_shape=(sim.npoints + 1,),
        softtimer="detector",
        detectors=detectors,
        lima_in_manual_mode=True,
        **kwargs,
    )

# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest
from glob import glob
from blisswriter.utils import scan_utils
from blisswriter.writer import scan_url_info
from blisswriter.io import nexus
from bliss.common import scans
from tests.nexus_writer.helpers import nxw_test_utils


@pytest.mark.parametrize("null_writer", (False, True))
@pytest.mark.parametrize("save", (True, False))
@pytest.mark.parametrize("separate_scan_files", (False, True))
def test_scan_utils(null_writer, save, separate_scan_files, nexus_writer_config):
    _test_scan_utils(
        null_writer=null_writer,
        save=save,
        separate_scan_files=separate_scan_files,
        **nexus_writer_config,
    )


@pytest.mark.parametrize("null_writer", (False, True))
@pytest.mark.parametrize("save", (True, False))
@pytest.mark.parametrize("separate_scan_files", (False, True))
def test_scan_utils_nopolicy(
    null_writer, save, separate_scan_files, nexus_writer_config_nopolicy
):
    _test_scan_utils(
        null_writer=null_writer,
        save=save,
        separate_scan_files=separate_scan_files,
        **nexus_writer_config_nopolicy,
    )


@pytest.mark.parametrize("null_writer", (False, True))
@pytest.mark.parametrize("save", (True, False))
@pytest.mark.parametrize("separate_scan_files", (False, True))
def test_scan_utils_base(null_writer, save, separate_scan_files, nexus_writer_config):
    _test_scan_utils(
        null_writer=null_writer,
        save=save,
        separate_scan_files=separate_scan_files,
        **nexus_writer_config,
    )


@pytest.mark.parametrize("null_writer", (False, True))
@pytest.mark.parametrize("save", (True, False))
@pytest.mark.parametrize("separate_scan_files", (False, True))
def test_scan_utils_base_nopolicy(
    null_writer, save, separate_scan_files, nexus_writer_config_nopolicy
):
    _test_scan_utils(
        null_writer=null_writer,
        save=save,
        separate_scan_files=separate_scan_files,
        **nexus_writer_config_nopolicy,
    )


@nxw_test_utils.writer_stdout_on_exception
def _test_scan_utils(
    session=None,
    tmpdir=None,
    config: bool = True,
    policy: bool = True,
    null_writer: bool = False,
    save: bool = False,
    separate_scan_files: bool = False,
    writer=None,
    **_,
):
    if null_writer:
        session.scan_saving.writer = "null"
    else:
        session.scan_saving.writer_object.separate_scan_files = separate_scan_files
    is_saving = save and not null_writer

    scan = scans.sct(0.1, session.env_dict["diode3"], save=save)

    # Expected file names based in the policy alone (ignore save/writer settings)
    if policy:
        proposal_session_name = session.scan_saving.proposal_session_name
        scan_filename = tmpdir.join(
            session.name,
            "fs1",
            "id00",
            "tmp",
            "testproposal",
            "id00",
            proposal_session_name,
            "RAW_DATA",
            "sample",
            "sample_0001",
            "scan0001",
            "bliss_master.h5",
        )
        dataset_filename = tmpdir.join(
            session.name,
            "fs1",
            "id00",
            "tmp",
            "testproposal",
            "id00",
            proposal_session_name,
            "RAW_DATA",
            "sample",
            "sample_0001",
            "sample_0001.h5",
        )
        collection_filename = tmpdir.join(
            session.name,
            "fs1",
            "id00",
            "tmp",
            "testproposal",
            "id00",
            proposal_session_name,
            "RAW_DATA",
            "sample",
            "testproposal_sample.h5",
        )
        proposal_filename = tmpdir.join(
            session.name,
            "fs1",
            "id00",
            "tmp",
            "testproposal",
            "id00",
            proposal_session_name,
            "RAW_DATA",
            "testproposal_id00.h5",
        )
        master_filenames = {
            "dataset_collection": collection_filename,
            "proposal": proposal_filename,
        }
        filenames = {"dataset": dataset_filename, "scan": scan_filename}
    else:
        scan_filename = tmpdir.join(session.name, "scan0001", "bliss_master.h5")
        dataset_filename = tmpdir.join(session.name, "default_filename.h5")
        master_filenames = dict()
        filenames = {"dataset": dataset_filename, "scan": scan_filename}

    # Scan and dataset filename could be the same
    if separate_scan_files:
        master_filenames["dataset"] = dataset_filename
    else:
        scan_filename = dataset_filename
        filenames["scan"] = filenames["dataset"]

    filenames.update(master_filenames)

    # Check file existence based on policy/save/writer settings
    if is_saving:
        nxw_test_utils.wait_scan_data_exists([scan], writer=writer)
    saves_extra_masters = is_saving and config
    assert scan_filename.check(file=1) == is_saving, scan_filename
    assert dataset_filename.check(file=1) == is_saving, dataset_filename
    for name, f in master_filenames.items():
        assert f.check(file=1) == saves_extra_masters, f
    for name, f in filenames.items():
        expected = is_saving and (name in ("dataset", "scan") or saves_extra_masters)
        assert f.check(file=1) == expected, f

    # Remove unexpected files based on writer settings
    if null_writer:
        scan_filename = None
        dataset_filename = None
        master_filenames = dict()
        filenames = dict()
    elif not config:
        for name in ("dataset_collection", "proposal"):
            for lst in (filenames, master_filenames):
                lst.pop(name, None)

    # Check file names from session (save settings are irrelevant)
    session_filenames = dict(filenames)
    session_filenames.pop("scan", None)
    assert scan_utils.session_filename() == dataset_filename
    assert scan_utils.session_master_filenames(config=config) == master_filenames
    assert scan_utils.session_filenames(config=config) == session_filenames

    # Check scan url
    if scan_filename:
        subscan_url = f"{dataset_filename}::/1.1"
        raw_subscan_url = f"{scan_filename}::/1.1"
    else:
        subscan_url = None
        raw_subscan_url = None
    if raw_subscan_url:
        assert nexus.exists(raw_subscan_url) == is_saving
        assert nexus.exists(subscan_url) == is_saving
    else:
        assert not scan.writer.saving_enabled()

    # Remove unexpected files based on save/writer settings
    if not is_saving:
        scan_filename = None
        dataset_filename = None
        subscan_url = None
        raw_subscan_url = None
        master_filenames = dict()
        filenames = dict()
    elif not config:
        for name in ("dataset_collection", "proposal"):
            for adict in (filenames, master_filenames):
                adict.pop(name, None)

    # Check file names from scan object
    assert scan_url_info.scan_filename(scan.scan_info, raw=True) == scan_filename
    assert scan_url_info.scan_filename(scan.scan_info, raw=False) == dataset_filename
    assert scan_url_info.subscan_url(scan.scan_info, raw=True) == raw_subscan_url
    assert scan_url_info.subscan_url(scan.scan_info, raw=False) == subscan_url
    assert scan_url_info.scan_filenames(scan.scan_info, config=config) == filenames
    assert (
        scan_url_info.scan_master_filenames(scan.scan_info, config=config)
        == master_filenames
    )

    # Check file names from directory
    found = set(glob(str(tmpdir.join("**", "*.h5")), recursive=True))
    expected = set(filter(None, filenames.values()))
    assert found == expected

# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest
from bliss.common import scans
from tests.nexus_writer.helpers import nxw_test_utils
from tests.nexus_writer.helpers import nxw_test_data


@pytest.mark.parametrize("separate_scan_files", (True, False))
@pytest.mark.parametrize("save_images", (True, False))
def test_nxw_loopscan(separate_scan_files, save_images, nexus_writer_config):
    _test_nxw_loopscan(
        **nexus_writer_config,
        save_images=save_images,
        separate_scan_files=separate_scan_files
    )


def test_nxw_loopscan_alt(nexus_writer_config_alt):
    _test_nxw_loopscan(**nexus_writer_config_alt)


@pytest.mark.parametrize("separate_scan_files", (True, False))
def test_nxw_loopscan_nopolicy(separate_scan_files, nexus_writer_config_nopolicy):
    _test_nxw_loopscan(
        **nexus_writer_config_nopolicy, separate_scan_files=separate_scan_files
    )


@pytest.mark.parametrize("separate_scan_files", (True, False))
@pytest.mark.parametrize("save_images", (True, False))
def test_nxw_loopscan_base(separate_scan_files, save_images, nexus_writer_base):
    _test_nxw_loopscan(
        **nexus_writer_base,
        separate_scan_files=separate_scan_files,
        save_images=save_images
    )


def test_nxw_loopscan_base_alt(nexus_writer_base_alt):
    _test_nxw_loopscan(**nexus_writer_base_alt)


@pytest.mark.parametrize("separate_scan_files", (True, False))
def test_nxw_loopscan_base_nopolicy(separate_scan_files, nexus_writer_base_nopolicy):
    _test_nxw_loopscan(
        **nexus_writer_base_nopolicy, separate_scan_files=separate_scan_files
    )


@nxw_test_utils.writer_stdout_on_exception
def _test_nxw_loopscan(
    session=None,
    tmpdir=None,
    writer=None,
    save_images=True,
    separate_scan_files=False,
    **kwargs
):
    session.scan_saving.writer_object.separate_scan_files = separate_scan_files
    scan_shape = (10,)
    scan = scans.loopscan(scan_shape[0], 0.1, run=False, save_images=save_images)
    nxw_test_utils.run_scan(scan)
    nxw_test_utils.wait_scan_data_finished([scan], writer=writer)
    nxw_test_data.assert_scan_data(
        scan,
        scan_shape=scan_shape,
        positioners=[["elapsed_time", "epoch"]],
        save_images=save_images,
        **kwargs
    )

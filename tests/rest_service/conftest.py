import pytest
import socket
import socketio
import gevent
from typing import Any
from bliss.rest_service.rest_service import RestService
from bliss.common.greenlet_utils import asyncio_gevent


@pytest.fixture
def rest_session(beacon, scan_tmpdir):
    session = beacon.get("rest_session")
    session.setup()
    session.scan_saving.base_path = str(scan_tmpdir)
    yield session
    session.close()


@pytest.fixture
def rest_service(rest_session):
    """Rest service on top of the rest_session"""
    service = RestService(rest_session)
    service.start()
    yield service
    service.stop()


class CapSocketioEvents:
    """Capture events from a socketio"""

    def __init__(self, address: str, namespace: str):
        self._address = address
        self._namespace = namespace
        self._listener: gevent.Greenlet | None = None
        self._events: list[Any] = []

    @property
    def events(self) -> list[Any]:
        return self._events

    def connect(self):
        assert self._listener is None

        sio = socketio.AsyncClient()

        @sio.event
        async def connect():
            pass

        async def any_event(event, data):
            self._events.append((event, data))

        sio.on("*", any_event, namespace=self._namespace)

        @sio.event
        async def disconnect(reason: str):
            print("CapSocketioEvents disconnected", self._namespace, reason)

        g = asyncio_gevent.future_to_greenlet(
            sio.connect(self._address, transports=["websocket"])
        )
        g.join()

        async def main():
            try:
                await sio.wait()
            except Exception as e:
                print("CapSocketioEvents closed:", e)
            finally:
                await sio.shutdown()

        self._listener = asyncio_gevent.future_to_greenlet(main())

    def shutdown(self):
        assert self._listener is not None
        self._listener.kill()
        self._listener = None


@pytest.fixture
def hardware_capsocket(rest_service, asyncio_event_loop):
    cap = CapSocketioEvents(
        address=f"http://{socket.gethostname()}:{rest_service._port}/api",
        namespace="/object",
    )
    cap.connect()
    yield cap
    cap.shutdown()


@pytest.fixture
def call_capsocket(asyncio_event_loop, rest_service):
    cap = CapSocketioEvents(
        address=f"http://{socket.gethostname()}:{rest_service._port}/api",
        namespace="/call",
    )
    cap.connect()
    yield cap
    cap.shutdown()

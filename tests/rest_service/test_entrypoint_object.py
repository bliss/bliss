import json
import gevent


def test_property_set(rest_service):
    rest_service.object_store.register_object("rest_test_obj")
    with rest_service.app.app_context():
        client = rest_service.app.test_client()
        payload = json.dumps({"property": "string", "value": "bar"})
        response = client.put(
            "/api/object/rest_test_obj",
            headers={"Content-Type": "application/json"},
            data=payload,
        )
        assert response.status_code == 200, response.text
    gevent.sleep(0.5)
    obj = rest_service.object_store.get_object("rest_test_obj")
    assert obj.state.model_dump()["properties"]["string"] == "bar"


def test_property_set_wrong_type(rest_service):
    rest_service.object_store.register_object("rest_test_obj")
    with rest_service.app.app_context():
        client = rest_service.app.test_client()
        payload = json.dumps({"property": "string", "value": 10})
        response = client.put(
            "/api/object/rest_test_obj",
            headers={"Content-Type": "application/json"},
            data=payload,
        )
        assert response.status_code == 422, response.text
    gevent.sleep(0.5)
    obj = rest_service.object_store.get_object("rest_test_obj")
    assert obj.state.model_dump()["properties"]["string"] == "abcd"


def test_property_set_invalid_value(rest_service):
    rest_service.object_store.register_object("rest_test_obj")
    with rest_service.app.app_context():
        client = rest_service.app.test_client()
        payload = json.dumps({"property": "number_positive", "value": -10})
        response = client.put(
            "/api/object/rest_test_obj",
            headers={"Content-Type": "application/json"},
            data=payload,
        )
        assert response.status_code == 400, response.text
    gevent.sleep(0.5)
    obj = rest_service.object_store.get_object("rest_test_obj")
    assert obj.state.model_dump()["properties"]["number_positive"] == 1


def test_property_event(rest_service, hardware_capsocket, clean_gevent):
    clean_gevent["end-check"] = False
    clean_gevent["ignore-cannot-released"] = True

    rest_service.object_store.register_object("rest_test_obj")
    obj = rest_service.object_store.get_object("rest_test_obj")
    obj.set("string", "a")
    gevent.sleep(1.0)
    assert len(hardware_capsocket.events) == 1
    event = hardware_capsocket.events[0]
    assert event[0] == "change"
    assert event[1] == {
        "id": "rest_test_obj",
        "name": "rest_test_obj",
        "data": {"string": "a"},
    }


def test_register_and_get(rest_service):
    with rest_service.app.app_context():
        client = rest_service.app.test_client()
        payload = json.dumps({"names": ["rest_test_obj"]})
        response = client.post(
            "/api/object",
            headers={"Content-Type": "application/json"},
            data=payload,
        )
        assert response.status_code == 200, response.text

        response = client.get(
            "/api/object/rest_test_obj",
        )
        assert response.status_code == 200, response.text
        result = response.json
        assert result["name"] == "rest_test_obj"


def test_unregister_and_get(rest_service):
    with rest_service.app.app_context():
        client = rest_service.app.test_client()
        payload = json.dumps({"names": ["rest_test_obj"]})
        response = client.post(
            "/api/object",
            headers={"Content-Type": "application/json"},
            data=payload,
        )
        assert response.status_code == 200, response.text

        response = client.delete(
            "/api/object/rest_test_obj",
        )
        assert response.status_code == 204, response.text

        response = client.get(
            "/api/object/rest_test_obj",
        )
        assert response.status_code == 404, response.text


def test_call_api_no_args(rest_service):
    with rest_service.app.app_context():
        client = rest_service.app.test_client()
        payload = json.dumps({"names": ["rest_test_obj"]})
        response = client.post(
            "/api/object",
            headers={"Content-Type": "application/json"},
            data=payload,
        )
        assert response.status_code == 200, response.text

        payload = json.dumps({"function": "func0", "object": "rest_test_obj"})
        response = client.post(
            "/api/call",
            headers={"Content-Type": "application/json"},
            data=payload,
        )
        assert response.status_code == 200, response.text
        result = response.json
        call_id = result.get("call_id")
        assert call_id is not None, result

        while True:
            response = client.get(
                f"/api/call/{call_id}",
            )
            assert response.status_code == 200, response.text
            result = response.json
            if result.get("state") != "running":
                break
            gevent.sleep(0.1)

        assert result == {"return_value": None, "state": "terminated"}


def test_call_api_one_arg(rest_service):
    with rest_service.app.app_context():
        client = rest_service.app.test_client()
        payload = json.dumps({"names": ["rest_test_obj"]})
        response = client.post(
            "/api/object",
            headers={"Content-Type": "application/json"},
            data=payload,
        )
        assert response.status_code == 200, response.text

        payload = json.dumps(
            {"function": "func1", "args": ["Hey"], "object": "rest_test_obj"}
        )
        response = client.post(
            "/api/call",
            headers={"Content-Type": "application/json"},
            data=payload,
        )
        assert response.status_code == 200, response.text
        result = response.json
        call_id = result.get("call_id")
        assert call_id is not None, result

        while True:
            response = client.get(
                f"/api/call/{call_id}",
            )
            assert response.status_code == 200, response.text
            result = response.json
            if result.get("state") != "running":
                break
            gevent.sleep(0.1)

        assert result == {"return_value": None, "state": "terminated"}


def test_call_api_result(rest_service):
    with rest_service.app.app_context():
        client = rest_service.app.test_client()
        payload = json.dumps({"names": ["rest_test_obj"]})
        response = client.post(
            "/api/object",
            headers={"Content-Type": "application/json"},
            data=payload,
        )
        assert response.status_code == 200, response.text

        payload = json.dumps(
            {"function": "func_mul", "args": [3, 4], "object": "rest_test_obj"}
        )
        response = client.post(
            "/api/call",
            headers={"Content-Type": "application/json"},
            data=payload,
        )
        assert response.status_code == 200, response.text
        result = response.json
        call_id = result.get("call_id")
        assert call_id is not None, result

        while True:
            response = client.get(
                f"/api/call/{call_id}",
            )
            assert response.status_code == 200, response.text
            result = response.json
            if result.get("state") != "running":
                break
            gevent.sleep(0.1)

        assert result == {"return_value": 12, "state": "terminated"}


def test_call_api_inf(rest_service):
    with rest_service.app.app_context():
        client = rest_service.app.test_client()
        payload = json.dumps({"names": ["rest_test_obj"]})
        response = client.post(
            "/api/object",
            headers={"Content-Type": "application/json"},
            data=payload,
        )
        assert response.status_code == 200, response.text

        payload = json.dumps(
            {
                "function": "func_mul",
                "args": [3, {"__type__": "posinf"}],
                "object": "rest_test_obj",
            }
        )
        response = client.post(
            "/api/call",
            headers={"Content-Type": "application/json"},
            data=payload,
        )
        assert response.status_code == 200, response.text
        result = response.json
        call_id = result.get("call_id")
        assert call_id is not None, result

        while True:
            response = client.get(
                f"/api/call/{call_id}",
            )
            assert response.status_code == 200, response.text
            result = response.json
            if result.get("state") != "running":
                break
            gevent.sleep(0.1)

        assert result == {"return_value": {"__type__": "posinf"}, "state": "terminated"}


def test_call_api_with_unknown_function(rest_service):
    with rest_service.app.app_context():
        client = rest_service.app.test_client()
        payload = json.dumps({"names": ["rest_test_obj"]})
        response = client.post(
            "/api/object",
            headers={"Content-Type": "application/json"},
            data=payload,
        )
        assert response.status_code == 200, response.text

        payload = json.dumps(
            {"function": "non_existing_func", "object": "rest_test_obj"}
        )
        response = client.post(
            "/api/call",
            headers={"Content-Type": "application/json"},
            data=payload,
        )
        assert response.status_code == 404, response.text


def test_call_api_with_wrong_type(rest_service):
    with rest_service.app.app_context():
        client = rest_service.app.test_client()
        payload = json.dumps({"names": ["rest_test_obj"]})
        response = client.post(
            "/api/object",
            headers={"Content-Type": "application/json"},
            data=payload,
        )
        assert response.status_code == 200, response.text

        payload = json.dumps(
            {"function": "func1", "args": [1], "object": "rest_test_obj"}
        )
        response = client.post(
            "/api/call",
            headers={"Content-Type": "application/json"},
            data=payload,
        )
        assert response.status_code == 422, response.text


def test_abort_call(rest_service):
    """Check that the function is called with a BLISS object"""

    with rest_service.app.app_context():
        client = rest_service.app.test_client()
        payload: dict
        payload = {"names": ["rest_test_obj"]}
        response = client.post(
            "/api/object",
            headers={"Content-Type": "application/json"},
            data=json.dumps(payload),
        )
        assert response.status_code == 200, response.text

        payload = {"function": "long_process", "object": "rest_test_obj"}
        response = client.post(
            "/api/call",
            headers={"Content-Type": "application/json"},
            data=json.dumps(payload),
        )
        assert response.status_code == 200, response.text
        result = response.json
        call_id = result.get("call_id")
        assert call_id is not None, result

        for _ in range(10):
            response = client.get(f"/api/call/{call_id}")
            assert response.status_code == 200, response.text
            result = response.json
            if result.get("state") != "running":
                break
            gevent.sleep(0.1)

        # Should still be running
        assert result.get("state") == "running"

        response = client.delete(f"/api/call/{call_id}")
        assert response.status_code == 204, response.text

        for _ in range(20):
            response = client.get(f"/api/call/{call_id}")
            assert response.status_code == 200, response.text
            result = response.json
            if result.get("state") != "running":
                break
            gevent.sleep(0.1)

        assert result == {"state": "killed"}


def test_missing_object(rest_service):
    with rest_service.app.app_context():
        client = rest_service.app.test_client()
        payload = json.dumps({"names": ["not_an_object_name"]})
        response = client.post(
            "/api/object",
            headers={"Content-Type": "application/json"},
            data=payload,
        )
        assert response.status_code == 400, response.text
        assert response.json == {
            "error": "Could not register hardware objects",
            "objects": [
                {
                    "error": "Object 'not_an_object_name' doesn't exist in config",
                    "name": "not_an_object_name",
                },
            ],
        }


def test_online_event(rest_service, hardware_capsocket, clean_gevent):
    clean_gevent["end-check"] = False
    clean_gevent["ignore-cannot-released"] = True

    rest_service.object_store.register_object("rest_test_obj")
    obj = rest_service.object_store.get_object("rest_test_obj")
    obj.set_online(False)
    gevent.sleep(1.0)
    assert len(hardware_capsocket.events) == 1
    event = hardware_capsocket.events[0]
    assert event[0] == "online"
    assert event[1] == {
        "id": "rest_test_obj",
        "name": "rest_test_obj",
        "state": False,
    }


def test_locking_event(rest_service, hardware_capsocket, clean_gevent):
    clean_gevent["end-check"] = False
    clean_gevent["ignore-cannot-released"] = True

    rest_service.object_store.register_object("rest_test_obj")
    obj = rest_service.object_store.get_object("rest_test_obj")
    obj.set_locked(reason="It's mine")
    gevent.sleep(1.0)
    assert len(hardware_capsocket.events) == 1
    event = hardware_capsocket.events[0]
    assert event[0] == "locked"
    assert event[1] == {
        "id": "rest_test_obj",
        "name": "rest_test_obj",
        "state": {"reason": "It's mine"},
    }

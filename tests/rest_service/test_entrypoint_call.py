import pytest
import gevent
import json


def test_async_call(rest_service):
    with rest_service.app.app_context():
        client = rest_service.app.test_client()
        payload = json.dumps({"function": "lsmot"})
        response = client.post(
            "/api/call",
            headers={"Content-Type": "application/json"},
            data=payload,
        )
        assert response.status_code == 200, response.text
        result = response.json
        call_id = result.get("call_id")
        assert call_id is not None, result

        while True:
            response = client.get(
                f"/api/call/{call_id}",
            )
            assert response.status_code == 200, response.text
            result = response.json
            if result.get("state") != "running":
                break
            gevent.sleep(0.1)

        assert result == {"return_value": None, "state": "terminated"}


def test_function_from_object(rest_service):
    """Test the `/call` API with a registred object"""
    rest_service.object_store.register_object("rest_test_obj")
    with rest_service.app.app_context():
        client = rest_service.app.test_client()
        payload = json.dumps({"object": "rest_test_obj", "function": "func0"})
        response = client.post(
            "/api/call",
            headers={"Content-Type": "application/json"},
            data=payload,
        )
        assert response.status_code == 200, response.text
        result = response.json
        call_id = result.get("call_id")
        assert call_id is not None, result

        while True:
            response = client.get(
                f"/api/call/{call_id}",
            )
            assert response.status_code == 200, response.text
            result = response.json
            if result.get("state") != "running":
                break
            gevent.sleep(0.1)

            client.post
        assert result == {"return_value": None, "state": "terminated"}


def test_function_from_env_object(rest_session, rest_service):
    class Foo:
        def __init__(self, obj):
            self.__obj = obj

        @property
        def ref(self):
            return self.__obj

    class Message:
        def get_message(self) -> str:
            return "Hi!"

    rest_session.env_dict["my_obj"] = Foo(Foo(Message()))

    with rest_service.app.app_context():
        client = rest_service.app.test_client()
        payload = json.dumps(
            {"env_object": "my_obj.ref.ref", "function": "get_message"}
        )
        response = client.post(
            "/api/call",
            headers={"Content-Type": "application/json"},
            data=payload,
        )
        assert response.status_code == 200, response.text
        result = response.json
        call_id = result.get("call_id")
        assert call_id is not None, result

        while True:
            response = client.get(
                f"/api/call/{call_id}",
            )
            assert response.status_code == 200, response.text
            result = response.json
            if result.get("state") != "running":
                break
            gevent.sleep(0.1)

        assert result == {"return_value": "Hi!", "state": "terminated"}


def test_non_callable(rest_session, rest_service):
    class Foo:
        def __init__(self, obj):
            self.__obj = obj

        @property
        def ref(self):
            return self.__obj

    class Message:
        def get_message(self) -> str:
            return "hi!"

    rest_session.env_dict["my_obj"] = Foo(Foo(Message()))

    with rest_service.app.app_context():
        client = rest_service.app.test_client()
        payload = json.dumps({"env_object": "my_obj.ref", "function": "ref"})
        response = client.post(
            "/api/call",
            headers={"Content-Type": "application/json"},
            data=payload,
        )
        assert response.status_code == 404, response.text


def test_call_with_obj_argument(rest_session, rest_service):
    """Check that the function is called with a BLISS object"""

    def get_obj_name(obj):
        return obj.name

    rest_session.env_dict["get_obj_name"] = get_obj_name

    with rest_service.app.app_context():
        client = rest_service.app.test_client()
        payload = json.dumps(
            {
                "function": "get_obj_name",
                "args": [{"__type__": "object", "name": "roby"}],
            }
        )
        response = client.post(
            "/api/call",
            headers={"Content-Type": "application/json"},
            data=payload,
        )
        assert response.status_code == 200, response.text
        result = response.json
        call_id = result.get("call_id")
        assert call_id is not None, result

        while True:
            response = client.get(
                f"/api/call/{call_id}",
            )
            assert response.status_code == 200, response.text
            result = response.json
            if result.get("state") != "running":
                break
            gevent.sleep(0.1)

        assert result == {"return_value": "roby", "state": "terminated"}


def test_call_with_obj_return(rest_session, rest_service):
    """Check that the function is called with a BLISS object"""

    def get_axis():
        return rest_session.config.get("roby")

    rest_session.env_dict["get_axis"] = get_axis

    with rest_service.app.app_context():
        client = rest_service.app.test_client()
        payload = json.dumps({"function": "get_axis"})
        response = client.post(
            "/api/call",
            headers={"Content-Type": "application/json"},
            data=payload,
        )
        assert response.status_code == 200, response.text
        result = response.json
        call_id = result.get("call_id")
        assert call_id is not None, result

        while True:
            response = client.get(
                f"/api/call/{call_id}",
            )
            assert response.status_code == 200, response.text
            result = response.json
            if result.get("state") != "running":
                break
            gevent.sleep(0.1)

        assert result == {
            "return_value": {"__type__": "object", "name": "roby"},
            "state": "terminated",
        }


def test_call_has_scan_factory(rest_session, rest_service):
    """Check that the function is called with a BLISS object"""
    rest_session.config.get("diode")

    with rest_service.app.app_context():
        client = rest_service.app.test_client()
        payload = {
            "function": "ct",
            "args": [1.0, {"__type__": "object", "name": "diode"}],
            "has_scan_factory": True,
        }
        response = client.post(
            "/api/call",
            headers={"Content-Type": "application/json"},
            data=json.dumps(payload),
        )
        assert response.status_code == 200, response.text
        result = response.json
        call_id = result.get("call_id")
        assert call_id is not None, result

        while True:
            response = client.get(
                f"/api/call/{call_id}",
            )
            assert response.status_code == 200, response.text
            result = response.json
            if result.get("state") != "running":
                break
            gevent.sleep(0.1)

        assert (
            result.get("progress", {})
            .get("scan", {})
            .get("key", "")
            .startswith("esrf:scan:")
        )
        result.get("progress", {}).get("scan", {})["key"] = "####"
        result.get("return_value", {})["key"] = "####"
        assert result == {
            "state": "terminated",
            "return_value": {"__type__": "scan", "key": "####"},
            "progress": {
                "scan": {"__type__": "scan", "key": "####"},
            },
        }


def test_abort_call(rest_session, rest_service):
    """Check that the function is called with a BLISS object"""

    def long_process():
        import gevent

        return gevent.sleep(20.0)

    rest_session.env_dict["long_process"] = long_process

    with rest_service.app.app_context():
        client = rest_service.app.test_client()
        payload = {"function": "long_process"}
        response = client.post(
            "/api/call",
            headers={"Content-Type": "application/json"},
            data=json.dumps(payload),
        )
        assert response.status_code == 200, response.text
        result = response.json
        call_id = result.get("call_id")
        assert call_id is not None, result

        for _ in range(10):
            response = client.get(f"/api/call/{call_id}")
            assert response.status_code == 200, response.text
            result = response.json
            if result.get("state") != "running":
                break
            gevent.sleep(0.1)

        # Should still be running
        assert result.get("state") == "running"

        response = client.delete(f"/api/call/{call_id}")
        assert response.status_code == 204, response.text

        for _ in range(20):
            response = client.get(f"/api/call/{call_id}")
            assert response.status_code == 200, response.text
            result = response.json
            if result.get("state") != "running":
                break
            gevent.sleep(0.1)

        assert result == {"state": "killed"}


def test_async_call_in_terminal(
    rest_session, clear_pt_context, rest_service, bliss_repl
):
    """
    Check execution of a call into the BLISS REPL terminal.

    This also check that tool based on prompt toolkit UI are working well.
    """
    with bliss_repl() as br:
        rest_session._set_bliss_repl(br)
        with br.run_context():
            with rest_service.app.app_context():
                client = rest_service.app.test_client()
                payload = json.dumps(
                    {
                        "function": "umv",
                        "args": [{"__type__": "object", "name": "roby"}, 1.0],
                        "in_terminal": True,
                    }
                )
                response = client.post(
                    "/api/call",
                    headers={"Content-Type": "application/json"},
                    data=payload,
                )
                assert response.status_code == 200, response.text
                result = response.json
                call_id = result.get("call_id")
                assert call_id is not None, result

                while True:
                    response = client.get(
                        f"/api/call/{call_id}",
                    )
                    assert response.status_code == 200, response.text
                    result = response.json
                    if result.get("state") != "running":
                        break
                    gevent.sleep(0.1)

                assert result == {"return_value": None, "state": "terminated"}
                gevent.sleep(0.1)


def test_abort_async_call_in_terminal(
    rest_session, clear_pt_context, rest_service, bliss_repl
):
    """
    Check interruption from the REST API of a call into the BLISS REPL terminal.
    """
    something_state = "uninitialized"

    def setup_something():
        nonlocal something_state
        something_state = "processing"
        try:
            gevent.sleep(10.0)
        except BaseException:
            something_state = "aborted"
            raise
        else:
            something_state = "done"

    rest_session.env_dict["setup_something"] = setup_something

    with bliss_repl() as br:
        rest_session._set_bliss_repl(br)
        with br.run_context():
            with rest_service.app.app_context():
                client = rest_service.app.test_client()
                payload = json.dumps(
                    {
                        "function": "setup_something",
                        "in_terminal": True,
                    }
                )
                response = client.post(
                    "/api/call",
                    headers={"Content-Type": "application/json"},
                    data=payload,
                )
                assert response.status_code == 200, response.text
                result = response.json
                call_id = result.get("call_id")
                assert call_id is not None, result

                gevent.sleep(1.0)
                response = client.delete(
                    f"/api/call/{call_id}",
                )
                assert response.status_code == 204, response.text

                gevent.sleep(0.1)

                while True:
                    response = client.get(
                        f"/api/call/{call_id}",
                    )
                    assert response.status_code == 200, response.text
                    result = response.json
                    if result.get("state") != "running":
                        break
                    gevent.sleep(0.1)

                gevent.sleep(0.1)

    assert result == {"state": "killed"}
    assert (
        something_state == "aborted"
    ), f"The task was not aborted. Found: {something_state}"


def test_async_call_with_events(
    rest_session,
    clear_pt_context,
    rest_service,
    call_capsocket,
    clean_gevent,
    bliss_repl,
):
    """
    Check execution of a call remotely with stdout events.
    """
    clean_gevent["end-check"] = False
    clean_gevent["ignore-cannot-released"] = True

    def show_something():
        from bliss.shell.standard import print_html

        print("Here is something")
        print_html("Here is <red>red text</red>")

    rest_session.env_dict["show_something"] = show_something

    with bliss_repl() as br:
        rest_session._set_bliss_repl(br)
        with br.run_context():
            with rest_service.app.app_context():
                client = rest_service.app.test_client()
                payload = json.dumps(
                    {
                        "function": "show_something",
                        "emit_stdout": True,
                    }
                )
                response = client.post(
                    "/api/call",
                    headers={"Content-Type": "application/json"},
                    data=payload,
                )
                assert response.status_code == 200, response.text
                result = response.json
                call_id = result.get("call_id")
                assert call_id is not None, result

                while True:
                    response = client.get(
                        f"/api/call/{call_id}",
                    )
                    assert response.status_code == 200, response.text
                    result = response.json
                    if result.get("state") != "running":
                        break
                    gevent.sleep(0.1)

                assert result == {"return_value": None, "state": "terminated"}
                gevent.sleep(0.1)

    evts = call_capsocket.events
    assert len(evts) != 0
    assert "stdout" in evts[0][0]
    assert "call_id" in evts[0][1]
    assert "message" in evts[0][1]

    stdout = "".join([e[1]["message"] for e in evts])
    assert "Here is something" in stdout
    # For now the formatting is gone
    assert "Here is red text" in stdout


@pytest.mark.skip()
def _test_async_call_in_terminal_with_events(
    rest_session,
    clear_pt_context,
    rest_service,
    bliss_repl,
    call_capsocket,
    clean_gevent,
):
    """
    Check execution of a call remotely, executed in the terminal and sending
    back stdout events.

    Actually this test is not working. The `Output` object used for test is
    not `BlissOutput` which make the test harder to test.
    """
    clean_gevent["end-check"] = False
    clean_gevent["ignore-cannot-released"] = True

    def show_something():
        from bliss.shell.standard import print_html

        print("Here is something")
        print_html("Here is <red>red text</red>")

    rest_session.env_dict["show_something"] = show_something

    with bliss_repl() as br:
        rest_session._set_bliss_repl(br)
        with br.run_context():
            with rest_service.app.app_context():
                client = rest_service.app.test_client()
                payload = json.dumps(
                    {
                        "function": "show_something",
                        "emit_stdout": True,
                        "in_terminal": True,
                    }
                )
                response = client.post(
                    "/api/call",
                    headers={"Content-Type": "application/json"},
                    data=payload,
                )
                assert response.status_code == 200, response.text
                result = response.json
                call_id = result.get("call_id")
                assert call_id is not None, result

                while True:
                    response = client.get(
                        f"/api/call/{call_id}",
                    )
                    assert response.status_code == 200, response.text
                    result = response.json
                    if result.get("state") != "running":
                        break
                    gevent.sleep(0.1)

                assert result == {"return_value": None, "state": "terminated"}
                gevent.sleep(0.1)

    evts = call_capsocket.events
    assert len(evts) != 0
    assert "stdout" in evts[0][0]
    assert "call_id" in evts[0][1]
    assert "message" in evts[0][1]

    stdout = "".join([e[1]["message"] for e in evts])
    assert "Here is something" in stdout
    # For now the formatting is gone
    assert "Here is red text" in stdout

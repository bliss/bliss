DISCONNECTED_REF = {
    "name": "lima_simulator",
    "type": "lima",
    "online": True,
    "errors": [],
    "alias": None,
    "properties": {
        "state": "OFFLINE",
        "static": None,
        "rotation": 90,
        "binning": [1, 1],
        "raw_roi": [0, 0, 0, 0],
        "roi": [0, 0, 0, 0],
        "flip": [False, False],
        "size": [0, 0],
        "acc_max_expo_time": None,
    },
    "user_tags": [],
    "locked": None,
}

CONNECTED_REF = {
    "name": "lima_simulator",
    "type": "lima",
    "online": True,
    "errors": [],
    "alias": None,
    "properties": {
        "state": "READY",
        "static": {
            "lima_version": "1.10.1",
            "lima_type": "Simulator",
            "camera_type": "Simulator",
            "camera_model": "Generator",
            "camera_pixelsize": [1.0, 1.0],
            "image_max_dim": [1024, 1024],
        },
        "rotation": 90,
        "binning": [1, 1],
        "raw_roi": [0, 0, 0, 0],
        "roi": [0, 0, 0, 0],
        "flip": [False, False],
        "size": [0, 0],
        "acc_max_expo_time": 2.0,
    },
    "user_tags": [],
    "locked": None,
}


def test_disconnected(rest_service):
    rest_service.object_store.register_object("lima_simulator")
    mockupshutter = rest_service.object_store.get_object("lima_simulator")
    assert mockupshutter.state.model_dump() == DISCONNECTED_REF


def test_connected(rest_service, lima_simulator):
    rest_service.object_store.register_object("lima_simulator")
    mockupshutter = rest_service.object_store.get_object("lima_simulator")
    assert mockupshutter.state.model_dump() == CONNECTED_REF

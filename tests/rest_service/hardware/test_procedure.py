import pytest
import gevent
from bliss import current_session
import os


OBJ_REF = {
    "name": "procedure1",
    "type": "procedure",
    "online": True,
    "errors": [],
    "alias": None,
    "properties": {
        "state": "STANDBY",
        "previous_run_state": "NONE",
        "previous_run_exception": None,
        "parameters": {},
    },
    "user_tags": [],
    "locked": None,
}


@pytest.fixture
def setup_user_script(beacon_directory):
    script_root = os.path.join(beacon_directory, "sessions", "scripts")
    current_session.user_script_homedir(script_root)


def test_state(rest_service, setup_user_script):
    rest_service.object_store.register_object("procedure1")
    obj = rest_service.object_store.get_object("procedure1")
    result = obj.state.model_dump()
    assert result == OBJ_REF


def test_run(rest_service, setup_user_script):
    rest_service.object_store.register_object("procedure1")
    obj = rest_service.object_store.get_object("procedure1")
    obj.call("start")
    gevent.sleep(2.0)
    result = obj.state.model_dump()
    assert result["properties"] == {
        "state": "STANDBY",
        "previous_run_state": "FAILED",
        "previous_run_exception": "ARG",
        "parameters": {},
    }

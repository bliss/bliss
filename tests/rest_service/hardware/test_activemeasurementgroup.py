from bliss.common.measurementgroup import get_active_name

ACTIVE_MG_REF = {
    "name": "ACTIVE_MG",
    "type": "objectref",
    "online": True,
    "errors": [],
    "alias": None,
    "properties": {"ref": "hardware:"},
    "user_tags": [],
    "locked": None,
}


def test_state(rest_service):
    rest_service.object_store.register_object("ACTIVE_MG")
    rest_obj = rest_service.object_store.get_object("ACTIVE_MG")
    assert rest_obj.state.model_dump() == ACTIVE_MG_REF


def test_activate_from_bliss(rest_session, rest_service):
    """Check that the active MG is updated"""
    rest_service.object_store.register_object("ACTIVE_MG")

    config = rest_session.config
    config.get("m1enc")
    config.get("diode")
    test_mg_enc = config.get("test_mg_enc")
    test_mg_enc.set_active()

    rest_obj = rest_service.object_store.get_object("ACTIVE_MG")
    assert rest_obj.state.model_dump()["properties"]["ref"] == "hardware:test_mg_enc"


def test_activate_from_rest(rest_session, rest_service):
    """Check that the active MG is updated"""
    rest_service.object_store.register_object("ACTIVE_MG")

    config = rest_session.config
    test_mg = config.get("test_mg")

    rest_obj = rest_service.object_store.get_object("ACTIVE_MG")
    rest_obj.set("ref", f"hardware:{test_mg.name}")
    assert get_active_name() == test_mg.name

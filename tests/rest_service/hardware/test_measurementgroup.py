MG_REF = {
    "name": "test_mg_enc",
    "type": "measurementgroup",
    "online": True,
    "errors": [],
    "alias": None,
    "properties": {
        "available": [
            "simulation_diode_sampling_controller:diode",
            "encoder:m1enc",
        ],
        "disabled": [],
    },
    "user_tags": [],
    "locked": None,
}


def test_mg(rest_session, rest_service):
    config = rest_session.config
    config.get("m1enc")
    config.get("diode")

    rest_service.object_store.register_object("test_mg_enc")
    robz = rest_service.object_store.get_object("test_mg_enc")
    assert robz.state.model_dump() == MG_REF


def test_add_available_from_bliss(rest_session, rest_service):
    """Check that the available field is updated during the life-cycle of the MG"""
    config = rest_session.config
    config.get("m1enc")
    config.get("diode")
    m2enc = config.get("m2enc")

    rest_service.object_store.register_object("test_mg_enc")
    test_mg_enc = config.get("test_mg_enc")
    test_mg_enc.add(m2enc)

    robz = rest_service.object_store.get_object("test_mg_enc")
    assert robz.state.model_dump()["properties"]["available"] == [
        "simulation_diode_sampling_controller:diode",
        "encoder:m1enc",
        "encoder:m2enc",
    ]


def test_disabled_from_bliss(rest_session, rest_service):
    """Check that the disabled field is updated during the life-cycle of the MG"""
    config = rest_session.config
    m1enc = config.get("m1enc")
    config.get("diode")

    rest_service.object_store.register_object("test_mg_enc")

    test_mg_enc = config.get("test_mg_enc")
    test_mg_enc.disable(m1enc.fullname)

    robz = rest_service.object_store.get_object("test_mg_enc")
    assert robz.state.model_dump()["properties"]["disabled"] == ["encoder:m1enc"]

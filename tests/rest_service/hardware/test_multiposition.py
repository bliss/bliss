OBJ_REF = {
    "name": "beamstop",
    "type": "multiposition",
    "online": True,
    "errors": [],
    "alias": None,
    "properties": {
        "state": "READY",
        "position": "unknown",
        "positions": [
            {
                "position": "IN",
                "description": "Beamstop position IN the beam",
                "target": [
                    {"object": "bsy", "destination": 2.5, "tolerance": 0.01},
                    {"object": "bsz", "destination": 1.0, "tolerance": 0.2},
                ],
            },
            {
                "position": "OUT",
                "description": "Beamstop position OUT of the beam",
                "target": [
                    {"object": "bsy", "destination": 3.5, "tolerance": 0.01},
                    {"object": "bsz", "destination": 2.0, "tolerance": 0.2},
                ],
            },
        ],
    },
    "user_tags": [],
    "locked": None,
}


def test_state(rest_session, rest_service):
    # Load it this way to force it to be part of the session
    # So what it is cleaned up automatically at end
    rest_session.config.get("beamstop")

    rest_service.object_store.register_object("beamstop")
    obj = rest_service.object_store.get_object("beamstop")
    assert obj.state.model_dump() == OBJ_REF

OBJ_REF = {
    "name": "actuator_cmd",
    "type": "actuator",
    "online": True,
    "errors": [],
    "alias": None,
    "properties": {"state": "UNKNOWN"},
    "user_tags": [],
    "locked": None,
}


def test_state(rest_service):
    rest_service.object_store.register_object("actuator_cmd")
    obj = rest_service.object_store.get_object("actuator_cmd")
    assert obj.state.model_dump() == OBJ_REF


def test_move_in(rest_service):
    rest_service.object_store.register_object("actuator_cmd")
    obj = rest_service.object_store.get_object("actuator_cmd")
    obj.call("move_in")
    assert obj.state.model_dump()["properties"]["state"] == "IN"


def test_move_out(rest_service):
    rest_service.object_store.register_object("actuator_cmd")
    obj = rest_service.object_store.get_object("actuator_cmd")
    obj.call("move_out")
    assert obj.state.model_dump()["properties"]["state"] == "OUT"

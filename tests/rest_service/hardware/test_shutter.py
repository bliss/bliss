SHUTTER_REF = {
    "name": "mockupshutter",
    "type": "shutter",
    "online": True,
    "errors": [],
    "alias": None,
    "properties": {
        "state": "UNKNOWN",
        "status": "",
        "valid": True,
        "open_text": "",
        "closed_text": "",
    },
    "user_tags": [],
    "locked": None,
}


def test_shutter(rest_service):
    rest_service.object_store.register_object("mockupshutter")
    mockupshutter = rest_service.object_store.get_object("mockupshutter")
    assert mockupshutter.state.model_dump() == SHUTTER_REF


def test_open_from_api(rest_service):
    rest_service.object_store.register_object("mockupshutter")
    mockupshutter = rest_service.object_store.get_object("mockupshutter")
    mockupshutter.call("open")
    assert mockupshutter.state.model_dump()["properties"]["state"] == "OPEN"

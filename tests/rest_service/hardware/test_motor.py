import pytest

ROBZ_REF = {
    "name": "robz",
    "type": "motor",
    "online": True,
    "errors": [],
    "alias": None,
    "properties": {
        "position": 0.0,
        "target": 0.0,
        "tolerance": 0.0001,
        "acceleration": 300.0,
        "velocity": 100.0,
        "limits": [-1000.0, 1000000000.0],
        "state": ["READY"],
        "unit": "mm",
        "offset": 0.0,
        "sign": 1,
        "display_digits": 6,
    },
    "user_tags": ["TEST.ROBZ"],
    "locked": None,
}


def test_motor(rest_service):
    rest_service.object_store.register_object("robz")
    robz = rest_service.object_store.get_object("robz")
    assert robz.state.model_dump() == ROBZ_REF


def test_move_from_api(rest_service):
    rest_service.object_store.register_object("robz")
    robz = rest_service.object_store.get_object("robz")
    robz.call("move", 1.0)
    assert robz.state.model_dump()["properties"]["position"] == 1.0


def test_exception_after_move(rest_service):
    rest_service.object_store.register_object("motor_with_weird_tolerance")
    mot = rest_service.object_store.get_object("motor_with_weird_tolerance")
    with pytest.raises(RuntimeError):
        mot.call("move", 1.0)


def test_non_standard_state(rest_service):
    rest_service.object_store.register_object("robz")
    mot = rest_service.object_store.get_object("robz")
    mot._object.controller.custom_park(mot._object)
    state = mot.state.model_dump()["properties"]["state"]
    assert "_PARKED:mot au parking" in state


def test_offline_motor(rest_service):
    rest_service.object_store.register_object("offline_mot")
    mot = rest_service.object_store.get_object("offline_mot")
    data = mot.state.model_dump()
    assert data["online"] is False
    # FIXME: Actually there is way to fetch some properties
    assert data["properties"] == {}
    assert data["errors"] == []

SCANSAVING_REF = {
    "name": "SCAN_SAVING",
    "type": "scansaving",
    "online": True,
    "errors": [],
    "alias": None,
    "properties": {
        "base_path": "####",
        "beamline": None,
        "data_path": "####/rest_session/data",
        "root_path": "####/rest_session/",
        "filename": "####/rest_session/data.h5",
        "template": "rest_session/",
        "data_filename": "data",
        "proposal_name": None,
        "proposal_session_name": None,
        "collection_name": None,
        "dataset_name": None,
        "dataset_definition": None,
        "sample_description": None,
        "sample_name": None,
        "sample_notes": None,
    },
    "user_tags": [],
    "locked": None,
}


def test_state(rest_service, scan_tmpdir):
    rest_service.object_store.register_object("SCAN_SAVING")
    mockupshutter = rest_service.object_store.get_object("SCAN_SAVING")
    result = mockupshutter.state.model_dump()
    for k in ["base_path", "data_path", "root_path", "filename"]:
        result["properties"][k] = result["properties"][k].replace(
            str(scan_tmpdir), "####"
        )

    assert result == SCANSAVING_REF

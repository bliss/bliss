import pytest
from typing import Any

FAILED_REF: dict[str, Any] = {
    "name": "mythen1",
    "type": "unknown",
    "alias": None,
    "errors": [],
    "locked": None,
    "online": False,
    "properties": {},
    "user_tags": [],
}

OPAQUE_REF: dict[str, Any] = {
    "name": "bcmock",
    "type": "unknown",
    "alias": None,
    "errors": [],
    "locked": None,
    "online": True,
    "properties": {},
    "user_tags": [],
}


def test_failing_object(rest_session, rest_service):
    """The mythen server is not there so the object will fail at loading"""
    with pytest.raises(Exception):
        _ = rest_session.config.get("mythen1")
    rest_service.object_store.register_object("mythen1")
    obj = rest_service.object_store.get_object("mythen1")
    assert obj.state.model_dump() == FAILED_REF


def test_opaque_object(rest_session, rest_service):
    """The object is valid in BLISS and can be loaded, but we dont know
    what to do with.
    """
    # Make sure it can be loaded in BLISS
    _ = rest_session.config.get("bcmock")
    rest_service.object_store.register_object("bcmock")
    obj = rest_service.object_store.get_object("bcmock")
    assert obj.state.model_dump() == OPAQUE_REF

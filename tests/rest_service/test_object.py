import pytest
import gevent
from pydantic_core import ValidationError
from bliss.config.conductor import client as beacon_client


def test_register(rest_service):
    rest_service.object_store.register_object("rest_test_obj")
    obj = rest_service.object_store.get_object("rest_test_obj")
    assert obj is not None


def test_property(rest_service):
    rest_service.object_store.register_object("rest_test_obj")
    obj = rest_service.object_store.get_object("rest_test_obj")
    assert obj.state.model_dump()["properties"] == {
        "state": "ON",
        "number": 123.456,
        "number_positive": 1.0,
        "string": "abcd",
        "option": "one",
        "read_only": 42,
    }


def test_call_empty_func(rest_service):
    rest_service.object_store.register_object("rest_test_obj")
    obj = rest_service.object_store.get_object("rest_test_obj")
    obj.call("func0")


def test_call_1arg_func(rest_service):
    rest_service.object_store.register_object("rest_test_obj")
    obj = rest_service.object_store.get_object("rest_test_obj")
    obj.call("func1", "foo")


def test_call_empty_func__wrong_nb_args(rest_service):
    rest_service.object_store.register_object("rest_test_obj")
    obj = rest_service.object_store.get_object("rest_test_obj")
    with pytest.raises(ValidationError):
        obj.call("func0", 1, 2)


def test_call_1arg_func__wrong_type(rest_service):
    rest_service.object_store.register_object("rest_test_obj")
    obj = rest_service.object_store.get_object("rest_test_obj")
    with pytest.raises(ValidationError):
        obj.call("func1", 1)


def test_lock(rest_session, rest_service):
    bliss_obj = rest_session.config.get("rest_test_obj")

    rest_service.object_store.register_object("rest_test_obj")
    obj = rest_service.object_store.get_object("rest_test_obj")

    beacon_client.lock(bliss_obj)
    gevent.sleep(2.0)
    try:
        assert obj.locked() is not None
        assert obj.state.model_dump()["locked"] is not None
        assert "reason" in obj.state.model_dump()["locked"]
    finally:
        beacon_client.unlock(bliss_obj)

    gevent.sleep(2.0)
    assert obj.locked() is None
    assert obj.state.model_dump()["locked"] is None

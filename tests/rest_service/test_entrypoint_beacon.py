# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.


def test_read_root_file(rest_service):
    with rest_service.app.app_context():
        client = rest_service.app.test_client()
        response = client.get(
            "/api/beacon/__init__.yml",
        )
        assert response.status_code == 200, response.text
        result = response.json
        assert result["content"]["beamline"] == "ID00"


def test_read_file(rest_service):
    with rest_service.app.app_context():
        client = rest_service.app.test_client()
        response = client.get(
            "/api/beacon/tango/machinfo.yml",
        )
        assert response.status_code == 200, response.text
        result = response.json
        assert result["content"]["server"] == "machinfo_tg_server"


def test_read_directory(rest_service):
    with rest_service.app.app_context():
        client = rest_service.app.test_client()
        response = client.get(
            "/api/beacon/sessions",
        )
        assert response.status_code == 400, response.text


def test_read_wrong_file(rest_service):
    with rest_service.app.app_context():
        client = rest_service.app.test_client()
        response = client.get(
            "/api/beacon/sessions/../foo/bar.yml",
        )
        assert response.status_code == 400, response.text


def test_not_existing_file(rest_service):
    with rest_service.app.app_context():
        client = rest_service.app.test_client()
        response = client.get(
            "/api/beacon/not_existing_file.yml",
        )
        assert response.status_code == 404, response.text

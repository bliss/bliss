TYPE_TEST = {
    "callables": {
        "func0": None,
        "func1": None,
        "func2": None,
        "func_named_args1": None,
        "func_mul": None,
        "long_process": None,
    },
    "properties": {
        "number": {"title": "Number", "type": "number"},
        "number_positive": {
            "title": "Number Positive",
            "type": "number",
        },
        "option": {
            "enum": ["one", "two", "three"],
            "title": "Option",
            "type": "string",
        },
        "read_only": {"readOnly": True, "title": "Read Only", "type": "integer"},
        "state": {"readOnly": True, "title": "State", "type": "string"},
        "string": {"title": "String", "type": "string"},
    },
    "state_ok": ["ON", "OFF"],
    "type": "test",
}

FUNC_MUL = {
    "args": {
        "maxItems": 2,
        "minItems": 2,
        "prefixItems": [
            {
                "type": "number",
            },
            {
                "type": "number",
            },
        ],
        "title": "Args",
        "type": "array",
    },
    "kwargs": {
        "additionalProperties": {
            "type": "null",
        },
        "title": "Kwargs",
        "type": "object",
    },
}


def test_schemas(rest_service):
    rest_service.object_store.register_object("rest_test_obj")
    with rest_service.app.app_context():
        client = rest_service.app.test_client()
        response = client.get("/api/object_type")
        assert response.status_code == 200, response.text
        result = response.json
    types = [schema["type"] for schema in result["results"]]
    assert "test" in types
    assert "motor" in types
    assert "shutter" in types
    # Check that this special type is available
    assert "scansaving" in types


def test_schema(rest_service):
    rest_service.object_store.register_object("rest_test_obj")
    with rest_service.app.app_context():
        client = rest_service.app.test_client()
        response = client.get("/api/object_type/test")
        assert response.status_code == 200, response.text
        result = response.json
        callables = result["callables"]
        result["callables"] = {k: None for k, _ in result["callables"].items()}
        assert result == TYPE_TEST
        assert callables["func_mul"] == FUNC_MUL

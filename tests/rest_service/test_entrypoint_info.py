from importlib import metadata


def test_info(rest_service):
    with rest_service.app.app_context():
        client = rest_service.app.test_client()
        response = client.get("/api/info")
        assert response.status_code == 200, response.text
        result = response.json
        assert result == {
            "beamline": "ID00",
            "bliss_version": metadata.version("bliss"),
            "blissdata_version": metadata.version("blissdata"),
            "blisstomo_version": "",
            "blisswebui_version": "",
            "flint_version": metadata.version("flint"),
            "fscan_version": "",
            "instrument": "esrf-id00a",
            "session": "rest_session",
            "synchrotron": "ESRF",
        }

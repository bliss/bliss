# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import gevent
import pytest
from bliss.common import event
from bliss.testutils.rpc_controller import controller_in_another_process


@pytest.mark.can_segfault
def test_axis(default_session, dummy_axis_server):
    axis = default_session.config.get("tango_axis1")
    assert axis.position == 1.4078913
    assert axis.velocity == 5.0
    assert axis.acceleration == 125.0
    axis.acceleration = 100.0
    assert axis.acceleration == 100.0


@pytest.mark.can_segfault
def test_acceleration_time(default_session, dummy_axis_server):
    """Test a tango device exposing an `acceleration_time` instead
    of an `acceleration`"""
    axis = default_session.config.get("tango_axis__acceleration_time")
    assert axis.position == 1.4078913
    assert axis.velocity == 5.0
    assert axis.acceleration == 0.04
    axis.acceleration = 100.0
    assert axis.acceleration == 100.0


@pytest.mark.can_segfault
def test_without_velocity_and_acceleration(default_session, dummy_axis_server):
    """
    When the velocity/acceleration attributes are not set,
    a default value is returned.
    """
    axis = default_session.config.get("tango_axis__no_velocity_no_acc")
    assert axis.position == 1.4078913
    assert axis.velocity == 2000.0
    assert axis.acceleration == 500.0


@pytest.mark.can_segfault
def test_changed_from_outside(default_session, dummy_axis_server):
    """
    Check that the state of the BLISS controller is still up to date
    even when the tango device was updated from outside.
    """
    _, proxy = dummy_axis_server
    axis = default_session.config.get("tango_axis1")
    # Force lazy initialization else the events will not work
    axis.position

    last_events = {}

    def on_new_event(value, signal: str, **kwargs):
        nonlocal last_events
        last_events[signal] = value

    event.connect(axis, "velocity", on_new_event)
    event.connect(axis, "position", on_new_event)
    event.connect(axis, "acceleration", on_new_event)
    event.connect(axis, "state", on_new_event)

    expected = proxy.position + 1.0
    proxy.position = expected
    # Let the motion terminate + delay for the event
    gevent.sleep(5.0)
    assert last_events.get("position") == pytest.approx(expected, abs=0.001)

    expected = proxy.velocity + 1.0
    proxy.velocity = expected
    gevent.sleep(2.0)
    assert last_events.get("velocity") == expected

    expected = proxy.acceleration + 1.0
    proxy.acceleration = expected
    gevent.sleep(2.0)
    assert last_events.get("acceleration") == expected

    proxy.off()
    gevent.sleep(2.0)
    last_state = last_events.get("state")
    assert last_state
    assert last_state.OFF


@pytest.mark.can_segfault
def test_event_on_motion(default_session, dummy_axis_server):
    """
    Check that the cache is still fine after a motion.
    """
    _, proxy = dummy_axis_server
    axis = default_session.config.get("tango_axis1")
    # Force lazy initialization else the events will not work
    axis.position

    last_events = {}

    def on_new_event(value, signal: str, **kwargs):
        nonlocal last_events
        last_events[signal] = value

    event.connect(axis, "position", on_new_event)
    expected = proxy.position + 1.0
    axis.move(expected)
    gevent.sleep(2.0)

    assert last_events.get("position") == pytest.approx(expected, abs=0.001)


@pytest.mark.can_segfault
def test_motion_from_another_session(
    default_session, dummy_axis_server, beacon, beacon_host_port
):
    """Create the same controller into 2 sessions.

    Move the axis in the other session, and check that the local axis is
    up-to-date (the synchronization of the channels are there).
    """
    beacon_host, beacon_port = beacon_host_port
    with controller_in_another_process(
        beacon_host, beacon_port, "tango_axis1"
    ) as other_axis:
        axis = default_session.config.get("tango_axis1")
        expected = axis.position + 1

        last_events = {}

        def on_new_event(value, signal: str, **kwargs):
            nonlocal last_events
            last_events[signal] = value

        event.connect(axis, "position", on_new_event)
        other_axis.move(expected)
        gevent.sleep(2.0)

        assert other_axis.position == pytest.approx(expected, abs=0.001)
        assert last_events.get("position") == pytest.approx(expected, abs=0.001)

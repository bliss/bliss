# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import time
import logging
from bliss.common.shutter import BaseShutterState
from bliss.common import event


def test_tango_shutter(session, dummy_tango_server, caplog):
    sh = session.config.get("safshut")

    assert sh.name == "safshut"
    assert sh.config["name"] == "safshut"

    sh.open(timeout=4)
    assert sh.state == BaseShutterState.OPEN
    assert sh.state_string[0] == BaseShutterState.OPEN.value
    with caplog.at_level(logging.DEBUG, logger=f"global.controllers.{sh.name}"):
        sh.open()
        assert sh.state == BaseShutterState.OPEN
    assert "ignored" in caplog.messages[-1]

    sh.close(timeout=4)
    assert sh.state == BaseShutterState.CLOSED
    assert sh.state_string[0] == BaseShutterState.CLOSED.value
    with caplog.at_level(logging.DEBUG, logger=f"global.controllers.{sh.name}"):
        sh.close()
        assert sh.state == BaseShutterState.CLOSED
    assert "ignored" in caplog.messages[-1]

    assert isinstance(sh.state_string, tuple)

    sh.proxy.setDisabled(True)
    with caplog.at_level(logging.DEBUG, logger=f"global.controllers.{sh.name}"):
        sh.open()
        assert sh.state == BaseShutterState.DISABLE
    assert "ignored" in caplog.messages[-1]
    with caplog.at_level(logging.DEBUG, logger=f"global.controllers.{sh.name}"):
        sh.close()
        assert sh.state == BaseShutterState.DISABLE
    assert "ignored" in caplog.messages[-1]


def test_tango_shutter_from_outside(session, dummy_tango_server, caplog):
    """Setup a tango shutter.

    Make sure state and events are propagated even if the requests
    come from tango.
    """
    events = []

    def event_callback(value):
        nonlocal events
        events.append(value)

    sh = session.config.get("safshut")
    tango_proxy = sh.proxy
    time.sleep(1)
    event.connect(sh, "state", event_callback)

    tango_proxy.close()
    assert sh.is_closed
    time.sleep(1)

    tango_proxy.open()
    assert sh.is_open
    time.sleep(2)

    assert len(events) != 0
    assert events[-1] == BaseShutterState.OPEN, events

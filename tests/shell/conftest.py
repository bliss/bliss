def pytest_addoption(parser):
    # To allow to display the pt menus
    parser.addoption("--ptmenu", action="store_true", default=False)

# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import subprocess
import os


ROOT = os.path.dirname(__file__)


def test_library_script(beacon):
    script = subprocess.Popen(
        ["python", os.path.join(ROOT, "check_library_mode_script.py")],
        stdout=subprocess.PIPE,
    )

    output, err = script.communicate()

    assert script.returncode == 0, output
    assert err is None


def test_shell_script(beacon):
    script = subprocess.Popen(
        ["python", os.path.join(ROOT, "check_shell_mode_script.py")],
        stdout=subprocess.PIPE,
    )

    output, err = script.communicate()

    assert script.returncode == 0, output
    assert err is None


def test_sync_lib_mode(capsys, default_session):
    """stdout should not have anything"""
    commands = (
        "from bliss.config import static",
        "config = static.get_config()",
        "roby = config.get('roby')",
        "roby.sync_hard()",
    )

    script = subprocess.Popen(
        ["python", "-c", ";".join(commands)], stdout=subprocess.PIPE
    )

    output, err = script.communicate()

    assert b"Forcing axes synchronization with hardware" not in output, output
    assert script.returncode == 0, output
    assert len(output) == 0, output

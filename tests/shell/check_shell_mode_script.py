from bliss.shell.cli import repl
import sys

from bliss import is_bliss_shell
from prompt_toolkit.input.defaults import create_pipe_input


with create_pipe_input() as inp:

    cli = repl.cli(
        input=inp,
        plain_output=True,
        session_name="flint",
        expert_error_report=True,
    )

sys.exit(0 if is_bliss_shell() else 1)

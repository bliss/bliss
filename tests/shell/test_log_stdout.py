import pytest


@pytest.fixture
def bliss_repl_test_session(beacon, bliss_repl):
    with bliss_repl(session_name="test_session") as br:
        yield br


def test_log_stdout(clear_pt_context, bliss_repl_test_session, tmpdir):
    br = bliss_repl_test_session
    session = br.bliss_session
    # br.send_input("from bliss.shell.standard._logging import log_stdout\r")

    # check log_stdout is disabled
    br.send_input("log_stdout()\r")
    assert "stdout logging is disabled" in br.app.output[-1]

    # get expected logging file path and check
    fpath = br._build_stdout_file_path(fdir=tmpdir)
    assert fpath.startswith(f"{tmpdir}")
    assert session.name in fpath

    # activate stdout logging and check logging file path is as expected
    br.send_input(f"log_stdout('{tmpdir}')\r")
    br.send_input("log_stdout()\r")
    assert f"stdout is logged to {fpath}" in br.app.output[-1]

    # type commands in the shell
    br.send_input("print('HELLO')\r")
    # br.send_input("\r")
    # br.send_input("\r")
    br.send_input("loopscan(3, 0.001, diode, save=False)\r")

    # now open logging file and check content
    with open(fpath, "r") as f:
        lines = f.readlines()

    expected = [
        "\n",
        "TEST_SESSION [3]: log_stdout()\n",
        f"stdout is logged to {fpath}\n",
        "\n",
        "TEST_SESSION [4]: print('HELLO')\n",
        "HELLO\n",
        "\n",
        "TEST_SESSION [5]: loopscan(3, 0.001, diode, save=False)\n",
    ]

    assert lines[0 : len(expected)] == expected

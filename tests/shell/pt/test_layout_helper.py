from __future__ import annotations

import pytest
from bliss.shell.pt import layout_helper


@pytest.mark.parametrize(
    "arguments,expected_size",
    [
        ((10, 5), 2),
        ((4, 5), 1),
        ((5, 5), 1),
        ((6, 5), 2),
    ],
)
def test_layout_grid_flow_min_size(arguments, expected_size):
    size = layout_helper.layout_grid_flow_min_size(*arguments)
    assert size == expected_size


def test_layout_box():
    assert layout_helper.layout_box(20, 5, 2) == 3
    assert layout_helper.layout_box(20, 9, 2) == 2
    assert layout_helper.layout_box(20, 10, 1) == 1
    assert layout_helper.layout_box(20, 10, 0) == 2


@pytest.mark.parametrize(
    "arguments,expected_remaining,expected_columns,expected_rows",
    [
        ((10, 5, 2), 0, 5, 2),
        ((8, 5, 2), 0, 4, 2),
        ((9, 5, 2), 0, 5, 2),
        ((4, 5, 2), 0, 4, 1),
        ((15, 5, 3), 0, 5, 3),
        ((10, 5, 3), 0, 5, 2),
        ((6, 5, 3), 0, 3, 2),
        ((40, 5, 2), 30, 5, 2),
        ((40, 10, 2), 20, 10, 2),
    ],
)
def test_layout_packed_grid(
    arguments, expected_remaining, expected_columns, expected_rows
):
    nb, max_columns, max_rows = arguments
    columns, rows = layout_helper.layout_packed_grid(nb, max_columns, max_rows)
    assert columns <= max_columns
    assert rows <= max_rows
    remaining = nb
    print()
    for r in range(max_rows):
        if remaining > columns:
            print(("@" * columns) + ("." * (max_columns - columns)))
            remaining -= columns
        else:
            print(("@" * remaining) + ("." * (max_columns - remaining)))
            remaining = 0
    assert remaining == expected_remaining
    assert columns == expected_columns
    assert rows == expected_rows

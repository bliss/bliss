import gevent
import gevent.event
import pytest
import logging
from bliss.shell.pt.text_block_app import TextBlockApplication
from bliss.common.greenlet_utils.killmask import KillMask


def test_abort_with_ctrl_c(capsys, pt_test_context):
    """
    Ctrl-C can be used to abort the display
    """

    def render():
        return 1, "Hey!"

    app = TextBlockApplication(
        render=render,
        refresh_interval=0.3,
    )

    def click_later():
        gevent.sleep(2.0)
        app.wait_render()
        pt_test_context.input.send_text("\x03")

    g = gevent.spawn(click_later)

    with pytest.raises(KeyboardInterrupt):
        app.exec()
    g.join()
    assert capsys.readouterr().out.count("Hey!") == 1


def test_abort_with_end_of_processing(capsys, pt_test_context):
    """
    The end of the processing is the end of the application
    """

    def render():
        return 1, "Hey!"

    app = TextBlockApplication(
        render=render,
        refresh_interval=0.3,
    )

    def process():
        gevent.sleep(2.0)
        app.wait_render()

    app.exec(process=process)
    assert capsys.readouterr().out.count("Hey!") == 1


def test_abort_processing_with_ctrl_c(capsys, pt_test_context):
    """
    Ctrl-C can be used to abort the display and the processing.

    In this case the processing is interrupted.
    """
    interrupted = False

    def process():
        nonlocal interrupted
        try:
            gevent.sleep(20.0)
        except BaseException:
            interrupted = True
            raise

    def render():
        return 1, "Hey!"

    app = TextBlockApplication(
        render=render,
        refresh_interval=0.3,
    )

    def click_later():
        gevent.sleep(2.0)
        app.wait_render()
        pt_test_context.input.send_text("\x03")

    g = gevent.spawn(click_later)

    with pytest.raises(KeyboardInterrupt):
        app.exec(process=process)
    g.join()
    assert capsys.readouterr().out.count("Hey!") == 1
    assert interrupted


def test_abort_twice_processing_with_ctrl_c(pt_test_context):
    """
    Ctrl-C can be used to abort the processing even if a killmask
    is used.

    In this case the processing is interrupted after 2 user interruptions.
    """
    interrupted = False

    def process():
        nonlocal interrupted
        try:
            with KillMask(masked_kill_nb=1):
                gevent.sleep(20.0)
        except BaseException:
            interrupted = True
            raise

    def render():
        return 1, "Hey!"

    app = TextBlockApplication(
        render=render,
        refresh_interval=0.3,
    )

    def click_later():
        gevent.sleep(2.0)
        app.wait_render()
        pt_test_context.input.send_text("\x03")
        gevent.sleep(0.5)
        assert not interrupted
        pt_test_context.input.send_text("\x03")

    g = gevent.spawn(click_later)

    with pytest.raises(KeyboardInterrupt):
        app.exec(process=process)
    g.join()
    assert interrupted


def test_rendering_error(pt_test_context, mocker):
    """
    If render function fails, the processing is not aborted and the
    exception is logged only once to avoid spam.
    """

    def process():
        gevent.sleep(2.0)

    i = 0

    def render():
        nonlocal i
        i = i + 1
        if i > 3:
            raise RuntimeError("Oupsi")
        else:
            return 1, "Hey!"

    logger = logging.getLogger("bliss.shell.pt.text_block_app")
    spy_logger = mocker.spy(logger, "error")

    app = TextBlockApplication(
        render=render,
        refresh_interval=0.3,
    )
    app.exec(process=process)
    spy_logger.assert_called_once()


def test_failing_processing(pt_test_context):
    """
    If the processing fails the exception is propagated
    """

    def process():
        gevent.sleep(1.0)
        raise RuntimeError("Oupsi")

    def render():
        return 1, "Hey!"

    app = TextBlockApplication(
        render=render,
        refresh_interval=0.3,
    )
    with pytest.raises(RuntimeError) as excinfo:
        app.exec(process=process)
    assert "Oupsi" in str(excinfo.value)


def test_killing_app(pt_test_context):
    """
    If the app is part of a greenlet that we kill.

    The processing is killed and GreenletExit is propagated.
    """
    interrupted = False
    interrupted_with = None

    def process():
        nonlocal interrupted
        try:
            gevent.sleep(20.0)
        except BaseException:
            interrupted = True
            raise

    def render():
        return 1, "Hey!"

    app = TextBlockApplication(
        render=render,
        refresh_interval=0.3,
    )

    def exec():
        nonlocal interrupted_with
        try:
            app.exec(process=process)
        except BaseException as e:
            interrupted_with = e
            raise e

    g = gevent.spawn(exec)
    app.wait_render()
    g.kill()
    assert interrupted
    assert isinstance(interrupted_with, gevent.GreenletExit)


def test_ctx_abort_with_ctrl_c(capsys, pt_test_context):
    """
    Ctrl-C can be used to abort the context
    """

    def f():
        def render():
            return 1, "Hey!"

        app = TextBlockApplication(
            render=render,
            refresh_interval=0.3,
            app_session=pt_test_context.app_session,
        )

        def click_later():
            gevent.sleep(2.0)
            app.wait_render()
            gevent.sleep(1.0)
            pt_test_context.input.send_text("\x03")

        g = gevent.spawn(click_later)
        with pytest.raises(KeyboardInterrupt):
            with app.exec_context():
                gevent.sleep(10)

        g.join()

        assert capsys.readouterr().out.count("Hey!") == 1

    gtest = gevent.spawn(f)
    gtest.get()


def test_ctx_abort_with_end_of_processing(capsys, pt_test_context):
    """
    The end of the processing is the end of the application
    """

    def f():
        def render():
            return 1, "Hey!"

        app = TextBlockApplication(
            render=render,
            refresh_interval=0.3,
            app_session=pt_test_context.app_session,
        )

        with app.exec_context():
            gevent.sleep(2.0)
            app.wait_render()

        assert capsys.readouterr().out.count("Hey!") == 1

    gtest = gevent.spawn(f)
    gtest.get()


def test_ctx_abort_processing_with_ctrl_c(capsys, pt_test_context):
    """
    Ctrl-C can be used to abort the display and the processing.

    In this case the processing is interrupted.
    """

    def f():
        interrupted = False

        def render():
            return 1, "Hey!"

        app = TextBlockApplication(
            render=render,
            refresh_interval=0.3,
            app_session=pt_test_context.app_session,
        )

        def click_later():
            gevent.sleep(2.0)
            app.wait_render()
            pt_test_context.input.send_text("\x03")

        g = gevent.spawn(click_later)

        with pytest.raises(KeyboardInterrupt):
            with app.exec_context():
                try:
                    gevent.sleep(20.0)
                except BaseException:
                    interrupted = True
                    raise
        g.join()
        assert capsys.readouterr().out.count("Hey!") == 1
        assert interrupted

    gtest = gevent.spawn(f)
    gtest.get()


def test_ctx_abort_twice_processing_with_ctrl_c(pt_test_context):
    """
    Ctrl-C can be used to abort the processing even if a killmask
    is used.

    In this case the processing is interrupted after 2 user interruptions.
    """

    def f():
        interrupted = False

        def render():
            return 1, "Hey!"

        app = TextBlockApplication(
            render=render,
            refresh_interval=0.3,
            app_session=pt_test_context.app_session,
        )

        def click_later():
            gevent.sleep(2.0)
            app.wait_render()
            pt_test_context.input.send_text("\x03")
            gevent.sleep(0.5)
            assert not interrupted
            pt_test_context.input.send_text("\x03")

        g = gevent.spawn(click_later)

        with pytest.raises(KeyboardInterrupt):
            with app.exec_context():
                try:
                    with KillMask(masked_kill_nb=1):
                        gevent.sleep(20.0)
                except BaseException:
                    interrupted = True
                    raise

        g.join()
        assert interrupted

    gtest = gevent.spawn(f)
    gtest.get()


def test_ctx_rendering_error(pt_test_context, mocker):
    """
    If render function fails, the processing is aborted and the
    exception is propagated
    """

    def f():
        i = 0

        def render():
            nonlocal i
            i = i + 1
            if i > 10:
                raise RuntimeError("Oupsi")
            else:
                return 1, "Hey!"

        logger = logging.getLogger("bliss.shell.pt.text_block_app")
        spy_logger = mocker.spy(logger, "error")

        app = TextBlockApplication(
            render=render,
            refresh_interval=0.3,
            app_session=pt_test_context.app_session,
        )
        with app.exec_context():
            app.wait_render()
            gevent.sleep(4.0)
        spy_logger.assert_called_once()

    gtest = gevent.spawn(f)
    gtest.get()


def test_ctx_failing_processing(pt_test_context):
    """
    If the processing fails the exception is propagated
    """

    def f():
        def render():
            return 1, "Hey!"

        app = TextBlockApplication(
            render=render,
            refresh_interval=0.3,
            app_session=pt_test_context.app_session,
        )
        with pytest.raises(RuntimeError) as excinfo:
            with app.exec_context():
                gevent.sleep(1.0)
                raise RuntimeError("Oupsi")
        assert "Oupsi" in str(excinfo.value)

    gtest = gevent.spawn(f)
    gtest.get()


def test_ctx_killing_app(pt_test_context):
    """
    If the app is part of a greenlet that we kill.

    The processing is killed and GreenletExit is propagated.
    """

    def f():
        interrupted = False
        interrupted_with = None

        def render():
            return 1, "Hey!"

        app = TextBlockApplication(
            render=render,
            refresh_interval=0.3,
            app_session=pt_test_context.app_session,
        )

        def exec():
            nonlocal interrupted, interrupted_with
            try:
                with app.exec_context():
                    try:
                        gevent.sleep(20.0)
                    except BaseException:
                        interrupted = True
                        raise
            except BaseException as e:
                interrupted_with = e
                raise e

        g = gevent.spawn(exec)
        app.wait_render()
        g.kill()
        assert interrupted
        assert isinstance(interrupted_with, gevent.GreenletExit)

    gtest = gevent.spawn(f)
    gtest.get()

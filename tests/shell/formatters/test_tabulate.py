# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from __future__ import annotations

import pytest
import io
from bliss.shell.formatters import tabulate
from prompt_toolkit import print_formatted_text, HTML
from prompt_toolkit.output import ColorDepth
from prompt_toolkit.output.vt100 import Vt100_Output
from prompt_toolkit.data_structures import Size


@pytest.fixture
def formatter_util(pytestconfig):
    class Formatter:
        def display_if_no_capture(self, string: str):
            if pytestconfig.option.capture == "no":
                print()
                print_formatted_text(string)

        def to_text(self, string: str) -> str:
            stream = io.StringIO()
            print_formatted_text(string, file=stream)
            return stream.getvalue()

        def to_ansi(self, string: str) -> str:
            stream = io.StringIO()
            output = Vt100_Output(stream, get_size=lambda: Size(40, 40))
            print_formatted_text(
                string, output=output, color_depth=ColorDepth.DEPTH_4_BIT
            )
            return stream.getvalue()

    return Formatter()


def test_format_text(formatter_util):
    res = tabulate.tabulate([["aaaa", "bb"], ["ccc", "dddd"]])
    formatter_util.display_if_no_capture(res)
    text = formatter_util.to_text(res)
    assert text == "aaaa  bb  \r\nccc   dddd\r\n"


def test_format_empty_cell(formatter_util):
    res = tabulate.tabulate([["aaaa", "bb"], ["ccc"]])
    formatter_util.display_if_no_capture(res)
    text = formatter_util.to_text(res)
    assert text == "aaaa  bb\r\nccc     \r\n"


def test_format_tuple(formatter_util):
    res = tabulate.tabulate([[("fg:red", "aaaa"), "bb"], ["ccc", "dddd"]])
    formatter_util.display_if_no_capture(res)
    ansi = formatter_util.to_ansi(res)
    assert ansi == "\x1b[0m\x1b[?7h\x1b[0;91maaaa\x1b[0m  bb  \r\nccc   dddd\r\n\x1b[0m"


def test_format_html(formatter_util):
    res = tabulate.tabulate([[HTML("<red>aaaa</red>"), "bb"], ["ccc", "dddd"]])
    formatter_util.display_if_no_capture(res)
    ansi = formatter_util.to_ansi(res)
    assert ansi == "\x1b[0m\x1b[?7h\x1b[0;91maaaa\x1b[0m  bb  \r\nccc   dddd\r\n\x1b[0m"


def test_left_align(formatter_util):
    res = tabulate.tabulate([["aaaaa"], ["bbb"], ["cc"]], stralign="left")
    formatter_util.display_if_no_capture(res)
    text = formatter_util.to_text(res)
    assert text == "aaaaa\r\nbbb  \r\ncc   \r\n"


def test_right_align(formatter_util):
    res = tabulate.tabulate([["aaaaa"], ["bbb"], ["cc"]], stralign="right")
    formatter_util.display_if_no_capture(res)
    text = formatter_util.to_text(res)
    assert text == "aaaaa\r\n  bbb\r\n   cc\r\n"


def test_center_align(formatter_util):
    res = tabulate.tabulate([["aaaaa"], ["bbb"], ["cc"]], stralign="center")
    formatter_util.display_if_no_capture(res)
    text = formatter_util.to_text(res)
    assert text == "aaaaa\r\n bbb \r\n cc  \r\n"


def test_number(formatter_util):
    res = tabulate.tabulate([[("fg:red", 10)], [2.10], [0.000000001]], stralign="left")
    formatter_util.display_if_no_capture(res)
    text = formatter_util.to_text(res)
    assert text == "10   \r\n2.1  \r\n1e-09\r\n"


def test_float_format(formatter_util):
    res = tabulate.tabulate(
        [[10], [2.10], [0.000000001]], stralign="left", floatfmt="0.3f"
    )
    formatter_util.display_if_no_capture(res)
    text = formatter_util.to_text(res)
    assert text == "10   \r\n2.100\r\n0.000\r\n"


def test_decimal_align(formatter_util):
    res = tabulate.tabulate(
        [[10], [2.10], [0.000000001]],
        stralign="left",
        numalign="decimal",
    )
    formatter_util.display_if_no_capture(res)
    text = formatter_util.to_text(res)
    assert text == "10    \r\n 2.1  \r\n 1e-09\r\n"


def test_separator(formatter_util):
    res = tabulate.tabulate(
        [
            ["aaaa", "bb"],
            [tabulate.separator("------"), tabulate.separator("----")],
            ["ccc", "dddd"],
        ]
    )
    formatter_util.display_if_no_capture(res)
    text = formatter_util.to_text(res)
    assert text == "aaaa    bb  \r\n------  ----\r\nccc     dddd\r\n"

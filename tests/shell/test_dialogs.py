# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from bliss.common.user_status_info import status_message
from unittest.mock import patch
import gevent
import time
import pytest
import typing


from bliss.shell.cli.user_dialog import (
    UserMsg,
    UserInput,
    UserIntInput,
    UserFloatInput,
    UserFileInput,
    UserChoice,
    UserChoice2,
    UserSelect,
    UserCheckBox,
    UserCheckBoxList,
    Container,
    check,
    BlissWizard,
)
from bliss.shell.cli.pt_widgets import BlissDialog, ResultNotValid
from bliss.shell.standard import menu


@pytest.fixture()
def dialog_launcher(pytestconfig):
    def dialog_launcher(user_dlg, shortcut: bool = False):
        dlg = BlissDialog(
            user_dlg,
            title="My title",
            paddings=(1, 1),
            show_help=True,
            disable_tmux_mouse=True,
            shortcut=shortcut,
        )
        if pytestconfig.getoption("ptmenu"):
            # Interactive display
            result = dlg.show()
        else:
            # For unittest
            result = dlg._get_results()
        return result

    return dialog_launcher


def test_multi_dialog(session, dialog_launcher):
    """
    The menu can be displayed with `pytest --ptmenu -s`
    """
    roby = session.config.get("roby")
    robz = session.config.get("robz")

    motor_obj = [roby, robz]
    motor_names = [a.name for a in motor_obj]
    ch_values = list(zip(motor_obj, motor_names))

    dlg_float_input = UserFloatInput(label="", name="myfloat", defval=1.2)
    dlg = [
        [UserMsg(label="I am a long message", text_align="CENTER", text_expand=True)],
        [
            Container(
                [
                    UserIntInput(name="myint", defval=100),
                    dlg_float_input,
                    UserInput(
                        name="myinput",
                        label="frange_1.1.3",
                        validator=check["frange"],
                        defval="6",
                    ),
                    UserInput(
                        label="word_1.1.4",
                        defval="The observable univers has at least one hundred billion galaxies",
                    ),
                    UserCheckBoxList(
                        "mychecklist",
                        values=[
                            (roby, "aaaa"),
                            (robz, "bbbb"),
                        ],
                        defval=[roby],
                    ),
                    UserCheckBox(
                        "mycheck",
                        defval=True,
                    ),
                ],
                title="Group 1",
                border=1,
                padding=1,
                splitting="h",
            )
        ],
        [UserInput(label="motor_2.1", completer=motor_names)],
        [UserFileInput(label="path_3.1", defval="")],
        [
            Container(
                [
                    UserChoice(
                        name="mychoice",
                        values=ch_values,
                        defval=1,
                        label="Select a motor",
                    ),
                    UserChoice2(
                        name="mychoice2",
                        values=ch_values,
                        defval=robz,
                        label="Select a motor",
                    ),
                ],
                title="Motors",
                border=1,
            ),
            Container(
                [
                    UserCheckBox(label="BELGIAN BEER"),
                    UserCheckBox(label="red wine"),
                    UserCheckBox(label="rhum"),
                    UserCheckBox(label="chartreuse"),
                ],
                title="Drinks",
                border=1,
            ),
        ],
    ]
    result = dialog_launcher(dlg)

    assert result is not False
    assert result["myint"] == 100
    assert result["myfloat"] == 1.2
    assert result["myinput"] == "6"
    assert result["mycheck"] is True
    assert result["mychecklist"] == [roby]
    assert result["mychoice"] is robz
    assert result["mychoice2"] is robz
    assert dlg_float_input in result


def test_widget_hash(dialog_launcher):
    """UserDialog are keys inside the dialog result.

    Make sure the mutability does not impact the hash.
    """
    r: dict[typing.Any, typing.Any] = {}
    dlg_float_input = UserFloatInput(name="myfloat", defval=1.2)
    dlg_check_list = UserCheckBoxList(
        values=[("a", "aaaa")],
        defval=["a"],
    )

    # Make sure we can add userdlg compound by unhashable stuffs
    r[dlg_check_list] = ["a"]
    assert dlg_check_list in r

    # defval is updated at the dialog validation
    r[dlg_float_input] = 1
    dlg_float_input.defval = 1.3
    assert dlg_float_input in r


def test_user_select(dialog_launcher):
    """
    The menu can be displayed with `pytest --ptmenu -s`
    """
    ch_values = [("roby", "roby"), ("robz", "robz"), ("foo", "Something else")]
    dlg = [
        [
            UserSelect(
                name="myselect",
                values=ch_values,
                defval="robz",
                label="Select a motor",
            ),
        ],
    ]
    result = dialog_launcher(dlg, shortcut=False)
    assert result is not False
    assert result["myselect"] is None


def test_user_multi_select_with_shortcut(dialog_launcher):
    """
    The menu can be displayed with `pytest --ptmenu -s`
    """
    ch_values1 = [("roby", "roby"), ("robz", "robz")]
    ch_values2 = [("foo", "Something else")]
    dlg = [
        [
            UserSelect(
                name="motor",
                values=ch_values1,
                defval="robz",
                label="Select a motor",
            ),
        ],
        [
            UserSelect(
                name="something",
                values=ch_values2,
                defval="robz",
                label="Select something",
            ),
        ],
    ]
    result = dialog_launcher(dlg, shortcut=True)
    assert result is not False
    assert result["motor"] is None
    assert result["something"] is None


def test_user_int__too_small(dialog_launcher):
    with pytest.raises(ResultNotValid):
        dialog_launcher([[UserIntInput(defval=-1, minimum=0)]])


def test_user_int__too_big(dialog_launcher):
    with pytest.raises(ResultNotValid):
        dialog_launcher([[UserIntInput(defval=10, maximum=9)]])


def test_user_float__too_small(dialog_launcher):
    with pytest.raises(ResultNotValid):
        dialog_launcher([[UserFloatInput(defval=-1, minimum=0)]])


def test_user_float__too_big(dialog_launcher):
    with pytest.raises(ResultNotValid):
        dialog_launcher([[UserFloatInput(defval=10, maximum=9)]])


def wizard(title="wizard"):
    from bliss.common.utils import get_axes_names_iter, get_axes_iter

    motor_names = list(get_axes_names_iter())
    motor_obj = list(get_axes_iter())

    ch_values = list(zip(motor_obj, motor_names))

    dlg1 = BlissDialog(
        [
            [
                Container(
                    [
                        UserIntInput(label="int_1"),
                        UserFloatInput(label="float_1.1.2"),
                        UserInput(
                            label="frange_1.1.3", validator=check["frange"], defval=6
                        ),
                        UserInput(label="word_1.1.4"),
                    ],
                    title="Group 1",
                    border=1,
                    padding=1,
                    splitting="h",
                )
            ],
            [UserInput(label="motor_2.1", completer=motor_names)],
            [UserFileInput(label="path_3.1", defval="")],
            [
                Container(
                    [UserChoice(values=ch_values, defval=0, label="Select a motor")],
                    title="Motors",
                    border=1,
                ),
                Container(
                    [
                        UserCheckBox(label="BELGIAN BEER"),
                        UserCheckBox(label="red wine"),
                        UserCheckBox(label="rhum"),
                        UserCheckBox(label="chartreuse"),
                    ],
                    title="Drinks",
                    border=1,
                ),
            ],
        ],
        title=title,
        paddings=(1, 1),
    )

    dlg2 = BlissDialog(
        [
            [
                Container(
                    [
                        UserIntInput(label="int_1"),
                        UserFloatInput(label="float_1.1.2"),
                        UserInput(
                            label="frange_1.1.3", validator=check["frange"], defval=6
                        ),
                        UserInput(label="word_1.1.4"),
                    ],
                    title="Group 1",
                    border=1,
                    padding=1,
                    splitting="h",
                )
            ],
            [UserInput(label="motor_2.1", completer=motor_names)],
            [UserFileInput(label="path_3.1", defval="")],
            [
                Container(
                    [UserChoice(values=ch_values, defval=0, label="Select a motor")],
                    title="Motors",
                    border=1,
                )
            ],
        ],
        title=title,
        paddings=(1, 1),
    )

    return BlissWizard([dlg1, dlg2]).show()


def dlg_from_wardrobe(ward_robe):
    dico = ward_robe.to_dict()

    str_fields = []
    bool_option = []
    other = []
    for key, value in dico.items():

        if isinstance(value, str):
            str_fields.append(UserInput(label=key, defval=value))
        elif isinstance(value, bool):
            bool_option.append(UserCheckBox(label=key))
        else:
            other.append(UserInput(label=key, defval=value))

    user_dlgs = [
        [Container(str_fields, title="set options", border=1)],
        [Container(bool_option, title="enable options", border=1)],
    ]

    return BlissDialog(user_dlgs, title="WardRobe", paddings=(1, 1)).show()


def test_user_status_info():
    def is_finished():
        return (time.perf_counter() - t0) > 2

    def my_seq():
        gevent.sleep(0.2)

    t0 = time.perf_counter()

    with status_message() as p:
        while not is_finished():
            my_seq()
            p("salut")


def test_show_helper(session):
    available = menu()
    names = """ACTIVE_MG MG1 MG2 ascan
    test_mg transfocator_simulator""".split()
    for name in names:
        assert name in available


def test_mg_menu(session):
    active_mg = session.env_dict["ACTIVE_MG"]
    with patch("bliss.shell.cli.pt_widgets.BlissDialog", autospec=True) as md:
        menu(active_mg)

    # check if dialog was called with good args
    dialog_args = md.call_args.args[0][0]
    container = dialog_args[0]
    assert active_mg.available  # ensure there are some counters defined
    assert set(active_mg.available) == set(container.defval)

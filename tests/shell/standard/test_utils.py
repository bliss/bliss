# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import gevent
from bliss.shell.standard import lsconfig
from bliss.shell.standard import bench


def test_lsconfig(session, capsys):
    lsconfig()
    captured = capsys.readouterr()
    # print(captured.out)
    assert "Motor:" in captured.out
    assert "v6biturbo" in captured.out
    assert "wrong_counter" in captured.out
    assert "dummy1" in captured.out
    assert "testMG" in captured.out
    assert "working_ctrl" in captured.out
    assert "machinfo" in captured.out
    assert "times2_2d" in captured.out
    assert "xia1" in captured.out


def test_bench(beacon, capsys):
    with bench():
        gevent.sleep(1)

    captured = capsys.readouterr()
    assert "Execution time: 1s" in captured.out

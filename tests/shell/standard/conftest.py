# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from __future__ import annotations
import typing
import pytest
import pathlib
import re
import sys
import io
import collections.abc


@pytest.fixture
def s1hg(default_session):
    s1hg = default_session.config.get("s1hg")
    yield s1hg


def pytest_addoption(parser):
    parser.addoption(
        "--store-test-result",
        action="store_true",
        default=False,
        help="Update the expectation to match the test",
    )


class ResourceHelper:
    def __init__(self, pytestconfig, request):
        self.store_test_result = pytestconfig.getoption("--store-test-result")
        self.test_name = request.node.name
        self.resource_root = pathlib.Path(__file__).parent / "expected_data"

    def _resource_file(self, resource_name):
        return self.resource_root / resource_name

    def load(self, resource_name: str, mode: str) -> str:
        """Load a resource"""
        filename = self._resource_file(resource_name)
        with open(filename, mode) as f:
            result = f.read()
        if "b" in mode:
            result = result.decode("utf-8")
        return result

    def save(self, resource_name: str, result: str, mode: str):
        """Save a resource"""
        filename = self._resource_file(resource_name)
        data: str | bytes
        if "b" in mode:
            data = result.encode("utf-8")
        else:
            data = result
        with open(filename, mode=mode) as f:
            f.write(data)

    def _complete_resource_name(self, resource_name: str) -> str:
        return resource_name.format(test_name=self.test_name.replace("test_", ""))

    def assert_str(self, resource_name: str, result: str):
        resource_name = self._complete_resource_name(resource_name)
        assert isinstance(result, str)
        filename = self._resource_file(resource_name)
        if self.store_test_result or not filename.exists():
            self.save(resource_name, result, mode="wt")
            return
        expected = self.load(resource_name, mode="rt")
        assert (
            result == expected
        ), "Discrepancy with the output reference. If this changes are fine, rerun the tests with --store-test-result"

    def assert_ansi(self, resource_name: str, result: str):
        resource_name = self._complete_resource_name(resource_name)
        assert isinstance(result, str)
        filename = self._resource_file(resource_name)
        if self.store_test_result or not filename.exists():
            self.save(resource_name, result, mode="wb")
            return
        expected = self.load(resource_name, mode="rb")
        assert (
            result == expected
        ), "Discrepancy with the output reference. If this changes are fine, rerun the tests with --store-test-result"


@pytest.fixture()
def resource_helper(pytestconfig, request):
    yield ResourceHelper(pytestconfig, request)


class TextIOSpy(io.TextIOBase):
    """Capture the stream but let it goes out"""

    def __init__(self, output: typing.TextIO):
        self.__output: typing.TextIO = output
        self.__data = io.StringIO()

    def write(self, data):
        self.__output.write(data)
        self.__data.write(data)

    def flush(self):
        self.__output.flush()

    def getvalue(self):
        return self.__data.getvalue()


class CheckInShell:
    def __init__(self, bliss_repl, capture: bool = True):
        self.remove_var_content: collections.abc.Callable[str, [str]] | None = None
        self.capture = capture
        self.bliss_repl = bliss_repl

    def run_bliss_repl(
        self,
        cmd: str,
        pre_cmd: str | list[str] = "",
        locals: dict[str, typing.Any] | None = None,
        text_block: bool = False,
    ):
        """
        Run a command in a new bliss repl.

        Arguments:
            locals: Extra locals to be exposed from the shell
            pre_cmd: Command(s) to execute to prepare the session
            cmd: Command to execute and to check
            text_block: If true, assume that the command use text block.
                The ansi result is stripped to return the last and the last
                redisplay only.
        """
        from bliss import current_session

        # Clear the cache
        from bliss.shell.pt import default_style

        default_style._CACHE = {}

        screen: typing.TextIO | TextIOSpy
        if self.capture:
            screen = io.StringIO()
        else:
            screen = TextIOSpy(sys.stdout)

        try:
            session_name = current_session.name
        except AttributeError:
            raise RuntimeError("A bliss session fixture have to be used")

        with self.bliss_repl(
            locals,
            vt100=True,
            session_name=session_name,
            no_cli=True,
            stdout=screen,
        ) as br:
            br.initialize_session()
            if isinstance(pre_cmd, list):
                for c in pre_cmd:
                    br.send_input(c)
            elif pre_cmd != "":
                br.send_input(pre_cmd)
            br.send_input(cmd)
            output = br.get_cell_output(-1)
            if screen is not None:
                stdout = screen.getvalue()
                index = stdout.rfind("BLISS [")
                stdout = stdout[index:]
            else:
                stdout = screen.getvalue()

        if text_block:
            stdouts = re.split("\x1b\\[\\d+C", stdout)
            stdout = f"#### LAST DISPLAY\n{stdouts[-1]}"

        if self.remove_var_content is not None:
            self.output = self.remove_var_content(output)
            self.stdout = self.remove_var_content(stdout)
        else:
            self.output = output
            self.stdout = stdout


@pytest.fixture()
def check_in_shell(pytestconfig, bliss_repl):
    """Provides an helper to check the commands in the bliss shell context

    Capture stdout without capsys, but use `capsys_disabled` to disable the capture

    It is mostly there for factorization
    """
    capture = pytestconfig.option.capture != "no"
    yield CheckInShell(bliss_repl=bliss_repl, capture=capture)

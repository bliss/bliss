# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
FIXME: Dispatch that tests in the right module and remove dups
"""

from bliss.shell.iter_common import iter_axes_position_all, iter_axes_position
from bliss.common.standard import info, move
from bliss.common.utils import ErrorWithTraceback
import pytest
import math


def test_wa_normal(default_session):
    bad = default_session.config.get("bad")
    bad.controller.bad_position = False
    assert next(iter_axes_position_all()) == (bad, False, None, None, 0.0, 0.0)


def test_wa_exception(default_session, capsys):
    bad = default_session.config.get("bad")
    bad.controller.bad_position = True
    out = next(iter_axes_position_all())
    assert out.axis == bad
    assert isinstance(out.error, ErrorWithTraceback)
    assert math.isnan(out.user_position)
    assert math.isnan(out.dial_position)


@pytest.fixture
def s1hg(default_session):
    s1hg = default_session.config.get("s1hg")
    yield s1hg


def test_wa_slits(s1hg, capsys):
    out = next(iter_axes_position_all())
    assert out.axis == s1hg
    assert out.unit is None
    assert out.user_position == 0
    assert out.dial_position == 0


def test_wm_normal(default_session, capsys):
    bad = default_session.config.get("bad")
    bad.controller.bad_position = False
    out = next(iter_axes_position("bad"))
    inf = float("inf")
    assert out == (bad, False, None, None, 0, inf, -inf, 0, 0, inf, -inf)


def test_wm_exception(default_session, capsys):
    bad = default_session.config.get("bad")
    bad.controller.bad_position = True
    out = next(iter_axes_position("bad"))
    inf = float("inf")
    assert out.axis == bad
    assert out.unit is None
    assert isinstance(out.error, ErrorWithTraceback)
    assert math.isnan(out.user_position)
    assert math.isnan(out.dial_position)
    assert out.user_high_limit == inf == out.dial_high_limit
    assert out.user_low_limit == -inf == out.dial_low_limit
    assert out.offset == 0


def test_move(default_session):
    roby = default_session.config.get("roby")

    with pytest.raises(TypeError):
        assert move(())
    with pytest.raises(TypeError):
        move(roby, None)
    with pytest.raises(ValueError):
        move(roby, float("nan"))
    move(roby, 1)
    assert roby.position == 1
    # this was possible before, but should not be!
    with pytest.raises(TypeError):
        move("roby", 2)


def test_info(default_session):
    class TestInfoGood:
        def __info__(self):
            return "good info message"

    class TestInfoBad:
        def __info__(self):
            return 5

    class TestInfoException:
        def __info__(self):
            raise RuntimeError("exception")

    class TestNoInfo:
        pass

    assert info(TestInfoGood()) == "good info message"
    t = TestNoInfo()
    assert info(t) == repr(t)
    with pytest.raises(TypeError):
        info(TestInfoBad())
    with pytest.raises(RuntimeError):
        info(TestInfoException())

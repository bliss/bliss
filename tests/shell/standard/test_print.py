import numpy


def test_print_empty(
    beacon, default_session, as_bliss_shell, resource_helper, check_in_shell
):
    """Make sure `umv` looks what we expect"""
    check_in_shell.run_bliss_repl(
        cmd="print()",
    )
    resource_helper.assert_str("{test_name}_cell.txt", check_in_shell.output)
    resource_helper.assert_ansi("{test_name}_shell.ansi", check_in_shell.stdout)


def test_print_none(
    beacon, default_session, as_bliss_shell, resource_helper, check_in_shell
):
    """Make sure `umv` looks what we expect"""
    check_in_shell.run_bliss_repl(
        cmd="print(None)",
    )
    resource_helper.assert_str("{test_name}_cell.txt", check_in_shell.output)
    resource_helper.assert_ansi("{test_name}_shell.ansi", check_in_shell.stdout)


def test_print_callable(
    beacon, default_session, as_bliss_shell, resource_helper, check_in_shell
):
    """Make sure `umv` looks what we expect"""
    check_in_shell.run_bliss_repl(
        locals={"numpy": numpy},
        cmd="print(numpy.array)",
    )
    resource_helper.assert_str("{test_name}_cell.txt", check_in_shell.output)
    resource_helper.assert_ansi("{test_name}_shell.ansi", check_in_shell.stdout)


def test_print_numpy(
    beacon, default_session, as_bliss_shell, resource_helper, check_in_shell
):
    """Make sure `umv` looks what we expect"""
    locals = {"numpy": numpy}
    check_in_shell.run_bliss_repl(
        locals=locals,
        cmd="print(numpy.array([1, 2, 3]))",
    )
    resource_helper.assert_str("{test_name}_cell.txt", check_in_shell.output)
    resource_helper.assert_ansi("{test_name}_shell.ansi", check_in_shell.stdout)

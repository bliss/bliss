# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest
from bliss.shell.standard import umv


def test_umv_typecheck(default_session, pt_test_context):
    m0 = default_session.config.get("m0")
    calc_mot5 = default_session.config.get("calc_mot5")
    umv(m0, 1.2)
    with pytest.raises(TypeError):
        umv(m0, 1, 2)
    with pytest.raises(TypeError):
        umv(1, m0)
    with pytest.raises(TypeError):
        umv()
    with pytest.raises(TypeError):
        umv(calc_mot5, 1)


def test_umv_signature(session):
    assert str(umv.__signature__) == "(*args: 'motor1, pos1, motor2, pos2, ...')"


def test_umv(beacon, default_session, as_bliss_shell, resource_helper, check_in_shell):
    """Make sure `umv` looks what we expect"""
    locals = {"umv": umv}
    check_in_shell.run_bliss_repl(
        locals=locals,
        pre_cmd="config.get('roby')",
        cmd="umv(roby, 1)",
        text_block=True,
    )
    resource_helper.assert_str("{test_name}_cell.txt", check_in_shell.output)
    resource_helper.assert_ansi("{test_name}_shell.ansi", check_in_shell.stdout)


def test_umv_with_calc(
    beacon, default_session, as_bliss_shell, resource_helper, check_in_shell
):
    """Make sure `umv` looks what we expect"""
    try:
        locals = {"umv": umv}
        check_in_shell.run_bliss_repl(
            locals=locals,
            pre_cmd="config.get('calc_mot2')",
            cmd="umv(calc_mot2, 4)",
            text_block=True,
        )
        resource_helper.assert_str("{test_name}_cell.txt", check_in_shell.output)
        resource_helper.assert_ansi("{test_name}_shell.ansi", check_in_shell.stdout)
    finally:
        # FIXME: It would be good to remove that
        default_session.config.get("calc_mot1").controller.close()
        default_session.config.get("calc_mot2").controller.close()


def test_umv_error(default_session, as_bliss_shell, pt_test_context):
    roby = default_session.config.get("roby")
    roby.controller.set_hw_limits(roby, -2, 2)
    with pytest.raises(RuntimeError):
        umv(roby, 3)

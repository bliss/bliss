import os
from bliss.common.logtools import Elogbook
from datetime import datetime
from bliss.shell.standard import elog_add
from bliss.shell.standard import log_stdout
from bliss.testutils.data_policies import set_data_policy


def test_elog_add_without_arg(beacon, mocker, bliss_repl):
    """Check that elog_add()

    - Request the right BlissRepl API
    - Feed the elogbook with the right content
    """
    mock_elogbook_comment = mocker.patch.object(Elogbook, "comment", return_value=None)
    repl_locals = {"elog_add": elog_add}
    with bliss_repl(repl_locals) as br:
        bliss_repl_get_cell_output = mocker.spy(br, "get_cell_output")
        br.initialize_session()
        br.send_input("print('A')")
        br.send_input("elog_add()")

    bliss_repl_get_cell_output.assert_called_once_with(-1)
    mock_elogbook_comment.assert_called_once_with("A\n", formatted=True)


def test_elog_add_with_arg(beacon, mocker, bliss_repl):
    """Check that elog_add(-2)

    - Request the right BlissRepl API
    - Feed the elogbook with the right content
    """
    mock_elogbook_comment = mocker.patch.object(Elogbook, "comment", return_value=None)
    repl_locals = {"elog_add": elog_add}
    with bliss_repl(repl_locals) as br:
        bliss_repl_get_cell_output = mocker.spy(br, "get_cell_output")
        br.initialize_session()
        br.send_input("print('B')")
        br.send_input("print('A')")
        br.send_input("elog_add(-2)")

    bliss_repl_get_cell_output.assert_called_once_with(-2)
    mock_elogbook_comment.assert_called_once_with("B\n", formatted=True)


def test_elog_add_no_content(beacon, mocker, bliss_repl):
    """Check that if a cell is empty, no logbook comment was added."""
    mock_elogbook_comment = mocker.patch.object(Elogbook, "comment", return_value=None)
    repl_locals = {"elog_add": elog_add}
    with bliss_repl(repl_locals) as br:
        bliss_repl_get_cell_output = mocker.spy(br, "get_cell_output")
        br.initialize_session()
        br.send_input("pass")
        br.send_input("elog_add()")

    bliss_repl_get_cell_output.assert_called_once_with(-1)
    mock_elogbook_comment.assert_not_called()


def test_elog_add_error(beacon, mocker, bliss_repl):
    """Check that if a cell have raised an exception, an error is logged."""
    mock_elogbook_error = mocker.patch.object(Elogbook, "error", return_value=None)
    mock_elogbook_comment = mocker.patch.object(Elogbook, "comment", return_value=None)
    repl_locals = {"elog_add": elog_add}
    with bliss_repl(repl_locals) as br:
        bliss_repl_get_cell_output = mocker.spy(br, "get_cell_output")
        br.initialize_session()
        br.send_input("1 / 0")
        br.send_input("elog_add()")

    bliss_repl_get_cell_output.assert_called_once_with(-1)
    mock_elogbook_comment.assert_called_once()
    assert (
        "!!! === ZeroDivisionError: division by zero"
        in mock_elogbook_comment.call_args[0][0]
    )
    mock_elogbook_error.assert_called_once_with("ZeroDivisionError: division by zero")


def test_log_stdout_file(session, tmpdir_factory, bliss_repl):
    def build_abspath(fdir, fname):
        now = datetime.now()
        fname = ""
        if hasattr(session.scan_saving, "beamline"):
            fname += f"{session.scan_saving.beamline}_"
        fname += f"{br.session_name}_{now.year}{now.month:02}{now.day:02}"
        if hasattr(session.scan_saving, "proposal_name"):
            fname += f"_{session.scan_saving.proposal_name}"
        fname += ".log"
        return os.path.join(fdir, fname)

    raw_print = print
    with bliss_repl(session_name=session.name) as br:

        set_data_policy(session, "esrf")
        # check that we start without stdout settings
        assert br._stdout_settings.get("fdir") is None
        assert br._stdout_settings.get("fname") is None
        assert br._log_stdout_file_output_dict == {}

        # check that log_stdout cmd with no args does not affect settings
        log_stdout()
        assert br._stdout_settings.get("fdir") is None
        assert br._stdout_settings.get("fname") is None
        assert br._log_stdout_file_output_dict == {}

        # check log_stdout(fdir) updates settings and creates log file with standardize name
        tmpdir = str(tmpdir_factory.getbasetemp())
        expected_path = build_abspath(tmpdir, None)
        raw_print("==== expected_path:", expected_path)
        log_stdout(tmpdir)
        assert br._stdout_settings.get("fdir") == tmpdir
        assert br._stdout_settings.get("fname") is None
        assert list(br._log_stdout_file_output_dict.keys())[0] == expected_path
        assert os.path.exists(expected_path)
        oldpath = expected_path

        # check ansi output
        # colorations are skipped from the output
        br.eval(r"print('\x1b[37m\x1b[1mhi!\x1b[0m\x1b[0m')" + "\n")
        with open(expected_path, "r") as f:
            waout = "".join(f.readlines())
            assert "\nhi!\n" in waout

        # check output redirection of wa()
        br.eval("from bliss.shell.standard import wa\n")
        br.eval("wa()\n")
        session.config.get("roby")
        with open(expected_path, "r") as f:
            waout = "".join(f.readlines())
            assert "roby" in waout

        # check that default file name is updated when proposal name has changed
        session.scan_saving.proposal_name = "test"
        br.eval("\n")
        expected_path = build_abspath(tmpdir, None)
        raw_print("==== expected_path:", expected_path)
        assert br._stdout_settings.get("fdir") == tmpdir
        assert br._stdout_settings.get("fname") is None
        assert list(br._log_stdout_file_output_dict.keys())[0] == expected_path
        assert os.path.exists(expected_path)
        assert oldpath != expected_path

        # check log_stdout(fname=xXx) updates file's name and keeps fdir unchanged
        fname = "testlogging.log"
        expected_path = os.path.join(tmpdir, fname)
        raw_print("==== expected_path:", expected_path)
        log_stdout(fname=fname)
        assert br._stdout_settings.get("fdir") == tmpdir
        assert br._stdout_settings.get("fname") == fname
        assert list(br._log_stdout_file_output_dict.keys())[0] == expected_path
        assert os.path.exists(expected_path)

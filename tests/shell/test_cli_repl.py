from bliss.shell.formatters.repl import format_repl

from bliss.physics.units import ur


class WithInfo:
    def __info__(self):
        return "with info"


class WithFailingInfo:
    def __info__(self):
        raise RuntimeError("Oupsi")


class WithRepr:
    def __repr__(self):
        return "with repr"


def test_format_repl_with_info():
    i = WithInfo()
    assert format_repl(i).__pt_repr__() == "with info"


def test_format_failing_repl_with_info(caplog):
    """Make sure information are displayed when __info__ is failing"""
    i = WithFailingInfo()
    assert "WithFailingInfo" in format_repl(i).__pt_repr__()
    assert len(caplog.records) == 1
    record = caplog.records[0]
    assert "while formatting" in record.getMessage()


def test_format_repl_with_repr():
    r = WithRepr()
    assert repr(format_repl(r)) == "with repr"


def test_format_repl_with_quantity():
    q = ur.Quantity(1.2345, ur.kg * ur.m / ur.s**2)
    assert repr(format_repl(q)) == f"{q:~P}"

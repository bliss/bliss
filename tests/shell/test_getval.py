# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest
import contextlib
import gevent

from bliss.shell import getval as getval_module
from bliss.shell.getval import getval_yes_no, getval_name, getval_int, getval_float
from bliss.shell.getval import getval_idx_list, getval_char_list
from bliss.common.utils import Undefined


@contextlib.contextmanager
def bliss_prompt_session(timeout=10):
    bliss_prompt_orig = getval_module._prompt_factory
    bp = bliss_prompt_orig()
    getval_module._prompt_factory = lambda: bp
    try:
        with gevent.Timeout(timeout) as expected_timeout:
            yield bp
    except gevent.Timeout as this_timeout:
        if expected_timeout is this_timeout:
            raise TimeoutError
        raise
    finally:
        getval_module._prompt_factory = bliss_prompt_orig


@pytest.mark.parametrize(
    "data",
    [
        ("y\n", True, Undefined),
        ("n\n", False, Undefined),
        ("yes\n", True, Undefined),
        ("YES\n", True, Undefined),
        ("NO\n", False, Undefined),
        ("yeS\n", True, Undefined),
        ("\x03", True, "yeah"),
        ("\x03", False, "yeah"),
    ],
)
def test_getval_yes_no(pt_test_context, data):
    keyboard, expected, ki_default = data
    with bliss_prompt_session() as bps:
        pt_test_context.send_input_later(1, keyboard, bps.wait_initialization)
        result = getval_yes_no("do you want", default=False, ki_default=ki_default)
    if ki_default != Undefined:
        assert result == ki_default
    else:
        assert result is expected
    assert bps.default_buffer.validation_error is None


def test_getval_yes_no_invalid(pt_test_context):
    with pytest.raises(TimeoutError):
        with bliss_prompt_session(timeout=3) as bps:
            pt_test_context.send_input_later(1, "yp\n", bps.wait_initialization)
            getval_yes_no("do you want", default=False)
    assert "The input have to" in bps.default_buffer.validation_error.message


def test_getval_yes_no_keyboardinterrupt(pt_test_context):
    with pytest.raises(KeyboardInterrupt):
        with bliss_prompt_session() as bps:
            pt_test_context.send_input_later(1, "\x03", bps.wait_initialization)
            getval_yes_no("do you want", default=False)


@pytest.mark.parametrize("data", [("titi\n", "titi"), ("tutu3\n", "tutu3")])
def test_getval_name(pt_test_context, data):
    keyboard, expected = data
    with bliss_prompt_session() as bps:
        pt_test_context.send_input_later(1, keyboard + "\n", bps.wait_initialization)
        result = getval_name("Enter name", default="toto")
    assert result == expected
    assert bps.default_buffer.validation_error is None


def test_getval_name_invalid(pt_test_context):
    with pytest.raises(TimeoutError):
        with bliss_prompt_session(timeout=3) as bps:
            pt_test_context.send_input_later(1, "1toto\n", bps.wait_initialization)
            getval_name("Enter name", default="toto")
    assert bps.default_buffer.validation_error is not None


@pytest.mark.parametrize(
    "data",
    [
        (1, 10, "1\n", 1),
        (1, 10, "50\b\n", 5),
        (-1, 10, "-1\n", -1),
        (1, 10, "\n", 9),
    ],
)
def test_getval_int(pt_test_context, data):
    minimum, maximum, keyboard, expected = data
    with bliss_prompt_session() as bps:
        pt_test_context.send_input_later(1, keyboard, bps.wait_initialization)
        result = getval_int("Give me", minimum, maximum, default=9)
    assert result == expected
    assert bps.default_buffer.validation_error is None


def test_getval_int_too_small(pt_test_context):
    with pytest.raises(TimeoutError):
        with bliss_prompt_session(timeout=3) as bps:
            pt_test_context.send_input_later(1, "1\n", bps.wait_initialization)
            getval_int("Give me", 2, 400, default=None)
    assert bps.default_buffer.validation_error is not None


def test_getval_int_too_big(pt_test_context):
    with pytest.raises(TimeoutError):
        with bliss_prompt_session(timeout=3) as bps:
            pt_test_context.send_input_later(1, "401\n", bps.wait_initialization)
            getval_int("Give me", 2, 400, default=112)
    assert bps.default_buffer.validation_error is not None


def test_getval_int_too_big_default(pt_test_context):
    with pytest.raises(RuntimeError):
        with bliss_prompt_session():
            getval_int("Give me", 1, 10, default=19)


@pytest.mark.parametrize(
    "data",
    [
        (1, 10, "1.1\n", 1.1),
        (1, 10, "1\n", 1.0),
        (1.1, 10.1, "50\b\n", 5.0),
        (-1, 10, "-1\n", -1.0),
        (1, 10, "\n", 9.0),
    ],
)
def test_getval_float(pt_test_context, data):
    minimum, maximum, keyboard, expected = data
    with bliss_prompt_session() as bps:
        pt_test_context.send_input_later(1, keyboard, bps.wait_initialization)
        result = getval_float("Give me", minimum, maximum, default=9.0)
    assert result == expected
    assert bps.default_buffer.validation_error is None


def test_getval_idx_list(pt_test_context):
    dspacing_list = ["111", "311", "642"]
    with bliss_prompt_session() as bps:
        pt_test_context.send_input_later(1, "2\n", bps.wait_initialization)
        result = getval_idx_list(dspacing_list, "enter value")
    assert result == (2, "311")
    assert bps.default_buffer.validation_error is None


def test_getval_char_list(pt_test_context):
    actions_list = [("a", "add a roi"), ("r", "remove a roi"), ("m", "modify a roi")]
    with bliss_prompt_session() as bps:
        pt_test_context.send_input_later(1, "\na\n", bps.wait_initialization)
        result = getval_char_list(actions_list, "enter value")
    assert result == ("a", "add a roi")
    assert bps.default_buffer.validation_error is None


def test_getval_char_list__default(pt_test_context):
    actions_list = [("a", "add a roi"), ("r", "remove a roi"), ("m", "modify a roi")]
    with bliss_prompt_session() as bps:
        pt_test_context.send_input_later(1, "a\n", bps.wait_initialization)
        result = getval_char_list(actions_list, "enter value", default="a")
    assert result == ("a", "add a roi")
    assert bps.default_buffer.validation_error is None

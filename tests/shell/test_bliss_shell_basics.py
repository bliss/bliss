# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from __future__ import annotations

import os
import re
import glob
import io
import jedi
import random
from importlib.metadata import version
import pytest
import gevent
from bliss.common.utils import autocomplete_property, UserNamespace
from bliss.shell.cli import config as cli_config
import bliss.shell.cli.repl
from bliss.common.greenlet_utils import asyncio_gevent


def test_shell_exit(feed_cli_with_input):
    result, cli, br = feed_cli_with_input(chr(0x4) + "y", check_line_ending=False)
    assert (result, cli, br) == (None, None, None)


def test_shell_exit2(feed_cli_with_input):
    result, cli, br = feed_cli_with_input(chr(0x4) + "\r", check_line_ending=False)
    assert (result, cli, br) == (None, None, None)


def test_shell_noexit(feed_cli_with_input):
    result, cli, br = feed_cli_with_input(
        chr(0x4) + "nprint 1 2\r", check_line_ending=True, confirm_exit=True
    )
    assert result == "print(1,2)"


@pytest.mark.parametrize(
    "delay", (0,) + tuple(random.randint(0, 5) / 10 for i in range(3))
)
def test_shell_kbint_during_move(beacon, clear_pt_context, delay, bliss_repl):
    roby = beacon.get("roby")
    with bliss_repl({"roby": roby}) as br:
        br.app.input.send_text("roby.move(1000)\r")
        g = asyncio_gevent.future_to_greenlet(br.run_async())
        while roby.state.READY:
            gevent.sleep(0.005)
        gevent.sleep(delay)
        assert roby.state.MOVING
        killpos = roby.position
        assert br._current_eval_g
        br._current_eval_g.kill(KeyboardInterrupt)
        br.app.input.send_text(chr(0x4))  # ctrl-d
        g.join()
        assert roby.state.READY
        assert roby.position < 1000, f"KILL POS WAS {killpos}"


def test_shell_ctrl_r(feed_cli_with_input):

    result, cli, br = feed_cli_with_input(
        chr(0x12) + "bla blub\r\r", check_line_ending=True
    )
    assert result == ""

    result, cli, br = feed_cli_with_input(
        "from bliss import setup_globals\rfrom subprocess import Popen\r"
        + chr(0x12)
        + "from bl\r\r",
        check_line_ending=True,
    )
    assert result == "from bliss import setup_globals"


def test_shell_comma_backets(feed_cli_with_input):
    result, cli, _ = feed_cli_with_input("print 1 2\r")
    assert result == "print(1,2)"


def test_shell_string_input(feed_cli_with_input):
    result, cli, _ = feed_cli_with_input("a='to to'\r")
    assert result == "a='to to'"


def test_shell_string_parameter(feed_cli_with_input):
    result, cli, _ = feed_cli_with_input("print 'bla bla'\r")
    assert result == "print('bla bla')"


def test_shell_function_without_parameter(feed_cli_with_input):
    def f():
        pass

    result, cli, _ = feed_cli_with_input("f\r", locals_dict={"f": f})
    assert result == "f()"


def test_shell_object_function_without_parameter(feed_cli_with_input):
    class Obj:
        def f(self):
            pass

    result, cli, _ = feed_cli_with_input("obj.f\r", locals_dict={"obj": Obj()})
    assert result == "obj.f()"


def test_shell_user_namespace_function_without_parameter(feed_cli_with_input):
    from bliss.common.session import UserNamespace

    def f():
        pass

    id00 = UserNamespace(f=f)

    result, cli, _ = feed_cli_with_input("id00.f\r", locals_dict={"id00": id00})
    assert result == "id00.f()"


def test_shell_function_with_return_only(feed_cli_with_input):
    result, cli, _ = feed_cli_with_input("\r")
    assert result == ""


def test_shell_callable_with_args(feed_cli_with_input):
    result, cli, _ = feed_cli_with_input("sum\r")
    assert result == "sum"

    def f(arg):
        pass

    result, cli, _ = feed_cli_with_input("f\r", locals_dict={"f": f})
    assert result == "f"


def test_shell_callable_with_kwargs_only(feed_cli_with_input):
    result, cli, _ = feed_cli_with_input("property\r")
    assert result == "property()"

    def f(arg="bla"):
        pass

    result, cli, _ = feed_cli_with_input("f\r", locals_dict={"f": f})
    assert result == "f()"


def test_shell_callable_with_args_and_kwargs(feed_cli_with_input):
    result, cli, _ = feed_cli_with_input("compile\r")
    assert result == "compile"

    def f(arg, kwarg="bla"):
        pass

    result, cli, _ = feed_cli_with_input("f\r", locals_dict={"f": f})
    assert result == "f"


def test_shell_list(feed_cli_with_input):
    result, cli, _ = feed_cli_with_input("list\r")
    assert result == "list"

    lst = list()
    result, cli, _ = feed_cli_with_input("lst\r", locals_dict={"lst": lst})
    assert result == "lst"


def test_shell_ScanSaving(session, feed_cli_with_input):
    from bliss.scanning.scan_saving import ScanSaving

    s = ScanSaving()

    result, cli, _ = feed_cli_with_input("s\r", locals_dict={"s": s})
    assert result == "s"


def test_shell_func(feed_cli_with_input):
    def f():
        pass

    result, cli, _ = feed_cli_with_input("f\r", locals_dict={"f": f})
    assert result == "f()"


def test_shell_semicolon(feed_cli_with_input):
    result, cli, _ = feed_cli_with_input("print 1 2;print 1\r")
    assert result == "print(1,2);print(1)"
    result, cli, _ = feed_cli_with_input("print 1 2;print 1;print 23\r")
    assert result == "print(1,2);print(1);print(23)"


def test_shell_comma_outside_callable_assignment(feed_cli_with_input):
    result, cli, _ = feed_cli_with_input("a=True \r")
    assert result == "a=True"


def test_shell_comma_outside_callable_bool(feed_cli_with_input):
    result, cli, _ = feed_cli_with_input("True \r")
    assert result == "True"


def test_shell_comma_outside_callable_string(feed_cli_with_input):
    result, cli, _ = feed_cli_with_input("'bla' \r")
    assert result == "'bla'"


def test_shell_comma_outside_callable_number(feed_cli_with_input):
    result, cli, _ = feed_cli_with_input("1.1 + 1  \r")
    assert result == "1.1 + 1"


def test_shell_comma_after_comma(feed_cli_with_input):
    result, cli, _ = feed_cli_with_input("1, \r")
    assert result == "1,"


def test_protected_against_trailing_whitespaces(bliss_repl):
    """Check that the number of spaces (N) after a command doesn't make the command to be repeated N-1 times"""

    with bliss_repl() as br:
        br.raw_eval("f=lambda: 'Om Mani Padme Hum'\r")
        br.send_input(f"f(){' '*5}\r")

    assert br.get_cell_output(-1).strip() == "'Om Mani Padme Hum'"
    with pytest.raises(IndexError):
        assert br.get_cell_output(-2)


def test_info_dunder(bliss_repl):
    class A:
        def __repr__(self):
            return "repr-string"

        def __str__(self):
            return "str-string"

        def __info__(self):
            return "info-string"

        def titi(self):
            return "titi-method"

    class B:
        def __repr__(self):
            return "repr-string"

    class C:
        pass

    # '__info__()' method called at object call.
    with bliss_repl({"A": A(), "B": B(), "C": C()}) as br:
        br.send_input("A\r")
        assert "info-string" in br.get_cell_output(-1)

        br.send_input("[A]\r")
        assert "[repr-string]" in br.get_cell_output(-1)

        # 2 parenthesis added to method if not present
        br.send_input("A.titi\r")
        assert "titi-method" in br.get_cell_output(-1)

        # Closing parenthesis added if only opening one is present.
        br.send_input("A.titi(\r")
        assert "titi-method" in br.get_cell_output(-1)

        # Ok if finishing by a closing parenthesis.
        br.send_input("A.titi()\r")
        assert "titi-method" in br.get_cell_output(-1)

        # '__repr__()' used if no '__info__()' method is defined.
        br.send_input("B\r")
        assert "repr-string" in br.get_cell_output(-1)

        # Default behaviour for object without specific method.
        br.send_input("C\r")
        assert "C object at " in br.get_cell_output(-1)

    cli_config.typing_helper_active = False
    try:
        with bliss_repl({"A": A, "B": B, "A.titi": A.titi}) as br:
            br.send_input("A\r")
            assert (
                "<class 'tests.shell.test_bliss_shell_basics.test_info_dunder.<locals>.A'>\n"
                == br.get_cell_output(-1)
            )
    finally:
        cli_config.typing_helper_active = True


def test_shell_dict_list_not_callable(feed_cli_with_input):
    result, cli, _ = feed_cli_with_input("d \r", locals_dict={"d": dict()})
    assert result == "d"


def test_property_evaluation(feed_cli_with_input):
    class Bla:
        def __init__(self):
            self.i = 0

        @property
        def test(self):
            self.i += 1
            return self.i

    b = Bla()

    result, cli, _ = feed_cli_with_input("b.test     \r", locals_dict={"b": b})
    assert b.test == 1
    result, cli, _ = feed_cli_with_input("b.test;print 1\r", locals_dict={"b": b})
    result, cli, _ = feed_cli_with_input("b.test;print 1\r", locals_dict={"b": b})
    result, cli, _ = feed_cli_with_input("b.test;print 1\r", locals_dict={"b": b})
    assert b.test == 2


def test_func_no_args(feed_cli_with_input):
    result, cli, _ = feed_cli_with_input("f \r", locals_dict={"f": lambda: None})
    assert result == "f()"


def test_nested_property_evaluation(feed_cli_with_input):
    class A:
        def __init__(self):
            self.count = 0

        @property
        def foo(self):
            self.count += 1
            return self.count

    class B:
        def __init__(self):
            self.a = A()

        @property
        def bar(self):
            return self.a

    b = B()

    result, cli, _ = feed_cli_with_input("b.bar.foo\r", locals_dict={"b": b})
    result, cli, _ = feed_cli_with_input("b.bar.foo\r", locals_dict={"b": b})
    assert b.bar.foo == 1


def test_deprecation_warning(beacon, log_context, bliss_repl):
    """
    Check that the BlissRepl properly store deprecation warningin the
    cell output.
    """

    def test_deprecated():
        from bliss.common.deprecation import deprecated_warning

        print("bla")
        deprecated_warning(
            kind="function",
            name="ct",
            replacement="sct",
            reason="`ct` does no longer allow to save data",
            since_version="1.5.0",
            skip_backtrace_count=5,
            only_once=False,
        )

    with bliss_repl({"func": test_deprecated}) as br:
        br.send_input("func()\r")
        output = br.get_cell_output(-1)
        assert "bla" in output
        assert "Function ct is deprecated since" in output


def test_captured_output(bliss_repl):
    """
    Check that the BlissRepl properly store execution cells.
    """
    with bliss_repl() as br:
        br.raw_eval("f=lambda num: print(num+1) or num+2\r")
        br.raw_eval("fnone=lambda: None\r")
        br.raw_eval("fnoneprint=lambda: print('hello\\nworld')\r")

        br.send_input("f(1)")
        br.send_input("f(3)")
        br.send_input("fnone()")
        br.send_input("f(4)")
        br.send_input("fnoneprint()")
        br.send_input("f(5)")

        assert br.get_cell_output(1) == "2\n3\n"
        assert br.get_cell_output(2) == "4\n5\n"
        assert br.get_cell_output(3) is None
        assert br.get_cell_output(4) == "5\n6\n"
        assert br.get_cell_output(5) == "hello\nworld\n"
        assert br.get_cell_output(6) == "6\n7\n"

        assert br.get_cell_output(-1) == "6\n7\n"
        assert br.get_cell_output(-2) == "hello\nworld\n"
        assert br.get_cell_output(-3) == "5\n6\n"
        assert br.get_cell_output(-4) is None
        assert br.get_cell_output(-5) == "4\n5\n"
        assert br.get_cell_output(-6) == "2\n3\n"

        with pytest.raises(
            IndexError, match=re.escape("the first available cell is OUT [1]")
        ):
            br.get_cell_output(-7)
        with pytest.raises(IndexError, match=re.escape("the last cell is OUT [6]")):
            br.get_cell_output(7)
        with pytest.raises(
            IndexError, match=re.escape("the first available cell is OUT [1]")
        ):
            br.get_cell_output(0)

        # Fill the output history with None's
        MAXLEN = br.app.output._MAXLEN
        for _ in range(MAXLEN - 6):
            br.send_input("fnone()")

        assert br.get_cell_output(1) == "2\n3\n"

        # Push the first entry out of the history
        br.send_input("fnone()")

        with pytest.raises(
            IndexError, match=re.escape("the first available cell is OUT [2]")
        ):
            br.get_cell_output(1)

        # Push the second entry out of the history
        br.send_input("fnone()")

        with pytest.raises(
            IndexError, match=re.escape("the first available cell is OUT [3]")
        ):
            br.get_cell_output(1)

        with pytest.raises(
            IndexError, match=re.escape("the first available cell is OUT [3]")
        ):
            br.get_cell_output(-MAXLEN - 3)
        with pytest.raises(
            IndexError, match=re.escape(f"the last cell is OUT [{MAXLEN+2}]")
        ):
            br.get_cell_output(MAXLEN + 3)
        with pytest.raises(
            IndexError, match=re.escape("the first available cell is OUT [3]")
        ):
            br.get_cell_output(0)


def test_getattribute_exception(feed_cli_with_input):
    """Make sure __getattribute__ exceptions are handled by ptpython"""
    # https://github.com/prompt-toolkit/ptpython/issues/351
    raise_exception = True

    class A:
        def __getattribute__(self, name):
            nonlocal raise_exception

            if raise_exception:
                raise_exception = False
                raise RuntimeError("should be captured by ptpython")

            # This object is used in cleanup fixtures with gc.get_objects()
            # so do not raise an exception again.
            return None

    a = A()

    try:
        result, cli, _ = feed_cli_with_input("a.myattribute\r", locals_dict={"a": a})
        assert not raise_exception, "the exception was not raised"
    finally:
        raise_exception = False


def test_excepthook(beacon, bliss_repl):
    repl_locals = {}
    stdout = io.StringIO()
    with bliss_repl(repl_locals, stdout=stdout) as br:
        br.initialize_session()
        expression = "raise RuntimeError('excepthook test')\r"
        br.send_input(expression)

    output = stdout.getvalue()
    assert (
        "!!! === RuntimeError: excepthook test === !!! ( for more details type cmd 'last_error(0)' )"
        in output
    )


def test_history_archiving(tmpdir):
    history_filename = tmpdir / ".bliss_pytest_dummy_history"

    # ensure there exist no such history files before testing
    for p in glob.glob(f"{history_filename}*"):
        os.remove(p)

    def dummy_entry(id):
        return f"# 1970-01-01 12:00:{id}.000000\n+dummy_cmd_{id}()\n\n"

    # create a fifty entries history file
    history = "".join([dummy_entry(i) for i in range(50)])
    with open(history_filename, "w") as f:
        f.write(history)

    # file size < threshold : no archiving
    bliss.shell.cli.repl._archive_history(history_filename, file_size_thresh=3000)
    with open(history_filename, "r") as f:
        assert f.read() == history

    # file size > threshold : archiving
    bliss.shell.cli.repl._archive_history(
        history_filename, file_size_thresh=2000, keep_active_entries=15
    )
    with open(history_filename, "r") as f:
        assert f.read() == "".join([dummy_entry(i) for i in range(50 - 15, 50)])

    archive_file = glob.glob(f"{history_filename}_*")[0]
    with open(archive_file, "r") as f:
        assert f.read() == "".join([dummy_entry(i) for i in range(50 - 15)])


def test_autocomplete_property(capsys):
    jedi_autocompletes_since = 0, 19
    jedi_version = tuple(int(s) for s in version("jedi").split("."))
    property_autocompletes = jedi_version >= jedi_autocompletes_since

    class C:
        @property
        def titi(self):
            print("TITI")

    class A:
        @property
        def foo(self):
            print("FOO")

        @property
        def propa(self):
            print("PROPA")
            return C()

        @autocomplete_property
        def propb(self):
            print("PROPB")
            return C()

    class B:
        @autocomplete_property
        def bar(self):
            print("BAR")
            return A()

        @property
        def baz(self):
            print("BAZ")
            return A()

        @autocomplete_property
        def toto(self):
            print("TOTO")

    b = B()

    def autocomplete(text, **kw):
        i = jedi.Interpreter(text, path="input-text", **kw)
        completions = {
            c.complete for c in i.complete() if not c.complete.startswith("_")
        }
        output = {s for s in capsys.readouterr().out.split("\n") if s}
        return completions, output

    # Autocomplete: object

    completions, output = autocomplete("b.", namespaces=[locals(), globals()])
    assert completions == {"bar", "baz", "toto"}
    assert not output

    # Autocomplete: object -> autocomplete_property

    completions, output = autocomplete("b.toto", namespaces=[locals(), globals()])
    assert completions == {""}
    assert not output

    completions, output = autocomplete("b.toto.", namespaces=[locals(), globals()])
    assert not completions
    assert output == {"TOTO"}

    # Autocomplete: object -> autocomplete_property

    completions, output = autocomplete("b.bar", namespaces=[locals(), globals()])
    assert completions == {""}
    assert not output

    completions, output = autocomplete("b.bar.", namespaces=[locals(), globals()])
    assert completions == {"foo", "propa", "propb"}
    assert output == {"BAR"}

    # Autocomplete: object -> autocomplete_property -> property

    completions, output = autocomplete("b.bar.propa", namespaces=[locals(), globals()])
    assert completions == {""}
    assert output == {"BAR"}

    completions, output = autocomplete("b.bar.propa.", namespaces=[locals(), globals()])
    if property_autocompletes:
        assert completions == {"titi"}
        assert output == {"BAR", "PROPA"}
    else:
        assert not completions
        assert output == {"BAR"}

    # Autocomplete: object -> autocomplete_property -> autocomplete_property

    completions, output = autocomplete("b.bar.propb", namespaces=[locals(), globals()])
    assert completions == {""}
    assert output == {"BAR"}

    completions, output = autocomplete("b.bar.propb.", namespaces=[locals(), globals()])
    assert completions == {"titi"}
    assert output == {"BAR", "PROPB"}

    # Autocomplete: object -> property

    completions, output = autocomplete("b.baz", namespaces=[locals(), globals()])
    assert completions == {""}
    assert not output

    completions, output = autocomplete("b.baz.", namespaces=[locals(), globals()])
    if property_autocompletes:
        assert completions == {"foo", "propa", "propb"}
        assert output == {"BAZ"}
    else:
        assert not completions
        assert not output

    # Autocomplete: object -> property -> property

    completions, output = autocomplete("b.baz.propa", namespaces=[locals(), globals()])
    if property_autocompletes:
        assert completions == {""}
        assert output == {"BAZ"}
    else:
        assert not completions
        assert not output

    completions, output = autocomplete("b.baz.propa.", namespaces=[locals(), globals()])
    if property_autocompletes:
        assert completions == {"titi"}
        assert output == {"BAZ", "PROPA"}
    else:
        assert not completions
        assert not output

    # Autocomplete: object -> property -> autocomplete_property

    completions, output = autocomplete("b.baz.propb", namespaces=[locals(), globals()])
    if property_autocompletes:
        assert completions == {""}
        assert output == {"BAZ"}
    else:
        assert not completions
        assert not output

    completions, output = autocomplete("b.baz.propb.", namespaces=[locals(), globals()])
    if property_autocompletes:
        assert completions == {"titi"}
        assert output == {"BAZ", "PROPB"}
    else:
        assert not completions
        assert not output

    # Autocomplete: namespace -> object

    un = UserNamespace(b=b)

    completions, output = autocomplete("un.b", namespaces=[locals(), globals()])
    assert completions == {""}
    assert not output

    completions, output = autocomplete("un.b.", namespaces=[locals(), globals()])
    assert completions == {"bar", "baz", "toto"}
    assert not output

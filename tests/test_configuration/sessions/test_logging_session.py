print("Executing test_logging_session.py ...")

from bliss.setup_globals import *  # noqa: F403, E402
from bliss.common import logtools  # noqa: E402


class LoggingSessionDummy:
    def __init__(self):
        logtools.log_error(self, "test_logging_session.py: Beacon error")


logtools.elog_error("test_logging_session.py: E-logbook error")
LoggingSessionDummy()

setupfinished = True

load_script("logscript.py")  # noqa: F405

print("End of test_logging_session.py.")

import gevent
import pytest
from bliss.common.tango_callbacks import TangoCallbacks
from bliss.common.tango import DevState


@pytest.fixture
def dummy_tango_device_with_callbacks(dummy_tango_server):
    _, proxy = dummy_tango_server
    callbacks = TangoCallbacks(proxy)
    yield proxy, callbacks
    callbacks.stop()


def test_event_state(dummy_tango_device_with_callbacks):
    proxy, callbacks = dummy_tango_device_with_callbacks

    last_events = []
    event_from_gevent = None

    def on_change(attr_name, new_value):
        nonlocal last_events, event_from_gevent
        last_events.append((attr_name, new_value))

        g = gevent.getcurrent()
        event_from_gevent = isinstance(g, gevent.Greenlet)

    callbacks.add_callback("state", on_change)

    expected = DevState.OPEN
    proxy.open()
    gevent.sleep(2.0)

    assert len(last_events) == 1
    assert last_events[-1] == ("state", expected)
    assert event_from_gevent, "Event callbacks have to be called from gevent"
    assert callbacks._events["state"].polled is False


def test_event_number(dummy_tango_device_with_callbacks):
    proxy, callbacks = dummy_tango_device_with_callbacks

    last_events = []
    event_from_gevent = None

    def on_change(attr_name, new_value):
        nonlocal last_events, event_from_gevent
        last_events.append((attr_name, new_value))

        g = gevent.getcurrent()
        event_from_gevent = isinstance(g, gevent.Greenlet)

    callbacks.add_callback("velocity", on_change)

    expected = proxy.velocity + 1.0
    proxy.velocity = expected
    gevent.sleep(2.0)
    assert len(last_events) == 1
    assert last_events[-1] == ("velocity", expected)
    assert event_from_gevent, "Event callbacks have to be called from gevent"
    assert callbacks._events["velocity"].polled is False

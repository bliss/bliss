import pytest
from bliss.common.scans import loopscan
from bliss.testutils.data_policies import set_data_policy


@pytest.fixture
def esrf_writer_session(session, icat_mock_client, nexus_writer_service):
    set_data_policy(session, "esrf")
    session.scan_saving.writer = "nexus"
    yield session


def test_expiration_esrf_proposal(esrf_writer_session):
    diode = esrf_writer_session.env_dict["diode"]
    loopscan(1, 0.1, diode)

    proposal = esrf_writer_session.scan_saving.proposal
    collection = esrf_writer_session.scan_saving.dataset
    dataset = esrf_writer_session.scan_saving.dataset

    esrf_writer_session.scan_saving.newproposal("new" + proposal.name)

    # previous proposal objects are no longer accessible
    with pytest.raises(KeyError):
        proposal.name
    with pytest.raises(KeyError):
        collection.name
    with pytest.raises(KeyError):
        dataset.name

# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import os
import pytest
from bliss.scanning.writer.base import FileWriter
from bliss.scanning.scan_saving import BasicScanSaving
from bliss.common import scans
from bliss.common.data_store import get_default_data_store
from bliss.scanning.group import Sequence
from bliss.config.conductor.client import get_redis_proxy
from bliss.testutils.resources_utils import GreenletsContext, RedisConnectionContext


@pytest.mark.parametrize(
    "exception_on",
    [
        "get_filename",
        "create_path",
        "_on_master_event",
        "_on_device_event",
        "prepare",
        "finalize",
    ],
)
def test_scan_cleanup_writerexceptions(exception_on, session):
    session._set_scan_saving(_CustomScanSaving)
    detectors = (session.env_dict["diode"],)
    session.scan_saving.exception_on = exception_on
    with pytest.raises(RuntimeError):
        scans.loopscan(5, 0.1, *detectors)


@pytest.fixture
def preconnect_redis_connections(beacon):
    # Expand reusable connection pools so that don't show up in resource
    # monitoring. Note: we need to keep a reference to the proxies,
    # otherwise the pools get garbage collected.
    nconnections = 50
    keep_references = []
    for db in [0, 1]:
        for caching in [True, False]:
            proxy = get_redis_proxy(db=db, caching=caching)
            keep_references.append(proxy)
            pool = proxy.connection_pool
            pool.preconnect(nconnections)

    data_store = get_default_data_store()
    pool = data_store._redis.connection_pool
    connections = [pool.get_connection(None) for _ in range(nconnections)]
    for connection in connections:
        pool.release(connection)

    yield


def test_scan_resources(session, preconnect_redis_connections):
    with GreenletsContext() as context1:
        with RedisConnectionContext() as context2:
            diode = session.env_dict["diode"]
            scans.loopscan(3, 0.1, diode)

    assert context2.all_resources_released
    assert context1.all_resources_released


def test_scan_sequence_resources(session, preconnect_redis_connections):
    with GreenletsContext() as context1:
        with RedisConnectionContext() as context2:
            diode = session.env_dict["diode"]
            seq = Sequence()
            with seq.sequence_context() as seq_context:
                s1 = scans.loopscan(3, 0.1, diode)
                seq_context.add(s1)
                s2 = scans.loopscan(3, 0.2, diode)
                seq_context.add(s2)

    assert context2.all_resources_released
    assert context1.all_resources_released


class _CustomWriter(FileWriter):
    _FILE_EXTENSION = ".xyz"

    def __init__(self, *args, exception_on=False, **kw):
        self.exception_on = exception_on
        super().__init__(*args, **kw)
        self._master_event_callback = self._on_master_event
        self._slave_event_callback = self._on_device_event

    def get_filename(self) -> str:
        if "get_filename" in self.exception_on:
            raise RuntimeError("Raise on 'get_filename' for testing scan cleanup")
        return super().get_filename()

    def create_path(self, path: str) -> bool:
        if "create_path" in self.exception_on:
            raise RuntimeError("Raise on 'create_path' for testing scan cleanup")
        return super().create_path(path)

    def _on_master_event(self, *_) -> None:
        if "_on_master_event" in self.exception_on:
            raise RuntimeError("Raise on '_on_master_event' for testing scan cleanup")

    def _on_device_event(self, *_) -> None:
        if "_on_device_event" in self.exception_on:
            raise RuntimeError("Raise on '_on_device_event' for testing scan cleanup")

    def prepare(self, scan) -> None:
        if "prepare" in self.exception_on:
            raise RuntimeError("Raise on 'prepare' for testing scan cleanup")
        super().prepare(scan)

    def finalize(self, scan) -> None:
        if "finalize" in self.exception_on:
            raise RuntimeError("Raise on 'finalize' for testing scan cleanup")
        super().finalize(scan)


class _CustomScanSaving(BasicScanSaving):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.exception_on = ""

    @property
    def writer_class(self) -> type[FileWriter]:
        return _CustomWriter

    @property
    def writer_object(self) -> _CustomWriter:
        klass = self.writer_class
        exception_on = self.exception_on.split(",")
        writer = klass(
            self.root_path,
            self.images_path,
            self.data_filename,
            exception_on=exception_on,
        )
        template = os.path.join(self.root_path, self.images_path, self.data_filename)
        writer.update_template(
            {name: f"{name}" for name in self._string_template_keys(template)}
        )
        return writer

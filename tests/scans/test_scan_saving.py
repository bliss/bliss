# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import os
import pytest

import bliss
from bliss.common.standard import info
from bliss.common.scans import ct, sct
from bliss.scanning import scan_saving as scan_saving_module


@pytest.mark.parametrize("writer", ["hdf5", "nexus", "null", "external"])
def test_scan_saving_path(writer, session):
    scan_saving = session.scan_saving
    scan_saving.template = "{session}/{scan_name}/{scan_number}"
    scan_saving.data_filename = "{session}_{scan_name}_data"
    scan_saving.writer = writer
    base_path = os.path.join(scan_saving.base_path, "subdir")
    scan_saving.base_path = base_path

    assert scan_saving.base_path == base_path
    assert (
        scan_saving._get_raw_value("template") == "{session}/{scan_name}/{scan_number}"
    )
    assert scan_saving._get_raw_value("data_filename") == "{session}_{scan_name}_data"

    # Test all path related methods and properties
    root_path = f"{base_path}/{session.name}/{{scan_name}}/{{scan_number}}"
    assert scan_saving.get_path() == root_path
    assert scan_saving.root_path == root_path
    images_path = f"{base_path}/{session.name}/{{scan_name}}/{{scan_number}}/scan{{scan_number}}/{{img_acq_device}}_"
    assert scan_saving.images_path == images_path
    data_path = f"{base_path}/{session.name}/{{scan_name}}/{{scan_number}}/{session.name}_{{scan_name}}_data"
    assert scan_saving.data_path == data_path
    if writer == "null":
        data_fullpath = data_path + "."
    else:
        data_fullpath = data_path + ".h5"
    assert scan_saving.data_fullpath == data_fullpath
    assert scan_saving.eval_data_filename == f"{session.name}_{{scan_name}}_data"
    if writer == "null":
        filename = None
    else:
        filename = data_fullpath
    assert scan_saving.filename == filename

    getdict = scan_saving.get()
    getdict.pop("writer")
    expected = {
        "root_path": root_path,
        "data_path": data_path,
        "images_path": images_path,
    }
    assert getdict == expected

    scan_saving_repr = """Parameters (default) -

  .base_path            = '{base_path}'
  .data_filename        = '{{session}}_{{scan_name}}_data'
  .template             = '{{session}}/{{scan_name}}/{{scan_number}}'
  .images_path_relative = True
  .images_path_template = 'scan{{scan_number}}'
  .images_prefix        = '{{img_acq_device}}_'
  .date_format          = '%Y%m%d'
  .scan_number_format   = '%04d'
  .session              = '{session}'
  .date                 = '{date}'
  .user_name            = '{user_name}'
  .scan_name            = '{{scan_name}}'
  .scan_number          = '{{scan_number}}'
  .img_acq_device       = '{{img_acq_device}}'
  .data_policy          = 'None'
  .writer               = '{writer}'
--------------  ---------  -----------
does not exist  filename   {filename}
does not exist  directory  {root_path}
--------------  ---------  -----------""".format(
        base_path=base_path,
        session=session.name,
        user_name=scan_saving.user_name,
        date=scan_saving.date,
        writer=writer,
        filename=filename,
        root_path=root_path,
    )

    expected = scan_saving_repr.split("\n")
    actual = info(scan_saving).split("\n")
    if writer == "null":
        expected = expected[:-1]
        expected[-3:] = ["---------", "NO SAVING", "---------"]
    else:
        add = "-" * (len(actual[-1]) - len(expected[-1]))
        expected[-4] += add
        expected[-1] += add
    assert actual == expected

    # Test scan saving related things after a scan
    if writer in ("hdf5", "null"):
        scan = sct(0.1, session.env_dict["diode"], save=True, name="sname")
    else:
        scan = ct(0.1, session.env_dict["diode"])
        assert scan.scan_info["filename"] is None

    assert scan.scan_info["data_policy"] == "None"
    assert scan.scan_info["data_writer"] == writer


def test_session_scan_saving_config(beacon):
    class TestESRFScanSaving(scan_saving_module.ESRFScanSaving):
        pass

    scan_saving_module.TestESRFScanSaving = TestESRFScanSaving

    scan_saving_test_session = beacon.get("scan_saving_test_session")
    scan_saving_test_session.setup()

    try:
        scan_saving = scan_saving_test_session.scan_saving
        assert isinstance(scan_saving, scan_saving_module.TestESRFScanSaving)
        scan_saving.newproposal("ihr0000")
        assert scan_saving.base_path == "/tmp/scans/visitor_test_scan_saving"
    finally:
        scan_saving_test_session.close()


def test_default_session_scan_saving(session):
    session.enable_esrf_data_policy()
    scan_saving = session.scan_saving
    scan_saving.beamline


@pytest.mark.parametrize("writer", ["null", "hdf5", "nexus", "external"])
def test_scan_saving_writer_options(session, writer):
    scan_saving = session.scan_saving

    scan_saving.writer = writer
    if writer == "null":
        expected = dict()
    else:
        expected = {
            "chunk_options": dict(),
            "separate_scan_files": None,
        }
    assert scan_saving.get_writer_options() == expected

    if writer == "null":
        return

    writer = scan_saving.writer_object
    writer.compression_limit = 1
    writer.chunk_size = 1
    writer.compression_scheme = "gzip"
    writer.chunk_split = 4
    writer.separate_scan_files = True
    assert scan_saving.get_writer_options() == {
        "chunk_options": {
            "chunk_nbytes": 1 << 20,
            "compression_scheme": "gzip",
            "compression_limit_nbytes": 1 << 20,
            "chunk_split": 4,
        },
        "separate_scan_files": True,
    }


def test_scan_saving_warning_tmpdir(session, caplog):
    bliss.set_bliss_shell_mode()
    try:
        sct(session.env_dict["diode"])
    finally:
        bliss.set_bliss_shell_mode(False)
    expected = "scan data are currently saved under /tmp, where files are volatile."
    assert expected in caplog.text

# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import gevent
import contextlib
import numpy
import pytest
from bliss.controllers.lima.lima_base import Lima
from bliss.scanning.toolbox import ChainBuilder
from bliss.scanning.acquisition.timer import SoftwareTimerMaster
from bliss.scanning.acquisition.motor import SoftwarePositionTriggerMaster
from bliss.scanning.acquisition.counter import SamplingCounterAcquisitionSlave
from bliss.scanning.acquisition.mca import McaAcquisitionSlave
from bliss.scanning.acquisition.motor import LinearStepTriggerMaster
from bliss.scanning.scan import Scan
from bliss.scanning.scan_info import ScanInfo
from bliss.scanning.scan_events import ScansObserver, ScansWatcher
from bliss.scanning.chain import AcquisitionChain
from bliss.common import scans
from bliss.scanning.group import Sequence


@pytest.fixture
def test_observer(mocker):
    """Helper to check post mortem the events from the scans watcher"""
    observer = mocker.Mock(spec=ScansObserver)

    def read_scan_info(observer, method_name):
        call = getattr(observer, method_name).call_args_list[0]
        scan_info = call[0][1]
        return scan_info

    def read_scalar_data(observer, channel_name, scan_name):
        data = []
        for (
            _scan_name,
            _channel_name,
            _index,
            _data,
        ), _ in observer.on_scalar_data_received.call_args_list:
            if _channel_name != channel_name:
                continue
            if _scan_name != scan_name:
                continue
            data.append(_data)
        if len(data) == 0:
            return []
        return numpy.concatenate(data)

    observer.on_scan_created__scan_info = lambda: read_scan_info(
        observer, "on_scan_created"
    )
    observer.on_scan_started__scan_info = lambda: read_scan_info(
        observer, "on_scan_started"
    )
    observer.on_scan_finished__scan_info = lambda: read_scan_info(
        observer, "on_scan_finished"
    )
    observer.on_scalar_data_received__get_data = (
        lambda channel_name, scan_key: read_scalar_data(
            observer, channel_name, scan_key
        )
    )

    yield observer


@contextlib.contextmanager
def watching(session, observer, exclude_groups=False):
    watcher = ScansWatcher(session.name)
    if not exclude_groups:
        watcher.set_watch_scan_group(True)
    watcher.set_observer(observer)
    gevent.spawn(watcher.run)
    watcher.ready_event.wait(timeout=10)
    try:
        yield watcher
    finally:
        watcher.stop()


def test_simple_continuous_scan_with_session_watcher(session, test_observer):
    m1 = session.config.get("m1")
    counter = session.config.get("diode")
    master = SoftwarePositionTriggerMaster(m1, 0, 1, 10, time=1)
    acq_dev = SamplingCounterAcquisitionSlave(counter, count_time=0.01, npoints=10)
    chain = AcquisitionChain()
    chain.add(master, acq_dev)

    with watching(session, test_observer):
        scan = Scan(chain, save=False)
        scan.run()

    test_observer.on_scan_created.assert_called_once()
    test_observer.on_scan_started.assert_called_once()
    test_observer.on_scalar_data_received.assert_called()
    test_observer.on_scan_finished.assert_called_once()

    sf = ScanInfo()
    scan_info = test_observer.on_scan_created__scan_info()
    assert "axis:m1" in scan_info["channels"]
    assert "simulation_diode_sampling_controller:diode" in scan_info["channels"]
    device_key = sf._get_device_key_from_acq_obj(acq_dev)
    assert device_key in scan_info["devices"]
    device_key = sf._get_device_key_from_acq_obj(master)
    assert device_key in scan_info["devices"]

    scan_data_m1 = []
    for call_args in test_observer.on_scalar_data_received.call_args_list:
        kwargs = call_args[0]
        if kwargs[1] == "axis:m1":
            scan_data_m1.append(kwargs[3])

    scan_data_m1 = numpy.concatenate(scan_data_m1)
    assert numpy.allclose(scan_data_m1, master._positions, atol=1e-1)


def test_mca_with_watcher(session, test_observer):
    m0 = session.config.get("roby")
    simu = session.config.get("simu1")
    mca_device = McaAcquisitionSlave(*simu.counters, npoints=3, preset_time=0.1)
    chain = AcquisitionChain()
    chain.add(LinearStepTriggerMaster(3, m0, 0, 1), mca_device)
    scan = Scan(chain, "mca_test", save=False)

    with watching(session, test_observer):
        scan.run()

    test_observer.on_scan_created.assert_called_once()
    test_observer.on_scan_started.assert_called_once()
    test_observer.on_ndim_data_received.assert_called()
    test_observer.on_scan_finished.assert_called_once()

    sf = ScanInfo()
    device_key = sf._get_device_key_from_acq_obj(mca_device)
    scan_info = test_observer.on_scan_started__scan_info()
    assert device_key in scan_info["devices"]
    assert scan_info["devices"][device_key]["type"] == "mca"


def test_limatake_with_watcher(session, lima_simulator, test_observer):
    lima_simulator = session.config.get("lima_simulator")

    ff = lima_simulator.saving.file_format
    lima_simulator.saving.file_format = ff

    lima_params = {
        "acq_nb_frames": 1,
        "acq_expo_time": 0.01,
        "acq_mode": "SINGLE",
        "acq_trigger_mode": "INTERNAL_TRIGGER",
        "prepare_once": False,
        "start_once": False,
    }

    chain = AcquisitionChain(parallel_prepare=True)
    builder = ChainBuilder([lima_simulator])
    for node in builder.get_nodes_by_controller_type(Lima):
        node.set_parameters(acq_params=lima_params)
        chain.add(node)
        lima_acq_obj = node.acquisition_obj

    scan = Scan(chain, name="limatake", save=False)

    with watching(session, test_observer):
        scan.run()

    test_observer.on_scan_created.assert_called_once()
    test_observer.on_scan_started.assert_called_once()
    test_observer.on_lima_event_received.assert_called()
    test_observer.on_scan_finished.assert_called_once()

    sf = ScanInfo()
    device_key = sf._get_device_key_from_acq_obj(lima_acq_obj)
    scan_info = test_observer.on_scan_started__scan_info()
    assert device_key in scan_info["devices"]
    assert scan_info["devices"][device_key]["type"] == "lima"


def test_parallel_scans(default_session, test_observer):
    diode = default_session.config.get("diode")
    sim_ct_gauss = default_session.config.get("sim_ct_gauss")
    robz = default_session.config.get("robz")

    s1 = scans.loopscan(20, 0.1, diode, run=False)
    s2 = scans.ascan(robz, 0, 10, 25, 0.09, sim_ct_gauss, run=False)

    with watching(default_session, test_observer):
        g1 = gevent.spawn(s1.run)
        g2 = gevent.spawn(s2.run)
        gs = [g1, g2]
        gevent.joinall(gs, raise_error=True)

    assert len(test_observer.on_scan_created.call_args_list) == 2
    assert len(test_observer.on_scan_started.call_args_list) == 2
    assert len(test_observer.on_scan_finished.call_args_list) == 2

    for (key, info), _ in test_observer.on_scan_created.call_args_list:
        if info["type"] == "loopscan":
            loopscan_key = key
        if info["type"] == "ascan":
            ascan_key = key

    loopscan_data = test_observer.on_scalar_data_received__get_data(
        channel_name="simulation_diode_sampling_controller:diode",
        scan_key=loopscan_key,
    )

    ascan_data = test_observer.on_scalar_data_received__get_data(
        channel_name="simulation_counter_controller:sim_ct_gauss", scan_key=ascan_key
    )

    assert len(ascan_data) == 26
    assert len(loopscan_data) == 20


def test_scan_sequence(default_session, mocker, test_observer):
    diode = default_session.config.get("diode")

    with watching(default_session, test_observer):
        seq = Sequence()
        with seq.sequence_context() as scan_seq:
            s1 = scans.loopscan(5, 0.1, diode, run=False)
            scan_seq.add(s1)
            s1.run()

    assert len(test_observer.on_scan_created.call_args_list) == 2
    assert len(test_observer.on_scan_started.call_args_list) == 2
    assert len(test_observer.on_scan_finished.call_args_list) == 2


def test_scan_sequence_excluding_groups(default_session, test_observer):
    diode = default_session.config.get("diode")

    with watching(default_session, test_observer, exclude_groups=True):
        seq = Sequence()
        with seq.sequence_context() as scan_seq:
            s1 = scans.loopscan(5, 0.1, diode, run=False)
            scan_seq.add(s1)
            s1.run()

    test_observer.on_scan_created.assert_called_once()
    test_observer.on_scan_started.assert_called_once()
    test_observer.on_scan_finished.assert_called_once()


def test_watcher_stop(session, diode_acq_device_factory, test_observer):
    chain = AcquisitionChain()
    acquisition_device_1, _ = diode_acq_device_factory.get(count_time=0.1, npoints=100)
    master = SoftwareTimerMaster(0.1, npoints=1)
    chain.add(master, acquisition_device_1)

    try:
        with watching(session, test_observer) as watcher:
            scan = Scan(chain, save=False)
            g = gevent.spawn(scan.run)
            gevent.sleep(3)
            watcher.stop(wait_running_scans=False)

        test_observer.on_scan_created.assert_called_once()
        test_observer.on_scan_started.assert_called_once()
        test_observer.on_scan_finished.assert_not_called()
    finally:
        g.kill()


def test_scan_observer(
    session, scan_saving, diode_acq_device_factory, test_observer, mocker
):
    chain = AcquisitionChain()
    acquisition_device_1, _ = diode_acq_device_factory.get(count_time=0.1, npoints=1)
    master = SoftwareTimerMaster(0.1, npoints=1)
    chain.add(master, acquisition_device_1)
    scan = Scan(chain, "test", save=False)

    def side_effect(timing):
        if timing == acquisition_device_1.META_TIMING.PREPARED:
            return {"kind": "foo"}
        else:
            return None

    acquisition_device_1.get_acquisition_metadata = mocker.Mock(side_effect=side_effect)

    with watching(session, test_observer):
        scan.run()

    # TODO check the order of the received events
    test_observer.on_scan_created.assert_called_once()
    test_observer.on_scan_started.assert_called_once()
    test_observer.on_scan_finished.assert_called_once()

    # Check that the scan_info looks like what it is expected
    sf = ScanInfo()
    device_key = sf._get_device_key_from_acq_obj(acquisition_device_1)

    scan_info = test_observer.on_scan_created__scan_info()
    assert scan_info["session_name"] == scan_saving.session
    assert scan_info["user_name"] == scan_saving.user_name
    assert "positioners_start" in scan_info["positioners"]
    assert "start_time" in scan_info
    assert device_key in scan_info["devices"]

    scan_info = test_observer.on_scan_started__scan_info()
    assert "kind" in scan_info["devices"][device_key]["metadata"]

    scan_info = test_observer.on_scan_finished__scan_info()
    assert "end_time" in scan_info
    assert "positioners_end" in scan_info["positioners"]

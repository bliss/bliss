# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from __future__ import annotations

import gevent
import gevent.event
import pytest
import numpy as np

from bliss.common import scans
from bliss.scanning.scan_state import ScanState
from bliss.scanning.scan import ScanAbort, ScanPreset, WatchdogCallback
from bliss.common.data_store import get_default_data_store
from bliss.scanning.channel import AcquisitionChannel
from bliss.scanning.scan_sequence import (
    ScanSequence,
    SequenceContext,
    ScanSequenceError,
)


def test_empty_sequence(session):
    """The simplest sequence is valid"""

    def run_sequence(seq_context: SequenceContext):
        pass

    scan = ScanSequence(runner=run_sequence)
    scan.run()


def test_sequencial_scans(session):
    """A sequence of 2 sequencial scans is valid"""
    diode = session.config.get("diode")

    s1 = None
    s2 = None

    def run_sequence(seq_context: SequenceContext):
        nonlocal s1, s2
        s1 = scans.loopscan(3, 0.1, diode, run=False, name="foo")
        seq_context.add_and_run(s1)
        s2 = scans.loopscan(3, 0.2, diode, run=False, name="bar")
        seq_context.add_and_run(s2)

    scan = ScanSequence(runner=run_sequence)
    scan.run()

    assert s1 is not None
    assert s2 is not None

    # Make sure every scans have it's own number
    assert int(s1.scan_number) == int(scan.scan_number) + 1
    assert int(s2.scan_number) == int(scan.scan_number) + 2

    data_store = get_default_data_store()
    _, key = data_store.get_next_scan(block=False, since=0)
    sequence_scan = data_store.load_scan(key)
    assert sequence_scan.info["title"] == "sequence_of_scans"
    assert "SUBSCANS" in sequence_scan.streams

    subscans_stream = sequence_scan.streams["SUBSCANS"]
    subscans = list(subscans_stream)
    assert len(subscans) == 2
    assert subscans[0].key == s1._scan_data.key
    assert subscans[1].key == s2._scan_data.key


def test_parallel_scans_with_wait_all_subscans(session):
    """A sequence of 2 sequencial scans is valid"""
    diode = session.config.get("diode")
    sim_ct_gauss = session.config.get("sim_ct_gauss")
    robz = session.config.get("robz")

    g1 = None
    g2 = None

    # test that wait_all_subscans works
    def run_sequence(seq_context: SequenceContext):
        nonlocal g1, g2
        s1 = scans.loopscan(20, 0.1, diode, run=False)
        s2 = scans.ascan(robz, 0, 1, 20, 0.1, sim_ct_gauss, run=False)
        seq_context.add(s1)
        seq_context.add(s2)
        g1 = gevent.spawn(s1.run)
        g2 = gevent.spawn(s2.run)

        gevent.sleep(0)
        seq_context.wait_all_subscans()

    scan = ScanSequence(runner=run_sequence)
    scan.run()

    gevent.joinall([g1, g2], raise_error=True)


def test_parallel_scans_with_joinall(session):
    """A sequence of 2 sequencial scans is valid"""

    diode = session.config.get("diode")
    sim_ct_gauss = session.config.get("sim_ct_gauss")
    robz = session.config.get("robz")

    s1 = None
    s2 = None

    # test that wait_all_subscans works
    def run_sequence(seq_context: SequenceContext):
        nonlocal s1, s2
        s1 = scans.loopscan(20, 0.1, diode, run=False)
        s2 = scans.ascan(robz, 0, 1, 20, 0.1, sim_ct_gauss, run=False)
        seq_context.add(s1)
        seq_context.add(s2)
        g1 = gevent.spawn(s1.run)
        g2 = gevent.spawn(s2.run)
        gevent.joinall([g1, g2], raise_error=True)

    scan = ScanSequence(runner=run_sequence)
    scan.run()

    assert s1 is not None
    assert s2 is not None

    # Make sure every scans have it's own number
    assert int(s1.scan_number) > int(scan.scan_number)
    assert int(s2.scan_number) > int(scan.scan_number)
    assert abs(int(s1.scan_number) - int(s2.scan_number)) == 1


def test_add_an_already_started_scan(session):
    """Add an already started scan is not allowed"""
    diode = session.config.get("diode")

    def run_sequence(seq_context: SequenceContext):
        s0 = scans.loopscan(1, 0.1, diode)
        seq_context.add(s0)

    scan = ScanSequence(runner=run_sequence)
    with pytest.raises(ScanSequenceError):
        scan.run()


def test_add_and_run_an_already_started_scan(session):
    """Add and run an already started scan is an error"""
    diode = session.config.get("diode")

    def run_sequence(seq_context: SequenceContext):
        s0 = scans.loopscan(1, 0.1, diode)
        seq_context.add_and_run(s0)

    scan = ScanSequence(runner=run_sequence)
    with pytest.raises(ScanSequenceError):
        scan.run()


def test_scan_not_started(session):
    """Sequence endding with a non started scan is an error"""
    diode = session.config.get("diode")

    def run_sequence(seq_context: SequenceContext):
        s1 = scans.loopscan(20, 0.1, diode, run=False)
        seq_context.add(s1)

    scan = ScanSequence(runner=run_sequence)
    with pytest.raises(ScanSequenceError):
        scan.run()


def test_sequence_custom_channel(session):
    """Custom channels can be handled"""
    diode = session.config.get("diode")

    s1 = None
    s2 = None

    def run_sequence(seq_context: SequenceContext):
        nonlocal s1, s2
        s1 = scans.loopscan(3, 0.1, diode, run=False)
        seq_context.add(s1)
        seq_context.emit("mychannel", [1.1])
        s1.run()
        seq_context.emit("mychannel", [2.2, 3.3])
        s2 = scans.loopscan(3, 0.05, diode, run=False)
        seq_context.add_and_run(s2)
        seq_context.emit("mychannel", [4.4])

    mychannel = AcquisitionChannel("mychannel", np.float64, ())
    scan = ScanSequence(
        runner=run_sequence, scan_info={"something": "else"}, channels=[mychannel]
    )
    scan.scan_info["technique"] = {"hey": "you"}
    scan.scan_info["channels"]["mychannel"]["foo"] = "bar"
    scan.run()

    assert s1 is not None
    assert s2 is not None

    data_store = get_default_data_store()
    _, key = data_store.get_next_scan(block=False, since=0)
    sequence_scan = data_store.load_scan(key)
    assert sequence_scan.info["title"] == "sequence_of_scans"
    assert sequence_scan.info["something"] == "else"
    assert sequence_scan.info["channels"]["mychannel"]["dim"] == 0
    assert sequence_scan.info["channels"]["mychannel"]["foo"] == "bar"
    assert sequence_scan.info["technique"]["hey"] == "you"

    assert {"SUBSCANS", "mychannel"} == sequence_scan.streams.keys()

    mychannel_stream = sequence_scan.streams["mychannel"]
    assert np.array_equal(mychannel_stream[:], [1.1, 2.2, 3.3, 4.4])
    assert mychannel_stream.info["dtype"] == "float64"

    subscans_stream = sequence_scan.streams["SUBSCANS"]
    subscans = list(subscans_stream)
    assert len(subscans) == 2
    assert subscans[0].key == s1._scan_data.key
    assert subscans[1].key == s2._scan_data.key


def test_sequence_missing_events(session):
    # Test for issue #2423
    diode = session.config.get("diode")

    def run_sequence1(seq_context: SequenceContext):
        # Stop publishing sequence events to check that
        # ScanSequenceError is raised upon exiting the context
        seq_context._scan_sequence.group_acq_master.scan_queue.put(StopIteration)
        # Add a scan so that some sequence events are expected
        s = scans.loopscan(3, 0.1, diode, run=False)
        seq_context.add_and_run(s)

    scan = ScanSequence(runner=run_sequence1)
    with gevent.Timeout(10):
        with pytest.raises(ScanSequenceError):
            scan.run()

    def run_sequence2(seq_context: SequenceContext):
        # Stop publishing sequence events to check that
        # ScanSequenceError is raised upon exiting the context
        seq_context._scan_sequence.group_acq_master.scan_queue.put(StopIteration)
        # Do not add scans so no sequence events are expected

    scan = ScanSequence(runner=run_sequence2)
    with gevent.Timeout(10):
        scan.run()


def test_exception_during_the_sequence(session):
    """Create a sequence which raises a user error

    The sequence is in failure, but the child scans are valids
    """
    diode = session.config.get("diode")

    class MyException(RuntimeError):
        pass

    def run_sequence(seq_context: SequenceContext):
        s1 = scans.loopscan(2, 0.1, diode, run=False)
        seq_context.add_and_run(s1)
        raise MyException("Oupsi")

    scan = ScanSequence(runner=run_sequence)
    with pytest.raises(MyException) as excinfo:
        scan.run()
    assert ("Oupsi",) == excinfo.value.args

    assert scan.state == ScanState.KILLED
    assert scan.scans[0].state == ScanState.DONE


def test_keyboard_interrupt_during_a_child_scan(session):
    """Create a sequence which is aborted by the user

    The actual scan and the sequence are aborted
    """
    diode = session.config.get("diode")

    def run_sequence(seq_context: SequenceContext):
        s1 = scans.loopscan(1, 0.1, diode, run=False)
        seq_context.add_and_run(s1)
        s2 = scans.loopscan(10, 1, diode, run=False)
        with gevent.Timeout(1, KeyboardInterrupt):
            seq_context.add_and_run(s2)

    scan = ScanSequence(runner=run_sequence)
    with pytest.raises(ScanAbort):
        scan.run()

    assert scan.state == ScanState.USER_ABORTED
    assert scan.scans[0].state == ScanState.DONE
    assert scan.scans[1].state == ScanState.USER_ABORTED


def test_retry_interrupted_scans(session):
    """Create a sequence which a scan which fail.

    If catched, we can create a new scan and the sequence is still valid
    """
    diode = session.config.get("diode")

    def run_sequence(seq_context: SequenceContext):
        s1 = scans.loopscan(10, 1, diode, run=False)
        try:
            with gevent.Timeout(1, TimeoutError):
                seq_context.add_and_run(s1)
        except TimeoutError:
            s2 = scans.loopscan(1, 0.1, diode, run=False)
            seq_context.add_and_run(s2)

    scan = ScanSequence(runner=run_sequence)
    scan.run()

    assert scan.state == ScanState.DONE
    assert scan.scans[0].state == ScanState.KILLED
    assert scan.scans[1].state == ScanState.DONE


def dummy_fulltomo(diode, run=False, create_failure=False, user_abort=False):
    def run_sequence(seq_context: SequenceContext):
        if create_failure:
            s1 = scans.loopscan(10, 1, diode, run=False)
            with gevent.Timeout(1, RuntimeError):
                seq_context.add_and_run(s1)
        elif user_abort:
            s1 = scans.loopscan(10, 1, diode, run=False)
            with gevent.Timeout(1, KeyboardInterrupt):
                seq_context.add_and_run(s1)
        else:
            s1 = scans.loopscan(1, 0.1, diode, run=False)
            seq_context.add_and_run(s1)

    scan = ScanSequence(runner=run_sequence)
    if run:
        scan.run()
    return scan


def test_sequence_in_sequence(session):
    """A sequence can contain a sequence"""
    diode = session.config.get("diode")

    def run_sequence(seq_context: SequenceContext):
        s1 = dummy_fulltomo(diode, run=False)
        seq_context.add_and_run(s1)
        s2 = dummy_fulltomo(diode, run=False)
        seq_context.add_and_run(s2)

    scan = ScanSequence(runner=run_sequence)
    scan.run()

    assert scan.state == ScanState.DONE
    seq1, seq2 = scan.scans
    assert isinstance(seq1, ScanSequence)
    assert isinstance(seq2, ScanSequence)
    assert seq1.state == ScanState.DONE
    assert seq1.scans[0].state == ScanState.DONE
    assert seq2.state == ScanState.DONE
    assert seq2.scans[0].state == ScanState.DONE


def test_multi_sequence__cascading_failure(session):
    """A failure in a child sequence is propagated by default"""
    diode = session.config.get("diode")

    def run_sequence(seq_context: SequenceContext):
        s1 = dummy_fulltomo(diode, run=False)
        seq_context.add_and_run(s1)
        s2 = dummy_fulltomo(diode, run=False, create_failure=True)
        seq_context.add_and_run(s2)

    scan = ScanSequence(runner=run_sequence)
    with pytest.raises(RuntimeError):
        scan.run()

    assert scan.state == ScanState.KILLED
    seq1, seq2 = scan.scans
    assert isinstance(seq1, ScanSequence)
    assert isinstance(seq2, ScanSequence)
    assert seq1.state == ScanState.DONE
    assert seq1.scans[0].state == ScanState.DONE
    assert seq2.state == ScanState.KILLED
    assert seq2.scans[0].state == ScanState.KILLED


def test_multi_sequence__cascading_abort(session):
    """A user abort in a child sequence is propagated by default"""
    diode = session.config.get("diode")

    def run_sequence(seq_context: SequenceContext):
        s1 = dummy_fulltomo(diode, run=False)
        seq_context.add_and_run(s1)
        s2 = dummy_fulltomo(diode, run=False, user_abort=True)
        seq_context.add_and_run(s2)

    scan = ScanSequence(runner=run_sequence)
    with pytest.raises(ScanAbort):
        scan.run()

    assert scan.state == ScanState.USER_ABORTED
    seq1, seq2 = scan.scans
    assert isinstance(seq1, ScanSequence)
    assert isinstance(seq2, ScanSequence)
    assert seq1.state == ScanState.DONE
    assert seq1.scans[0].state == ScanState.DONE
    assert seq2.state == ScanState.USER_ABORTED
    assert seq2.scans[0].state == ScanState.USER_ABORTED


def test_preset(session):
    """Make sure the scan preset is properly executed on the sequence"""
    diode = session.config.get("diode")

    events: list[str] = []

    def run_sequence(seq_context: SequenceContext):
        nonlocal events
        assert events == ["prepare", "start"]
        s1 = scans.loopscan(1, 0.1, diode, run=False)
        seq_context.add_and_run(s1)

    class MyPreset(ScanPreset):
        def prepare(self, scan):
            nonlocal events
            events.append("prepare")

        def start(self, scan):
            nonlocal events
            events.append("start")

        def stop(self, scan):
            nonlocal events
            events.append("stop")

    scan = ScanSequence(runner=run_sequence)
    p = MyPreset()
    scan.add_preset(p)
    scan.run()

    assert events == ["prepare", "start", "stop"]


def test_watchdog(session):
    """Make sure the watchdog is working on the sequence"""
    diode = session.config.get("diode")

    events: list[str] = []

    def run_sequence(seq_context: SequenceContext):
        nonlocal events
        assert events == ["new"]
        s1 = scans.loopscan(1, 0.1, diode, run=False)
        seq_context.add_and_run(s1)

    class MyWatchdog(WatchdogCallback):
        def on_timeout(self):
            pass

        def on_scan_new(self, scan, scan_info):
            events.append("new")
            pass

        def on_scan_data(self, data_events, scan_info):
            pass

        def on_scan_end(self, scan_info):
            nonlocal events
            events.append("end")

    scan = ScanSequence(runner=run_sequence)
    w = MyWatchdog()
    scan.set_watchdog_callback(w)
    scan.run()

    assert events == ["new", "end"]


def test_abort_with_watchdog(session):
    """Make sure the watchdog timeout can abort the sequence"""
    diode = session.config.get("diode")

    count = 0

    def run_sequence(seq_context: SequenceContext):
        s1 = scans.loopscan(10, 1, diode, run=False)
        seq_context.add_and_run(s1)

    class MyWatchdog(WatchdogCallback):
        def on_timeout(self):
            nonlocal count
            count += 1
            if count == 2:
                raise RuntimeError("Oupsi")

    scan = ScanSequence(runner=run_sequence)
    w = MyWatchdog()
    scan.set_watchdog_callback(w)
    with pytest.raises(RuntimeError) as excinfo:
        scan.run()
    assert ("Oupsi",) == excinfo.value.args

    assert count == 2
    assert scan.state == ScanState.KILLED


def test_scans_in_session(session):
    """Create a scan then a sequence

    - The child of the sequence must not be part of the session.scans
    - The sequence must be found after the scan
    """
    diode = session.config.get("diode")

    child_scan = None

    first_scan = scans.loopscan(3, 0.1, diode, name="foo")

    def run_sequence(seq_context: SequenceContext):
        nonlocal child_scan
        child_scan = scans.loopscan(3, 0.1, diode, run=False, name="bar")
        seq_context.add_and_run(child_scan)

    scan = ScanSequence(runner=run_sequence)
    scan.run()

    assert child_scan is not None
    ss = session.scans
    assert len(ss) >= 2
    assert ss[-1] is scan
    assert ss[-2] is first_scan
    assert not any([s is child_scan for s in ss])

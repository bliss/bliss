# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from bliss.scanning.toolbox import ChainBuilder
from bliss.scanning.scan import Scan
from bliss.scanning.chain import AcquisitionChain
from bliss.scanning.acquisition.timer import SoftwareTimerMaster
from bliss.common.scans.simulation import HWTriggeringSimulator
from bliss.common.scans import DEFAULT_CHAIN, loopscan


def build_lima_scan(
    npoints, count_time, limacam, acq_params, top_master=None, print_chain=True
):
    chain = AcquisitionChain()
    builder = ChainBuilder([limacam])
    limanode = builder.get_nodes_by_controller_name(limacam.name)[0]
    limanode.set_parameters(acq_params=acq_params)
    if top_master is None:
        chain.add(limanode)
    else:
        chain.add(top_master, limanode)

    scan_info = {
        "npoints": npoints,
        "count_time": count_time,
        "type": "custom_scan",
    }

    sc = Scan(
        chain,
        name="test_custom_scan",
        scan_info=scan_info,
        save=False,
        save_images=False,
    )

    if print_chain:
        print("\n" + str(sc.acq_chain.tree))

    return sc, limanode.acquisition_obj


def check_scan(
    npoints,
    count_time,
    limacam,
    frames,
    triggermode,
    waitframeid,
    prepare_once,
    start_once,
    acqmode,
    top_master=None,
):

    acq_params = {
        "acq_nb_frames": frames,
        "acq_expo_time": count_time,
        "acq_mode": acqmode,
        "acq_trigger_mode": triggermode,
        "wait_frame_id": waitframeid,
        "prepare_once": prepare_once,
        "start_once": start_once,
    }

    txt = f"\n=== Scan parameters (top_master = {top_master.name if top_master else None}) =============\n"
    txt += f" acqmode = {acqmode}\n"
    txt += f" count_time = {count_time}\n"
    txt += f" triggermode = {triggermode}\n"
    txt += f" prepare_once = {prepare_once}\n"
    txt += f" start_once = {start_once}\n"
    txt += f" npoints = {npoints}\n"
    txt += f" frames = {frames}\n"
    txt += f" waitframeid = {waitframeid}\n"
    print(txt)

    sc, limaacqobj = build_lima_scan(
        npoints, count_time, limacam, acq_params, top_master
    )
    sc.run()

    assert limacam.acquisition.status == "Ready"
    assert limacam.acquisition.nb_frames == frames
    assert limacam.acquisition.expo_time == count_time
    assert limacam.acquisition.mode == acqmode
    assert limacam.acquisition.trigger_mode == triggermode

    assert limacam.proxy.read_attribute("last_image_acquired").value == frames - 1

    roi_data_shape = sc.get_data("r1_sum").shape
    if prepare_once:
        assert limaacqobj.number_of_acquired_frames == frames
        assert roi_data_shape == (frames,)
    else:
        assert limaacqobj.number_of_acquired_frames == npoints * frames
        assert roi_data_shape == (npoints * frames,)


def check_sw_trigger_scans(npoints, count_time, limacam, acqmode, with_top_master=True):

    top_master = None

    # === FALSE FALSE INTERNAL_TRIGGER (Nframes per scan iter)
    frames = 2
    triggermode = "INTERNAL_TRIGGER"
    waitframeid = None if with_top_master else [frames - 1] * npoints
    prepare_once = False
    start_once = False
    if with_top_master:
        top_master = SoftwareTimerMaster(count_time, npoints=npoints)
    check_scan(
        npoints,
        count_time,
        limacam,
        frames,
        triggermode,
        waitframeid,
        prepare_once,
        start_once,
        acqmode,
        top_master,
    )

    # === TRUE FALSE INTERNAL_TRIGGER_MULTI (1 frame per scan iter with one prepare)
    frames = npoints
    triggermode = "INTERNAL_TRIGGER_MULTI"
    waitframeid = None if with_top_master else range(frames)
    prepare_once = True
    start_once = False
    if with_top_master:
        top_master = SoftwareTimerMaster(count_time, npoints=npoints)
    check_scan(
        npoints,
        count_time,
        limacam,
        frames,
        triggermode,
        waitframeid,
        prepare_once,
        start_once,
        acqmode,
        top_master,
    )


def check_hw_trigger_scans(npoints, count_time, limacam, acqmode):
    proxy = limacam._get_proxy(limacam.proxy.lima_type)
    on_trigger_callback = proxy.trigExternal
    triggers_delta = max(
        count_time * 1.1, 0.2
    )  # minimum time between 2 HW triggers (with 0.2s latency for the simulator)

    # === FALSE FALSE EXTERNAL_TRIGGER
    frames = 2
    triggermode = "EXTERNAL_TRIGGER"
    waitframeid = [frames - 1] * npoints
    prepare_once = False
    start_once = False
    triggers_number = 1
    top_master = HWTriggeringSimulator(
        on_trigger_callback, npoints, triggers_number, triggers_delta
    )
    check_scan(
        npoints,
        count_time,
        limacam,
        frames,
        triggermode,
        waitframeid,
        prepare_once,
        start_once,
        acqmode,
        top_master,
    )

    # === FALSE FALSE EXTERNAL_TRIGGER
    frames = 2
    triggermode = "EXTERNAL_TRIGGER_MULTI"
    waitframeid = [frames - 1] * npoints
    prepare_once = False
    start_once = False
    triggers_number = frames
    top_master = HWTriggeringSimulator(
        on_trigger_callback, npoints, triggers_number, triggers_delta
    )
    check_scan(
        npoints,
        count_time,
        limacam,
        frames,
        triggermode,
        waitframeid,
        prepare_once,
        start_once,
        acqmode,
        top_master,
    )

    # === ONE FRAME PER SCAN ITERATION
    frames_per_point = 2
    frames = npoints * frames_per_point
    triggermode = "EXTERNAL_TRIGGER_MULTI"
    waitframeid = range(frames_per_point - 1, frames, frames_per_point)
    prepare_once = True
    start_once = True
    triggers_number = [frames_per_point] * npoints
    top_master = HWTriggeringSimulator(
        on_trigger_callback, npoints, triggers_number, triggers_delta
    )
    check_scan(
        npoints,
        count_time,
        limacam,
        frames,
        triggermode,
        waitframeid,
        prepare_once,
        start_once,
        acqmode,
        top_master,
    )


def test_lima_scans(default_session, lima_simulator):
    limacam = default_session.config.get("lima_simulator")
    limacam.roi_counters["r1"] = 0, 0, 10, 20
    count_time = 0.01
    npoints = 3

    for sync_mode in ["IMAGE", "TRIGGER"]:
        limacam.camera.synchro_mode = sync_mode
        for acqmode in ["SINGLE"]:  # , 'CONCATENATION', 'ACCUMULATION']:
            print(
                f"\n =======SYNC MODE = {sync_mode} / ACQ MODE = {acqmode} ===================="
            )

            check_sw_trigger_scans(
                npoints, count_time, limacam, acqmode, with_top_master=True
            )
            check_sw_trigger_scans(
                npoints, count_time, limacam, acqmode, with_top_master=False
            )
            check_hw_trigger_scans(npoints, count_time, limacam, acqmode)


def test_lima_default_acq_params(default_session, lima_simulator):
    """check that lima get_default_chain_parameters takes into account eventual extra acq_params defined on the DEFAULT_CHAIN"""
    cam = default_session.config.get("lima_simulator")

    # check default case without DEFAULT_CHAIN customization
    s = loopscan(3, 0.01, cam, run=False)
    acq_params = s.acq_chain.nodes_list[1].acq_params
    assert acq_params["latency_time"] == 0

    # check case where DEFAULT_CHAIN has been customizatied for lima det
    DEFAULT_CHAIN.set_settings(
        [
            {
                "device": cam,
                "acquisition_settings": {"latency_time": 0.012},
            },
        ]
    )
    s = loopscan(3, 0.01, cam, run=False)
    acq_params = s.acq_chain.nodes_list[1].acq_params
    assert acq_params["latency_time"] == 0.012

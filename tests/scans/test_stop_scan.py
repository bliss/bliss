# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest
import gevent

from bliss.common import scans
from bliss.scanning.scan import ScanAbort
from bliss.scanning.scan_state import ScanState


def test_abort_scan(default_session, lima_simulator):
    cam = default_session.config.get("lima_simulator")
    diode = default_session.config.get("diode")
    diode2 = default_session.config.get("diode2")

    diode.mode = "MEAN"
    diode2.mode = "SINGLE"

    s = scans.loopscan(10, 0.1, diode, diode2, cam, run=False)
    task = gevent.spawn(s.run)
    s.wait_state(ScanState.STARTING)
    gevent.sleep(0.25)
    s.abort()

    with pytest.raises(ScanAbort) as excinfo:
        task.get()
    assert "Abort on request" in str(excinfo)

    # check abort is not blocking if scan is not running
    with gevent.Timeout(0.01):
        s.abort()


def test_stop_scan(default_session, lima_simulator):
    cam = default_session.config.get("lima_simulator")
    diode = default_session.config.get("diode")
    diode2 = default_session.config.get("diode2")

    diode.mode = "MEAN"
    diode2.mode = "SINGLE"

    s = scans.loopscan(10, 0.1, diode, diode2, cam, run=False)
    task = gevent.spawn(s.run)
    s.wait_state(ScanState.STARTING)
    gevent.sleep(0.25)
    s.stop()
    assert task.dead
    task.get()

    # check stop is not blocking if scan is not running
    with gevent.Timeout(0.01):
        s.stop()

import pytest

from bliss.scanning.scan_saving.template_store import Field
from bliss.scanning.scan_saving.template_store import FieldLocation
from bliss.scanning.scan_saving.template_store import TemplateStore


@pytest.mark.parametrize(
    "location",
    [FieldLocation.local, FieldLocation.external, FieldLocation.attribute],
)
@pytest.mark.parametrize("value", [None, 10, "string"])
def test_template_store_resolve(location, value):
    var1_value = value
    if isinstance(var1_value, str):
        value += "_"
    elif isinstance(var1_value, int):
        value += 1
    var2_value = value

    class MyStore(TemplateStore):
        class _Fields:
            var1 = Field(
                default=var1_value, location=location, data_type=type(var1_value)
            )
            var2 = Field(location=location, data_type=type(var2_value))
            template = Field(default="{var1}:{var2}", location=location)

    external = dict()
    mystore = MyStore(external, var2=var2_value)

    def validate():
        assert mystore.var1 == var1_value
        assert mystore.var2 == var2_value
        assert mystore.template == f"{var1_value}:{var2_value}"

        assert mystore["var1"] == var1_value
        assert mystore["var2"] == var2_value
        assert mystore["template"] == f"{var1_value}:{var2_value}"

        expected = {
            "var1": var1_value,
            "var2": var2_value,
            "template": f"{var1_value}:{var2_value}",
        }
        assert dict(mystore) == expected

        expected = {
            "var1": var1_value,
            "var2": var2_value,
            "template": "{var1}:{var2}",
        }
        assert mystore.unresolved_dict() == expected

        if location == FieldLocation.external:
            assert len(external) == 3
        else:
            assert len(external) == 0

    validate()

    if isinstance(var1_value, str):
        var1_value += "_"
        mystore.var1 += "_"
    elif isinstance(var1_value, int):
        var1_value += 1
        mystore.var1 += 1

    if isinstance(var2_value, str):
        var2_value += "_"
        mystore.var2 += "_"
    elif isinstance(var2_value, int):
        var2_value += 1
        mystore.var2 += 1

    validate()

    original = mystore
    mystore = mystore.copy()
    assert mystore == original

    validate()


def test_template_store_empty():
    class MyStore(TemplateStore):
        class _Fields:
            pass

    external = dict()
    mystore = MyStore(external)
    assert dict(mystore) == dict()


@pytest.mark.parametrize(
    "location",
    [FieldLocation.local, FieldLocation.external, FieldLocation.attribute],
)
def test_template_store_circular_template(location):
    class MyStore(TemplateStore):
        class _Fields:
            var1 = Field(default="a{var2}", location=location)
            var2 = Field(default="b{var3}", location=location)
            var3 = Field(default="c{var1}", location=location)

    mystore = MyStore(dict())
    assert mystore.var1 == "abca{var2}"


@pytest.mark.parametrize(
    "location",
    [FieldLocation.local, FieldLocation.external, FieldLocation.attribute],
)
def test_template_store_unresolved_template(location):
    class MyStore(TemplateStore):
        class _Fields:
            var1 = Field(default="a{var2}", location=location)
            var2 = Field(default="b{var3}", location=location)

    mystore = MyStore(dict())
    assert mystore.var1 == "ab{var3}"


@pytest.mark.parametrize(
    "location",
    [FieldLocation.local, FieldLocation.external, FieldLocation.attribute],
)
def test_template_store_immutable(location):
    class MyStore(TemplateStore):
        class _Fields:
            var1 = Field(default="value1", location=location, mutable=False)

    mystore = MyStore(dict())
    assert mystore.var1 == "value1"
    with pytest.raises(AttributeError, match="can't set attribute 'var1'"):
        mystore.var1 = "value2"


def test_template_store_add():
    class MyStore(TemplateStore):
        class _Fields:
            var1 = Field(default="value1")

    external = dict()
    mystore1 = MyStore(external)
    mystore1.add("var2", "value2")
    assert len(mystore1) == 2
    assert dict(mystore1) == {"var1": "value1", "var2": "value2"}
    assert external == {"var1": "value1", "var2": "value2"}

    mystore2 = MyStore(external)
    assert len(mystore2) == 2
    assert dict(mystore2) == {"var1": "value1", "var2": "value2"}

# How to contribute to BLISS ?

## Consensus-based decision making on proposals: Martha's rules

Anyone who wishes may sponsor a formal proposal by adding an issue on BLISS
gitlab issues tracker. The proposal follows a standard format:

* a one-line summary (the subject line of the issue)
* the full text of the proposal
* any required background information
* pros and cons
* possible alternatives

Once a person has sponsored a proposal, they are responsible for it. The group
may not discuss or vote on the issue unless the sponsor is present. The sponsor
is also responsible for presenting the item to the group.

After the sponsor presents the proposal, a pre-vote is cast for the proposal,
prior to ANY discussion. This vote is called a "sense" vote, wherein the group
can get an idea of how everyone feels about the issue:

* Who likes the proposal? (represented as a thumbs up)
* Who can live with the proposal? (represented as a sideways thumb)
* Who is uncomfortable with the proposal? (represented as a thumbs down)

If all or most of the group likes or can live with the proposal, it is
immediately moved to the next stage, a formal vote, with no discussion
necessary.

If most of the group is uncomfortable with the proposal, it is postponed for
further rework by the sponsor.

If some members are uncomfortable, they can state their objections, and the
clock for a brief (10 minute) discussion begins.

At the conclusion of the timer, the meeting facilitator calls for a vote on the
following question:

* "Should we implement this decision over the stated objections of the minority,
  when a majority of the group feels it is workable?"

A quorum is established in a meeting if half or more of voting members are
present.

A "yes" vote would lead to a majority rule, and a no vote would postpone the
decision for future rework by the sponsor.


## Developer\'s Guide

See: [Developper's Guidelines](doc/docs/dev_guidelines.md) for BLISS guidelines:
* Naming conventions
* Formatting with black
* Changelog creation with scriv


## Writing good commit messages

(freely taken from https://raw.githubusercontent.com/RomuloOliveira/commit-messages-guide/master/README.md)

### Why are commit messages important?

- To speed up and streamline code reviews
- To help in the understanding of a change
- To explain "the whys" that cannot be described only with code
- To help future maintainers figure out why and how changes were made, making
  troubleshooting and debugging easier

To maximize those outcomes, we can use some good practices and standards
described in the next section.

### Good practices

These are some practices collected from my experiences, internet articles, and
other guides. If you have others (or disagree with some) feel free to open a
Pull Request and contribute.

#### Use imperative form

```
# Good
Use InventoryBackendPool to retrieve inventory backend
```

```
# Bad
Used InventoryBackendPool to retrieve inventory backend
```

_But why use the imperative form?_

A commit message describes what the referenced change actually **does**, its
effects, not what was done.

[This excellent article from Chris
Beams](https://chris.beams.io/posts/git-commit/) gives us a simple sentence that
can be used to help us write better commit messages in imperative form:

```
If applied, this commit will <commit message>
```

Examples:

```
# Good
If applied, this commit will use InventoryBackendPool to retrieve inventory backend
```

```
# Bad
If applied, this commit will used InventoryBackendPool to retrieve inventory backend
```

#### Capitalize the first letter

```
# Good
Add `use` method to Credit model
```

```
# Bad
add `use` method to Credit model
```

The reason that the first letter should be capitalized is to follow the grammar
rule of using capital letters at the beginning of sentences.

The use of this practice may vary from person to person, team to team, or even
from language to language.

Capitalized or not, an important point is to stick to a single standard and
follow it.

#### Try to communicate what the change does without having to look at the source code

```
# Good
Add `use` method to Credit model

```

```
# Bad
Add `use` method
```

```
# Good
Increase left padding between textbox and layout frame
```

```
# Bad
Adjust css
```

It is useful in many scenarios (e.g. multiple commits, several changes and
refactors) to help reviewers understand what the committer was thinking.

#### Use the message body to explain "why", "for what", "how" and additional details

```
# Good
Fix method name of InventoryBackend child classes

Classes derived from InventoryBackend were not
respecting the base class interface.

It worked because the cart was calling the backend implementation
incorrectly.
```

```
# Good
Serialize and deserialize credits to json in Cart

Convert the Credit instances to dict for two main reasons:

  - Pickle relies on file path for classes and we do not want to break up
    everything if a refactor is needed
  - Dict and built-in types are pickleable by default
```

```
# Good
Add `use` method to Credit

Change from namedtuple to class because we need to
setup a new attribute (in_use_amount) with a new value
```

The subject and the body of the messages are separated by a blank line.
Additional blank lines are considered as a part of the message body.

Characters like `-`, `*` and \` are elements that improve readability.

#### Avoid generic messages or messages without any context

```
# Bad
Fix this

Fix stuff

It should work now

Change stuff

Adjust css
```

#### Limit the number of characters

[It's
recommended](https://git-scm.com/book/en/v2/Distributed-Git-Contributing-to-a-Project#_commit_guidelines)
to use a maximum of 50 characters for the subject and 72 for the body.

#### Keep language consistency

For project owners: Choose a language and write all commit messages using that
language. Ideally, it should match the code comments, default translation locale
(for localized projects), etc.

For contributors: Write your commit messages using the same language as the existing commit history.

```
# Good
ababab Add `use` method to Credit model
efefef Use InventoryBackendPool to retrieve inventory backend
bebebe Fix method name of InventoryBackend child classes
```

```
# Good (Portuguese example)
ababab Adiciona o método `use` ao model Credit
efefef Usa o InventoryBackendPool para recuperar o backend de estoque
bebebe Corrige nome de método na classe InventoryBackend
```

```
# Bad (mixes English and Portuguese)
ababab Usa o InventoryBackendPool para recuperar o backend de estoque
efefef Add `use` method to Credit model
cdcdcd Agora vai
```

### Template

This is a template, [written originally by Tim
Pope](http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html),
which appears in the [_Pro Git
Book_](https://git-scm.com/book/en/v2/Distributed-Git-Contributing-to-a-Project).

```
Summarize changes in around 50 characters or less

More detailed explanatory text, if necessary. Wrap it to about 72
characters or so. In some contexts, the first line is treated as the
subject of the commit and the rest of the text as the body. The
blank line separating the summary from the body is critical (unless
you omit the body entirely); various tools like `log`, `shortlog`
and `rebase` can get confused if you run the two together.

Explain the problem that this commit is solving. Focus on why you
are making this change as opposed to how (the code explains that).
Are there side effects or other unintuitive consequences of this
change? Here's the place to explain them.

Further paragraphs come after blank lines.

 - Bullet points are okay, too

 - Typically a hyphen or asterisk is used for the bullet, preceded
   by a single space, with blank lines in between, but conventions
   vary here

If you use an issue tracker, put references to them at the bottom,
like this:

Resolves: #123
See also: #456, #789
```

## Merge requests need to have a meaningful commit history

Git commit messages are the fingerprints that you leave on the code you
touch. Any code that you commit today, a year from now when you look at the same
change; you would be thankful for a clear, meaningful commit message that you
wrote, and it will also make the lives of your fellow developers easier. When
commits are isolated based on context, a bug which was introduced by a certain
commit becomes quicker to find, and the easier it is to revert the commit which
caused the bug in the first place.

Merge requests will not be considered if the commit history does not correspond
to the project standards.

## How to write code review comments

(freely imported from https://google.github.io/eng-practices/review/reviewer/comments.html)

### Summary

- Be kind.
- Explain your reasoning.
- Balance giving explicit directions with just pointing out problems and letting
  the developer decide.
- Encourage developers to simplify code or add code comments instead of just
  explaining the complexity to you.

### Courtesy

In general, it is important to be courteous and respectful while also being very
clear and helpful to the developer whose code you are reviewing. One way to do
this is to be sure that you are always making comments about the code and never
making comments about the developer. You don’t always have to follow this
practice, but you should definitely use it when saying something that might
otherwise be upsetting or contentious. For example:

Bad: “Why did you use threads here when there’s obviously no benefit to be
gained from concurrency?”

Good: “The concurrency model here is adding complexity to the system without any
actual performance benefit that I can see. Because there’s no performance
benefit, it’s best for this code to be single-threaded instead of using multiple
threads.”

### Explain Why

One thing you’ll notice about the “good” example from above is that it helps the
developer understand why you are making your comment. You don’t always need to
include this information in your review comments, but sometimes it’s appropriate
to give a bit more explanation around your intent, the best practice you’re
following, or how your suggestion improves code health.

### Giving Guidance

In general it is the developer’s responsibility to fix a CL, not the
reviewer’s. You are not required to do detailed design of a solution or write
code for the developer.

This doesn’t mean the reviewer should be unhelpful, though. In general you
should strike an appropriate balance between pointing out problems and providing
direct guidance. Pointing out problems and letting the developer make a decision
often helps the developer learn, and makes it easier to do code reviews. It also
can result in a better solution, because the developer is closer to the code
than the reviewer is.

However, sometimes direct instructions, suggestions, or even code are more
helpful. The primary goal of code review is to get the best CL possible. A
secondary goal is improving the skills of developers so that they require less
and less review over time.

### Accepting Explanations

If you ask a developer to explain a piece of code that you don’t understand,
that should usually result in them rewriting the code more
clearly. Occasionally, adding a comment in the code is also an appropriate
response, as long as it’s not just explaining overly complex code.

Explanations written only in the code review tool are not helpful to future code
readers. They are acceptable only in a few circumstances, such as when you are
reviewing an area you are not very familiar with and the developer explains
something that normal readers of the code would have already known.
